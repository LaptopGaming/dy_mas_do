#!bin/bash

g++ GenRoot_csv.cpp hists1d.cxx -o GenRoot `root-config --cflags --glibs`
./GenRoot e

g++ plots.cpp hists1d.cxx -o plots `root-config --cflags --glibs`
./plots
