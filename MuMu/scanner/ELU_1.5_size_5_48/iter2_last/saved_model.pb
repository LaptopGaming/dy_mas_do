��
��
8
Const
output"dtype"
valuetensor"
dtypetype

NoOp
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
@
ReadVariableOp
resource
value"dtype"
dtypetype�
�
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring �
q
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape�"serve*2.2.02unknown8��
x
Input_9/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*
shared_nameInput_9/kernel
q
"Input_9/kernel/Read/ReadVariableOpReadVariableOpInput_9/kernel*
_output_shapes

:00*
dtype0
p
Input_9/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*
shared_nameInput_9/bias
i
 Input_9/bias/Read/ReadVariableOpReadVariableOpInput_9/bias*
_output_shapes
:0*
dtype0
v
ELU0_8/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*
shared_nameELU0_8/kernel
o
!ELU0_8/kernel/Read/ReadVariableOpReadVariableOpELU0_8/kernel*
_output_shapes

:00*
dtype0
n
ELU0_8/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*
shared_nameELU0_8/bias
g
ELU0_8/bias/Read/ReadVariableOpReadVariableOpELU0_8/bias*
_output_shapes
:0*
dtype0
v
ELU1_9/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*
shared_nameELU1_9/kernel
o
!ELU1_9/kernel/Read/ReadVariableOpReadVariableOpELU1_9/kernel*
_output_shapes

:00*
dtype0
n
ELU1_9/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*
shared_nameELU1_9/bias
g
ELU1_9/bias/Read/ReadVariableOpReadVariableOpELU1_9/bias*
_output_shapes
:0*
dtype0
v
ELU2_9/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*
shared_nameELU2_9/kernel
o
!ELU2_9/kernel/Read/ReadVariableOpReadVariableOpELU2_9/kernel*
_output_shapes

:00*
dtype0
n
ELU2_9/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*
shared_nameELU2_9/bias
g
ELU2_9/bias/Read/ReadVariableOpReadVariableOpELU2_9/bias*
_output_shapes
:0*
dtype0
v
ELU3_9/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*
shared_nameELU3_9/kernel
o
!ELU3_9/kernel/Read/ReadVariableOpReadVariableOpELU3_9/kernel*
_output_shapes

:00*
dtype0
n
ELU3_9/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*
shared_nameELU3_9/bias
g
ELU3_9/bias/Read/ReadVariableOpReadVariableOpELU3_9/bias*
_output_shapes
:0*
dtype0
v
ELU4_9/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*
shared_nameELU4_9/kernel
o
!ELU4_9/kernel/Read/ReadVariableOpReadVariableOpELU4_9/kernel*
_output_shapes

:00*
dtype0
n
ELU4_9/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*
shared_nameELU4_9/bias
g
ELU4_9/bias/Read/ReadVariableOpReadVariableOpELU4_9/bias*
_output_shapes
:0*
dtype0
�
batch_normalization_9/gammaVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*,
shared_namebatch_normalization_9/gamma
�
/batch_normalization_9/gamma/Read/ReadVariableOpReadVariableOpbatch_normalization_9/gamma*
_output_shapes
:0*
dtype0
�
batch_normalization_9/betaVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*+
shared_namebatch_normalization_9/beta
�
.batch_normalization_9/beta/Read/ReadVariableOpReadVariableOpbatch_normalization_9/beta*
_output_shapes
:0*
dtype0
�
!batch_normalization_9/moving_meanVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*2
shared_name#!batch_normalization_9/moving_mean
�
5batch_normalization_9/moving_mean/Read/ReadVariableOpReadVariableOp!batch_normalization_9/moving_mean*
_output_shapes
:0*
dtype0
�
%batch_normalization_9/moving_varianceVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*6
shared_name'%batch_normalization_9/moving_variance
�
9batch_normalization_9/moving_variance/Read/ReadVariableOpReadVariableOp%batch_normalization_9/moving_variance*
_output_shapes
:0*
dtype0
z
Output_9/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:0	* 
shared_nameOutput_9/kernel
s
#Output_9/kernel/Read/ReadVariableOpReadVariableOpOutput_9/kernel*
_output_shapes

:0	*
dtype0
r
Output_9/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:	*
shared_nameOutput_9/bias
k
!Output_9/bias/Read/ReadVariableOpReadVariableOpOutput_9/bias*
_output_shapes
:	*
dtype0
f
	Adam/iterVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_name	Adam/iter
_
Adam/iter/Read/ReadVariableOpReadVariableOp	Adam/iter*
_output_shapes
: *
dtype0	
j
Adam/beta_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_1
c
Adam/beta_1/Read/ReadVariableOpReadVariableOpAdam/beta_1*
_output_shapes
: *
dtype0
j
Adam/beta_2VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_2
c
Adam/beta_2/Read/ReadVariableOpReadVariableOpAdam/beta_2*
_output_shapes
: *
dtype0
h

Adam/decayVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name
Adam/decay
a
Adam/decay/Read/ReadVariableOpReadVariableOp
Adam/decay*
_output_shapes
: *
dtype0
x
Adam/learning_rateVarHandleOp*
_output_shapes
: *
dtype0*
shape: *#
shared_nameAdam/learning_rate
q
&Adam/learning_rate/Read/ReadVariableOpReadVariableOpAdam/learning_rate*
_output_shapes
: *
dtype0
^
totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nametotal
W
total/Read/ReadVariableOpReadVariableOptotal*
_output_shapes
: *
dtype0
^
countVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namecount
W
count/Read/ReadVariableOpReadVariableOpcount*
_output_shapes
: *
dtype0
b
total_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	total_1
[
total_1/Read/ReadVariableOpReadVariableOptotal_1*
_output_shapes
: *
dtype0
b
count_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	count_1
[
count_1/Read/ReadVariableOpReadVariableOpcount_1*
_output_shapes
: *
dtype0
t
true_positivesVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_nametrue_positives
m
"true_positives/Read/ReadVariableOpReadVariableOptrue_positives*
_output_shapes
:*
dtype0
v
false_positivesVarHandleOp*
_output_shapes
: *
dtype0*
shape:* 
shared_namefalse_positives
o
#false_positives/Read/ReadVariableOpReadVariableOpfalse_positives*
_output_shapes
:*
dtype0
�
Adam/Input_9/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*&
shared_nameAdam/Input_9/kernel/m

)Adam/Input_9/kernel/m/Read/ReadVariableOpReadVariableOpAdam/Input_9/kernel/m*
_output_shapes

:00*
dtype0
~
Adam/Input_9/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*$
shared_nameAdam/Input_9/bias/m
w
'Adam/Input_9/bias/m/Read/ReadVariableOpReadVariableOpAdam/Input_9/bias/m*
_output_shapes
:0*
dtype0
�
Adam/ELU0_8/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*%
shared_nameAdam/ELU0_8/kernel/m
}
(Adam/ELU0_8/kernel/m/Read/ReadVariableOpReadVariableOpAdam/ELU0_8/kernel/m*
_output_shapes

:00*
dtype0
|
Adam/ELU0_8/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*#
shared_nameAdam/ELU0_8/bias/m
u
&Adam/ELU0_8/bias/m/Read/ReadVariableOpReadVariableOpAdam/ELU0_8/bias/m*
_output_shapes
:0*
dtype0
�
Adam/ELU1_9/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*%
shared_nameAdam/ELU1_9/kernel/m
}
(Adam/ELU1_9/kernel/m/Read/ReadVariableOpReadVariableOpAdam/ELU1_9/kernel/m*
_output_shapes

:00*
dtype0
|
Adam/ELU1_9/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*#
shared_nameAdam/ELU1_9/bias/m
u
&Adam/ELU1_9/bias/m/Read/ReadVariableOpReadVariableOpAdam/ELU1_9/bias/m*
_output_shapes
:0*
dtype0
�
Adam/ELU2_9/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*%
shared_nameAdam/ELU2_9/kernel/m
}
(Adam/ELU2_9/kernel/m/Read/ReadVariableOpReadVariableOpAdam/ELU2_9/kernel/m*
_output_shapes

:00*
dtype0
|
Adam/ELU2_9/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*#
shared_nameAdam/ELU2_9/bias/m
u
&Adam/ELU2_9/bias/m/Read/ReadVariableOpReadVariableOpAdam/ELU2_9/bias/m*
_output_shapes
:0*
dtype0
�
Adam/ELU3_9/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*%
shared_nameAdam/ELU3_9/kernel/m
}
(Adam/ELU3_9/kernel/m/Read/ReadVariableOpReadVariableOpAdam/ELU3_9/kernel/m*
_output_shapes

:00*
dtype0
|
Adam/ELU3_9/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*#
shared_nameAdam/ELU3_9/bias/m
u
&Adam/ELU3_9/bias/m/Read/ReadVariableOpReadVariableOpAdam/ELU3_9/bias/m*
_output_shapes
:0*
dtype0
�
Adam/ELU4_9/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*%
shared_nameAdam/ELU4_9/kernel/m
}
(Adam/ELU4_9/kernel/m/Read/ReadVariableOpReadVariableOpAdam/ELU4_9/kernel/m*
_output_shapes

:00*
dtype0
|
Adam/ELU4_9/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*#
shared_nameAdam/ELU4_9/bias/m
u
&Adam/ELU4_9/bias/m/Read/ReadVariableOpReadVariableOpAdam/ELU4_9/bias/m*
_output_shapes
:0*
dtype0
�
"Adam/batch_normalization_9/gamma/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*3
shared_name$"Adam/batch_normalization_9/gamma/m
�
6Adam/batch_normalization_9/gamma/m/Read/ReadVariableOpReadVariableOp"Adam/batch_normalization_9/gamma/m*
_output_shapes
:0*
dtype0
�
!Adam/batch_normalization_9/beta/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*2
shared_name#!Adam/batch_normalization_9/beta/m
�
5Adam/batch_normalization_9/beta/m/Read/ReadVariableOpReadVariableOp!Adam/batch_normalization_9/beta/m*
_output_shapes
:0*
dtype0
�
Adam/Output_9/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:0	*'
shared_nameAdam/Output_9/kernel/m
�
*Adam/Output_9/kernel/m/Read/ReadVariableOpReadVariableOpAdam/Output_9/kernel/m*
_output_shapes

:0	*
dtype0
�
Adam/Output_9/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:	*%
shared_nameAdam/Output_9/bias/m
y
(Adam/Output_9/bias/m/Read/ReadVariableOpReadVariableOpAdam/Output_9/bias/m*
_output_shapes
:	*
dtype0
�
Adam/Input_9/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*&
shared_nameAdam/Input_9/kernel/v

)Adam/Input_9/kernel/v/Read/ReadVariableOpReadVariableOpAdam/Input_9/kernel/v*
_output_shapes

:00*
dtype0
~
Adam/Input_9/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*$
shared_nameAdam/Input_9/bias/v
w
'Adam/Input_9/bias/v/Read/ReadVariableOpReadVariableOpAdam/Input_9/bias/v*
_output_shapes
:0*
dtype0
�
Adam/ELU0_8/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*%
shared_nameAdam/ELU0_8/kernel/v
}
(Adam/ELU0_8/kernel/v/Read/ReadVariableOpReadVariableOpAdam/ELU0_8/kernel/v*
_output_shapes

:00*
dtype0
|
Adam/ELU0_8/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*#
shared_nameAdam/ELU0_8/bias/v
u
&Adam/ELU0_8/bias/v/Read/ReadVariableOpReadVariableOpAdam/ELU0_8/bias/v*
_output_shapes
:0*
dtype0
�
Adam/ELU1_9/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*%
shared_nameAdam/ELU1_9/kernel/v
}
(Adam/ELU1_9/kernel/v/Read/ReadVariableOpReadVariableOpAdam/ELU1_9/kernel/v*
_output_shapes

:00*
dtype0
|
Adam/ELU1_9/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*#
shared_nameAdam/ELU1_9/bias/v
u
&Adam/ELU1_9/bias/v/Read/ReadVariableOpReadVariableOpAdam/ELU1_9/bias/v*
_output_shapes
:0*
dtype0
�
Adam/ELU2_9/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*%
shared_nameAdam/ELU2_9/kernel/v
}
(Adam/ELU2_9/kernel/v/Read/ReadVariableOpReadVariableOpAdam/ELU2_9/kernel/v*
_output_shapes

:00*
dtype0
|
Adam/ELU2_9/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*#
shared_nameAdam/ELU2_9/bias/v
u
&Adam/ELU2_9/bias/v/Read/ReadVariableOpReadVariableOpAdam/ELU2_9/bias/v*
_output_shapes
:0*
dtype0
�
Adam/ELU3_9/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*%
shared_nameAdam/ELU3_9/kernel/v
}
(Adam/ELU3_9/kernel/v/Read/ReadVariableOpReadVariableOpAdam/ELU3_9/kernel/v*
_output_shapes

:00*
dtype0
|
Adam/ELU3_9/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*#
shared_nameAdam/ELU3_9/bias/v
u
&Adam/ELU3_9/bias/v/Read/ReadVariableOpReadVariableOpAdam/ELU3_9/bias/v*
_output_shapes
:0*
dtype0
�
Adam/ELU4_9/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*%
shared_nameAdam/ELU4_9/kernel/v
}
(Adam/ELU4_9/kernel/v/Read/ReadVariableOpReadVariableOpAdam/ELU4_9/kernel/v*
_output_shapes

:00*
dtype0
|
Adam/ELU4_9/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*#
shared_nameAdam/ELU4_9/bias/v
u
&Adam/ELU4_9/bias/v/Read/ReadVariableOpReadVariableOpAdam/ELU4_9/bias/v*
_output_shapes
:0*
dtype0
�
"Adam/batch_normalization_9/gamma/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*3
shared_name$"Adam/batch_normalization_9/gamma/v
�
6Adam/batch_normalization_9/gamma/v/Read/ReadVariableOpReadVariableOp"Adam/batch_normalization_9/gamma/v*
_output_shapes
:0*
dtype0
�
!Adam/batch_normalization_9/beta/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*2
shared_name#!Adam/batch_normalization_9/beta/v
�
5Adam/batch_normalization_9/beta/v/Read/ReadVariableOpReadVariableOp!Adam/batch_normalization_9/beta/v*
_output_shapes
:0*
dtype0
�
Adam/Output_9/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:0	*'
shared_nameAdam/Output_9/kernel/v
�
*Adam/Output_9/kernel/v/Read/ReadVariableOpReadVariableOpAdam/Output_9/kernel/v*
_output_shapes

:0	*
dtype0
�
Adam/Output_9/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:	*%
shared_nameAdam/Output_9/bias/v
y
(Adam/Output_9/bias/v/Read/ReadVariableOpReadVariableOpAdam/Output_9/bias/v*
_output_shapes
:	*
dtype0

NoOpNoOp
�g
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*�g
value�gB�g B�g
�
layer_with_weights-0
layer-0
layer_with_weights-1
layer-1
layer_with_weights-2
layer-2
layer_with_weights-3
layer-3
layer_with_weights-4
layer-4
layer_with_weights-5
layer-5
layer_with_weights-6
layer-6
layer_with_weights-7
layer-7
	layer-8

	optimizer
trainable_variables
regularization_losses
	variables
	keras_api

signatures
x

activation

kernel
bias
trainable_variables
regularization_losses
	variables
	keras_api
x

activation

kernel
bias
trainable_variables
regularization_losses
	variables
	keras_api
x

activation

kernel
 bias
!trainable_variables
"regularization_losses
#	variables
$	keras_api
x
%
activation

&kernel
'bias
(trainable_variables
)regularization_losses
*	variables
+	keras_api
x
,
activation

-kernel
.bias
/trainable_variables
0regularization_losses
1	variables
2	keras_api
x
3
activation

4kernel
5bias
6trainable_variables
7regularization_losses
8	variables
9	keras_api
�
:axis
	;gamma
<beta
=moving_mean
>moving_variance
?trainable_variables
@regularization_losses
A	variables
B	keras_api
h

Ckernel
Dbias
Etrainable_variables
Fregularization_losses
G	variables
H	keras_api
R
Itrainable_variables
Jregularization_losses
K	variables
L	keras_api
�
Miter

Nbeta_1

Obeta_2
	Pdecay
Qlearning_ratem�m�m�m�m� m�&m�'m�-m�.m�4m�5m�;m�<m�Cm�Dm�v�v�v�v�v� v�&v�'v�-v�.v�4v�5v�;v�<v�Cv�Dv�
v
0
1
2
3
4
 5
&6
'7
-8
.9
410
511
;12
<13
C14
D15
 
�
0
1
2
3
4
 5
&6
'7
-8
.9
410
511
;12
<13
=14
>15
C16
D17
�
trainable_variables
Rmetrics
Snon_trainable_variables
Tlayer_metrics
Ulayer_regularization_losses
regularization_losses

Vlayers
	variables
 
R
Wtrainable_variables
Xregularization_losses
Y	variables
Z	keras_api
ZX
VARIABLE_VALUEInput_9/kernel6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUE
VT
VARIABLE_VALUEInput_9/bias4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUE

0
1
 

0
1
�
trainable_variables
[metrics
\non_trainable_variables
]layer_metrics
^layer_regularization_losses
regularization_losses

_layers
	variables
R
`trainable_variables
aregularization_losses
b	variables
c	keras_api
YW
VARIABLE_VALUEELU0_8/kernel6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUE
US
VARIABLE_VALUEELU0_8/bias4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUE

0
1
 

0
1
�
trainable_variables
dmetrics
enon_trainable_variables
flayer_metrics
glayer_regularization_losses
regularization_losses

hlayers
	variables
R
itrainable_variables
jregularization_losses
k	variables
l	keras_api
YW
VARIABLE_VALUEELU1_9/kernel6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUE
US
VARIABLE_VALUEELU1_9/bias4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUE

0
 1
 

0
 1
�
!trainable_variables
mmetrics
nnon_trainable_variables
olayer_metrics
player_regularization_losses
"regularization_losses

qlayers
#	variables
R
rtrainable_variables
sregularization_losses
t	variables
u	keras_api
YW
VARIABLE_VALUEELU2_9/kernel6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUE
US
VARIABLE_VALUEELU2_9/bias4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUE

&0
'1
 

&0
'1
�
(trainable_variables
vmetrics
wnon_trainable_variables
xlayer_metrics
ylayer_regularization_losses
)regularization_losses

zlayers
*	variables
R
{trainable_variables
|regularization_losses
}	variables
~	keras_api
YW
VARIABLE_VALUEELU3_9/kernel6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUE
US
VARIABLE_VALUEELU3_9/bias4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUE

-0
.1
 

-0
.1
�
/trainable_variables
metrics
�non_trainable_variables
�layer_metrics
 �layer_regularization_losses
0regularization_losses
�layers
1	variables
V
�trainable_variables
�regularization_losses
�	variables
�	keras_api
YW
VARIABLE_VALUEELU4_9/kernel6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUE
US
VARIABLE_VALUEELU4_9/bias4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUE

40
51
 

40
51
�
6trainable_variables
�metrics
�non_trainable_variables
�layer_metrics
 �layer_regularization_losses
7regularization_losses
�layers
8	variables
 
fd
VARIABLE_VALUEbatch_normalization_9/gamma5layer_with_weights-6/gamma/.ATTRIBUTES/VARIABLE_VALUE
db
VARIABLE_VALUEbatch_normalization_9/beta4layer_with_weights-6/beta/.ATTRIBUTES/VARIABLE_VALUE
rp
VARIABLE_VALUE!batch_normalization_9/moving_mean;layer_with_weights-6/moving_mean/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUE%batch_normalization_9/moving_variance?layer_with_weights-6/moving_variance/.ATTRIBUTES/VARIABLE_VALUE

;0
<1
 

;0
<1
=2
>3
�
?trainable_variables
�metrics
�non_trainable_variables
�layer_metrics
 �layer_regularization_losses
@regularization_losses
�layers
A	variables
[Y
VARIABLE_VALUEOutput_9/kernel6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUE
WU
VARIABLE_VALUEOutput_9/bias4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUE

C0
D1
 

C0
D1
�
Etrainable_variables
�metrics
�non_trainable_variables
�layer_metrics
 �layer_regularization_losses
Fregularization_losses
�layers
G	variables
 
 
 
�
Itrainable_variables
�metrics
�non_trainable_variables
�layer_metrics
 �layer_regularization_losses
Jregularization_losses
�layers
K	variables
HF
VARIABLE_VALUE	Adam/iter)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_1+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_2+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUE
JH
VARIABLE_VALUE
Adam/decay*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEAdam/learning_rate2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUE

�0
�1
�2

=0
>1
 
 
?
0
1
2
3
4
5
6
7
	8
 
 
 
�
Wtrainable_variables
�metrics
�non_trainable_variables
�layer_metrics
 �layer_regularization_losses
Xregularization_losses
�layers
Y	variables
 
 
 
 

0
 
 
 
�
`trainable_variables
�metrics
�non_trainable_variables
�layer_metrics
 �layer_regularization_losses
aregularization_losses
�layers
b	variables
 
 
 
 

0
 
 
 
�
itrainable_variables
�metrics
�non_trainable_variables
�layer_metrics
 �layer_regularization_losses
jregularization_losses
�layers
k	variables
 
 
 
 

0
 
 
 
�
rtrainable_variables
�metrics
�non_trainable_variables
�layer_metrics
 �layer_regularization_losses
sregularization_losses
�layers
t	variables
 
 
 
 

%0
 
 
 
�
{trainable_variables
�metrics
�non_trainable_variables
�layer_metrics
 �layer_regularization_losses
|regularization_losses
�layers
}	variables
 
 
 
 

,0
 
 
 
�
�trainable_variables
�metrics
�non_trainable_variables
�layer_metrics
 �layer_regularization_losses
�regularization_losses
�layers
�	variables
 
 
 
 

30
 

=0
>1
 
 
 
 
 
 
 
 
 
 
 
 
 
8

�total

�count
�	variables
�	keras_api
I

�total

�count
�
_fn_kwargs
�	variables
�	keras_api
\
�
thresholds
�true_positives
�false_positives
�	variables
�	keras_api
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
OM
VARIABLE_VALUEtotal4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE
OM
VARIABLE_VALUEcount4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE

�0
�1

�	variables
QO
VARIABLE_VALUEtotal_14keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUE
QO
VARIABLE_VALUEcount_14keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUE
 

�0
�1

�	variables
 
a_
VARIABLE_VALUEtrue_positives=keras_api/metrics/2/true_positives/.ATTRIBUTES/VARIABLE_VALUE
ca
VARIABLE_VALUEfalse_positives>keras_api/metrics/2/false_positives/.ATTRIBUTES/VARIABLE_VALUE

�0
�1

�	variables
}{
VARIABLE_VALUEAdam/Input_9/kernel/mRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/Input_9/bias/mPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
|z
VARIABLE_VALUEAdam/ELU0_8/kernel/mRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
xv
VARIABLE_VALUEAdam/ELU0_8/bias/mPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
|z
VARIABLE_VALUEAdam/ELU1_9/kernel/mRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
xv
VARIABLE_VALUEAdam/ELU1_9/bias/mPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
|z
VARIABLE_VALUEAdam/ELU2_9/kernel/mRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
xv
VARIABLE_VALUEAdam/ELU2_9/bias/mPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
|z
VARIABLE_VALUEAdam/ELU3_9/kernel/mRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
xv
VARIABLE_VALUEAdam/ELU3_9/bias/mPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
|z
VARIABLE_VALUEAdam/ELU4_9/kernel/mRlayer_with_weights-5/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
xv
VARIABLE_VALUEAdam/ELU4_9/bias/mPlayer_with_weights-5/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE"Adam/batch_normalization_9/gamma/mQlayer_with_weights-6/gamma/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE!Adam/batch_normalization_9/beta/mPlayer_with_weights-6/beta/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/Output_9/kernel/mRlayer_with_weights-7/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/Output_9/bias/mPlayer_with_weights-7/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/Input_9/kernel/vRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/Input_9/bias/vPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
|z
VARIABLE_VALUEAdam/ELU0_8/kernel/vRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
xv
VARIABLE_VALUEAdam/ELU0_8/bias/vPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
|z
VARIABLE_VALUEAdam/ELU1_9/kernel/vRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
xv
VARIABLE_VALUEAdam/ELU1_9/bias/vPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
|z
VARIABLE_VALUEAdam/ELU2_9/kernel/vRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
xv
VARIABLE_VALUEAdam/ELU2_9/bias/vPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
|z
VARIABLE_VALUEAdam/ELU3_9/kernel/vRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
xv
VARIABLE_VALUEAdam/ELU3_9/bias/vPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
|z
VARIABLE_VALUEAdam/ELU4_9/kernel/vRlayer_with_weights-5/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
xv
VARIABLE_VALUEAdam/ELU4_9/bias/vPlayer_with_weights-5/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE"Adam/batch_normalization_9/gamma/vQlayer_with_weights-6/gamma/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE!Adam/batch_normalization_9/beta/vPlayer_with_weights-6/beta/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/Output_9/kernel/vRlayer_with_weights-7/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/Output_9/bias/vPlayer_with_weights-7/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
~
serving_default_Input_inputPlaceholder*'
_output_shapes
:���������0*
dtype0*
shape:���������0
�
StatefulPartitionedCallStatefulPartitionedCallserving_default_Input_inputInput_9/kernelInput_9/biasELU0_8/kernelELU0_8/biasELU1_9/kernelELU1_9/biasELU2_9/kernelELU2_9/biasELU3_9/kernelELU3_9/biasELU4_9/kernelELU4_9/bias%batch_normalization_9/moving_variancebatch_normalization_9/gamma!batch_normalization_9/moving_meanbatch_normalization_9/betaOutput_9/kernelOutput_9/bias*
Tin
2*
Tout
2*'
_output_shapes
:���������	*4
_read_only_resource_inputs
	
**
config_proto

CPU

GPU 2J 8*/
f*R(
&__inference_signature_wrapper_13537844
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
�
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filename"Input_9/kernel/Read/ReadVariableOp Input_9/bias/Read/ReadVariableOp!ELU0_8/kernel/Read/ReadVariableOpELU0_8/bias/Read/ReadVariableOp!ELU1_9/kernel/Read/ReadVariableOpELU1_9/bias/Read/ReadVariableOp!ELU2_9/kernel/Read/ReadVariableOpELU2_9/bias/Read/ReadVariableOp!ELU3_9/kernel/Read/ReadVariableOpELU3_9/bias/Read/ReadVariableOp!ELU4_9/kernel/Read/ReadVariableOpELU4_9/bias/Read/ReadVariableOp/batch_normalization_9/gamma/Read/ReadVariableOp.batch_normalization_9/beta/Read/ReadVariableOp5batch_normalization_9/moving_mean/Read/ReadVariableOp9batch_normalization_9/moving_variance/Read/ReadVariableOp#Output_9/kernel/Read/ReadVariableOp!Output_9/bias/Read/ReadVariableOpAdam/iter/Read/ReadVariableOpAdam/beta_1/Read/ReadVariableOpAdam/beta_2/Read/ReadVariableOpAdam/decay/Read/ReadVariableOp&Adam/learning_rate/Read/ReadVariableOptotal/Read/ReadVariableOpcount/Read/ReadVariableOptotal_1/Read/ReadVariableOpcount_1/Read/ReadVariableOp"true_positives/Read/ReadVariableOp#false_positives/Read/ReadVariableOp)Adam/Input_9/kernel/m/Read/ReadVariableOp'Adam/Input_9/bias/m/Read/ReadVariableOp(Adam/ELU0_8/kernel/m/Read/ReadVariableOp&Adam/ELU0_8/bias/m/Read/ReadVariableOp(Adam/ELU1_9/kernel/m/Read/ReadVariableOp&Adam/ELU1_9/bias/m/Read/ReadVariableOp(Adam/ELU2_9/kernel/m/Read/ReadVariableOp&Adam/ELU2_9/bias/m/Read/ReadVariableOp(Adam/ELU3_9/kernel/m/Read/ReadVariableOp&Adam/ELU3_9/bias/m/Read/ReadVariableOp(Adam/ELU4_9/kernel/m/Read/ReadVariableOp&Adam/ELU4_9/bias/m/Read/ReadVariableOp6Adam/batch_normalization_9/gamma/m/Read/ReadVariableOp5Adam/batch_normalization_9/beta/m/Read/ReadVariableOp*Adam/Output_9/kernel/m/Read/ReadVariableOp(Adam/Output_9/bias/m/Read/ReadVariableOp)Adam/Input_9/kernel/v/Read/ReadVariableOp'Adam/Input_9/bias/v/Read/ReadVariableOp(Adam/ELU0_8/kernel/v/Read/ReadVariableOp&Adam/ELU0_8/bias/v/Read/ReadVariableOp(Adam/ELU1_9/kernel/v/Read/ReadVariableOp&Adam/ELU1_9/bias/v/Read/ReadVariableOp(Adam/ELU2_9/kernel/v/Read/ReadVariableOp&Adam/ELU2_9/bias/v/Read/ReadVariableOp(Adam/ELU3_9/kernel/v/Read/ReadVariableOp&Adam/ELU3_9/bias/v/Read/ReadVariableOp(Adam/ELU4_9/kernel/v/Read/ReadVariableOp&Adam/ELU4_9/bias/v/Read/ReadVariableOp6Adam/batch_normalization_9/gamma/v/Read/ReadVariableOp5Adam/batch_normalization_9/beta/v/Read/ReadVariableOp*Adam/Output_9/kernel/v/Read/ReadVariableOp(Adam/Output_9/bias/v/Read/ReadVariableOpConst*J
TinC
A2?	*
Tout
2*
_output_shapes
: * 
_read_only_resource_inputs
 **
config_proto

CPU

GPU 2J 8**
f%R#
!__inference__traced_save_13538647
�
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenameInput_9/kernelInput_9/biasELU0_8/kernelELU0_8/biasELU1_9/kernelELU1_9/biasELU2_9/kernelELU2_9/biasELU3_9/kernelELU3_9/biasELU4_9/kernelELU4_9/biasbatch_normalization_9/gammabatch_normalization_9/beta!batch_normalization_9/moving_mean%batch_normalization_9/moving_varianceOutput_9/kernelOutput_9/bias	Adam/iterAdam/beta_1Adam/beta_2
Adam/decayAdam/learning_ratetotalcounttotal_1count_1true_positivesfalse_positivesAdam/Input_9/kernel/mAdam/Input_9/bias/mAdam/ELU0_8/kernel/mAdam/ELU0_8/bias/mAdam/ELU1_9/kernel/mAdam/ELU1_9/bias/mAdam/ELU2_9/kernel/mAdam/ELU2_9/bias/mAdam/ELU3_9/kernel/mAdam/ELU3_9/bias/mAdam/ELU4_9/kernel/mAdam/ELU4_9/bias/m"Adam/batch_normalization_9/gamma/m!Adam/batch_normalization_9/beta/mAdam/Output_9/kernel/mAdam/Output_9/bias/mAdam/Input_9/kernel/vAdam/Input_9/bias/vAdam/ELU0_8/kernel/vAdam/ELU0_8/bias/vAdam/ELU1_9/kernel/vAdam/ELU1_9/bias/vAdam/ELU2_9/kernel/vAdam/ELU2_9/bias/vAdam/ELU3_9/kernel/vAdam/ELU3_9/bias/vAdam/ELU4_9/kernel/vAdam/ELU4_9/bias/v"Adam/batch_normalization_9/gamma/v!Adam/batch_normalization_9/beta/vAdam/Output_9/kernel/vAdam/Output_9/bias/v*I
TinB
@2>*
Tout
2*
_output_shapes
: * 
_read_only_resource_inputs
 **
config_proto

CPU

GPU 2J 8*-
f(R&
$__inference__traced_restore_13538842ݭ
�
�
D__inference_Output_layer_call_and_return_conditional_losses_13537497

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:0	*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������	2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:	*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������	2	
BiasAddd
IdentityIdentityBiasAdd:output:0*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0:::O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
c
G__inference_softmax_9_layer_call_and_return_conditional_losses_13537518

inputs
identityW
SoftmaxSoftmaxinputs*
T0*'
_output_shapes
:���������	2	
Softmaxe
IdentityIdentitySoftmax:softmax:0*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*&
_input_shapes
:���������	:O K
'
_output_shapes
:���������	
 
_user_specified_nameinputs
�m
�
V__inference_ELU_1.5_size_5_48_iter_2_layer_call_and_return_conditional_losses_13538058

inputs(
$input_matmul_readvariableop_resource)
%input_biasadd_readvariableop_resource'
#elu0_matmul_readvariableop_resource(
$elu0_biasadd_readvariableop_resource'
#elu1_matmul_readvariableop_resource(
$elu1_biasadd_readvariableop_resource'
#elu2_matmul_readvariableop_resource(
$elu2_biasadd_readvariableop_resource'
#elu3_matmul_readvariableop_resource(
$elu3_biasadd_readvariableop_resource'
#elu4_matmul_readvariableop_resource(
$elu4_biasadd_readvariableop_resource;
7batch_normalization_9_batchnorm_readvariableop_resource?
;batch_normalization_9_batchnorm_mul_readvariableop_resource=
9batch_normalization_9_batchnorm_readvariableop_1_resource=
9batch_normalization_9_batchnorm_readvariableop_2_resource)
%output_matmul_readvariableop_resource*
&output_biasadd_readvariableop_resource
identity��
Input/MatMul/ReadVariableOpReadVariableOp$input_matmul_readvariableop_resource*
_output_shapes

:00*
dtype02
Input/MatMul/ReadVariableOp�
Input/MatMulMatMulinputs#Input/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
Input/MatMul�
Input/BiasAdd/ReadVariableOpReadVariableOp%input_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
Input/BiasAdd/ReadVariableOp�
Input/BiasAddBiasAddInput/MatMul:product:0$Input/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
Input/BiasAddu
Input/elu_54/EluEluInput/BiasAdd:output:0*
T0*'
_output_shapes
:���������02
Input/elu_54/Eluu
Input/elu_54/Greater/yConst*
_output_shapes
: *
dtype0*
valueB
 *    2
Input/elu_54/Greater/y�
Input/elu_54/GreaterGreaterInput/BiasAdd:output:0Input/elu_54/Greater/y:output:0*
T0*'
_output_shapes
:���������02
Input/elu_54/Greaterm
Input/elu_54/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
Input/elu_54/mul/x�
Input/elu_54/mulMulInput/elu_54/mul/x:output:0Input/elu_54/Elu:activations:0*
T0*'
_output_shapes
:���������02
Input/elu_54/mul�
Input/elu_54/SelectV2SelectV2Input/elu_54/Greater:z:0Input/elu_54/Elu:activations:0Input/elu_54/mul:z:0*
T0*'
_output_shapes
:���������02
Input/elu_54/SelectV2�
ELU0/MatMul/ReadVariableOpReadVariableOp#elu0_matmul_readvariableop_resource*
_output_shapes

:00*
dtype02
ELU0/MatMul/ReadVariableOp�
ELU0/MatMulMatMulInput/elu_54/SelectV2:output:0"ELU0/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
ELU0/MatMul�
ELU0/BiasAdd/ReadVariableOpReadVariableOp$elu0_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
ELU0/BiasAdd/ReadVariableOp�
ELU0/BiasAddBiasAddELU0/MatMul:product:0#ELU0/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
ELU0/BiasAddr
ELU0/elu_55/EluEluELU0/BiasAdd:output:0*
T0*'
_output_shapes
:���������02
ELU0/elu_55/Elus
ELU0/elu_55/Greater/yConst*
_output_shapes
: *
dtype0*
valueB
 *    2
ELU0/elu_55/Greater/y�
ELU0/elu_55/GreaterGreaterELU0/BiasAdd:output:0ELU0/elu_55/Greater/y:output:0*
T0*'
_output_shapes
:���������02
ELU0/elu_55/Greaterk
ELU0/elu_55/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
ELU0/elu_55/mul/x�
ELU0/elu_55/mulMulELU0/elu_55/mul/x:output:0ELU0/elu_55/Elu:activations:0*
T0*'
_output_shapes
:���������02
ELU0/elu_55/mul�
ELU0/elu_55/SelectV2SelectV2ELU0/elu_55/Greater:z:0ELU0/elu_55/Elu:activations:0ELU0/elu_55/mul:z:0*
T0*'
_output_shapes
:���������02
ELU0/elu_55/SelectV2�
ELU1/MatMul/ReadVariableOpReadVariableOp#elu1_matmul_readvariableop_resource*
_output_shapes

:00*
dtype02
ELU1/MatMul/ReadVariableOp�
ELU1/MatMulMatMulELU0/elu_55/SelectV2:output:0"ELU1/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
ELU1/MatMul�
ELU1/BiasAdd/ReadVariableOpReadVariableOp$elu1_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
ELU1/BiasAdd/ReadVariableOp�
ELU1/BiasAddBiasAddELU1/MatMul:product:0#ELU1/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
ELU1/BiasAddr
ELU1/elu_56/EluEluELU1/BiasAdd:output:0*
T0*'
_output_shapes
:���������02
ELU1/elu_56/Elus
ELU1/elu_56/Greater/yConst*
_output_shapes
: *
dtype0*
valueB
 *    2
ELU1/elu_56/Greater/y�
ELU1/elu_56/GreaterGreaterELU1/BiasAdd:output:0ELU1/elu_56/Greater/y:output:0*
T0*'
_output_shapes
:���������02
ELU1/elu_56/Greaterk
ELU1/elu_56/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
ELU1/elu_56/mul/x�
ELU1/elu_56/mulMulELU1/elu_56/mul/x:output:0ELU1/elu_56/Elu:activations:0*
T0*'
_output_shapes
:���������02
ELU1/elu_56/mul�
ELU1/elu_56/SelectV2SelectV2ELU1/elu_56/Greater:z:0ELU1/elu_56/Elu:activations:0ELU1/elu_56/mul:z:0*
T0*'
_output_shapes
:���������02
ELU1/elu_56/SelectV2�
ELU2/MatMul/ReadVariableOpReadVariableOp#elu2_matmul_readvariableop_resource*
_output_shapes

:00*
dtype02
ELU2/MatMul/ReadVariableOp�
ELU2/MatMulMatMulELU1/elu_56/SelectV2:output:0"ELU2/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
ELU2/MatMul�
ELU2/BiasAdd/ReadVariableOpReadVariableOp$elu2_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
ELU2/BiasAdd/ReadVariableOp�
ELU2/BiasAddBiasAddELU2/MatMul:product:0#ELU2/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
ELU2/BiasAddr
ELU2/elu_57/EluEluELU2/BiasAdd:output:0*
T0*'
_output_shapes
:���������02
ELU2/elu_57/Elus
ELU2/elu_57/Greater/yConst*
_output_shapes
: *
dtype0*
valueB
 *    2
ELU2/elu_57/Greater/y�
ELU2/elu_57/GreaterGreaterELU2/BiasAdd:output:0ELU2/elu_57/Greater/y:output:0*
T0*'
_output_shapes
:���������02
ELU2/elu_57/Greaterk
ELU2/elu_57/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
ELU2/elu_57/mul/x�
ELU2/elu_57/mulMulELU2/elu_57/mul/x:output:0ELU2/elu_57/Elu:activations:0*
T0*'
_output_shapes
:���������02
ELU2/elu_57/mul�
ELU2/elu_57/SelectV2SelectV2ELU2/elu_57/Greater:z:0ELU2/elu_57/Elu:activations:0ELU2/elu_57/mul:z:0*
T0*'
_output_shapes
:���������02
ELU2/elu_57/SelectV2�
ELU3/MatMul/ReadVariableOpReadVariableOp#elu3_matmul_readvariableop_resource*
_output_shapes

:00*
dtype02
ELU3/MatMul/ReadVariableOp�
ELU3/MatMulMatMulELU2/elu_57/SelectV2:output:0"ELU3/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
ELU3/MatMul�
ELU3/BiasAdd/ReadVariableOpReadVariableOp$elu3_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
ELU3/BiasAdd/ReadVariableOp�
ELU3/BiasAddBiasAddELU3/MatMul:product:0#ELU3/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
ELU3/BiasAddr
ELU3/elu_58/EluEluELU3/BiasAdd:output:0*
T0*'
_output_shapes
:���������02
ELU3/elu_58/Elus
ELU3/elu_58/Greater/yConst*
_output_shapes
: *
dtype0*
valueB
 *    2
ELU3/elu_58/Greater/y�
ELU3/elu_58/GreaterGreaterELU3/BiasAdd:output:0ELU3/elu_58/Greater/y:output:0*
T0*'
_output_shapes
:���������02
ELU3/elu_58/Greaterk
ELU3/elu_58/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
ELU3/elu_58/mul/x�
ELU3/elu_58/mulMulELU3/elu_58/mul/x:output:0ELU3/elu_58/Elu:activations:0*
T0*'
_output_shapes
:���������02
ELU3/elu_58/mul�
ELU3/elu_58/SelectV2SelectV2ELU3/elu_58/Greater:z:0ELU3/elu_58/Elu:activations:0ELU3/elu_58/mul:z:0*
T0*'
_output_shapes
:���������02
ELU3/elu_58/SelectV2�
ELU4/MatMul/ReadVariableOpReadVariableOp#elu4_matmul_readvariableop_resource*
_output_shapes

:00*
dtype02
ELU4/MatMul/ReadVariableOp�
ELU4/MatMulMatMulELU3/elu_58/SelectV2:output:0"ELU4/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
ELU4/MatMul�
ELU4/BiasAdd/ReadVariableOpReadVariableOp$elu4_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
ELU4/BiasAdd/ReadVariableOp�
ELU4/BiasAddBiasAddELU4/MatMul:product:0#ELU4/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
ELU4/BiasAddr
ELU4/elu_59/EluEluELU4/BiasAdd:output:0*
T0*'
_output_shapes
:���������02
ELU4/elu_59/Elus
ELU4/elu_59/Greater/yConst*
_output_shapes
: *
dtype0*
valueB
 *    2
ELU4/elu_59/Greater/y�
ELU4/elu_59/GreaterGreaterELU4/BiasAdd:output:0ELU4/elu_59/Greater/y:output:0*
T0*'
_output_shapes
:���������02
ELU4/elu_59/Greaterk
ELU4/elu_59/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
ELU4/elu_59/mul/x�
ELU4/elu_59/mulMulELU4/elu_59/mul/x:output:0ELU4/elu_59/Elu:activations:0*
T0*'
_output_shapes
:���������02
ELU4/elu_59/mul�
ELU4/elu_59/SelectV2SelectV2ELU4/elu_59/Greater:z:0ELU4/elu_59/Elu:activations:0ELU4/elu_59/mul:z:0*
T0*'
_output_shapes
:���������02
ELU4/elu_59/SelectV2�
.batch_normalization_9/batchnorm/ReadVariableOpReadVariableOp7batch_normalization_9_batchnorm_readvariableop_resource*
_output_shapes
:0*
dtype020
.batch_normalization_9/batchnorm/ReadVariableOp�
%batch_normalization_9/batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2'
%batch_normalization_9/batchnorm/add/y�
#batch_normalization_9/batchnorm/addAddV26batch_normalization_9/batchnorm/ReadVariableOp:value:0.batch_normalization_9/batchnorm/add/y:output:0*
T0*
_output_shapes
:02%
#batch_normalization_9/batchnorm/add�
%batch_normalization_9/batchnorm/RsqrtRsqrt'batch_normalization_9/batchnorm/add:z:0*
T0*
_output_shapes
:02'
%batch_normalization_9/batchnorm/Rsqrt�
2batch_normalization_9/batchnorm/mul/ReadVariableOpReadVariableOp;batch_normalization_9_batchnorm_mul_readvariableop_resource*
_output_shapes
:0*
dtype024
2batch_normalization_9/batchnorm/mul/ReadVariableOp�
#batch_normalization_9/batchnorm/mulMul)batch_normalization_9/batchnorm/Rsqrt:y:0:batch_normalization_9/batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes
:02%
#batch_normalization_9/batchnorm/mul�
%batch_normalization_9/batchnorm/mul_1MulELU4/elu_59/SelectV2:output:0'batch_normalization_9/batchnorm/mul:z:0*
T0*'
_output_shapes
:���������02'
%batch_normalization_9/batchnorm/mul_1�
0batch_normalization_9/batchnorm/ReadVariableOp_1ReadVariableOp9batch_normalization_9_batchnorm_readvariableop_1_resource*
_output_shapes
:0*
dtype022
0batch_normalization_9/batchnorm/ReadVariableOp_1�
%batch_normalization_9/batchnorm/mul_2Mul8batch_normalization_9/batchnorm/ReadVariableOp_1:value:0'batch_normalization_9/batchnorm/mul:z:0*
T0*
_output_shapes
:02'
%batch_normalization_9/batchnorm/mul_2�
0batch_normalization_9/batchnorm/ReadVariableOp_2ReadVariableOp9batch_normalization_9_batchnorm_readvariableop_2_resource*
_output_shapes
:0*
dtype022
0batch_normalization_9/batchnorm/ReadVariableOp_2�
#batch_normalization_9/batchnorm/subSub8batch_normalization_9/batchnorm/ReadVariableOp_2:value:0)batch_normalization_9/batchnorm/mul_2:z:0*
T0*
_output_shapes
:02%
#batch_normalization_9/batchnorm/sub�
%batch_normalization_9/batchnorm/add_1AddV2)batch_normalization_9/batchnorm/mul_1:z:0'batch_normalization_9/batchnorm/sub:z:0*
T0*'
_output_shapes
:���������02'
%batch_normalization_9/batchnorm/add_1�
Output/MatMul/ReadVariableOpReadVariableOp%output_matmul_readvariableop_resource*
_output_shapes

:0	*
dtype02
Output/MatMul/ReadVariableOp�
Output/MatMulMatMul)batch_normalization_9/batchnorm/add_1:z:0$Output/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������	2
Output/MatMul�
Output/BiasAdd/ReadVariableOpReadVariableOp&output_biasadd_readvariableop_resource*
_output_shapes
:	*
dtype02
Output/BiasAdd/ReadVariableOp�
Output/BiasAddBiasAddOutput/MatMul:product:0%Output/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������	2
Output/BiasAdd|
softmax_9/SoftmaxSoftmaxOutput/BiasAdd:output:0*
T0*'
_output_shapes
:���������	2
softmax_9/Softmaxo
IdentityIdentitysoftmax_9/Softmax:softmax:0*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*n
_input_shapes]
[:���������0:::::::::::::::::::O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�*
�
S__inference_batch_normalization_9_layer_call_and_return_conditional_losses_13537212

inputs
assignmovingavg_13537187
assignmovingavg_1_13537193)
%batchnorm_mul_readvariableop_resource%
!batchnorm_readvariableop_resource
identity��#AssignMovingAvg/AssignSubVariableOp�%AssignMovingAvg_1/AssignSubVariableOp�
moments/mean/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 2 
moments/mean/reduction_indices�
moments/meanMeaninputs'moments/mean/reduction_indices:output:0*
T0*
_output_shapes

:0*
	keep_dims(2
moments/mean|
moments/StopGradientStopGradientmoments/mean:output:0*
T0*
_output_shapes

:02
moments/StopGradient�
moments/SquaredDifferenceSquaredDifferenceinputsmoments/StopGradient:output:0*
T0*'
_output_shapes
:���������02
moments/SquaredDifference�
"moments/variance/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 2$
"moments/variance/reduction_indices�
moments/varianceMeanmoments/SquaredDifference:z:0+moments/variance/reduction_indices:output:0*
T0*
_output_shapes

:0*
	keep_dims(2
moments/variance�
moments/SqueezeSqueezemoments/mean:output:0*
T0*
_output_shapes
:0*
squeeze_dims
 2
moments/Squeeze�
moments/Squeeze_1Squeezemoments/variance:output:0*
T0*
_output_shapes
:0*
squeeze_dims
 2
moments/Squeeze_1�
AssignMovingAvg/decayConst*+
_class!
loc:@AssignMovingAvg/13537187*
_output_shapes
: *
dtype0*
valueB
 *
�#<2
AssignMovingAvg/decay�
AssignMovingAvg/ReadVariableOpReadVariableOpassignmovingavg_13537187*
_output_shapes
:0*
dtype02 
AssignMovingAvg/ReadVariableOp�
AssignMovingAvg/subSub&AssignMovingAvg/ReadVariableOp:value:0moments/Squeeze:output:0*
T0*+
_class!
loc:@AssignMovingAvg/13537187*
_output_shapes
:02
AssignMovingAvg/sub�
AssignMovingAvg/mulMulAssignMovingAvg/sub:z:0AssignMovingAvg/decay:output:0*
T0*+
_class!
loc:@AssignMovingAvg/13537187*
_output_shapes
:02
AssignMovingAvg/mul�
#AssignMovingAvg/AssignSubVariableOpAssignSubVariableOpassignmovingavg_13537187AssignMovingAvg/mul:z:0^AssignMovingAvg/ReadVariableOp*+
_class!
loc:@AssignMovingAvg/13537187*
_output_shapes
 *
dtype02%
#AssignMovingAvg/AssignSubVariableOp�
AssignMovingAvg_1/decayConst*-
_class#
!loc:@AssignMovingAvg_1/13537193*
_output_shapes
: *
dtype0*
valueB
 *
�#<2
AssignMovingAvg_1/decay�
 AssignMovingAvg_1/ReadVariableOpReadVariableOpassignmovingavg_1_13537193*
_output_shapes
:0*
dtype02"
 AssignMovingAvg_1/ReadVariableOp�
AssignMovingAvg_1/subSub(AssignMovingAvg_1/ReadVariableOp:value:0moments/Squeeze_1:output:0*
T0*-
_class#
!loc:@AssignMovingAvg_1/13537193*
_output_shapes
:02
AssignMovingAvg_1/sub�
AssignMovingAvg_1/mulMulAssignMovingAvg_1/sub:z:0 AssignMovingAvg_1/decay:output:0*
T0*-
_class#
!loc:@AssignMovingAvg_1/13537193*
_output_shapes
:02
AssignMovingAvg_1/mul�
%AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOpassignmovingavg_1_13537193AssignMovingAvg_1/mul:z:0!^AssignMovingAvg_1/ReadVariableOp*-
_class#
!loc:@AssignMovingAvg_1/13537193*
_output_shapes
 *
dtype02'
%AssignMovingAvg_1/AssignSubVariableOpg
batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2
batchnorm/add/y�
batchnorm/addAddV2moments/Squeeze_1:output:0batchnorm/add/y:output:0*
T0*
_output_shapes
:02
batchnorm/addc
batchnorm/RsqrtRsqrtbatchnorm/add:z:0*
T0*
_output_shapes
:02
batchnorm/Rsqrt�
batchnorm/mul/ReadVariableOpReadVariableOp%batchnorm_mul_readvariableop_resource*
_output_shapes
:0*
dtype02
batchnorm/mul/ReadVariableOp�
batchnorm/mulMulbatchnorm/Rsqrt:y:0$batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes
:02
batchnorm/mulv
batchnorm/mul_1Mulinputsbatchnorm/mul:z:0*
T0*'
_output_shapes
:���������02
batchnorm/mul_1{
batchnorm/mul_2Mulmoments/Squeeze:output:0batchnorm/mul:z:0*
T0*
_output_shapes
:02
batchnorm/mul_2�
batchnorm/ReadVariableOpReadVariableOp!batchnorm_readvariableop_resource*
_output_shapes
:0*
dtype02
batchnorm/ReadVariableOp�
batchnorm/subSub batchnorm/ReadVariableOp:value:0batchnorm/mul_2:z:0*
T0*
_output_shapes
:02
batchnorm/sub�
batchnorm/add_1AddV2batchnorm/mul_1:z:0batchnorm/sub:z:0*
T0*'
_output_shapes
:���������02
batchnorm/add_1�
IdentityIdentitybatchnorm/add_1:z:0$^AssignMovingAvg/AssignSubVariableOp&^AssignMovingAvg_1/AssignSubVariableOp*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*6
_input_shapes%
#:���������0::::2J
#AssignMovingAvg/AssignSubVariableOp#AssignMovingAvg/AssignSubVariableOp2N
%AssignMovingAvg_1/AssignSubVariableOp%AssignMovingAvg_1/AssignSubVariableOp:O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
|
'__inference_ELU1_layer_call_fn_13538215

inputs
unknown
	unknown_0
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_ELU1_layer_call_and_return_conditional_losses_135373402
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
�
C__inference_Input_layer_call_and_return_conditional_losses_13537276

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:00*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02	
BiasAddc

elu_54/EluEluBiasAdd:output:0*
T0*'
_output_shapes
:���������02

elu_54/Elui
elu_54/Greater/yConst*
_output_shapes
: *
dtype0*
valueB
 *    2
elu_54/Greater/y�
elu_54/GreaterGreaterBiasAdd:output:0elu_54/Greater/y:output:0*
T0*'
_output_shapes
:���������02
elu_54/Greatera
elu_54/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
elu_54/mul/x�

elu_54/mulMulelu_54/mul/x:output:0elu_54/Elu:activations:0*
T0*'
_output_shapes
:���������02

elu_54/mul�
elu_54/SelectV2SelectV2elu_54/Greater:z:0elu_54/Elu:activations:0elu_54/mul:z:0*
T0*'
_output_shapes
:���������02
elu_54/SelectV2l
IdentityIdentityelu_54/SelectV2:output:0*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0:::O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
�
B__inference_ELU0_layer_call_and_return_conditional_losses_13538181

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:00*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02	
BiasAddc

elu_55/EluEluBiasAdd:output:0*
T0*'
_output_shapes
:���������02

elu_55/Elui
elu_55/Greater/yConst*
_output_shapes
: *
dtype0*
valueB
 *    2
elu_55/Greater/y�
elu_55/GreaterGreaterBiasAdd:output:0elu_55/Greater/y:output:0*
T0*'
_output_shapes
:���������02
elu_55/Greatera
elu_55/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
elu_55/mul/x�

elu_55/mulMulelu_55/mul/x:output:0elu_55/Elu:activations:0*
T0*'
_output_shapes
:���������02

elu_55/mul�
elu_55/SelectV2SelectV2elu_55/Greater:z:0elu_55/Elu:activations:0elu_55/mul:z:0*
T0*'
_output_shapes
:���������02
elu_55/SelectV2l
IdentityIdentityelu_55/SelectV2:output:0*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0:::O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
�
B__inference_ELU2_layer_call_and_return_conditional_losses_13537372

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:00*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02	
BiasAddc

elu_57/EluEluBiasAdd:output:0*
T0*'
_output_shapes
:���������02

elu_57/Elui
elu_57/Greater/yConst*
_output_shapes
: *
dtype0*
valueB
 *    2
elu_57/Greater/y�
elu_57/GreaterGreaterBiasAdd:output:0elu_57/Greater/y:output:0*
T0*'
_output_shapes
:���������02
elu_57/Greatera
elu_57/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
elu_57/mul/x�

elu_57/mulMulelu_57/mul/x:output:0elu_57/Elu:activations:0*
T0*'
_output_shapes
:���������02

elu_57/mul�
elu_57/SelectV2SelectV2elu_57/Greater:z:0elu_57/Elu:activations:0elu_57/mul:z:0*
T0*'
_output_shapes
:���������02
elu_57/SelectV2l
IdentityIdentityelu_57/SelectV2:output:0*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0:::O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
�
B__inference_ELU2_layer_call_and_return_conditional_losses_13538231

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:00*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02	
BiasAddc

elu_57/EluEluBiasAdd:output:0*
T0*'
_output_shapes
:���������02

elu_57/Elui
elu_57/Greater/yConst*
_output_shapes
: *
dtype0*
valueB
 *    2
elu_57/Greater/y�
elu_57/GreaterGreaterBiasAdd:output:0elu_57/Greater/y:output:0*
T0*'
_output_shapes
:���������02
elu_57/Greatera
elu_57/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
elu_57/mul/x�

elu_57/mulMulelu_57/mul/x:output:0elu_57/Elu:activations:0*
T0*'
_output_shapes
:���������02

elu_57/mul�
elu_57/SelectV2SelectV2elu_57/Greater:z:0elu_57/Elu:activations:0elu_57/mul:z:0*
T0*'
_output_shapes
:���������02
elu_57/SelectV2l
IdentityIdentityelu_57/SelectV2:output:0*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0:::O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
|
'__inference_ELU2_layer_call_fn_13538240

inputs
unknown
	unknown_0
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_ELU2_layer_call_and_return_conditional_losses_135373722
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
�
;__inference_ELU_1.5_size_5_48_iter_2_layer_call_fn_13538099

inputs
unknown
	unknown_0
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
	unknown_7
	unknown_8
	unknown_9

unknown_10

unknown_11

unknown_12

unknown_13

unknown_14

unknown_15

unknown_16
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16*
Tin
2*
Tout
2*'
_output_shapes
:���������	*2
_read_only_resource_inputs
	
**
config_proto

CPU

GPU 2J 8*_
fZRX
V__inference_ELU_1.5_size_5_48_iter_2_layer_call_and_return_conditional_losses_135376282
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*n
_input_shapes]
[:���������0::::::::::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
D__inference_Output_layer_call_and_return_conditional_losses_13538418

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:0	*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������	2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:	*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������	2	
BiasAddd
IdentityIdentityBiasAdd:output:0*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0:::O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
�
8__inference_batch_normalization_9_layer_call_fn_13538395

inputs
unknown
	unknown_0
	unknown_1
	unknown_2
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2*
Tin	
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*\
fWRU
S__inference_batch_normalization_9_layer_call_and_return_conditional_losses_135372122
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*6
_input_shapes%
#:���������0::::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
;__inference_ELU_1.5_size_5_48_iter_2_layer_call_fn_13537757
input_input
unknown
	unknown_0
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
	unknown_7
	unknown_8
	unknown_9

unknown_10

unknown_11

unknown_12

unknown_13

unknown_14

unknown_15

unknown_16
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16*
Tin
2*
Tout
2*'
_output_shapes
:���������	*4
_read_only_resource_inputs
	
**
config_proto

CPU

GPU 2J 8*_
fZRX
V__inference_ELU_1.5_size_5_48_iter_2_layer_call_and_return_conditional_losses_135377182
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*n
_input_shapes]
[:���������0::::::::::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:T P
'
_output_shapes
:���������0
%
_user_specified_nameInput_input:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
8__inference_batch_normalization_9_layer_call_fn_13538408

inputs
unknown
	unknown_0
	unknown_1
	unknown_2
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2*
Tin	
2*
Tout
2*'
_output_shapes
:���������0*&
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*\
fWRU
S__inference_batch_normalization_9_layer_call_and_return_conditional_losses_135372452
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*6
_input_shapes%
#:���������0::::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
;__inference_ELU_1.5_size_5_48_iter_2_layer_call_fn_13538140

inputs
unknown
	unknown_0
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
	unknown_7
	unknown_8
	unknown_9

unknown_10

unknown_11

unknown_12

unknown_13

unknown_14

unknown_15

unknown_16
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16*
Tin
2*
Tout
2*'
_output_shapes
:���������	*4
_read_only_resource_inputs
	
**
config_proto

CPU

GPU 2J 8*_
fZRX
V__inference_ELU_1.5_size_5_48_iter_2_layer_call_and_return_conditional_losses_135377182
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*n
_input_shapes]
[:���������0::::::::::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
S__inference_batch_normalization_9_layer_call_and_return_conditional_losses_13537245

inputs%
!batchnorm_readvariableop_resource)
%batchnorm_mul_readvariableop_resource'
#batchnorm_readvariableop_1_resource'
#batchnorm_readvariableop_2_resource
identity��
batchnorm/ReadVariableOpReadVariableOp!batchnorm_readvariableop_resource*
_output_shapes
:0*
dtype02
batchnorm/ReadVariableOpg
batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2
batchnorm/add/y�
batchnorm/addAddV2 batchnorm/ReadVariableOp:value:0batchnorm/add/y:output:0*
T0*
_output_shapes
:02
batchnorm/addc
batchnorm/RsqrtRsqrtbatchnorm/add:z:0*
T0*
_output_shapes
:02
batchnorm/Rsqrt�
batchnorm/mul/ReadVariableOpReadVariableOp%batchnorm_mul_readvariableop_resource*
_output_shapes
:0*
dtype02
batchnorm/mul/ReadVariableOp�
batchnorm/mulMulbatchnorm/Rsqrt:y:0$batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes
:02
batchnorm/mulv
batchnorm/mul_1Mulinputsbatchnorm/mul:z:0*
T0*'
_output_shapes
:���������02
batchnorm/mul_1�
batchnorm/ReadVariableOp_1ReadVariableOp#batchnorm_readvariableop_1_resource*
_output_shapes
:0*
dtype02
batchnorm/ReadVariableOp_1�
batchnorm/mul_2Mul"batchnorm/ReadVariableOp_1:value:0batchnorm/mul:z:0*
T0*
_output_shapes
:02
batchnorm/mul_2�
batchnorm/ReadVariableOp_2ReadVariableOp#batchnorm_readvariableop_2_resource*
_output_shapes
:0*
dtype02
batchnorm/ReadVariableOp_2�
batchnorm/subSub"batchnorm/ReadVariableOp_2:value:0batchnorm/mul_2:z:0*
T0*
_output_shapes
:02
batchnorm/sub�
batchnorm/add_1AddV2batchnorm/mul_1:z:0batchnorm/sub:z:0*
T0*'
_output_shapes
:���������02
batchnorm/add_1g
IdentityIdentitybatchnorm/add_1:z:0*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*6
_input_shapes%
#:���������0:::::O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
B__inference_ELU3_layer_call_and_return_conditional_losses_13537404

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:00*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02	
BiasAddc

elu_58/EluEluBiasAdd:output:0*
T0*'
_output_shapes
:���������02

elu_58/Elui
elu_58/Greater/yConst*
_output_shapes
: *
dtype0*
valueB
 *    2
elu_58/Greater/y�
elu_58/GreaterGreaterBiasAdd:output:0elu_58/Greater/y:output:0*
T0*'
_output_shapes
:���������02
elu_58/Greatera
elu_58/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
elu_58/mul/x�

elu_58/mulMulelu_58/mul/x:output:0elu_58/Elu:activations:0*
T0*'
_output_shapes
:���������02

elu_58/mul�
elu_58/SelectV2SelectV2elu_58/Greater:z:0elu_58/Elu:activations:0elu_58/mul:z:0*
T0*'
_output_shapes
:���������02
elu_58/SelectV2l
IdentityIdentityelu_58/SelectV2:output:0*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0:::O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
H
,__inference_softmax_9_layer_call_fn_13538437

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*'
_output_shapes
:���������	* 
_read_only_resource_inputs
 **
config_proto

CPU

GPU 2J 8*P
fKRI
G__inference_softmax_9_layer_call_and_return_conditional_losses_135375182
PartitionedCalll
IdentityIdentityPartitionedCall:output:0*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*&
_input_shapes
:���������	:O K
'
_output_shapes
:���������	
 
_user_specified_nameinputs
�*
�
S__inference_batch_normalization_9_layer_call_and_return_conditional_losses_13538362

inputs
assignmovingavg_13538337
assignmovingavg_1_13538343)
%batchnorm_mul_readvariableop_resource%
!batchnorm_readvariableop_resource
identity��#AssignMovingAvg/AssignSubVariableOp�%AssignMovingAvg_1/AssignSubVariableOp�
moments/mean/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 2 
moments/mean/reduction_indices�
moments/meanMeaninputs'moments/mean/reduction_indices:output:0*
T0*
_output_shapes

:0*
	keep_dims(2
moments/mean|
moments/StopGradientStopGradientmoments/mean:output:0*
T0*
_output_shapes

:02
moments/StopGradient�
moments/SquaredDifferenceSquaredDifferenceinputsmoments/StopGradient:output:0*
T0*'
_output_shapes
:���������02
moments/SquaredDifference�
"moments/variance/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 2$
"moments/variance/reduction_indices�
moments/varianceMeanmoments/SquaredDifference:z:0+moments/variance/reduction_indices:output:0*
T0*
_output_shapes

:0*
	keep_dims(2
moments/variance�
moments/SqueezeSqueezemoments/mean:output:0*
T0*
_output_shapes
:0*
squeeze_dims
 2
moments/Squeeze�
moments/Squeeze_1Squeezemoments/variance:output:0*
T0*
_output_shapes
:0*
squeeze_dims
 2
moments/Squeeze_1�
AssignMovingAvg/decayConst*+
_class!
loc:@AssignMovingAvg/13538337*
_output_shapes
: *
dtype0*
valueB
 *
�#<2
AssignMovingAvg/decay�
AssignMovingAvg/ReadVariableOpReadVariableOpassignmovingavg_13538337*
_output_shapes
:0*
dtype02 
AssignMovingAvg/ReadVariableOp�
AssignMovingAvg/subSub&AssignMovingAvg/ReadVariableOp:value:0moments/Squeeze:output:0*
T0*+
_class!
loc:@AssignMovingAvg/13538337*
_output_shapes
:02
AssignMovingAvg/sub�
AssignMovingAvg/mulMulAssignMovingAvg/sub:z:0AssignMovingAvg/decay:output:0*
T0*+
_class!
loc:@AssignMovingAvg/13538337*
_output_shapes
:02
AssignMovingAvg/mul�
#AssignMovingAvg/AssignSubVariableOpAssignSubVariableOpassignmovingavg_13538337AssignMovingAvg/mul:z:0^AssignMovingAvg/ReadVariableOp*+
_class!
loc:@AssignMovingAvg/13538337*
_output_shapes
 *
dtype02%
#AssignMovingAvg/AssignSubVariableOp�
AssignMovingAvg_1/decayConst*-
_class#
!loc:@AssignMovingAvg_1/13538343*
_output_shapes
: *
dtype0*
valueB
 *
�#<2
AssignMovingAvg_1/decay�
 AssignMovingAvg_1/ReadVariableOpReadVariableOpassignmovingavg_1_13538343*
_output_shapes
:0*
dtype02"
 AssignMovingAvg_1/ReadVariableOp�
AssignMovingAvg_1/subSub(AssignMovingAvg_1/ReadVariableOp:value:0moments/Squeeze_1:output:0*
T0*-
_class#
!loc:@AssignMovingAvg_1/13538343*
_output_shapes
:02
AssignMovingAvg_1/sub�
AssignMovingAvg_1/mulMulAssignMovingAvg_1/sub:z:0 AssignMovingAvg_1/decay:output:0*
T0*-
_class#
!loc:@AssignMovingAvg_1/13538343*
_output_shapes
:02
AssignMovingAvg_1/mul�
%AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOpassignmovingavg_1_13538343AssignMovingAvg_1/mul:z:0!^AssignMovingAvg_1/ReadVariableOp*-
_class#
!loc:@AssignMovingAvg_1/13538343*
_output_shapes
 *
dtype02'
%AssignMovingAvg_1/AssignSubVariableOpg
batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2
batchnorm/add/y�
batchnorm/addAddV2moments/Squeeze_1:output:0batchnorm/add/y:output:0*
T0*
_output_shapes
:02
batchnorm/addc
batchnorm/RsqrtRsqrtbatchnorm/add:z:0*
T0*
_output_shapes
:02
batchnorm/Rsqrt�
batchnorm/mul/ReadVariableOpReadVariableOp%batchnorm_mul_readvariableop_resource*
_output_shapes
:0*
dtype02
batchnorm/mul/ReadVariableOp�
batchnorm/mulMulbatchnorm/Rsqrt:y:0$batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes
:02
batchnorm/mulv
batchnorm/mul_1Mulinputsbatchnorm/mul:z:0*
T0*'
_output_shapes
:���������02
batchnorm/mul_1{
batchnorm/mul_2Mulmoments/Squeeze:output:0batchnorm/mul:z:0*
T0*
_output_shapes
:02
batchnorm/mul_2�
batchnorm/ReadVariableOpReadVariableOp!batchnorm_readvariableop_resource*
_output_shapes
:0*
dtype02
batchnorm/ReadVariableOp�
batchnorm/subSub batchnorm/ReadVariableOp:value:0batchnorm/mul_2:z:0*
T0*
_output_shapes
:02
batchnorm/sub�
batchnorm/add_1AddV2batchnorm/mul_1:z:0batchnorm/sub:z:0*
T0*'
_output_shapes
:���������02
batchnorm/add_1�
IdentityIdentitybatchnorm/add_1:z:0$^AssignMovingAvg/AssignSubVariableOp&^AssignMovingAvg_1/AssignSubVariableOp*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*6
_input_shapes%
#:���������0::::2J
#AssignMovingAvg/AssignSubVariableOp#AssignMovingAvg/AssignSubVariableOp2N
%AssignMovingAvg_1/AssignSubVariableOp%AssignMovingAvg_1/AssignSubVariableOp:O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
c
G__inference_softmax_9_layer_call_and_return_conditional_losses_13538432

inputs
identityW
SoftmaxSoftmaxinputs*
T0*'
_output_shapes
:���������	2	
Softmaxe
IdentityIdentitySoftmax:softmax:0*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*&
_input_shapes
:���������	:O K
'
_output_shapes
:���������	
 
_user_specified_nameinputs
�
�
S__inference_batch_normalization_9_layer_call_and_return_conditional_losses_13538382

inputs%
!batchnorm_readvariableop_resource)
%batchnorm_mul_readvariableop_resource'
#batchnorm_readvariableop_1_resource'
#batchnorm_readvariableop_2_resource
identity��
batchnorm/ReadVariableOpReadVariableOp!batchnorm_readvariableop_resource*
_output_shapes
:0*
dtype02
batchnorm/ReadVariableOpg
batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2
batchnorm/add/y�
batchnorm/addAddV2 batchnorm/ReadVariableOp:value:0batchnorm/add/y:output:0*
T0*
_output_shapes
:02
batchnorm/addc
batchnorm/RsqrtRsqrtbatchnorm/add:z:0*
T0*
_output_shapes
:02
batchnorm/Rsqrt�
batchnorm/mul/ReadVariableOpReadVariableOp%batchnorm_mul_readvariableop_resource*
_output_shapes
:0*
dtype02
batchnorm/mul/ReadVariableOp�
batchnorm/mulMulbatchnorm/Rsqrt:y:0$batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes
:02
batchnorm/mulv
batchnorm/mul_1Mulinputsbatchnorm/mul:z:0*
T0*'
_output_shapes
:���������02
batchnorm/mul_1�
batchnorm/ReadVariableOp_1ReadVariableOp#batchnorm_readvariableop_1_resource*
_output_shapes
:0*
dtype02
batchnorm/ReadVariableOp_1�
batchnorm/mul_2Mul"batchnorm/ReadVariableOp_1:value:0batchnorm/mul:z:0*
T0*
_output_shapes
:02
batchnorm/mul_2�
batchnorm/ReadVariableOp_2ReadVariableOp#batchnorm_readvariableop_2_resource*
_output_shapes
:0*
dtype02
batchnorm/ReadVariableOp_2�
batchnorm/subSub"batchnorm/ReadVariableOp_2:value:0batchnorm/mul_2:z:0*
T0*
_output_shapes
:02
batchnorm/sub�
batchnorm/add_1AddV2batchnorm/mul_1:z:0batchnorm/sub:z:0*
T0*'
_output_shapes
:���������02
batchnorm/add_1g
IdentityIdentitybatchnorm/add_1:z:0*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*6
_input_shapes%
#:���������0:::::O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
B__inference_ELU1_layer_call_and_return_conditional_losses_13538206

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:00*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02	
BiasAddc

elu_56/EluEluBiasAdd:output:0*
T0*'
_output_shapes
:���������02

elu_56/Elui
elu_56/Greater/yConst*
_output_shapes
: *
dtype0*
valueB
 *    2
elu_56/Greater/y�
elu_56/GreaterGreaterBiasAdd:output:0elu_56/Greater/y:output:0*
T0*'
_output_shapes
:���������02
elu_56/Greatera
elu_56/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
elu_56/mul/x�

elu_56/mulMulelu_56/mul/x:output:0elu_56/Elu:activations:0*
T0*'
_output_shapes
:���������02

elu_56/mul�
elu_56/SelectV2SelectV2elu_56/Greater:z:0elu_56/Elu:activations:0elu_56/mul:z:0*
T0*'
_output_shapes
:���������02
elu_56/SelectV2l
IdentityIdentityelu_56/SelectV2:output:0*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0:::O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�/
�
V__inference_ELU_1.5_size_5_48_iter_2_layer_call_and_return_conditional_losses_13537527
input_input
input_13537287
input_13537289
elu0_13537319
elu0_13537321
elu1_13537351
elu1_13537353
elu2_13537383
elu2_13537385
elu3_13537415
elu3_13537417
elu4_13537447
elu4_13537449"
batch_normalization_9_13537478"
batch_normalization_9_13537480"
batch_normalization_9_13537482"
batch_normalization_9_13537484
output_13537508
output_13537510
identity��ELU0/StatefulPartitionedCall�ELU1/StatefulPartitionedCall�ELU2/StatefulPartitionedCall�ELU3/StatefulPartitionedCall�ELU4/StatefulPartitionedCall�Input/StatefulPartitionedCall�Output/StatefulPartitionedCall�-batch_normalization_9/StatefulPartitionedCall�
Input/StatefulPartitionedCallStatefulPartitionedCallinput_inputinput_13537287input_13537289*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*L
fGRE
C__inference_Input_layer_call_and_return_conditional_losses_135372762
Input/StatefulPartitionedCall�
ELU0/StatefulPartitionedCallStatefulPartitionedCall&Input/StatefulPartitionedCall:output:0elu0_13537319elu0_13537321*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_ELU0_layer_call_and_return_conditional_losses_135373082
ELU0/StatefulPartitionedCall�
ELU1/StatefulPartitionedCallStatefulPartitionedCall%ELU0/StatefulPartitionedCall:output:0elu1_13537351elu1_13537353*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_ELU1_layer_call_and_return_conditional_losses_135373402
ELU1/StatefulPartitionedCall�
ELU2/StatefulPartitionedCallStatefulPartitionedCall%ELU1/StatefulPartitionedCall:output:0elu2_13537383elu2_13537385*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_ELU2_layer_call_and_return_conditional_losses_135373722
ELU2/StatefulPartitionedCall�
ELU3/StatefulPartitionedCallStatefulPartitionedCall%ELU2/StatefulPartitionedCall:output:0elu3_13537415elu3_13537417*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_ELU3_layer_call_and_return_conditional_losses_135374042
ELU3/StatefulPartitionedCall�
ELU4/StatefulPartitionedCallStatefulPartitionedCall%ELU3/StatefulPartitionedCall:output:0elu4_13537447elu4_13537449*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_ELU4_layer_call_and_return_conditional_losses_135374362
ELU4/StatefulPartitionedCall�
-batch_normalization_9/StatefulPartitionedCallStatefulPartitionedCall%ELU4/StatefulPartitionedCall:output:0batch_normalization_9_13537478batch_normalization_9_13537480batch_normalization_9_13537482batch_normalization_9_13537484*
Tin	
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*\
fWRU
S__inference_batch_normalization_9_layer_call_and_return_conditional_losses_135372122/
-batch_normalization_9/StatefulPartitionedCall�
Output/StatefulPartitionedCallStatefulPartitionedCall6batch_normalization_9/StatefulPartitionedCall:output:0output_13537508output_13537510*
Tin
2*
Tout
2*'
_output_shapes
:���������	*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*M
fHRF
D__inference_Output_layer_call_and_return_conditional_losses_135374972 
Output/StatefulPartitionedCall�
softmax_9/PartitionedCallPartitionedCall'Output/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*'
_output_shapes
:���������	* 
_read_only_resource_inputs
 **
config_proto

CPU

GPU 2J 8*P
fKRI
G__inference_softmax_9_layer_call_and_return_conditional_losses_135375182
softmax_9/PartitionedCall�
IdentityIdentity"softmax_9/PartitionedCall:output:0^ELU0/StatefulPartitionedCall^ELU1/StatefulPartitionedCall^ELU2/StatefulPartitionedCall^ELU3/StatefulPartitionedCall^ELU4/StatefulPartitionedCall^Input/StatefulPartitionedCall^Output/StatefulPartitionedCall.^batch_normalization_9/StatefulPartitionedCall*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*n
_input_shapes]
[:���������0::::::::::::::::::2<
ELU0/StatefulPartitionedCallELU0/StatefulPartitionedCall2<
ELU1/StatefulPartitionedCallELU1/StatefulPartitionedCall2<
ELU2/StatefulPartitionedCallELU2/StatefulPartitionedCall2<
ELU3/StatefulPartitionedCallELU3/StatefulPartitionedCall2<
ELU4/StatefulPartitionedCallELU4/StatefulPartitionedCall2>
Input/StatefulPartitionedCallInput/StatefulPartitionedCall2@
Output/StatefulPartitionedCallOutput/StatefulPartitionedCall2^
-batch_normalization_9/StatefulPartitionedCall-batch_normalization_9/StatefulPartitionedCall:T P
'
_output_shapes
:���������0
%
_user_specified_nameInput_input:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
|
'__inference_ELU0_layer_call_fn_13538190

inputs
unknown
	unknown_0
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_ELU0_layer_call_and_return_conditional_losses_135373082
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
��
�
$__inference__traced_restore_13538842
file_prefix#
assignvariableop_input_9_kernel#
assignvariableop_1_input_9_bias$
 assignvariableop_2_elu0_8_kernel"
assignvariableop_3_elu0_8_bias$
 assignvariableop_4_elu1_9_kernel"
assignvariableop_5_elu1_9_bias$
 assignvariableop_6_elu2_9_kernel"
assignvariableop_7_elu2_9_bias$
 assignvariableop_8_elu3_9_kernel"
assignvariableop_9_elu3_9_bias%
!assignvariableop_10_elu4_9_kernel#
assignvariableop_11_elu4_9_bias3
/assignvariableop_12_batch_normalization_9_gamma2
.assignvariableop_13_batch_normalization_9_beta9
5assignvariableop_14_batch_normalization_9_moving_mean=
9assignvariableop_15_batch_normalization_9_moving_variance'
#assignvariableop_16_output_9_kernel%
!assignvariableop_17_output_9_bias!
assignvariableop_18_adam_iter#
assignvariableop_19_adam_beta_1#
assignvariableop_20_adam_beta_2"
assignvariableop_21_adam_decay*
&assignvariableop_22_adam_learning_rate
assignvariableop_23_total
assignvariableop_24_count
assignvariableop_25_total_1
assignvariableop_26_count_1&
"assignvariableop_27_true_positives'
#assignvariableop_28_false_positives-
)assignvariableop_29_adam_input_9_kernel_m+
'assignvariableop_30_adam_input_9_bias_m,
(assignvariableop_31_adam_elu0_8_kernel_m*
&assignvariableop_32_adam_elu0_8_bias_m,
(assignvariableop_33_adam_elu1_9_kernel_m*
&assignvariableop_34_adam_elu1_9_bias_m,
(assignvariableop_35_adam_elu2_9_kernel_m*
&assignvariableop_36_adam_elu2_9_bias_m,
(assignvariableop_37_adam_elu3_9_kernel_m*
&assignvariableop_38_adam_elu3_9_bias_m,
(assignvariableop_39_adam_elu4_9_kernel_m*
&assignvariableop_40_adam_elu4_9_bias_m:
6assignvariableop_41_adam_batch_normalization_9_gamma_m9
5assignvariableop_42_adam_batch_normalization_9_beta_m.
*assignvariableop_43_adam_output_9_kernel_m,
(assignvariableop_44_adam_output_9_bias_m-
)assignvariableop_45_adam_input_9_kernel_v+
'assignvariableop_46_adam_input_9_bias_v,
(assignvariableop_47_adam_elu0_8_kernel_v*
&assignvariableop_48_adam_elu0_8_bias_v,
(assignvariableop_49_adam_elu1_9_kernel_v*
&assignvariableop_50_adam_elu1_9_bias_v,
(assignvariableop_51_adam_elu2_9_kernel_v*
&assignvariableop_52_adam_elu2_9_bias_v,
(assignvariableop_53_adam_elu3_9_kernel_v*
&assignvariableop_54_adam_elu3_9_bias_v,
(assignvariableop_55_adam_elu4_9_kernel_v*
&assignvariableop_56_adam_elu4_9_bias_v:
6assignvariableop_57_adam_batch_normalization_9_gamma_v9
5assignvariableop_58_adam_batch_normalization_9_beta_v.
*assignvariableop_59_adam_output_9_kernel_v,
(assignvariableop_60_adam_output_9_bias_v
identity_62��AssignVariableOp�AssignVariableOp_1�AssignVariableOp_10�AssignVariableOp_11�AssignVariableOp_12�AssignVariableOp_13�AssignVariableOp_14�AssignVariableOp_15�AssignVariableOp_16�AssignVariableOp_17�AssignVariableOp_18�AssignVariableOp_19�AssignVariableOp_2�AssignVariableOp_20�AssignVariableOp_21�AssignVariableOp_22�AssignVariableOp_23�AssignVariableOp_24�AssignVariableOp_25�AssignVariableOp_26�AssignVariableOp_27�AssignVariableOp_28�AssignVariableOp_29�AssignVariableOp_3�AssignVariableOp_30�AssignVariableOp_31�AssignVariableOp_32�AssignVariableOp_33�AssignVariableOp_34�AssignVariableOp_35�AssignVariableOp_36�AssignVariableOp_37�AssignVariableOp_38�AssignVariableOp_39�AssignVariableOp_4�AssignVariableOp_40�AssignVariableOp_41�AssignVariableOp_42�AssignVariableOp_43�AssignVariableOp_44�AssignVariableOp_45�AssignVariableOp_46�AssignVariableOp_47�AssignVariableOp_48�AssignVariableOp_49�AssignVariableOp_5�AssignVariableOp_50�AssignVariableOp_51�AssignVariableOp_52�AssignVariableOp_53�AssignVariableOp_54�AssignVariableOp_55�AssignVariableOp_56�AssignVariableOp_57�AssignVariableOp_58�AssignVariableOp_59�AssignVariableOp_6�AssignVariableOp_60�AssignVariableOp_7�AssignVariableOp_8�AssignVariableOp_9�	RestoreV2�RestoreV2_1�"
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:=*
dtype0*�!
value�!B�!=B6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-6/gamma/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-6/beta/.ATTRIBUTES/VARIABLE_VALUEB;layer_with_weights-6/moving_mean/.ATTRIBUTES/VARIABLE_VALUEB?layer_with_weights-6/moving_variance/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/2/true_positives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/2/false_positives/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-5/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-5/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-6/gamma/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-6/beta/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-7/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-7/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-5/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-5/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-6/gamma/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-6/beta/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-7/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-7/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE2
RestoreV2/tensor_names�
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:=*
dtype0*�
value�B�=B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
RestoreV2/shape_and_slices�
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*�
_output_shapes�
�:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*K
dtypesA
?2=	2
	RestoreV2X
IdentityIdentityRestoreV2:tensors:0*
T0*
_output_shapes
:2

Identity�
AssignVariableOpAssignVariableOpassignvariableop_input_9_kernelIdentity:output:0*
_output_shapes
 *
dtype02
AssignVariableOp\

Identity_1IdentityRestoreV2:tensors:1*
T0*
_output_shapes
:2

Identity_1�
AssignVariableOp_1AssignVariableOpassignvariableop_1_input_9_biasIdentity_1:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_1\

Identity_2IdentityRestoreV2:tensors:2*
T0*
_output_shapes
:2

Identity_2�
AssignVariableOp_2AssignVariableOp assignvariableop_2_elu0_8_kernelIdentity_2:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_2\

Identity_3IdentityRestoreV2:tensors:3*
T0*
_output_shapes
:2

Identity_3�
AssignVariableOp_3AssignVariableOpassignvariableop_3_elu0_8_biasIdentity_3:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_3\

Identity_4IdentityRestoreV2:tensors:4*
T0*
_output_shapes
:2

Identity_4�
AssignVariableOp_4AssignVariableOp assignvariableop_4_elu1_9_kernelIdentity_4:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_4\

Identity_5IdentityRestoreV2:tensors:5*
T0*
_output_shapes
:2

Identity_5�
AssignVariableOp_5AssignVariableOpassignvariableop_5_elu1_9_biasIdentity_5:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_5\

Identity_6IdentityRestoreV2:tensors:6*
T0*
_output_shapes
:2

Identity_6�
AssignVariableOp_6AssignVariableOp assignvariableop_6_elu2_9_kernelIdentity_6:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_6\

Identity_7IdentityRestoreV2:tensors:7*
T0*
_output_shapes
:2

Identity_7�
AssignVariableOp_7AssignVariableOpassignvariableop_7_elu2_9_biasIdentity_7:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_7\

Identity_8IdentityRestoreV2:tensors:8*
T0*
_output_shapes
:2

Identity_8�
AssignVariableOp_8AssignVariableOp assignvariableop_8_elu3_9_kernelIdentity_8:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_8\

Identity_9IdentityRestoreV2:tensors:9*
T0*
_output_shapes
:2

Identity_9�
AssignVariableOp_9AssignVariableOpassignvariableop_9_elu3_9_biasIdentity_9:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_9_
Identity_10IdentityRestoreV2:tensors:10*
T0*
_output_shapes
:2
Identity_10�
AssignVariableOp_10AssignVariableOp!assignvariableop_10_elu4_9_kernelIdentity_10:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_10_
Identity_11IdentityRestoreV2:tensors:11*
T0*
_output_shapes
:2
Identity_11�
AssignVariableOp_11AssignVariableOpassignvariableop_11_elu4_9_biasIdentity_11:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_11_
Identity_12IdentityRestoreV2:tensors:12*
T0*
_output_shapes
:2
Identity_12�
AssignVariableOp_12AssignVariableOp/assignvariableop_12_batch_normalization_9_gammaIdentity_12:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_12_
Identity_13IdentityRestoreV2:tensors:13*
T0*
_output_shapes
:2
Identity_13�
AssignVariableOp_13AssignVariableOp.assignvariableop_13_batch_normalization_9_betaIdentity_13:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_13_
Identity_14IdentityRestoreV2:tensors:14*
T0*
_output_shapes
:2
Identity_14�
AssignVariableOp_14AssignVariableOp5assignvariableop_14_batch_normalization_9_moving_meanIdentity_14:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_14_
Identity_15IdentityRestoreV2:tensors:15*
T0*
_output_shapes
:2
Identity_15�
AssignVariableOp_15AssignVariableOp9assignvariableop_15_batch_normalization_9_moving_varianceIdentity_15:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_15_
Identity_16IdentityRestoreV2:tensors:16*
T0*
_output_shapes
:2
Identity_16�
AssignVariableOp_16AssignVariableOp#assignvariableop_16_output_9_kernelIdentity_16:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_16_
Identity_17IdentityRestoreV2:tensors:17*
T0*
_output_shapes
:2
Identity_17�
AssignVariableOp_17AssignVariableOp!assignvariableop_17_output_9_biasIdentity_17:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_17_
Identity_18IdentityRestoreV2:tensors:18*
T0	*
_output_shapes
:2
Identity_18�
AssignVariableOp_18AssignVariableOpassignvariableop_18_adam_iterIdentity_18:output:0*
_output_shapes
 *
dtype0	2
AssignVariableOp_18_
Identity_19IdentityRestoreV2:tensors:19*
T0*
_output_shapes
:2
Identity_19�
AssignVariableOp_19AssignVariableOpassignvariableop_19_adam_beta_1Identity_19:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_19_
Identity_20IdentityRestoreV2:tensors:20*
T0*
_output_shapes
:2
Identity_20�
AssignVariableOp_20AssignVariableOpassignvariableop_20_adam_beta_2Identity_20:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_20_
Identity_21IdentityRestoreV2:tensors:21*
T0*
_output_shapes
:2
Identity_21�
AssignVariableOp_21AssignVariableOpassignvariableop_21_adam_decayIdentity_21:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_21_
Identity_22IdentityRestoreV2:tensors:22*
T0*
_output_shapes
:2
Identity_22�
AssignVariableOp_22AssignVariableOp&assignvariableop_22_adam_learning_rateIdentity_22:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_22_
Identity_23IdentityRestoreV2:tensors:23*
T0*
_output_shapes
:2
Identity_23�
AssignVariableOp_23AssignVariableOpassignvariableop_23_totalIdentity_23:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_23_
Identity_24IdentityRestoreV2:tensors:24*
T0*
_output_shapes
:2
Identity_24�
AssignVariableOp_24AssignVariableOpassignvariableop_24_countIdentity_24:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_24_
Identity_25IdentityRestoreV2:tensors:25*
T0*
_output_shapes
:2
Identity_25�
AssignVariableOp_25AssignVariableOpassignvariableop_25_total_1Identity_25:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_25_
Identity_26IdentityRestoreV2:tensors:26*
T0*
_output_shapes
:2
Identity_26�
AssignVariableOp_26AssignVariableOpassignvariableop_26_count_1Identity_26:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_26_
Identity_27IdentityRestoreV2:tensors:27*
T0*
_output_shapes
:2
Identity_27�
AssignVariableOp_27AssignVariableOp"assignvariableop_27_true_positivesIdentity_27:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_27_
Identity_28IdentityRestoreV2:tensors:28*
T0*
_output_shapes
:2
Identity_28�
AssignVariableOp_28AssignVariableOp#assignvariableop_28_false_positivesIdentity_28:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_28_
Identity_29IdentityRestoreV2:tensors:29*
T0*
_output_shapes
:2
Identity_29�
AssignVariableOp_29AssignVariableOp)assignvariableop_29_adam_input_9_kernel_mIdentity_29:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_29_
Identity_30IdentityRestoreV2:tensors:30*
T0*
_output_shapes
:2
Identity_30�
AssignVariableOp_30AssignVariableOp'assignvariableop_30_adam_input_9_bias_mIdentity_30:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_30_
Identity_31IdentityRestoreV2:tensors:31*
T0*
_output_shapes
:2
Identity_31�
AssignVariableOp_31AssignVariableOp(assignvariableop_31_adam_elu0_8_kernel_mIdentity_31:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_31_
Identity_32IdentityRestoreV2:tensors:32*
T0*
_output_shapes
:2
Identity_32�
AssignVariableOp_32AssignVariableOp&assignvariableop_32_adam_elu0_8_bias_mIdentity_32:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_32_
Identity_33IdentityRestoreV2:tensors:33*
T0*
_output_shapes
:2
Identity_33�
AssignVariableOp_33AssignVariableOp(assignvariableop_33_adam_elu1_9_kernel_mIdentity_33:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_33_
Identity_34IdentityRestoreV2:tensors:34*
T0*
_output_shapes
:2
Identity_34�
AssignVariableOp_34AssignVariableOp&assignvariableop_34_adam_elu1_9_bias_mIdentity_34:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_34_
Identity_35IdentityRestoreV2:tensors:35*
T0*
_output_shapes
:2
Identity_35�
AssignVariableOp_35AssignVariableOp(assignvariableop_35_adam_elu2_9_kernel_mIdentity_35:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_35_
Identity_36IdentityRestoreV2:tensors:36*
T0*
_output_shapes
:2
Identity_36�
AssignVariableOp_36AssignVariableOp&assignvariableop_36_adam_elu2_9_bias_mIdentity_36:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_36_
Identity_37IdentityRestoreV2:tensors:37*
T0*
_output_shapes
:2
Identity_37�
AssignVariableOp_37AssignVariableOp(assignvariableop_37_adam_elu3_9_kernel_mIdentity_37:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_37_
Identity_38IdentityRestoreV2:tensors:38*
T0*
_output_shapes
:2
Identity_38�
AssignVariableOp_38AssignVariableOp&assignvariableop_38_adam_elu3_9_bias_mIdentity_38:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_38_
Identity_39IdentityRestoreV2:tensors:39*
T0*
_output_shapes
:2
Identity_39�
AssignVariableOp_39AssignVariableOp(assignvariableop_39_adam_elu4_9_kernel_mIdentity_39:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_39_
Identity_40IdentityRestoreV2:tensors:40*
T0*
_output_shapes
:2
Identity_40�
AssignVariableOp_40AssignVariableOp&assignvariableop_40_adam_elu4_9_bias_mIdentity_40:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_40_
Identity_41IdentityRestoreV2:tensors:41*
T0*
_output_shapes
:2
Identity_41�
AssignVariableOp_41AssignVariableOp6assignvariableop_41_adam_batch_normalization_9_gamma_mIdentity_41:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_41_
Identity_42IdentityRestoreV2:tensors:42*
T0*
_output_shapes
:2
Identity_42�
AssignVariableOp_42AssignVariableOp5assignvariableop_42_adam_batch_normalization_9_beta_mIdentity_42:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_42_
Identity_43IdentityRestoreV2:tensors:43*
T0*
_output_shapes
:2
Identity_43�
AssignVariableOp_43AssignVariableOp*assignvariableop_43_adam_output_9_kernel_mIdentity_43:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_43_
Identity_44IdentityRestoreV2:tensors:44*
T0*
_output_shapes
:2
Identity_44�
AssignVariableOp_44AssignVariableOp(assignvariableop_44_adam_output_9_bias_mIdentity_44:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_44_
Identity_45IdentityRestoreV2:tensors:45*
T0*
_output_shapes
:2
Identity_45�
AssignVariableOp_45AssignVariableOp)assignvariableop_45_adam_input_9_kernel_vIdentity_45:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_45_
Identity_46IdentityRestoreV2:tensors:46*
T0*
_output_shapes
:2
Identity_46�
AssignVariableOp_46AssignVariableOp'assignvariableop_46_adam_input_9_bias_vIdentity_46:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_46_
Identity_47IdentityRestoreV2:tensors:47*
T0*
_output_shapes
:2
Identity_47�
AssignVariableOp_47AssignVariableOp(assignvariableop_47_adam_elu0_8_kernel_vIdentity_47:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_47_
Identity_48IdentityRestoreV2:tensors:48*
T0*
_output_shapes
:2
Identity_48�
AssignVariableOp_48AssignVariableOp&assignvariableop_48_adam_elu0_8_bias_vIdentity_48:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_48_
Identity_49IdentityRestoreV2:tensors:49*
T0*
_output_shapes
:2
Identity_49�
AssignVariableOp_49AssignVariableOp(assignvariableop_49_adam_elu1_9_kernel_vIdentity_49:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_49_
Identity_50IdentityRestoreV2:tensors:50*
T0*
_output_shapes
:2
Identity_50�
AssignVariableOp_50AssignVariableOp&assignvariableop_50_adam_elu1_9_bias_vIdentity_50:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_50_
Identity_51IdentityRestoreV2:tensors:51*
T0*
_output_shapes
:2
Identity_51�
AssignVariableOp_51AssignVariableOp(assignvariableop_51_adam_elu2_9_kernel_vIdentity_51:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_51_
Identity_52IdentityRestoreV2:tensors:52*
T0*
_output_shapes
:2
Identity_52�
AssignVariableOp_52AssignVariableOp&assignvariableop_52_adam_elu2_9_bias_vIdentity_52:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_52_
Identity_53IdentityRestoreV2:tensors:53*
T0*
_output_shapes
:2
Identity_53�
AssignVariableOp_53AssignVariableOp(assignvariableop_53_adam_elu3_9_kernel_vIdentity_53:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_53_
Identity_54IdentityRestoreV2:tensors:54*
T0*
_output_shapes
:2
Identity_54�
AssignVariableOp_54AssignVariableOp&assignvariableop_54_adam_elu3_9_bias_vIdentity_54:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_54_
Identity_55IdentityRestoreV2:tensors:55*
T0*
_output_shapes
:2
Identity_55�
AssignVariableOp_55AssignVariableOp(assignvariableop_55_adam_elu4_9_kernel_vIdentity_55:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_55_
Identity_56IdentityRestoreV2:tensors:56*
T0*
_output_shapes
:2
Identity_56�
AssignVariableOp_56AssignVariableOp&assignvariableop_56_adam_elu4_9_bias_vIdentity_56:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_56_
Identity_57IdentityRestoreV2:tensors:57*
T0*
_output_shapes
:2
Identity_57�
AssignVariableOp_57AssignVariableOp6assignvariableop_57_adam_batch_normalization_9_gamma_vIdentity_57:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_57_
Identity_58IdentityRestoreV2:tensors:58*
T0*
_output_shapes
:2
Identity_58�
AssignVariableOp_58AssignVariableOp5assignvariableop_58_adam_batch_normalization_9_beta_vIdentity_58:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_58_
Identity_59IdentityRestoreV2:tensors:59*
T0*
_output_shapes
:2
Identity_59�
AssignVariableOp_59AssignVariableOp*assignvariableop_59_adam_output_9_kernel_vIdentity_59:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_59_
Identity_60IdentityRestoreV2:tensors:60*
T0*
_output_shapes
:2
Identity_60�
AssignVariableOp_60AssignVariableOp(assignvariableop_60_adam_output_9_bias_vIdentity_60:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_60�
RestoreV2_1/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*1
value(B&B_CHECKPOINTABLE_OBJECT_GRAPH2
RestoreV2_1/tensor_names�
RestoreV2_1/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*
valueB
B 2
RestoreV2_1/shape_and_slices�
RestoreV2_1	RestoreV2file_prefix!RestoreV2_1/tensor_names:output:0%RestoreV2_1/shape_and_slices:output:0
^RestoreV2"/device:CPU:0*
_output_shapes
:*
dtypes
22
RestoreV2_19
NoOpNoOp"/device:CPU:0*
_output_shapes
 2
NoOp�
Identity_61Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_37^AssignVariableOp_38^AssignVariableOp_39^AssignVariableOp_4^AssignVariableOp_40^AssignVariableOp_41^AssignVariableOp_42^AssignVariableOp_43^AssignVariableOp_44^AssignVariableOp_45^AssignVariableOp_46^AssignVariableOp_47^AssignVariableOp_48^AssignVariableOp_49^AssignVariableOp_5^AssignVariableOp_50^AssignVariableOp_51^AssignVariableOp_52^AssignVariableOp_53^AssignVariableOp_54^AssignVariableOp_55^AssignVariableOp_56^AssignVariableOp_57^AssignVariableOp_58^AssignVariableOp_59^AssignVariableOp_6^AssignVariableOp_60^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9^NoOp"/device:CPU:0*
T0*
_output_shapes
: 2
Identity_61�
Identity_62IdentityIdentity_61:output:0^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_37^AssignVariableOp_38^AssignVariableOp_39^AssignVariableOp_4^AssignVariableOp_40^AssignVariableOp_41^AssignVariableOp_42^AssignVariableOp_43^AssignVariableOp_44^AssignVariableOp_45^AssignVariableOp_46^AssignVariableOp_47^AssignVariableOp_48^AssignVariableOp_49^AssignVariableOp_5^AssignVariableOp_50^AssignVariableOp_51^AssignVariableOp_52^AssignVariableOp_53^AssignVariableOp_54^AssignVariableOp_55^AssignVariableOp_56^AssignVariableOp_57^AssignVariableOp_58^AssignVariableOp_59^AssignVariableOp_6^AssignVariableOp_60^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9
^RestoreV2^RestoreV2_1*
T0*
_output_shapes
: 2
Identity_62"#
identity_62Identity_62:output:0*�
_input_shapes�
�: :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132*
AssignVariableOp_14AssignVariableOp_142*
AssignVariableOp_15AssignVariableOp_152*
AssignVariableOp_16AssignVariableOp_162*
AssignVariableOp_17AssignVariableOp_172*
AssignVariableOp_18AssignVariableOp_182*
AssignVariableOp_19AssignVariableOp_192(
AssignVariableOp_2AssignVariableOp_22*
AssignVariableOp_20AssignVariableOp_202*
AssignVariableOp_21AssignVariableOp_212*
AssignVariableOp_22AssignVariableOp_222*
AssignVariableOp_23AssignVariableOp_232*
AssignVariableOp_24AssignVariableOp_242*
AssignVariableOp_25AssignVariableOp_252*
AssignVariableOp_26AssignVariableOp_262*
AssignVariableOp_27AssignVariableOp_272*
AssignVariableOp_28AssignVariableOp_282*
AssignVariableOp_29AssignVariableOp_292(
AssignVariableOp_3AssignVariableOp_32*
AssignVariableOp_30AssignVariableOp_302*
AssignVariableOp_31AssignVariableOp_312*
AssignVariableOp_32AssignVariableOp_322*
AssignVariableOp_33AssignVariableOp_332*
AssignVariableOp_34AssignVariableOp_342*
AssignVariableOp_35AssignVariableOp_352*
AssignVariableOp_36AssignVariableOp_362*
AssignVariableOp_37AssignVariableOp_372*
AssignVariableOp_38AssignVariableOp_382*
AssignVariableOp_39AssignVariableOp_392(
AssignVariableOp_4AssignVariableOp_42*
AssignVariableOp_40AssignVariableOp_402*
AssignVariableOp_41AssignVariableOp_412*
AssignVariableOp_42AssignVariableOp_422*
AssignVariableOp_43AssignVariableOp_432*
AssignVariableOp_44AssignVariableOp_442*
AssignVariableOp_45AssignVariableOp_452*
AssignVariableOp_46AssignVariableOp_462*
AssignVariableOp_47AssignVariableOp_472*
AssignVariableOp_48AssignVariableOp_482*
AssignVariableOp_49AssignVariableOp_492(
AssignVariableOp_5AssignVariableOp_52*
AssignVariableOp_50AssignVariableOp_502*
AssignVariableOp_51AssignVariableOp_512*
AssignVariableOp_52AssignVariableOp_522*
AssignVariableOp_53AssignVariableOp_532*
AssignVariableOp_54AssignVariableOp_542*
AssignVariableOp_55AssignVariableOp_552*
AssignVariableOp_56AssignVariableOp_562*
AssignVariableOp_57AssignVariableOp_572*
AssignVariableOp_58AssignVariableOp_582*
AssignVariableOp_59AssignVariableOp_592(
AssignVariableOp_6AssignVariableOp_62*
AssignVariableOp_60AssignVariableOp_602(
AssignVariableOp_7AssignVariableOp_72(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_92
	RestoreV2	RestoreV22
RestoreV2_1RestoreV2_1:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: : 

_output_shapes
: :!

_output_shapes
: :"

_output_shapes
: :#

_output_shapes
: :$

_output_shapes
: :%

_output_shapes
: :&

_output_shapes
: :'

_output_shapes
: :(

_output_shapes
: :)

_output_shapes
: :*

_output_shapes
: :+

_output_shapes
: :,

_output_shapes
: :-

_output_shapes
: :.

_output_shapes
: :/

_output_shapes
: :0

_output_shapes
: :1

_output_shapes
: :2

_output_shapes
: :3

_output_shapes
: :4

_output_shapes
: :5

_output_shapes
: :6

_output_shapes
: :7

_output_shapes
: :8

_output_shapes
: :9

_output_shapes
: ::

_output_shapes
: :;

_output_shapes
: :<

_output_shapes
: :=

_output_shapes
: 
�/
�
V__inference_ELU_1.5_size_5_48_iter_2_layer_call_and_return_conditional_losses_13537718

inputs
input_13537672
input_13537674
elu0_13537677
elu0_13537679
elu1_13537682
elu1_13537684
elu2_13537687
elu2_13537689
elu3_13537692
elu3_13537694
elu4_13537697
elu4_13537699"
batch_normalization_9_13537702"
batch_normalization_9_13537704"
batch_normalization_9_13537706"
batch_normalization_9_13537708
output_13537711
output_13537713
identity��ELU0/StatefulPartitionedCall�ELU1/StatefulPartitionedCall�ELU2/StatefulPartitionedCall�ELU3/StatefulPartitionedCall�ELU4/StatefulPartitionedCall�Input/StatefulPartitionedCall�Output/StatefulPartitionedCall�-batch_normalization_9/StatefulPartitionedCall�
Input/StatefulPartitionedCallStatefulPartitionedCallinputsinput_13537672input_13537674*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*L
fGRE
C__inference_Input_layer_call_and_return_conditional_losses_135372762
Input/StatefulPartitionedCall�
ELU0/StatefulPartitionedCallStatefulPartitionedCall&Input/StatefulPartitionedCall:output:0elu0_13537677elu0_13537679*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_ELU0_layer_call_and_return_conditional_losses_135373082
ELU0/StatefulPartitionedCall�
ELU1/StatefulPartitionedCallStatefulPartitionedCall%ELU0/StatefulPartitionedCall:output:0elu1_13537682elu1_13537684*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_ELU1_layer_call_and_return_conditional_losses_135373402
ELU1/StatefulPartitionedCall�
ELU2/StatefulPartitionedCallStatefulPartitionedCall%ELU1/StatefulPartitionedCall:output:0elu2_13537687elu2_13537689*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_ELU2_layer_call_and_return_conditional_losses_135373722
ELU2/StatefulPartitionedCall�
ELU3/StatefulPartitionedCallStatefulPartitionedCall%ELU2/StatefulPartitionedCall:output:0elu3_13537692elu3_13537694*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_ELU3_layer_call_and_return_conditional_losses_135374042
ELU3/StatefulPartitionedCall�
ELU4/StatefulPartitionedCallStatefulPartitionedCall%ELU3/StatefulPartitionedCall:output:0elu4_13537697elu4_13537699*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_ELU4_layer_call_and_return_conditional_losses_135374362
ELU4/StatefulPartitionedCall�
-batch_normalization_9/StatefulPartitionedCallStatefulPartitionedCall%ELU4/StatefulPartitionedCall:output:0batch_normalization_9_13537702batch_normalization_9_13537704batch_normalization_9_13537706batch_normalization_9_13537708*
Tin	
2*
Tout
2*'
_output_shapes
:���������0*&
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*\
fWRU
S__inference_batch_normalization_9_layer_call_and_return_conditional_losses_135372452/
-batch_normalization_9/StatefulPartitionedCall�
Output/StatefulPartitionedCallStatefulPartitionedCall6batch_normalization_9/StatefulPartitionedCall:output:0output_13537711output_13537713*
Tin
2*
Tout
2*'
_output_shapes
:���������	*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*M
fHRF
D__inference_Output_layer_call_and_return_conditional_losses_135374972 
Output/StatefulPartitionedCall�
softmax_9/PartitionedCallPartitionedCall'Output/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*'
_output_shapes
:���������	* 
_read_only_resource_inputs
 **
config_proto

CPU

GPU 2J 8*P
fKRI
G__inference_softmax_9_layer_call_and_return_conditional_losses_135375182
softmax_9/PartitionedCall�
IdentityIdentity"softmax_9/PartitionedCall:output:0^ELU0/StatefulPartitionedCall^ELU1/StatefulPartitionedCall^ELU2/StatefulPartitionedCall^ELU3/StatefulPartitionedCall^ELU4/StatefulPartitionedCall^Input/StatefulPartitionedCall^Output/StatefulPartitionedCall.^batch_normalization_9/StatefulPartitionedCall*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*n
_input_shapes]
[:���������0::::::::::::::::::2<
ELU0/StatefulPartitionedCallELU0/StatefulPartitionedCall2<
ELU1/StatefulPartitionedCallELU1/StatefulPartitionedCall2<
ELU2/StatefulPartitionedCallELU2/StatefulPartitionedCall2<
ELU3/StatefulPartitionedCallELU3/StatefulPartitionedCall2<
ELU4/StatefulPartitionedCallELU4/StatefulPartitionedCall2>
Input/StatefulPartitionedCallInput/StatefulPartitionedCall2@
Output/StatefulPartitionedCallOutput/StatefulPartitionedCall2^
-batch_normalization_9/StatefulPartitionedCall-batch_normalization_9/StatefulPartitionedCall:O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
}
(__inference_Input_layer_call_fn_13538165

inputs
unknown
	unknown_0
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*L
fGRE
C__inference_Input_layer_call_and_return_conditional_losses_135372762
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
�
;__inference_ELU_1.5_size_5_48_iter_2_layer_call_fn_13537667
input_input
unknown
	unknown_0
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
	unknown_7
	unknown_8
	unknown_9

unknown_10

unknown_11

unknown_12

unknown_13

unknown_14

unknown_15

unknown_16
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16*
Tin
2*
Tout
2*'
_output_shapes
:���������	*2
_read_only_resource_inputs
	
**
config_proto

CPU

GPU 2J 8*_
fZRX
V__inference_ELU_1.5_size_5_48_iter_2_layer_call_and_return_conditional_losses_135376282
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*n
_input_shapes]
[:���������0::::::::::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:T P
'
_output_shapes
:���������0
%
_user_specified_nameInput_input:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
B__inference_ELU4_layer_call_and_return_conditional_losses_13538281

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:00*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02	
BiasAddc

elu_59/EluEluBiasAdd:output:0*
T0*'
_output_shapes
:���������02

elu_59/Elui
elu_59/Greater/yConst*
_output_shapes
: *
dtype0*
valueB
 *    2
elu_59/Greater/y�
elu_59/GreaterGreaterBiasAdd:output:0elu_59/Greater/y:output:0*
T0*'
_output_shapes
:���������02
elu_59/Greatera
elu_59/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
elu_59/mul/x�

elu_59/mulMulelu_59/mul/x:output:0elu_59/Elu:activations:0*
T0*'
_output_shapes
:���������02

elu_59/mul�
elu_59/SelectV2SelectV2elu_59/Greater:z:0elu_59/Elu:activations:0elu_59/mul:z:0*
T0*'
_output_shapes
:���������02
elu_59/SelectV2l
IdentityIdentityelu_59/SelectV2:output:0*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0:::O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
|
'__inference_ELU3_layer_call_fn_13538265

inputs
unknown
	unknown_0
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_ELU3_layer_call_and_return_conditional_losses_135374042
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
�
B__inference_ELU1_layer_call_and_return_conditional_losses_13537340

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:00*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02	
BiasAddc

elu_56/EluEluBiasAdd:output:0*
T0*'
_output_shapes
:���������02

elu_56/Elui
elu_56/Greater/yConst*
_output_shapes
: *
dtype0*
valueB
 *    2
elu_56/Greater/y�
elu_56/GreaterGreaterBiasAdd:output:0elu_56/Greater/y:output:0*
T0*'
_output_shapes
:���������02
elu_56/Greatera
elu_56/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
elu_56/mul/x�

elu_56/mulMulelu_56/mul/x:output:0elu_56/Elu:activations:0*
T0*'
_output_shapes
:���������02

elu_56/mul�
elu_56/SelectV2SelectV2elu_56/Greater:z:0elu_56/Elu:activations:0elu_56/mul:z:0*
T0*'
_output_shapes
:���������02
elu_56/SelectV2l
IdentityIdentityelu_56/SelectV2:output:0*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0:::O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
ۣ
�

#__inference__wrapped_model_13537116
input_inputA
=elu_1_5_size_5_48_iter_2_input_matmul_readvariableop_resourceB
>elu_1_5_size_5_48_iter_2_input_biasadd_readvariableop_resource@
<elu_1_5_size_5_48_iter_2_elu0_matmul_readvariableop_resourceA
=elu_1_5_size_5_48_iter_2_elu0_biasadd_readvariableop_resource@
<elu_1_5_size_5_48_iter_2_elu1_matmul_readvariableop_resourceA
=elu_1_5_size_5_48_iter_2_elu1_biasadd_readvariableop_resource@
<elu_1_5_size_5_48_iter_2_elu2_matmul_readvariableop_resourceA
=elu_1_5_size_5_48_iter_2_elu2_biasadd_readvariableop_resource@
<elu_1_5_size_5_48_iter_2_elu3_matmul_readvariableop_resourceA
=elu_1_5_size_5_48_iter_2_elu3_biasadd_readvariableop_resource@
<elu_1_5_size_5_48_iter_2_elu4_matmul_readvariableop_resourceA
=elu_1_5_size_5_48_iter_2_elu4_biasadd_readvariableop_resourceT
Pelu_1_5_size_5_48_iter_2_batch_normalization_9_batchnorm_readvariableop_resourceX
Telu_1_5_size_5_48_iter_2_batch_normalization_9_batchnorm_mul_readvariableop_resourceV
Relu_1_5_size_5_48_iter_2_batch_normalization_9_batchnorm_readvariableop_1_resourceV
Relu_1_5_size_5_48_iter_2_batch_normalization_9_batchnorm_readvariableop_2_resourceB
>elu_1_5_size_5_48_iter_2_output_matmul_readvariableop_resourceC
?elu_1_5_size_5_48_iter_2_output_biasadd_readvariableop_resource
identity��
4ELU_1.5_size_5_48_iter_2/Input/MatMul/ReadVariableOpReadVariableOp=elu_1_5_size_5_48_iter_2_input_matmul_readvariableop_resource*
_output_shapes

:00*
dtype026
4ELU_1.5_size_5_48_iter_2/Input/MatMul/ReadVariableOp�
%ELU_1.5_size_5_48_iter_2/Input/MatMulMatMulinput_input<ELU_1.5_size_5_48_iter_2/Input/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02'
%ELU_1.5_size_5_48_iter_2/Input/MatMul�
5ELU_1.5_size_5_48_iter_2/Input/BiasAdd/ReadVariableOpReadVariableOp>elu_1_5_size_5_48_iter_2_input_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype027
5ELU_1.5_size_5_48_iter_2/Input/BiasAdd/ReadVariableOp�
&ELU_1.5_size_5_48_iter_2/Input/BiasAddBiasAdd/ELU_1.5_size_5_48_iter_2/Input/MatMul:product:0=ELU_1.5_size_5_48_iter_2/Input/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02(
&ELU_1.5_size_5_48_iter_2/Input/BiasAdd�
)ELU_1.5_size_5_48_iter_2/Input/elu_54/EluElu/ELU_1.5_size_5_48_iter_2/Input/BiasAdd:output:0*
T0*'
_output_shapes
:���������02+
)ELU_1.5_size_5_48_iter_2/Input/elu_54/Elu�
/ELU_1.5_size_5_48_iter_2/Input/elu_54/Greater/yConst*
_output_shapes
: *
dtype0*
valueB
 *    21
/ELU_1.5_size_5_48_iter_2/Input/elu_54/Greater/y�
-ELU_1.5_size_5_48_iter_2/Input/elu_54/GreaterGreater/ELU_1.5_size_5_48_iter_2/Input/BiasAdd:output:08ELU_1.5_size_5_48_iter_2/Input/elu_54/Greater/y:output:0*
T0*'
_output_shapes
:���������02/
-ELU_1.5_size_5_48_iter_2/Input/elu_54/Greater�
+ELU_1.5_size_5_48_iter_2/Input/elu_54/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2-
+ELU_1.5_size_5_48_iter_2/Input/elu_54/mul/x�
)ELU_1.5_size_5_48_iter_2/Input/elu_54/mulMul4ELU_1.5_size_5_48_iter_2/Input/elu_54/mul/x:output:07ELU_1.5_size_5_48_iter_2/Input/elu_54/Elu:activations:0*
T0*'
_output_shapes
:���������02+
)ELU_1.5_size_5_48_iter_2/Input/elu_54/mul�
.ELU_1.5_size_5_48_iter_2/Input/elu_54/SelectV2SelectV21ELU_1.5_size_5_48_iter_2/Input/elu_54/Greater:z:07ELU_1.5_size_5_48_iter_2/Input/elu_54/Elu:activations:0-ELU_1.5_size_5_48_iter_2/Input/elu_54/mul:z:0*
T0*'
_output_shapes
:���������020
.ELU_1.5_size_5_48_iter_2/Input/elu_54/SelectV2�
3ELU_1.5_size_5_48_iter_2/ELU0/MatMul/ReadVariableOpReadVariableOp<elu_1_5_size_5_48_iter_2_elu0_matmul_readvariableop_resource*
_output_shapes

:00*
dtype025
3ELU_1.5_size_5_48_iter_2/ELU0/MatMul/ReadVariableOp�
$ELU_1.5_size_5_48_iter_2/ELU0/MatMulMatMul7ELU_1.5_size_5_48_iter_2/Input/elu_54/SelectV2:output:0;ELU_1.5_size_5_48_iter_2/ELU0/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02&
$ELU_1.5_size_5_48_iter_2/ELU0/MatMul�
4ELU_1.5_size_5_48_iter_2/ELU0/BiasAdd/ReadVariableOpReadVariableOp=elu_1_5_size_5_48_iter_2_elu0_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype026
4ELU_1.5_size_5_48_iter_2/ELU0/BiasAdd/ReadVariableOp�
%ELU_1.5_size_5_48_iter_2/ELU0/BiasAddBiasAdd.ELU_1.5_size_5_48_iter_2/ELU0/MatMul:product:0<ELU_1.5_size_5_48_iter_2/ELU0/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02'
%ELU_1.5_size_5_48_iter_2/ELU0/BiasAdd�
(ELU_1.5_size_5_48_iter_2/ELU0/elu_55/EluElu.ELU_1.5_size_5_48_iter_2/ELU0/BiasAdd:output:0*
T0*'
_output_shapes
:���������02*
(ELU_1.5_size_5_48_iter_2/ELU0/elu_55/Elu�
.ELU_1.5_size_5_48_iter_2/ELU0/elu_55/Greater/yConst*
_output_shapes
: *
dtype0*
valueB
 *    20
.ELU_1.5_size_5_48_iter_2/ELU0/elu_55/Greater/y�
,ELU_1.5_size_5_48_iter_2/ELU0/elu_55/GreaterGreater.ELU_1.5_size_5_48_iter_2/ELU0/BiasAdd:output:07ELU_1.5_size_5_48_iter_2/ELU0/elu_55/Greater/y:output:0*
T0*'
_output_shapes
:���������02.
,ELU_1.5_size_5_48_iter_2/ELU0/elu_55/Greater�
*ELU_1.5_size_5_48_iter_2/ELU0/elu_55/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2,
*ELU_1.5_size_5_48_iter_2/ELU0/elu_55/mul/x�
(ELU_1.5_size_5_48_iter_2/ELU0/elu_55/mulMul3ELU_1.5_size_5_48_iter_2/ELU0/elu_55/mul/x:output:06ELU_1.5_size_5_48_iter_2/ELU0/elu_55/Elu:activations:0*
T0*'
_output_shapes
:���������02*
(ELU_1.5_size_5_48_iter_2/ELU0/elu_55/mul�
-ELU_1.5_size_5_48_iter_2/ELU0/elu_55/SelectV2SelectV20ELU_1.5_size_5_48_iter_2/ELU0/elu_55/Greater:z:06ELU_1.5_size_5_48_iter_2/ELU0/elu_55/Elu:activations:0,ELU_1.5_size_5_48_iter_2/ELU0/elu_55/mul:z:0*
T0*'
_output_shapes
:���������02/
-ELU_1.5_size_5_48_iter_2/ELU0/elu_55/SelectV2�
3ELU_1.5_size_5_48_iter_2/ELU1/MatMul/ReadVariableOpReadVariableOp<elu_1_5_size_5_48_iter_2_elu1_matmul_readvariableop_resource*
_output_shapes

:00*
dtype025
3ELU_1.5_size_5_48_iter_2/ELU1/MatMul/ReadVariableOp�
$ELU_1.5_size_5_48_iter_2/ELU1/MatMulMatMul6ELU_1.5_size_5_48_iter_2/ELU0/elu_55/SelectV2:output:0;ELU_1.5_size_5_48_iter_2/ELU1/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02&
$ELU_1.5_size_5_48_iter_2/ELU1/MatMul�
4ELU_1.5_size_5_48_iter_2/ELU1/BiasAdd/ReadVariableOpReadVariableOp=elu_1_5_size_5_48_iter_2_elu1_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype026
4ELU_1.5_size_5_48_iter_2/ELU1/BiasAdd/ReadVariableOp�
%ELU_1.5_size_5_48_iter_2/ELU1/BiasAddBiasAdd.ELU_1.5_size_5_48_iter_2/ELU1/MatMul:product:0<ELU_1.5_size_5_48_iter_2/ELU1/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02'
%ELU_1.5_size_5_48_iter_2/ELU1/BiasAdd�
(ELU_1.5_size_5_48_iter_2/ELU1/elu_56/EluElu.ELU_1.5_size_5_48_iter_2/ELU1/BiasAdd:output:0*
T0*'
_output_shapes
:���������02*
(ELU_1.5_size_5_48_iter_2/ELU1/elu_56/Elu�
.ELU_1.5_size_5_48_iter_2/ELU1/elu_56/Greater/yConst*
_output_shapes
: *
dtype0*
valueB
 *    20
.ELU_1.5_size_5_48_iter_2/ELU1/elu_56/Greater/y�
,ELU_1.5_size_5_48_iter_2/ELU1/elu_56/GreaterGreater.ELU_1.5_size_5_48_iter_2/ELU1/BiasAdd:output:07ELU_1.5_size_5_48_iter_2/ELU1/elu_56/Greater/y:output:0*
T0*'
_output_shapes
:���������02.
,ELU_1.5_size_5_48_iter_2/ELU1/elu_56/Greater�
*ELU_1.5_size_5_48_iter_2/ELU1/elu_56/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2,
*ELU_1.5_size_5_48_iter_2/ELU1/elu_56/mul/x�
(ELU_1.5_size_5_48_iter_2/ELU1/elu_56/mulMul3ELU_1.5_size_5_48_iter_2/ELU1/elu_56/mul/x:output:06ELU_1.5_size_5_48_iter_2/ELU1/elu_56/Elu:activations:0*
T0*'
_output_shapes
:���������02*
(ELU_1.5_size_5_48_iter_2/ELU1/elu_56/mul�
-ELU_1.5_size_5_48_iter_2/ELU1/elu_56/SelectV2SelectV20ELU_1.5_size_5_48_iter_2/ELU1/elu_56/Greater:z:06ELU_1.5_size_5_48_iter_2/ELU1/elu_56/Elu:activations:0,ELU_1.5_size_5_48_iter_2/ELU1/elu_56/mul:z:0*
T0*'
_output_shapes
:���������02/
-ELU_1.5_size_5_48_iter_2/ELU1/elu_56/SelectV2�
3ELU_1.5_size_5_48_iter_2/ELU2/MatMul/ReadVariableOpReadVariableOp<elu_1_5_size_5_48_iter_2_elu2_matmul_readvariableop_resource*
_output_shapes

:00*
dtype025
3ELU_1.5_size_5_48_iter_2/ELU2/MatMul/ReadVariableOp�
$ELU_1.5_size_5_48_iter_2/ELU2/MatMulMatMul6ELU_1.5_size_5_48_iter_2/ELU1/elu_56/SelectV2:output:0;ELU_1.5_size_5_48_iter_2/ELU2/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02&
$ELU_1.5_size_5_48_iter_2/ELU2/MatMul�
4ELU_1.5_size_5_48_iter_2/ELU2/BiasAdd/ReadVariableOpReadVariableOp=elu_1_5_size_5_48_iter_2_elu2_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype026
4ELU_1.5_size_5_48_iter_2/ELU2/BiasAdd/ReadVariableOp�
%ELU_1.5_size_5_48_iter_2/ELU2/BiasAddBiasAdd.ELU_1.5_size_5_48_iter_2/ELU2/MatMul:product:0<ELU_1.5_size_5_48_iter_2/ELU2/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02'
%ELU_1.5_size_5_48_iter_2/ELU2/BiasAdd�
(ELU_1.5_size_5_48_iter_2/ELU2/elu_57/EluElu.ELU_1.5_size_5_48_iter_2/ELU2/BiasAdd:output:0*
T0*'
_output_shapes
:���������02*
(ELU_1.5_size_5_48_iter_2/ELU2/elu_57/Elu�
.ELU_1.5_size_5_48_iter_2/ELU2/elu_57/Greater/yConst*
_output_shapes
: *
dtype0*
valueB
 *    20
.ELU_1.5_size_5_48_iter_2/ELU2/elu_57/Greater/y�
,ELU_1.5_size_5_48_iter_2/ELU2/elu_57/GreaterGreater.ELU_1.5_size_5_48_iter_2/ELU2/BiasAdd:output:07ELU_1.5_size_5_48_iter_2/ELU2/elu_57/Greater/y:output:0*
T0*'
_output_shapes
:���������02.
,ELU_1.5_size_5_48_iter_2/ELU2/elu_57/Greater�
*ELU_1.5_size_5_48_iter_2/ELU2/elu_57/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2,
*ELU_1.5_size_5_48_iter_2/ELU2/elu_57/mul/x�
(ELU_1.5_size_5_48_iter_2/ELU2/elu_57/mulMul3ELU_1.5_size_5_48_iter_2/ELU2/elu_57/mul/x:output:06ELU_1.5_size_5_48_iter_2/ELU2/elu_57/Elu:activations:0*
T0*'
_output_shapes
:���������02*
(ELU_1.5_size_5_48_iter_2/ELU2/elu_57/mul�
-ELU_1.5_size_5_48_iter_2/ELU2/elu_57/SelectV2SelectV20ELU_1.5_size_5_48_iter_2/ELU2/elu_57/Greater:z:06ELU_1.5_size_5_48_iter_2/ELU2/elu_57/Elu:activations:0,ELU_1.5_size_5_48_iter_2/ELU2/elu_57/mul:z:0*
T0*'
_output_shapes
:���������02/
-ELU_1.5_size_5_48_iter_2/ELU2/elu_57/SelectV2�
3ELU_1.5_size_5_48_iter_2/ELU3/MatMul/ReadVariableOpReadVariableOp<elu_1_5_size_5_48_iter_2_elu3_matmul_readvariableop_resource*
_output_shapes

:00*
dtype025
3ELU_1.5_size_5_48_iter_2/ELU3/MatMul/ReadVariableOp�
$ELU_1.5_size_5_48_iter_2/ELU3/MatMulMatMul6ELU_1.5_size_5_48_iter_2/ELU2/elu_57/SelectV2:output:0;ELU_1.5_size_5_48_iter_2/ELU3/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02&
$ELU_1.5_size_5_48_iter_2/ELU3/MatMul�
4ELU_1.5_size_5_48_iter_2/ELU3/BiasAdd/ReadVariableOpReadVariableOp=elu_1_5_size_5_48_iter_2_elu3_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype026
4ELU_1.5_size_5_48_iter_2/ELU3/BiasAdd/ReadVariableOp�
%ELU_1.5_size_5_48_iter_2/ELU3/BiasAddBiasAdd.ELU_1.5_size_5_48_iter_2/ELU3/MatMul:product:0<ELU_1.5_size_5_48_iter_2/ELU3/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02'
%ELU_1.5_size_5_48_iter_2/ELU3/BiasAdd�
(ELU_1.5_size_5_48_iter_2/ELU3/elu_58/EluElu.ELU_1.5_size_5_48_iter_2/ELU3/BiasAdd:output:0*
T0*'
_output_shapes
:���������02*
(ELU_1.5_size_5_48_iter_2/ELU3/elu_58/Elu�
.ELU_1.5_size_5_48_iter_2/ELU3/elu_58/Greater/yConst*
_output_shapes
: *
dtype0*
valueB
 *    20
.ELU_1.5_size_5_48_iter_2/ELU3/elu_58/Greater/y�
,ELU_1.5_size_5_48_iter_2/ELU3/elu_58/GreaterGreater.ELU_1.5_size_5_48_iter_2/ELU3/BiasAdd:output:07ELU_1.5_size_5_48_iter_2/ELU3/elu_58/Greater/y:output:0*
T0*'
_output_shapes
:���������02.
,ELU_1.5_size_5_48_iter_2/ELU3/elu_58/Greater�
*ELU_1.5_size_5_48_iter_2/ELU3/elu_58/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2,
*ELU_1.5_size_5_48_iter_2/ELU3/elu_58/mul/x�
(ELU_1.5_size_5_48_iter_2/ELU3/elu_58/mulMul3ELU_1.5_size_5_48_iter_2/ELU3/elu_58/mul/x:output:06ELU_1.5_size_5_48_iter_2/ELU3/elu_58/Elu:activations:0*
T0*'
_output_shapes
:���������02*
(ELU_1.5_size_5_48_iter_2/ELU3/elu_58/mul�
-ELU_1.5_size_5_48_iter_2/ELU3/elu_58/SelectV2SelectV20ELU_1.5_size_5_48_iter_2/ELU3/elu_58/Greater:z:06ELU_1.5_size_5_48_iter_2/ELU3/elu_58/Elu:activations:0,ELU_1.5_size_5_48_iter_2/ELU3/elu_58/mul:z:0*
T0*'
_output_shapes
:���������02/
-ELU_1.5_size_5_48_iter_2/ELU3/elu_58/SelectV2�
3ELU_1.5_size_5_48_iter_2/ELU4/MatMul/ReadVariableOpReadVariableOp<elu_1_5_size_5_48_iter_2_elu4_matmul_readvariableop_resource*
_output_shapes

:00*
dtype025
3ELU_1.5_size_5_48_iter_2/ELU4/MatMul/ReadVariableOp�
$ELU_1.5_size_5_48_iter_2/ELU4/MatMulMatMul6ELU_1.5_size_5_48_iter_2/ELU3/elu_58/SelectV2:output:0;ELU_1.5_size_5_48_iter_2/ELU4/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02&
$ELU_1.5_size_5_48_iter_2/ELU4/MatMul�
4ELU_1.5_size_5_48_iter_2/ELU4/BiasAdd/ReadVariableOpReadVariableOp=elu_1_5_size_5_48_iter_2_elu4_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype026
4ELU_1.5_size_5_48_iter_2/ELU4/BiasAdd/ReadVariableOp�
%ELU_1.5_size_5_48_iter_2/ELU4/BiasAddBiasAdd.ELU_1.5_size_5_48_iter_2/ELU4/MatMul:product:0<ELU_1.5_size_5_48_iter_2/ELU4/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02'
%ELU_1.5_size_5_48_iter_2/ELU4/BiasAdd�
(ELU_1.5_size_5_48_iter_2/ELU4/elu_59/EluElu.ELU_1.5_size_5_48_iter_2/ELU4/BiasAdd:output:0*
T0*'
_output_shapes
:���������02*
(ELU_1.5_size_5_48_iter_2/ELU4/elu_59/Elu�
.ELU_1.5_size_5_48_iter_2/ELU4/elu_59/Greater/yConst*
_output_shapes
: *
dtype0*
valueB
 *    20
.ELU_1.5_size_5_48_iter_2/ELU4/elu_59/Greater/y�
,ELU_1.5_size_5_48_iter_2/ELU4/elu_59/GreaterGreater.ELU_1.5_size_5_48_iter_2/ELU4/BiasAdd:output:07ELU_1.5_size_5_48_iter_2/ELU4/elu_59/Greater/y:output:0*
T0*'
_output_shapes
:���������02.
,ELU_1.5_size_5_48_iter_2/ELU4/elu_59/Greater�
*ELU_1.5_size_5_48_iter_2/ELU4/elu_59/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2,
*ELU_1.5_size_5_48_iter_2/ELU4/elu_59/mul/x�
(ELU_1.5_size_5_48_iter_2/ELU4/elu_59/mulMul3ELU_1.5_size_5_48_iter_2/ELU4/elu_59/mul/x:output:06ELU_1.5_size_5_48_iter_2/ELU4/elu_59/Elu:activations:0*
T0*'
_output_shapes
:���������02*
(ELU_1.5_size_5_48_iter_2/ELU4/elu_59/mul�
-ELU_1.5_size_5_48_iter_2/ELU4/elu_59/SelectV2SelectV20ELU_1.5_size_5_48_iter_2/ELU4/elu_59/Greater:z:06ELU_1.5_size_5_48_iter_2/ELU4/elu_59/Elu:activations:0,ELU_1.5_size_5_48_iter_2/ELU4/elu_59/mul:z:0*
T0*'
_output_shapes
:���������02/
-ELU_1.5_size_5_48_iter_2/ELU4/elu_59/SelectV2�
GELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/ReadVariableOpReadVariableOpPelu_1_5_size_5_48_iter_2_batch_normalization_9_batchnorm_readvariableop_resource*
_output_shapes
:0*
dtype02I
GELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/ReadVariableOp�
>ELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2@
>ELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/add/y�
<ELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/addAddV2OELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/ReadVariableOp:value:0GELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/add/y:output:0*
T0*
_output_shapes
:02>
<ELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/add�
>ELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/RsqrtRsqrt@ELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/add:z:0*
T0*
_output_shapes
:02@
>ELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/Rsqrt�
KELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/mul/ReadVariableOpReadVariableOpTelu_1_5_size_5_48_iter_2_batch_normalization_9_batchnorm_mul_readvariableop_resource*
_output_shapes
:0*
dtype02M
KELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/mul/ReadVariableOp�
<ELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/mulMulBELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/Rsqrt:y:0SELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes
:02>
<ELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/mul�
>ELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/mul_1Mul6ELU_1.5_size_5_48_iter_2/ELU4/elu_59/SelectV2:output:0@ELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/mul:z:0*
T0*'
_output_shapes
:���������02@
>ELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/mul_1�
IELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/ReadVariableOp_1ReadVariableOpRelu_1_5_size_5_48_iter_2_batch_normalization_9_batchnorm_readvariableop_1_resource*
_output_shapes
:0*
dtype02K
IELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/ReadVariableOp_1�
>ELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/mul_2MulQELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/ReadVariableOp_1:value:0@ELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/mul:z:0*
T0*
_output_shapes
:02@
>ELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/mul_2�
IELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/ReadVariableOp_2ReadVariableOpRelu_1_5_size_5_48_iter_2_batch_normalization_9_batchnorm_readvariableop_2_resource*
_output_shapes
:0*
dtype02K
IELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/ReadVariableOp_2�
<ELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/subSubQELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/ReadVariableOp_2:value:0BELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/mul_2:z:0*
T0*
_output_shapes
:02>
<ELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/sub�
>ELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/add_1AddV2BELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/mul_1:z:0@ELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/sub:z:0*
T0*'
_output_shapes
:���������02@
>ELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/add_1�
5ELU_1.5_size_5_48_iter_2/Output/MatMul/ReadVariableOpReadVariableOp>elu_1_5_size_5_48_iter_2_output_matmul_readvariableop_resource*
_output_shapes

:0	*
dtype027
5ELU_1.5_size_5_48_iter_2/Output/MatMul/ReadVariableOp�
&ELU_1.5_size_5_48_iter_2/Output/MatMulMatMulBELU_1.5_size_5_48_iter_2/batch_normalization_9/batchnorm/add_1:z:0=ELU_1.5_size_5_48_iter_2/Output/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������	2(
&ELU_1.5_size_5_48_iter_2/Output/MatMul�
6ELU_1.5_size_5_48_iter_2/Output/BiasAdd/ReadVariableOpReadVariableOp?elu_1_5_size_5_48_iter_2_output_biasadd_readvariableop_resource*
_output_shapes
:	*
dtype028
6ELU_1.5_size_5_48_iter_2/Output/BiasAdd/ReadVariableOp�
'ELU_1.5_size_5_48_iter_2/Output/BiasAddBiasAdd0ELU_1.5_size_5_48_iter_2/Output/MatMul:product:0>ELU_1.5_size_5_48_iter_2/Output/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������	2)
'ELU_1.5_size_5_48_iter_2/Output/BiasAdd�
*ELU_1.5_size_5_48_iter_2/softmax_9/SoftmaxSoftmax0ELU_1.5_size_5_48_iter_2/Output/BiasAdd:output:0*
T0*'
_output_shapes
:���������	2,
*ELU_1.5_size_5_48_iter_2/softmax_9/Softmax�
IdentityIdentity4ELU_1.5_size_5_48_iter_2/softmax_9/Softmax:softmax:0*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*n
_input_shapes]
[:���������0:::::::::::::::::::T P
'
_output_shapes
:���������0
%
_user_specified_nameInput_input:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
|
'__inference_ELU4_layer_call_fn_13538290

inputs
unknown
	unknown_0
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_ELU4_layer_call_and_return_conditional_losses_135374362
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�/
�
V__inference_ELU_1.5_size_5_48_iter_2_layer_call_and_return_conditional_losses_13537628

inputs
input_13537582
input_13537584
elu0_13537587
elu0_13537589
elu1_13537592
elu1_13537594
elu2_13537597
elu2_13537599
elu3_13537602
elu3_13537604
elu4_13537607
elu4_13537609"
batch_normalization_9_13537612"
batch_normalization_9_13537614"
batch_normalization_9_13537616"
batch_normalization_9_13537618
output_13537621
output_13537623
identity��ELU0/StatefulPartitionedCall�ELU1/StatefulPartitionedCall�ELU2/StatefulPartitionedCall�ELU3/StatefulPartitionedCall�ELU4/StatefulPartitionedCall�Input/StatefulPartitionedCall�Output/StatefulPartitionedCall�-batch_normalization_9/StatefulPartitionedCall�
Input/StatefulPartitionedCallStatefulPartitionedCallinputsinput_13537582input_13537584*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*L
fGRE
C__inference_Input_layer_call_and_return_conditional_losses_135372762
Input/StatefulPartitionedCall�
ELU0/StatefulPartitionedCallStatefulPartitionedCall&Input/StatefulPartitionedCall:output:0elu0_13537587elu0_13537589*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_ELU0_layer_call_and_return_conditional_losses_135373082
ELU0/StatefulPartitionedCall�
ELU1/StatefulPartitionedCallStatefulPartitionedCall%ELU0/StatefulPartitionedCall:output:0elu1_13537592elu1_13537594*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_ELU1_layer_call_and_return_conditional_losses_135373402
ELU1/StatefulPartitionedCall�
ELU2/StatefulPartitionedCallStatefulPartitionedCall%ELU1/StatefulPartitionedCall:output:0elu2_13537597elu2_13537599*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_ELU2_layer_call_and_return_conditional_losses_135373722
ELU2/StatefulPartitionedCall�
ELU3/StatefulPartitionedCallStatefulPartitionedCall%ELU2/StatefulPartitionedCall:output:0elu3_13537602elu3_13537604*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_ELU3_layer_call_and_return_conditional_losses_135374042
ELU3/StatefulPartitionedCall�
ELU4/StatefulPartitionedCallStatefulPartitionedCall%ELU3/StatefulPartitionedCall:output:0elu4_13537607elu4_13537609*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_ELU4_layer_call_and_return_conditional_losses_135374362
ELU4/StatefulPartitionedCall�
-batch_normalization_9/StatefulPartitionedCallStatefulPartitionedCall%ELU4/StatefulPartitionedCall:output:0batch_normalization_9_13537612batch_normalization_9_13537614batch_normalization_9_13537616batch_normalization_9_13537618*
Tin	
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*\
fWRU
S__inference_batch_normalization_9_layer_call_and_return_conditional_losses_135372122/
-batch_normalization_9/StatefulPartitionedCall�
Output/StatefulPartitionedCallStatefulPartitionedCall6batch_normalization_9/StatefulPartitionedCall:output:0output_13537621output_13537623*
Tin
2*
Tout
2*'
_output_shapes
:���������	*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*M
fHRF
D__inference_Output_layer_call_and_return_conditional_losses_135374972 
Output/StatefulPartitionedCall�
softmax_9/PartitionedCallPartitionedCall'Output/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*'
_output_shapes
:���������	* 
_read_only_resource_inputs
 **
config_proto

CPU

GPU 2J 8*P
fKRI
G__inference_softmax_9_layer_call_and_return_conditional_losses_135375182
softmax_9/PartitionedCall�
IdentityIdentity"softmax_9/PartitionedCall:output:0^ELU0/StatefulPartitionedCall^ELU1/StatefulPartitionedCall^ELU2/StatefulPartitionedCall^ELU3/StatefulPartitionedCall^ELU4/StatefulPartitionedCall^Input/StatefulPartitionedCall^Output/StatefulPartitionedCall.^batch_normalization_9/StatefulPartitionedCall*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*n
_input_shapes]
[:���������0::::::::::::::::::2<
ELU0/StatefulPartitionedCallELU0/StatefulPartitionedCall2<
ELU1/StatefulPartitionedCallELU1/StatefulPartitionedCall2<
ELU2/StatefulPartitionedCallELU2/StatefulPartitionedCall2<
ELU3/StatefulPartitionedCallELU3/StatefulPartitionedCall2<
ELU4/StatefulPartitionedCallELU4/StatefulPartitionedCall2>
Input/StatefulPartitionedCallInput/StatefulPartitionedCall2@
Output/StatefulPartitionedCallOutput/StatefulPartitionedCall2^
-batch_normalization_9/StatefulPartitionedCall-batch_normalization_9/StatefulPartitionedCall:O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
&__inference_signature_wrapper_13537844
input_input
unknown
	unknown_0
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
	unknown_7
	unknown_8
	unknown_9

unknown_10

unknown_11

unknown_12

unknown_13

unknown_14

unknown_15

unknown_16
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16*
Tin
2*
Tout
2*'
_output_shapes
:���������	*4
_read_only_resource_inputs
	
**
config_proto

CPU

GPU 2J 8*,
f'R%
#__inference__wrapped_model_135371162
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*n
_input_shapes]
[:���������0::::::::::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:T P
'
_output_shapes
:���������0
%
_user_specified_nameInput_input:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�/
�
V__inference_ELU_1.5_size_5_48_iter_2_layer_call_and_return_conditional_losses_13537576
input_input
input_13537530
input_13537532
elu0_13537535
elu0_13537537
elu1_13537540
elu1_13537542
elu2_13537545
elu2_13537547
elu3_13537550
elu3_13537552
elu4_13537555
elu4_13537557"
batch_normalization_9_13537560"
batch_normalization_9_13537562"
batch_normalization_9_13537564"
batch_normalization_9_13537566
output_13537569
output_13537571
identity��ELU0/StatefulPartitionedCall�ELU1/StatefulPartitionedCall�ELU2/StatefulPartitionedCall�ELU3/StatefulPartitionedCall�ELU4/StatefulPartitionedCall�Input/StatefulPartitionedCall�Output/StatefulPartitionedCall�-batch_normalization_9/StatefulPartitionedCall�
Input/StatefulPartitionedCallStatefulPartitionedCallinput_inputinput_13537530input_13537532*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*L
fGRE
C__inference_Input_layer_call_and_return_conditional_losses_135372762
Input/StatefulPartitionedCall�
ELU0/StatefulPartitionedCallStatefulPartitionedCall&Input/StatefulPartitionedCall:output:0elu0_13537535elu0_13537537*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_ELU0_layer_call_and_return_conditional_losses_135373082
ELU0/StatefulPartitionedCall�
ELU1/StatefulPartitionedCallStatefulPartitionedCall%ELU0/StatefulPartitionedCall:output:0elu1_13537540elu1_13537542*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_ELU1_layer_call_and_return_conditional_losses_135373402
ELU1/StatefulPartitionedCall�
ELU2/StatefulPartitionedCallStatefulPartitionedCall%ELU1/StatefulPartitionedCall:output:0elu2_13537545elu2_13537547*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_ELU2_layer_call_and_return_conditional_losses_135373722
ELU2/StatefulPartitionedCall�
ELU3/StatefulPartitionedCallStatefulPartitionedCall%ELU2/StatefulPartitionedCall:output:0elu3_13537550elu3_13537552*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_ELU3_layer_call_and_return_conditional_losses_135374042
ELU3/StatefulPartitionedCall�
ELU4/StatefulPartitionedCallStatefulPartitionedCall%ELU3/StatefulPartitionedCall:output:0elu4_13537555elu4_13537557*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_ELU4_layer_call_and_return_conditional_losses_135374362
ELU4/StatefulPartitionedCall�
-batch_normalization_9/StatefulPartitionedCallStatefulPartitionedCall%ELU4/StatefulPartitionedCall:output:0batch_normalization_9_13537560batch_normalization_9_13537562batch_normalization_9_13537564batch_normalization_9_13537566*
Tin	
2*
Tout
2*'
_output_shapes
:���������0*&
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*\
fWRU
S__inference_batch_normalization_9_layer_call_and_return_conditional_losses_135372452/
-batch_normalization_9/StatefulPartitionedCall�
Output/StatefulPartitionedCallStatefulPartitionedCall6batch_normalization_9/StatefulPartitionedCall:output:0output_13537569output_13537571*
Tin
2*
Tout
2*'
_output_shapes
:���������	*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*M
fHRF
D__inference_Output_layer_call_and_return_conditional_losses_135374972 
Output/StatefulPartitionedCall�
softmax_9/PartitionedCallPartitionedCall'Output/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*'
_output_shapes
:���������	* 
_read_only_resource_inputs
 **
config_proto

CPU

GPU 2J 8*P
fKRI
G__inference_softmax_9_layer_call_and_return_conditional_losses_135375182
softmax_9/PartitionedCall�
IdentityIdentity"softmax_9/PartitionedCall:output:0^ELU0/StatefulPartitionedCall^ELU1/StatefulPartitionedCall^ELU2/StatefulPartitionedCall^ELU3/StatefulPartitionedCall^ELU4/StatefulPartitionedCall^Input/StatefulPartitionedCall^Output/StatefulPartitionedCall.^batch_normalization_9/StatefulPartitionedCall*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*n
_input_shapes]
[:���������0::::::::::::::::::2<
ELU0/StatefulPartitionedCallELU0/StatefulPartitionedCall2<
ELU1/StatefulPartitionedCallELU1/StatefulPartitionedCall2<
ELU2/StatefulPartitionedCallELU2/StatefulPartitionedCall2<
ELU3/StatefulPartitionedCallELU3/StatefulPartitionedCall2<
ELU4/StatefulPartitionedCallELU4/StatefulPartitionedCall2>
Input/StatefulPartitionedCallInput/StatefulPartitionedCall2@
Output/StatefulPartitionedCallOutput/StatefulPartitionedCall2^
-batch_normalization_9/StatefulPartitionedCall-batch_normalization_9/StatefulPartitionedCall:T P
'
_output_shapes
:���������0
%
_user_specified_nameInput_input:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
B__inference_ELU0_layer_call_and_return_conditional_losses_13537308

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:00*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02	
BiasAddc

elu_55/EluEluBiasAdd:output:0*
T0*'
_output_shapes
:���������02

elu_55/Elui
elu_55/Greater/yConst*
_output_shapes
: *
dtype0*
valueB
 *    2
elu_55/Greater/y�
elu_55/GreaterGreaterBiasAdd:output:0elu_55/Greater/y:output:0*
T0*'
_output_shapes
:���������02
elu_55/Greatera
elu_55/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
elu_55/mul/x�

elu_55/mulMulelu_55/mul/x:output:0elu_55/Elu:activations:0*
T0*'
_output_shapes
:���������02

elu_55/mul�
elu_55/SelectV2SelectV2elu_55/Greater:z:0elu_55/Elu:activations:0elu_55/mul:z:0*
T0*'
_output_shapes
:���������02
elu_55/SelectV2l
IdentityIdentityelu_55/SelectV2:output:0*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0:::O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
�
C__inference_Input_layer_call_and_return_conditional_losses_13538156

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:00*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02	
BiasAddc

elu_54/EluEluBiasAdd:output:0*
T0*'
_output_shapes
:���������02

elu_54/Elui
elu_54/Greater/yConst*
_output_shapes
: *
dtype0*
valueB
 *    2
elu_54/Greater/y�
elu_54/GreaterGreaterBiasAdd:output:0elu_54/Greater/y:output:0*
T0*'
_output_shapes
:���������02
elu_54/Greatera
elu_54/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
elu_54/mul/x�

elu_54/mulMulelu_54/mul/x:output:0elu_54/Elu:activations:0*
T0*'
_output_shapes
:���������02

elu_54/mul�
elu_54/SelectV2SelectV2elu_54/Greater:z:0elu_54/Elu:activations:0elu_54/mul:z:0*
T0*'
_output_shapes
:���������02
elu_54/SelectV2l
IdentityIdentityelu_54/SelectV2:output:0*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0:::O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
�
B__inference_ELU4_layer_call_and_return_conditional_losses_13537436

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:00*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02	
BiasAddc

elu_59/EluEluBiasAdd:output:0*
T0*'
_output_shapes
:���������02

elu_59/Elui
elu_59/Greater/yConst*
_output_shapes
: *
dtype0*
valueB
 *    2
elu_59/Greater/y�
elu_59/GreaterGreaterBiasAdd:output:0elu_59/Greater/y:output:0*
T0*'
_output_shapes
:���������02
elu_59/Greatera
elu_59/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
elu_59/mul/x�

elu_59/mulMulelu_59/mul/x:output:0elu_59/Elu:activations:0*
T0*'
_output_shapes
:���������02

elu_59/mul�
elu_59/SelectV2SelectV2elu_59/Greater:z:0elu_59/Elu:activations:0elu_59/mul:z:0*
T0*'
_output_shapes
:���������02
elu_59/SelectV2l
IdentityIdentityelu_59/SelectV2:output:0*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0:::O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
�
!__inference__traced_save_13538647
file_prefix-
)savev2_input_9_kernel_read_readvariableop+
'savev2_input_9_bias_read_readvariableop,
(savev2_elu0_8_kernel_read_readvariableop*
&savev2_elu0_8_bias_read_readvariableop,
(savev2_elu1_9_kernel_read_readvariableop*
&savev2_elu1_9_bias_read_readvariableop,
(savev2_elu2_9_kernel_read_readvariableop*
&savev2_elu2_9_bias_read_readvariableop,
(savev2_elu3_9_kernel_read_readvariableop*
&savev2_elu3_9_bias_read_readvariableop,
(savev2_elu4_9_kernel_read_readvariableop*
&savev2_elu4_9_bias_read_readvariableop:
6savev2_batch_normalization_9_gamma_read_readvariableop9
5savev2_batch_normalization_9_beta_read_readvariableop@
<savev2_batch_normalization_9_moving_mean_read_readvariableopD
@savev2_batch_normalization_9_moving_variance_read_readvariableop.
*savev2_output_9_kernel_read_readvariableop,
(savev2_output_9_bias_read_readvariableop(
$savev2_adam_iter_read_readvariableop	*
&savev2_adam_beta_1_read_readvariableop*
&savev2_adam_beta_2_read_readvariableop)
%savev2_adam_decay_read_readvariableop1
-savev2_adam_learning_rate_read_readvariableop$
 savev2_total_read_readvariableop$
 savev2_count_read_readvariableop&
"savev2_total_1_read_readvariableop&
"savev2_count_1_read_readvariableop-
)savev2_true_positives_read_readvariableop.
*savev2_false_positives_read_readvariableop4
0savev2_adam_input_9_kernel_m_read_readvariableop2
.savev2_adam_input_9_bias_m_read_readvariableop3
/savev2_adam_elu0_8_kernel_m_read_readvariableop1
-savev2_adam_elu0_8_bias_m_read_readvariableop3
/savev2_adam_elu1_9_kernel_m_read_readvariableop1
-savev2_adam_elu1_9_bias_m_read_readvariableop3
/savev2_adam_elu2_9_kernel_m_read_readvariableop1
-savev2_adam_elu2_9_bias_m_read_readvariableop3
/savev2_adam_elu3_9_kernel_m_read_readvariableop1
-savev2_adam_elu3_9_bias_m_read_readvariableop3
/savev2_adam_elu4_9_kernel_m_read_readvariableop1
-savev2_adam_elu4_9_bias_m_read_readvariableopA
=savev2_adam_batch_normalization_9_gamma_m_read_readvariableop@
<savev2_adam_batch_normalization_9_beta_m_read_readvariableop5
1savev2_adam_output_9_kernel_m_read_readvariableop3
/savev2_adam_output_9_bias_m_read_readvariableop4
0savev2_adam_input_9_kernel_v_read_readvariableop2
.savev2_adam_input_9_bias_v_read_readvariableop3
/savev2_adam_elu0_8_kernel_v_read_readvariableop1
-savev2_adam_elu0_8_bias_v_read_readvariableop3
/savev2_adam_elu1_9_kernel_v_read_readvariableop1
-savev2_adam_elu1_9_bias_v_read_readvariableop3
/savev2_adam_elu2_9_kernel_v_read_readvariableop1
-savev2_adam_elu2_9_bias_v_read_readvariableop3
/savev2_adam_elu3_9_kernel_v_read_readvariableop1
-savev2_adam_elu3_9_bias_v_read_readvariableop3
/savev2_adam_elu4_9_kernel_v_read_readvariableop1
-savev2_adam_elu4_9_bias_v_read_readvariableopA
=savev2_adam_batch_normalization_9_gamma_v_read_readvariableop@
<savev2_adam_batch_normalization_9_beta_v_read_readvariableop5
1savev2_adam_output_9_kernel_v_read_readvariableop3
/savev2_adam_output_9_bias_v_read_readvariableop
savev2_1_const

identity_1��MergeV2Checkpoints�SaveV2�SaveV2_1�
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*2
StaticRegexFullMatchc
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.part2
Const�
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*<
value3B1 B+_temp_5af083f17e7b47669283300fc775d58e/part2	
Const_1�
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: 2
Selectt

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: 2

StringJoinZ

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :2

num_shards
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : 2
ShardedFilename/shard�
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilename�"
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:=*
dtype0*�!
value�!B�!=B6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-6/gamma/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-6/beta/.ATTRIBUTES/VARIABLE_VALUEB;layer_with_weights-6/moving_mean/.ATTRIBUTES/VARIABLE_VALUEB?layer_with_weights-6/moving_variance/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/2/true_positives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/2/false_positives/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-5/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-5/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-6/gamma/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-6/beta/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-7/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-7/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-5/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-5/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-6/gamma/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-6/beta/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-7/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-7/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE2
SaveV2/tensor_names�
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:=*
dtype0*�
value�B�=B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
SaveV2/shape_and_slices�
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:0)savev2_input_9_kernel_read_readvariableop'savev2_input_9_bias_read_readvariableop(savev2_elu0_8_kernel_read_readvariableop&savev2_elu0_8_bias_read_readvariableop(savev2_elu1_9_kernel_read_readvariableop&savev2_elu1_9_bias_read_readvariableop(savev2_elu2_9_kernel_read_readvariableop&savev2_elu2_9_bias_read_readvariableop(savev2_elu3_9_kernel_read_readvariableop&savev2_elu3_9_bias_read_readvariableop(savev2_elu4_9_kernel_read_readvariableop&savev2_elu4_9_bias_read_readvariableop6savev2_batch_normalization_9_gamma_read_readvariableop5savev2_batch_normalization_9_beta_read_readvariableop<savev2_batch_normalization_9_moving_mean_read_readvariableop@savev2_batch_normalization_9_moving_variance_read_readvariableop*savev2_output_9_kernel_read_readvariableop(savev2_output_9_bias_read_readvariableop$savev2_adam_iter_read_readvariableop&savev2_adam_beta_1_read_readvariableop&savev2_adam_beta_2_read_readvariableop%savev2_adam_decay_read_readvariableop-savev2_adam_learning_rate_read_readvariableop savev2_total_read_readvariableop savev2_count_read_readvariableop"savev2_total_1_read_readvariableop"savev2_count_1_read_readvariableop)savev2_true_positives_read_readvariableop*savev2_false_positives_read_readvariableop0savev2_adam_input_9_kernel_m_read_readvariableop.savev2_adam_input_9_bias_m_read_readvariableop/savev2_adam_elu0_8_kernel_m_read_readvariableop-savev2_adam_elu0_8_bias_m_read_readvariableop/savev2_adam_elu1_9_kernel_m_read_readvariableop-savev2_adam_elu1_9_bias_m_read_readvariableop/savev2_adam_elu2_9_kernel_m_read_readvariableop-savev2_adam_elu2_9_bias_m_read_readvariableop/savev2_adam_elu3_9_kernel_m_read_readvariableop-savev2_adam_elu3_9_bias_m_read_readvariableop/savev2_adam_elu4_9_kernel_m_read_readvariableop-savev2_adam_elu4_9_bias_m_read_readvariableop=savev2_adam_batch_normalization_9_gamma_m_read_readvariableop<savev2_adam_batch_normalization_9_beta_m_read_readvariableop1savev2_adam_output_9_kernel_m_read_readvariableop/savev2_adam_output_9_bias_m_read_readvariableop0savev2_adam_input_9_kernel_v_read_readvariableop.savev2_adam_input_9_bias_v_read_readvariableop/savev2_adam_elu0_8_kernel_v_read_readvariableop-savev2_adam_elu0_8_bias_v_read_readvariableop/savev2_adam_elu1_9_kernel_v_read_readvariableop-savev2_adam_elu1_9_bias_v_read_readvariableop/savev2_adam_elu2_9_kernel_v_read_readvariableop-savev2_adam_elu2_9_bias_v_read_readvariableop/savev2_adam_elu3_9_kernel_v_read_readvariableop-savev2_adam_elu3_9_bias_v_read_readvariableop/savev2_adam_elu4_9_kernel_v_read_readvariableop-savev2_adam_elu4_9_bias_v_read_readvariableop=savev2_adam_batch_normalization_9_gamma_v_read_readvariableop<savev2_adam_batch_normalization_9_beta_v_read_readvariableop1savev2_adam_output_9_kernel_v_read_readvariableop/savev2_adam_output_9_bias_v_read_readvariableop"/device:CPU:0*
_output_shapes
 *K
dtypesA
?2=	2
SaveV2�
ShardedFilename_1/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B :2
ShardedFilename_1/shard�
ShardedFilename_1ShardedFilenameStringJoin:output:0 ShardedFilename_1/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilename_1�
SaveV2_1/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*1
value(B&B_CHECKPOINTABLE_OBJECT_GRAPH2
SaveV2_1/tensor_names�
SaveV2_1/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*
valueB
B 2
SaveV2_1/shape_and_slices�
SaveV2_1SaveV2ShardedFilename_1:filename:0SaveV2_1/tensor_names:output:0"SaveV2_1/shape_and_slices:output:0savev2_1_const^SaveV2"/device:CPU:0*
_output_shapes
 *
dtypes
22

SaveV2_1�
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0ShardedFilename_1:filename:0^SaveV2	^SaveV2_1"/device:CPU:0*
N*
T0*
_output_shapes
:2(
&MergeV2Checkpoints/checkpoint_prefixes�
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix	^SaveV2_1"/device:CPU:0*
_output_shapes
 2
MergeV2Checkpointsr
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 2

Identity�

Identity_1IdentityIdentity:output:0^MergeV2Checkpoints^SaveV2	^SaveV2_1*
T0*
_output_shapes
: 2

Identity_1"!

identity_1Identity_1:output:0*�
_input_shapes�
�: :00:0:00:0:00:0:00:0:00:0:00:0:0:0:0:0:0	:	: : : : : : : : : :::00:0:00:0:00:0:00:0:00:0:00:0:0:0:0	:	:00:0:00:0:00:0:00:0:00:0:00:0:0:0:0	:	: 2(
MergeV2CheckpointsMergeV2Checkpoints2
SaveV2SaveV22
SaveV2_1SaveV2_1:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:$ 

_output_shapes

:00: 

_output_shapes
:0:$ 

_output_shapes

:00: 

_output_shapes
:0:$ 

_output_shapes

:00: 

_output_shapes
:0:$ 

_output_shapes

:00: 

_output_shapes
:0:$	 

_output_shapes

:00: 


_output_shapes
:0:$ 

_output_shapes

:00: 

_output_shapes
:0: 

_output_shapes
:0: 

_output_shapes
:0: 

_output_shapes
:0: 

_output_shapes
:0:$ 

_output_shapes

:0	: 

_output_shapes
:	:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: : 

_output_shapes
:: 

_output_shapes
::$ 

_output_shapes

:00: 

_output_shapes
:0:$  

_output_shapes

:00: !

_output_shapes
:0:$" 

_output_shapes

:00: #

_output_shapes
:0:$$ 

_output_shapes

:00: %

_output_shapes
:0:$& 

_output_shapes

:00: '

_output_shapes
:0:$( 

_output_shapes

:00: )

_output_shapes
:0: *

_output_shapes
:0: +

_output_shapes
:0:$, 

_output_shapes

:0	: -

_output_shapes
:	:$. 

_output_shapes

:00: /

_output_shapes
:0:$0 

_output_shapes

:00: 1

_output_shapes
:0:$2 

_output_shapes

:00: 3

_output_shapes
:0:$4 

_output_shapes

:00: 5

_output_shapes
:0:$6 

_output_shapes

:00: 7

_output_shapes
:0:$8 

_output_shapes

:00: 9

_output_shapes
:0: :

_output_shapes
:0: ;

_output_shapes
:0:$< 

_output_shapes

:0	: =

_output_shapes
:	:>

_output_shapes
: 
�
�
V__inference_ELU_1.5_size_5_48_iter_2_layer_call_and_return_conditional_losses_13537959

inputs(
$input_matmul_readvariableop_resource)
%input_biasadd_readvariableop_resource'
#elu0_matmul_readvariableop_resource(
$elu0_biasadd_readvariableop_resource'
#elu1_matmul_readvariableop_resource(
$elu1_biasadd_readvariableop_resource'
#elu2_matmul_readvariableop_resource(
$elu2_biasadd_readvariableop_resource'
#elu3_matmul_readvariableop_resource(
$elu3_biasadd_readvariableop_resource'
#elu4_matmul_readvariableop_resource(
$elu4_biasadd_readvariableop_resource2
.batch_normalization_9_assignmovingavg_135379274
0batch_normalization_9_assignmovingavg_1_13537933?
;batch_normalization_9_batchnorm_mul_readvariableop_resource;
7batch_normalization_9_batchnorm_readvariableop_resource)
%output_matmul_readvariableop_resource*
&output_biasadd_readvariableop_resource
identity��9batch_normalization_9/AssignMovingAvg/AssignSubVariableOp�;batch_normalization_9/AssignMovingAvg_1/AssignSubVariableOp�
Input/MatMul/ReadVariableOpReadVariableOp$input_matmul_readvariableop_resource*
_output_shapes

:00*
dtype02
Input/MatMul/ReadVariableOp�
Input/MatMulMatMulinputs#Input/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
Input/MatMul�
Input/BiasAdd/ReadVariableOpReadVariableOp%input_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
Input/BiasAdd/ReadVariableOp�
Input/BiasAddBiasAddInput/MatMul:product:0$Input/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
Input/BiasAddu
Input/elu_54/EluEluInput/BiasAdd:output:0*
T0*'
_output_shapes
:���������02
Input/elu_54/Eluu
Input/elu_54/Greater/yConst*
_output_shapes
: *
dtype0*
valueB
 *    2
Input/elu_54/Greater/y�
Input/elu_54/GreaterGreaterInput/BiasAdd:output:0Input/elu_54/Greater/y:output:0*
T0*'
_output_shapes
:���������02
Input/elu_54/Greaterm
Input/elu_54/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
Input/elu_54/mul/x�
Input/elu_54/mulMulInput/elu_54/mul/x:output:0Input/elu_54/Elu:activations:0*
T0*'
_output_shapes
:���������02
Input/elu_54/mul�
Input/elu_54/SelectV2SelectV2Input/elu_54/Greater:z:0Input/elu_54/Elu:activations:0Input/elu_54/mul:z:0*
T0*'
_output_shapes
:���������02
Input/elu_54/SelectV2�
ELU0/MatMul/ReadVariableOpReadVariableOp#elu0_matmul_readvariableop_resource*
_output_shapes

:00*
dtype02
ELU0/MatMul/ReadVariableOp�
ELU0/MatMulMatMulInput/elu_54/SelectV2:output:0"ELU0/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
ELU0/MatMul�
ELU0/BiasAdd/ReadVariableOpReadVariableOp$elu0_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
ELU0/BiasAdd/ReadVariableOp�
ELU0/BiasAddBiasAddELU0/MatMul:product:0#ELU0/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
ELU0/BiasAddr
ELU0/elu_55/EluEluELU0/BiasAdd:output:0*
T0*'
_output_shapes
:���������02
ELU0/elu_55/Elus
ELU0/elu_55/Greater/yConst*
_output_shapes
: *
dtype0*
valueB
 *    2
ELU0/elu_55/Greater/y�
ELU0/elu_55/GreaterGreaterELU0/BiasAdd:output:0ELU0/elu_55/Greater/y:output:0*
T0*'
_output_shapes
:���������02
ELU0/elu_55/Greaterk
ELU0/elu_55/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
ELU0/elu_55/mul/x�
ELU0/elu_55/mulMulELU0/elu_55/mul/x:output:0ELU0/elu_55/Elu:activations:0*
T0*'
_output_shapes
:���������02
ELU0/elu_55/mul�
ELU0/elu_55/SelectV2SelectV2ELU0/elu_55/Greater:z:0ELU0/elu_55/Elu:activations:0ELU0/elu_55/mul:z:0*
T0*'
_output_shapes
:���������02
ELU0/elu_55/SelectV2�
ELU1/MatMul/ReadVariableOpReadVariableOp#elu1_matmul_readvariableop_resource*
_output_shapes

:00*
dtype02
ELU1/MatMul/ReadVariableOp�
ELU1/MatMulMatMulELU0/elu_55/SelectV2:output:0"ELU1/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
ELU1/MatMul�
ELU1/BiasAdd/ReadVariableOpReadVariableOp$elu1_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
ELU1/BiasAdd/ReadVariableOp�
ELU1/BiasAddBiasAddELU1/MatMul:product:0#ELU1/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
ELU1/BiasAddr
ELU1/elu_56/EluEluELU1/BiasAdd:output:0*
T0*'
_output_shapes
:���������02
ELU1/elu_56/Elus
ELU1/elu_56/Greater/yConst*
_output_shapes
: *
dtype0*
valueB
 *    2
ELU1/elu_56/Greater/y�
ELU1/elu_56/GreaterGreaterELU1/BiasAdd:output:0ELU1/elu_56/Greater/y:output:0*
T0*'
_output_shapes
:���������02
ELU1/elu_56/Greaterk
ELU1/elu_56/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
ELU1/elu_56/mul/x�
ELU1/elu_56/mulMulELU1/elu_56/mul/x:output:0ELU1/elu_56/Elu:activations:0*
T0*'
_output_shapes
:���������02
ELU1/elu_56/mul�
ELU1/elu_56/SelectV2SelectV2ELU1/elu_56/Greater:z:0ELU1/elu_56/Elu:activations:0ELU1/elu_56/mul:z:0*
T0*'
_output_shapes
:���������02
ELU1/elu_56/SelectV2�
ELU2/MatMul/ReadVariableOpReadVariableOp#elu2_matmul_readvariableop_resource*
_output_shapes

:00*
dtype02
ELU2/MatMul/ReadVariableOp�
ELU2/MatMulMatMulELU1/elu_56/SelectV2:output:0"ELU2/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
ELU2/MatMul�
ELU2/BiasAdd/ReadVariableOpReadVariableOp$elu2_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
ELU2/BiasAdd/ReadVariableOp�
ELU2/BiasAddBiasAddELU2/MatMul:product:0#ELU2/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
ELU2/BiasAddr
ELU2/elu_57/EluEluELU2/BiasAdd:output:0*
T0*'
_output_shapes
:���������02
ELU2/elu_57/Elus
ELU2/elu_57/Greater/yConst*
_output_shapes
: *
dtype0*
valueB
 *    2
ELU2/elu_57/Greater/y�
ELU2/elu_57/GreaterGreaterELU2/BiasAdd:output:0ELU2/elu_57/Greater/y:output:0*
T0*'
_output_shapes
:���������02
ELU2/elu_57/Greaterk
ELU2/elu_57/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
ELU2/elu_57/mul/x�
ELU2/elu_57/mulMulELU2/elu_57/mul/x:output:0ELU2/elu_57/Elu:activations:0*
T0*'
_output_shapes
:���������02
ELU2/elu_57/mul�
ELU2/elu_57/SelectV2SelectV2ELU2/elu_57/Greater:z:0ELU2/elu_57/Elu:activations:0ELU2/elu_57/mul:z:0*
T0*'
_output_shapes
:���������02
ELU2/elu_57/SelectV2�
ELU3/MatMul/ReadVariableOpReadVariableOp#elu3_matmul_readvariableop_resource*
_output_shapes

:00*
dtype02
ELU3/MatMul/ReadVariableOp�
ELU3/MatMulMatMulELU2/elu_57/SelectV2:output:0"ELU3/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
ELU3/MatMul�
ELU3/BiasAdd/ReadVariableOpReadVariableOp$elu3_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
ELU3/BiasAdd/ReadVariableOp�
ELU3/BiasAddBiasAddELU3/MatMul:product:0#ELU3/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
ELU3/BiasAddr
ELU3/elu_58/EluEluELU3/BiasAdd:output:0*
T0*'
_output_shapes
:���������02
ELU3/elu_58/Elus
ELU3/elu_58/Greater/yConst*
_output_shapes
: *
dtype0*
valueB
 *    2
ELU3/elu_58/Greater/y�
ELU3/elu_58/GreaterGreaterELU3/BiasAdd:output:0ELU3/elu_58/Greater/y:output:0*
T0*'
_output_shapes
:���������02
ELU3/elu_58/Greaterk
ELU3/elu_58/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
ELU3/elu_58/mul/x�
ELU3/elu_58/mulMulELU3/elu_58/mul/x:output:0ELU3/elu_58/Elu:activations:0*
T0*'
_output_shapes
:���������02
ELU3/elu_58/mul�
ELU3/elu_58/SelectV2SelectV2ELU3/elu_58/Greater:z:0ELU3/elu_58/Elu:activations:0ELU3/elu_58/mul:z:0*
T0*'
_output_shapes
:���������02
ELU3/elu_58/SelectV2�
ELU4/MatMul/ReadVariableOpReadVariableOp#elu4_matmul_readvariableop_resource*
_output_shapes

:00*
dtype02
ELU4/MatMul/ReadVariableOp�
ELU4/MatMulMatMulELU3/elu_58/SelectV2:output:0"ELU4/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
ELU4/MatMul�
ELU4/BiasAdd/ReadVariableOpReadVariableOp$elu4_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
ELU4/BiasAdd/ReadVariableOp�
ELU4/BiasAddBiasAddELU4/MatMul:product:0#ELU4/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
ELU4/BiasAddr
ELU4/elu_59/EluEluELU4/BiasAdd:output:0*
T0*'
_output_shapes
:���������02
ELU4/elu_59/Elus
ELU4/elu_59/Greater/yConst*
_output_shapes
: *
dtype0*
valueB
 *    2
ELU4/elu_59/Greater/y�
ELU4/elu_59/GreaterGreaterELU4/BiasAdd:output:0ELU4/elu_59/Greater/y:output:0*
T0*'
_output_shapes
:���������02
ELU4/elu_59/Greaterk
ELU4/elu_59/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
ELU4/elu_59/mul/x�
ELU4/elu_59/mulMulELU4/elu_59/mul/x:output:0ELU4/elu_59/Elu:activations:0*
T0*'
_output_shapes
:���������02
ELU4/elu_59/mul�
ELU4/elu_59/SelectV2SelectV2ELU4/elu_59/Greater:z:0ELU4/elu_59/Elu:activations:0ELU4/elu_59/mul:z:0*
T0*'
_output_shapes
:���������02
ELU4/elu_59/SelectV2�
4batch_normalization_9/moments/mean/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 26
4batch_normalization_9/moments/mean/reduction_indices�
"batch_normalization_9/moments/meanMeanELU4/elu_59/SelectV2:output:0=batch_normalization_9/moments/mean/reduction_indices:output:0*
T0*
_output_shapes

:0*
	keep_dims(2$
"batch_normalization_9/moments/mean�
*batch_normalization_9/moments/StopGradientStopGradient+batch_normalization_9/moments/mean:output:0*
T0*
_output_shapes

:02,
*batch_normalization_9/moments/StopGradient�
/batch_normalization_9/moments/SquaredDifferenceSquaredDifferenceELU4/elu_59/SelectV2:output:03batch_normalization_9/moments/StopGradient:output:0*
T0*'
_output_shapes
:���������021
/batch_normalization_9/moments/SquaredDifference�
8batch_normalization_9/moments/variance/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 2:
8batch_normalization_9/moments/variance/reduction_indices�
&batch_normalization_9/moments/varianceMean3batch_normalization_9/moments/SquaredDifference:z:0Abatch_normalization_9/moments/variance/reduction_indices:output:0*
T0*
_output_shapes

:0*
	keep_dims(2(
&batch_normalization_9/moments/variance�
%batch_normalization_9/moments/SqueezeSqueeze+batch_normalization_9/moments/mean:output:0*
T0*
_output_shapes
:0*
squeeze_dims
 2'
%batch_normalization_9/moments/Squeeze�
'batch_normalization_9/moments/Squeeze_1Squeeze/batch_normalization_9/moments/variance:output:0*
T0*
_output_shapes
:0*
squeeze_dims
 2)
'batch_normalization_9/moments/Squeeze_1�
+batch_normalization_9/AssignMovingAvg/decayConst*A
_class7
53loc:@batch_normalization_9/AssignMovingAvg/13537927*
_output_shapes
: *
dtype0*
valueB
 *
�#<2-
+batch_normalization_9/AssignMovingAvg/decay�
4batch_normalization_9/AssignMovingAvg/ReadVariableOpReadVariableOp.batch_normalization_9_assignmovingavg_13537927*
_output_shapes
:0*
dtype026
4batch_normalization_9/AssignMovingAvg/ReadVariableOp�
)batch_normalization_9/AssignMovingAvg/subSub<batch_normalization_9/AssignMovingAvg/ReadVariableOp:value:0.batch_normalization_9/moments/Squeeze:output:0*
T0*A
_class7
53loc:@batch_normalization_9/AssignMovingAvg/13537927*
_output_shapes
:02+
)batch_normalization_9/AssignMovingAvg/sub�
)batch_normalization_9/AssignMovingAvg/mulMul-batch_normalization_9/AssignMovingAvg/sub:z:04batch_normalization_9/AssignMovingAvg/decay:output:0*
T0*A
_class7
53loc:@batch_normalization_9/AssignMovingAvg/13537927*
_output_shapes
:02+
)batch_normalization_9/AssignMovingAvg/mul�
9batch_normalization_9/AssignMovingAvg/AssignSubVariableOpAssignSubVariableOp.batch_normalization_9_assignmovingavg_13537927-batch_normalization_9/AssignMovingAvg/mul:z:05^batch_normalization_9/AssignMovingAvg/ReadVariableOp*A
_class7
53loc:@batch_normalization_9/AssignMovingAvg/13537927*
_output_shapes
 *
dtype02;
9batch_normalization_9/AssignMovingAvg/AssignSubVariableOp�
-batch_normalization_9/AssignMovingAvg_1/decayConst*C
_class9
75loc:@batch_normalization_9/AssignMovingAvg_1/13537933*
_output_shapes
: *
dtype0*
valueB
 *
�#<2/
-batch_normalization_9/AssignMovingAvg_1/decay�
6batch_normalization_9/AssignMovingAvg_1/ReadVariableOpReadVariableOp0batch_normalization_9_assignmovingavg_1_13537933*
_output_shapes
:0*
dtype028
6batch_normalization_9/AssignMovingAvg_1/ReadVariableOp�
+batch_normalization_9/AssignMovingAvg_1/subSub>batch_normalization_9/AssignMovingAvg_1/ReadVariableOp:value:00batch_normalization_9/moments/Squeeze_1:output:0*
T0*C
_class9
75loc:@batch_normalization_9/AssignMovingAvg_1/13537933*
_output_shapes
:02-
+batch_normalization_9/AssignMovingAvg_1/sub�
+batch_normalization_9/AssignMovingAvg_1/mulMul/batch_normalization_9/AssignMovingAvg_1/sub:z:06batch_normalization_9/AssignMovingAvg_1/decay:output:0*
T0*C
_class9
75loc:@batch_normalization_9/AssignMovingAvg_1/13537933*
_output_shapes
:02-
+batch_normalization_9/AssignMovingAvg_1/mul�
;batch_normalization_9/AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOp0batch_normalization_9_assignmovingavg_1_13537933/batch_normalization_9/AssignMovingAvg_1/mul:z:07^batch_normalization_9/AssignMovingAvg_1/ReadVariableOp*C
_class9
75loc:@batch_normalization_9/AssignMovingAvg_1/13537933*
_output_shapes
 *
dtype02=
;batch_normalization_9/AssignMovingAvg_1/AssignSubVariableOp�
%batch_normalization_9/batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2'
%batch_normalization_9/batchnorm/add/y�
#batch_normalization_9/batchnorm/addAddV20batch_normalization_9/moments/Squeeze_1:output:0.batch_normalization_9/batchnorm/add/y:output:0*
T0*
_output_shapes
:02%
#batch_normalization_9/batchnorm/add�
%batch_normalization_9/batchnorm/RsqrtRsqrt'batch_normalization_9/batchnorm/add:z:0*
T0*
_output_shapes
:02'
%batch_normalization_9/batchnorm/Rsqrt�
2batch_normalization_9/batchnorm/mul/ReadVariableOpReadVariableOp;batch_normalization_9_batchnorm_mul_readvariableop_resource*
_output_shapes
:0*
dtype024
2batch_normalization_9/batchnorm/mul/ReadVariableOp�
#batch_normalization_9/batchnorm/mulMul)batch_normalization_9/batchnorm/Rsqrt:y:0:batch_normalization_9/batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes
:02%
#batch_normalization_9/batchnorm/mul�
%batch_normalization_9/batchnorm/mul_1MulELU4/elu_59/SelectV2:output:0'batch_normalization_9/batchnorm/mul:z:0*
T0*'
_output_shapes
:���������02'
%batch_normalization_9/batchnorm/mul_1�
%batch_normalization_9/batchnorm/mul_2Mul.batch_normalization_9/moments/Squeeze:output:0'batch_normalization_9/batchnorm/mul:z:0*
T0*
_output_shapes
:02'
%batch_normalization_9/batchnorm/mul_2�
.batch_normalization_9/batchnorm/ReadVariableOpReadVariableOp7batch_normalization_9_batchnorm_readvariableop_resource*
_output_shapes
:0*
dtype020
.batch_normalization_9/batchnorm/ReadVariableOp�
#batch_normalization_9/batchnorm/subSub6batch_normalization_9/batchnorm/ReadVariableOp:value:0)batch_normalization_9/batchnorm/mul_2:z:0*
T0*
_output_shapes
:02%
#batch_normalization_9/batchnorm/sub�
%batch_normalization_9/batchnorm/add_1AddV2)batch_normalization_9/batchnorm/mul_1:z:0'batch_normalization_9/batchnorm/sub:z:0*
T0*'
_output_shapes
:���������02'
%batch_normalization_9/batchnorm/add_1�
Output/MatMul/ReadVariableOpReadVariableOp%output_matmul_readvariableop_resource*
_output_shapes

:0	*
dtype02
Output/MatMul/ReadVariableOp�
Output/MatMulMatMul)batch_normalization_9/batchnorm/add_1:z:0$Output/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������	2
Output/MatMul�
Output/BiasAdd/ReadVariableOpReadVariableOp&output_biasadd_readvariableop_resource*
_output_shapes
:	*
dtype02
Output/BiasAdd/ReadVariableOp�
Output/BiasAddBiasAddOutput/MatMul:product:0%Output/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������	2
Output/BiasAdd|
softmax_9/SoftmaxSoftmaxOutput/BiasAdd:output:0*
T0*'
_output_shapes
:���������	2
softmax_9/Softmax�
IdentityIdentitysoftmax_9/Softmax:softmax:0:^batch_normalization_9/AssignMovingAvg/AssignSubVariableOp<^batch_normalization_9/AssignMovingAvg_1/AssignSubVariableOp*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*n
_input_shapes]
[:���������0::::::::::::::::::2v
9batch_normalization_9/AssignMovingAvg/AssignSubVariableOp9batch_normalization_9/AssignMovingAvg/AssignSubVariableOp2z
;batch_normalization_9/AssignMovingAvg_1/AssignSubVariableOp;batch_normalization_9/AssignMovingAvg_1/AssignSubVariableOp:O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
~
)__inference_Output_layer_call_fn_13538427

inputs
unknown
	unknown_0
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*'
_output_shapes
:���������	*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*M
fHRF
D__inference_Output_layer_call_and_return_conditional_losses_135374972
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
�
B__inference_ELU3_layer_call_and_return_conditional_losses_13538256

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:00*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02	
BiasAddc

elu_58/EluEluBiasAdd:output:0*
T0*'
_output_shapes
:���������02

elu_58/Elui
elu_58/Greater/yConst*
_output_shapes
: *
dtype0*
valueB
 *    2
elu_58/Greater/y�
elu_58/GreaterGreaterBiasAdd:output:0elu_58/Greater/y:output:0*
T0*'
_output_shapes
:���������02
elu_58/Greatera
elu_58/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *  �?2
elu_58/mul/x�

elu_58/mulMulelu_58/mul/x:output:0elu_58/Elu:activations:0*
T0*'
_output_shapes
:���������02

elu_58/mul�
elu_58/SelectV2SelectV2elu_58/Greater:z:0elu_58/Elu:activations:0elu_58/mul:z:0*
T0*'
_output_shapes
:���������02
elu_58/SelectV2l
IdentityIdentityelu_58/SelectV2:output:0*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0:::O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: "�L
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*�
serving_default�
C
Input_input4
serving_default_Input_input:0���������0=
	softmax_90
StatefulPartitionedCall:0���������	tensorflow/serving/predict:��
�R
layer_with_weights-0
layer-0
layer_with_weights-1
layer-1
layer_with_weights-2
layer-2
layer_with_weights-3
layer-3
layer_with_weights-4
layer-4
layer_with_weights-5
layer-5
layer_with_weights-6
layer-6
layer_with_weights-7
layer-7
	layer-8

	optimizer
trainable_variables
regularization_losses
	variables
	keras_api

signatures
�_default_save_signature
�__call__
+�&call_and_return_all_conditional_losses"�N
_tf_keras_sequential�M{"class_name": "Sequential", "name": "ELU_1.5_size_5_48_iter_2", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "config": {"name": "ELU_1.5_size_5_48_iter_2", "layers": [{"class_name": "Dense", "config": {"name": "Input", "trainable": true, "batch_input_shape": {"class_name": "__tuple__", "items": [null, 48]}, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_54", "trainable": true, "dtype": "float32", "alpha": 1.5}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Dense", "config": {"name": "ELU0", "trainable": true, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_55", "trainable": true, "dtype": "float32", "alpha": 1.5}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Dense", "config": {"name": "ELU1", "trainable": true, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_56", "trainable": true, "dtype": "float32", "alpha": 1.5}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Dense", "config": {"name": "ELU2", "trainable": true, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_57", "trainable": true, "dtype": "float32", "alpha": 1.5}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Dense", "config": {"name": "ELU3", "trainable": true, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_58", "trainable": true, "dtype": "float32", "alpha": 1.5}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Dense", "config": {"name": "ELU4", "trainable": true, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_59", "trainable": true, "dtype": "float32", "alpha": 1.5}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "BatchNormalization", "config": {"name": "batch_normalization_9", "trainable": true, "dtype": "float32", "axis": [1], "momentum": 0.99, "epsilon": 0.001, "center": true, "scale": true, "beta_initializer": {"class_name": "Zeros", "config": {}}, "gamma_initializer": {"class_name": "Ones", "config": {}}, "moving_mean_initializer": {"class_name": "Zeros", "config": {}}, "moving_variance_initializer": {"class_name": "Ones", "config": {}}, "beta_regularizer": null, "gamma_regularizer": null, "beta_constraint": null, "gamma_constraint": null}}, {"class_name": "Dense", "config": {"name": "Output", "trainable": true, "dtype": "float32", "units": 9, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Softmax", "config": {"name": "softmax_9", "trainable": true, "dtype": "float32", "axis": -1}}], "build_input_shape": {"class_name": "TensorShape", "items": [null, 48]}}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 48}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 48]}, "is_graph_network": true, "keras_version": "2.3.0-tf", "backend": "tensorflow", "model_config": {"class_name": "Sequential", "config": {"name": "ELU_1.5_size_5_48_iter_2", "layers": [{"class_name": "Dense", "config": {"name": "Input", "trainable": true, "batch_input_shape": {"class_name": "__tuple__", "items": [null, 48]}, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_54", "trainable": true, "dtype": "float32", "alpha": 1.5}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Dense", "config": {"name": "ELU0", "trainable": true, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_55", "trainable": true, "dtype": "float32", "alpha": 1.5}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Dense", "config": {"name": "ELU1", "trainable": true, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_56", "trainable": true, "dtype": "float32", "alpha": 1.5}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Dense", "config": {"name": "ELU2", "trainable": true, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_57", "trainable": true, "dtype": "float32", "alpha": 1.5}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Dense", "config": {"name": "ELU3", "trainable": true, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_58", "trainable": true, "dtype": "float32", "alpha": 1.5}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Dense", "config": {"name": "ELU4", "trainable": true, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_59", "trainable": true, "dtype": "float32", "alpha": 1.5}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "BatchNormalization", "config": {"name": "batch_normalization_9", "trainable": true, "dtype": "float32", "axis": [1], "momentum": 0.99, "epsilon": 0.001, "center": true, "scale": true, "beta_initializer": {"class_name": "Zeros", "config": {}}, "gamma_initializer": {"class_name": "Ones", "config": {}}, "moving_mean_initializer": {"class_name": "Zeros", "config": {}}, "moving_variance_initializer": {"class_name": "Ones", "config": {}}, "beta_regularizer": null, "gamma_regularizer": null, "beta_constraint": null, "gamma_constraint": null}}, {"class_name": "Dense", "config": {"name": "Output", "trainable": true, "dtype": "float32", "units": 9, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Softmax", "config": {"name": "softmax_9", "trainable": true, "dtype": "float32", "axis": -1}}], "build_input_shape": {"class_name": "TensorShape", "items": [null, 48]}}}, "training_config": {"loss": {"class_name": "CategoricalCrossentropy", "config": {"reduction": "auto", "name": "categorical_crossentropy", "from_logits": false, "label_smoothing": 0}}, "metrics": null, "weighted_metrics": ["accuracy", {"class_name": "Precision", "config": {"name": "precision", "dtype": "float32", "thresholds": null, "top_k": null, "class_id": null}}], "loss_weights": null, "sample_weight_mode": null, "optimizer_config": {"class_name": "Adam", "config": {"name": "Adam", "learning_rate": 0.0010000000474974513, "decay": 0.0, "beta_1": 0.8999999761581421, "beta_2": 0.9990000128746033, "epsilon": 1e-07, "amsgrad": false}}}}
�	

activation

kernel
bias
trainable_variables
regularization_losses
	variables
	keras_api
�__call__
+�&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "Dense", "name": "Input", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": {"class_name": "__tuple__", "items": [null, 48]}, "stateful": false, "config": {"name": "Input", "trainable": true, "batch_input_shape": {"class_name": "__tuple__", "items": [null, 48]}, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_54", "trainable": true, "dtype": "float32", "alpha": 1.5}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 48}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 48]}}
�

activation

kernel
bias
trainable_variables
regularization_losses
	variables
	keras_api
�__call__
+�&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "Dense", "name": "ELU0", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "ELU0", "trainable": true, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_55", "trainable": true, "dtype": "float32", "alpha": 1.5}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 48}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 48]}}
�

activation

kernel
 bias
!trainable_variables
"regularization_losses
#	variables
$	keras_api
�__call__
+�&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "Dense", "name": "ELU1", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "ELU1", "trainable": true, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_56", "trainable": true, "dtype": "float32", "alpha": 1.5}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 48}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 48]}}
�
%
activation

&kernel
'bias
(trainable_variables
)regularization_losses
*	variables
+	keras_api
�__call__
+�&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "Dense", "name": "ELU2", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "ELU2", "trainable": true, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_57", "trainable": true, "dtype": "float32", "alpha": 1.5}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 48}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 48]}}
�
,
activation

-kernel
.bias
/trainable_variables
0regularization_losses
1	variables
2	keras_api
�__call__
+�&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "Dense", "name": "ELU3", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "ELU3", "trainable": true, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_58", "trainable": true, "dtype": "float32", "alpha": 1.5}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 48}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 48]}}
�
3
activation

4kernel
5bias
6trainable_variables
7regularization_losses
8	variables
9	keras_api
�__call__
+�&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "Dense", "name": "ELU4", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "ELU4", "trainable": true, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_59", "trainable": true, "dtype": "float32", "alpha": 1.5}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 48}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 48]}}
�	
:axis
	;gamma
<beta
=moving_mean
>moving_variance
?trainable_variables
@regularization_losses
A	variables
B	keras_api
�__call__
+�&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "BatchNormalization", "name": "batch_normalization_9", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "batch_normalization_9", "trainable": true, "dtype": "float32", "axis": [1], "momentum": 0.99, "epsilon": 0.001, "center": true, "scale": true, "beta_initializer": {"class_name": "Zeros", "config": {}}, "gamma_initializer": {"class_name": "Ones", "config": {}}, "moving_mean_initializer": {"class_name": "Zeros", "config": {}}, "moving_variance_initializer": {"class_name": "Ones", "config": {}}, "beta_regularizer": null, "gamma_regularizer": null, "beta_constraint": null, "gamma_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": 2, "max_ndim": null, "min_ndim": null, "axes": {"1": 48}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 48]}}
�

Ckernel
Dbias
Etrainable_variables
Fregularization_losses
G	variables
H	keras_api
�__call__
+�&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "Dense", "name": "Output", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "Output", "trainable": true, "dtype": "float32", "units": 9, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 48}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 48]}}
�
Itrainable_variables
Jregularization_losses
K	variables
L	keras_api
�__call__
+�&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "Softmax", "name": "softmax_9", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "softmax_9", "trainable": true, "dtype": "float32", "axis": -1}}
�
Miter

Nbeta_1

Obeta_2
	Pdecay
Qlearning_ratem�m�m�m�m� m�&m�'m�-m�.m�4m�5m�;m�<m�Cm�Dm�v�v�v�v�v� v�&v�'v�-v�.v�4v�5v�;v�<v�Cv�Dv�"
	optimizer
�
0
1
2
3
4
 5
&6
'7
-8
.9
410
511
;12
<13
C14
D15"
trackable_list_wrapper
 "
trackable_list_wrapper
�
0
1
2
3
4
 5
&6
'7
-8
.9
410
511
;12
<13
=14
>15
C16
D17"
trackable_list_wrapper
�
trainable_variables
Rmetrics
Snon_trainable_variables
Tlayer_metrics
Ulayer_regularization_losses
regularization_losses

Vlayers
	variables
�__call__
�_default_save_signature
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
-
�serving_default"
signature_map
�
Wtrainable_variables
Xregularization_losses
Y	variables
Z	keras_api
�__call__
+�&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "ELU", "name": "elu_54", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "elu_54", "trainable": true, "dtype": "float32", "alpha": 1.5}}
 :002Input_9/kernel
:02Input_9/bias
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
�
trainable_variables
[metrics
\non_trainable_variables
]layer_metrics
^layer_regularization_losses
regularization_losses

_layers
	variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�
`trainable_variables
aregularization_losses
b	variables
c	keras_api
�__call__
+�&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "ELU", "name": "elu_55", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "elu_55", "trainable": true, "dtype": "float32", "alpha": 1.5}}
:002ELU0_8/kernel
:02ELU0_8/bias
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
�
trainable_variables
dmetrics
enon_trainable_variables
flayer_metrics
glayer_regularization_losses
regularization_losses

hlayers
	variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�
itrainable_variables
jregularization_losses
k	variables
l	keras_api
�__call__
+�&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "ELU", "name": "elu_56", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "elu_56", "trainable": true, "dtype": "float32", "alpha": 1.5}}
:002ELU1_9/kernel
:02ELU1_9/bias
.
0
 1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
0
 1"
trackable_list_wrapper
�
!trainable_variables
mmetrics
nnon_trainable_variables
olayer_metrics
player_regularization_losses
"regularization_losses

qlayers
#	variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�
rtrainable_variables
sregularization_losses
t	variables
u	keras_api
�__call__
+�&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "ELU", "name": "elu_57", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "elu_57", "trainable": true, "dtype": "float32", "alpha": 1.5}}
:002ELU2_9/kernel
:02ELU2_9/bias
.
&0
'1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
&0
'1"
trackable_list_wrapper
�
(trainable_variables
vmetrics
wnon_trainable_variables
xlayer_metrics
ylayer_regularization_losses
)regularization_losses

zlayers
*	variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�
{trainable_variables
|regularization_losses
}	variables
~	keras_api
�__call__
+�&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "ELU", "name": "elu_58", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "elu_58", "trainable": true, "dtype": "float32", "alpha": 1.5}}
:002ELU3_9/kernel
:02ELU3_9/bias
.
-0
.1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
-0
.1"
trackable_list_wrapper
�
/trainable_variables
metrics
�non_trainable_variables
�layer_metrics
 �layer_regularization_losses
0regularization_losses
�layers
1	variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�
�trainable_variables
�regularization_losses
�	variables
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "ELU", "name": "elu_59", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "elu_59", "trainable": true, "dtype": "float32", "alpha": 1.5}}
:002ELU4_9/kernel
:02ELU4_9/bias
.
40
51"
trackable_list_wrapper
 "
trackable_list_wrapper
.
40
51"
trackable_list_wrapper
�
6trainable_variables
�metrics
�non_trainable_variables
�layer_metrics
 �layer_regularization_losses
7regularization_losses
�layers
8	variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
):'02batch_normalization_9/gamma
(:&02batch_normalization_9/beta
1:/0 (2!batch_normalization_9/moving_mean
5:30 (2%batch_normalization_9/moving_variance
.
;0
<1"
trackable_list_wrapper
 "
trackable_list_wrapper
<
;0
<1
=2
>3"
trackable_list_wrapper
�
?trainable_variables
�metrics
�non_trainable_variables
�layer_metrics
 �layer_regularization_losses
@regularization_losses
�layers
A	variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
!:0	2Output_9/kernel
:	2Output_9/bias
.
C0
D1"
trackable_list_wrapper
 "
trackable_list_wrapper
.
C0
D1"
trackable_list_wrapper
�
Etrainable_variables
�metrics
�non_trainable_variables
�layer_metrics
 �layer_regularization_losses
Fregularization_losses
�layers
G	variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
Itrainable_variables
�metrics
�non_trainable_variables
�layer_metrics
 �layer_regularization_losses
Jregularization_losses
�layers
K	variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
:	 (2	Adam/iter
: (2Adam/beta_1
: (2Adam/beta_2
: (2
Adam/decay
: (2Adam/learning_rate
8
�0
�1
�2"
trackable_list_wrapper
.
=0
>1"
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
_
0
1
2
3
4
5
6
7
	8"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
Wtrainable_variables
�metrics
�non_trainable_variables
�layer_metrics
 �layer_regularization_losses
Xregularization_losses
�layers
Y	variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
'
0"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
`trainable_variables
�metrics
�non_trainable_variables
�layer_metrics
 �layer_regularization_losses
aregularization_losses
�layers
b	variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
'
0"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
itrainable_variables
�metrics
�non_trainable_variables
�layer_metrics
 �layer_regularization_losses
jregularization_losses
�layers
k	variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
'
0"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
rtrainable_variables
�metrics
�non_trainable_variables
�layer_metrics
 �layer_regularization_losses
sregularization_losses
�layers
t	variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
'
%0"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
{trainable_variables
�metrics
�non_trainable_variables
�layer_metrics
 �layer_regularization_losses
|regularization_losses
�layers
}	variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
'
,0"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�trainable_variables
�metrics
�non_trainable_variables
�layer_metrics
 �layer_regularization_losses
�regularization_losses
�layers
�	variables
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
'
30"
trackable_list_wrapper
 "
trackable_list_wrapper
.
=0
>1"
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�

�total

�count
�	variables
�	keras_api"�
_tf_keras_metricj{"class_name": "Mean", "name": "loss", "dtype": "float32", "config": {"name": "loss", "dtype": "float32"}}
�

�total

�count
�
_fn_kwargs
�	variables
�	keras_api"�
_tf_keras_metric�{"class_name": "MeanMetricWrapper", "name": "accuracy", "dtype": "float32", "config": {"name": "accuracy", "dtype": "float32", "fn": "categorical_accuracy"}}
�
�
thresholds
�true_positives
�false_positives
�	variables
�	keras_api"�
_tf_keras_metric�{"class_name": "Precision", "name": "precision", "dtype": "float32", "config": {"name": "precision", "dtype": "float32", "thresholds": null, "top_k": null, "class_id": null}}
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
:  (2total
:  (2count
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
:  (2total
:  (2count
 "
trackable_dict_wrapper
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
 "
trackable_list_wrapper
: (2true_positives
: (2false_positives
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
%:#002Adam/Input_9/kernel/m
:02Adam/Input_9/bias/m
$:"002Adam/ELU0_8/kernel/m
:02Adam/ELU0_8/bias/m
$:"002Adam/ELU1_9/kernel/m
:02Adam/ELU1_9/bias/m
$:"002Adam/ELU2_9/kernel/m
:02Adam/ELU2_9/bias/m
$:"002Adam/ELU3_9/kernel/m
:02Adam/ELU3_9/bias/m
$:"002Adam/ELU4_9/kernel/m
:02Adam/ELU4_9/bias/m
.:,02"Adam/batch_normalization_9/gamma/m
-:+02!Adam/batch_normalization_9/beta/m
&:$0	2Adam/Output_9/kernel/m
 :	2Adam/Output_9/bias/m
%:#002Adam/Input_9/kernel/v
:02Adam/Input_9/bias/v
$:"002Adam/ELU0_8/kernel/v
:02Adam/ELU0_8/bias/v
$:"002Adam/ELU1_9/kernel/v
:02Adam/ELU1_9/bias/v
$:"002Adam/ELU2_9/kernel/v
:02Adam/ELU2_9/bias/v
$:"002Adam/ELU3_9/kernel/v
:02Adam/ELU3_9/bias/v
$:"002Adam/ELU4_9/kernel/v
:02Adam/ELU4_9/bias/v
.:,02"Adam/batch_normalization_9/gamma/v
-:+02!Adam/batch_normalization_9/beta/v
&:$0	2Adam/Output_9/kernel/v
 :	2Adam/Output_9/bias/v
�2�
#__inference__wrapped_model_13537116�
���
FullArgSpec
args� 
varargsjargs
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� **�'
%�"
Input_input���������0
�2�
;__inference_ELU_1.5_size_5_48_iter_2_layer_call_fn_13537757
;__inference_ELU_1.5_size_5_48_iter_2_layer_call_fn_13538140
;__inference_ELU_1.5_size_5_48_iter_2_layer_call_fn_13537667
;__inference_ELU_1.5_size_5_48_iter_2_layer_call_fn_13538099�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
V__inference_ELU_1.5_size_5_48_iter_2_layer_call_and_return_conditional_losses_13537959
V__inference_ELU_1.5_size_5_48_iter_2_layer_call_and_return_conditional_losses_13538058
V__inference_ELU_1.5_size_5_48_iter_2_layer_call_and_return_conditional_losses_13537527
V__inference_ELU_1.5_size_5_48_iter_2_layer_call_and_return_conditional_losses_13537576�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
(__inference_Input_layer_call_fn_13538165�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
C__inference_Input_layer_call_and_return_conditional_losses_13538156�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
'__inference_ELU0_layer_call_fn_13538190�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
B__inference_ELU0_layer_call_and_return_conditional_losses_13538181�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
'__inference_ELU1_layer_call_fn_13538215�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
B__inference_ELU1_layer_call_and_return_conditional_losses_13538206�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
'__inference_ELU2_layer_call_fn_13538240�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
B__inference_ELU2_layer_call_and_return_conditional_losses_13538231�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
'__inference_ELU3_layer_call_fn_13538265�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
B__inference_ELU3_layer_call_and_return_conditional_losses_13538256�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
'__inference_ELU4_layer_call_fn_13538290�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
B__inference_ELU4_layer_call_and_return_conditional_losses_13538281�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
8__inference_batch_normalization_9_layer_call_fn_13538408
8__inference_batch_normalization_9_layer_call_fn_13538395�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
S__inference_batch_normalization_9_layer_call_and_return_conditional_losses_13538362
S__inference_batch_normalization_9_layer_call_and_return_conditional_losses_13538382�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
)__inference_Output_layer_call_fn_13538427�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
D__inference_Output_layer_call_and_return_conditional_losses_13538418�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
,__inference_softmax_9_layer_call_fn_13538437�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
G__inference_softmax_9_layer_call_and_return_conditional_losses_13538432�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
9B7
&__inference_signature_wrapper_13537844Input_input
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 �
B__inference_ELU0_layer_call_and_return_conditional_losses_13538181\/�,
%�"
 �
inputs���������0
� "%�"
�
0���������0
� z
'__inference_ELU0_layer_call_fn_13538190O/�,
%�"
 �
inputs���������0
� "����������0�
B__inference_ELU1_layer_call_and_return_conditional_losses_13538206\ /�,
%�"
 �
inputs���������0
� "%�"
�
0���������0
� z
'__inference_ELU1_layer_call_fn_13538215O /�,
%�"
 �
inputs���������0
� "����������0�
B__inference_ELU2_layer_call_and_return_conditional_losses_13538231\&'/�,
%�"
 �
inputs���������0
� "%�"
�
0���������0
� z
'__inference_ELU2_layer_call_fn_13538240O&'/�,
%�"
 �
inputs���������0
� "����������0�
B__inference_ELU3_layer_call_and_return_conditional_losses_13538256\-./�,
%�"
 �
inputs���������0
� "%�"
�
0���������0
� z
'__inference_ELU3_layer_call_fn_13538265O-./�,
%�"
 �
inputs���������0
� "����������0�
B__inference_ELU4_layer_call_and_return_conditional_losses_13538281\45/�,
%�"
 �
inputs���������0
� "%�"
�
0���������0
� z
'__inference_ELU4_layer_call_fn_13538290O45/�,
%�"
 �
inputs���������0
� "����������0�
V__inference_ELU_1.5_size_5_48_iter_2_layer_call_and_return_conditional_losses_13537527y &'-.45=>;<CD<�9
2�/
%�"
Input_input���������0
p

 
� "%�"
�
0���������	
� �
V__inference_ELU_1.5_size_5_48_iter_2_layer_call_and_return_conditional_losses_13537576y &'-.45>;=<CD<�9
2�/
%�"
Input_input���������0
p 

 
� "%�"
�
0���������	
� �
V__inference_ELU_1.5_size_5_48_iter_2_layer_call_and_return_conditional_losses_13537959t &'-.45=>;<CD7�4
-�*
 �
inputs���������0
p

 
� "%�"
�
0���������	
� �
V__inference_ELU_1.5_size_5_48_iter_2_layer_call_and_return_conditional_losses_13538058t &'-.45>;=<CD7�4
-�*
 �
inputs���������0
p 

 
� "%�"
�
0���������	
� �
;__inference_ELU_1.5_size_5_48_iter_2_layer_call_fn_13537667l &'-.45=>;<CD<�9
2�/
%�"
Input_input���������0
p

 
� "����������	�
;__inference_ELU_1.5_size_5_48_iter_2_layer_call_fn_13537757l &'-.45>;=<CD<�9
2�/
%�"
Input_input���������0
p 

 
� "����������	�
;__inference_ELU_1.5_size_5_48_iter_2_layer_call_fn_13538099g &'-.45=>;<CD7�4
-�*
 �
inputs���������0
p

 
� "����������	�
;__inference_ELU_1.5_size_5_48_iter_2_layer_call_fn_13538140g &'-.45>;=<CD7�4
-�*
 �
inputs���������0
p 

 
� "����������	�
C__inference_Input_layer_call_and_return_conditional_losses_13538156\/�,
%�"
 �
inputs���������0
� "%�"
�
0���������0
� {
(__inference_Input_layer_call_fn_13538165O/�,
%�"
 �
inputs���������0
� "����������0�
D__inference_Output_layer_call_and_return_conditional_losses_13538418\CD/�,
%�"
 �
inputs���������0
� "%�"
�
0���������	
� |
)__inference_Output_layer_call_fn_13538427OCD/�,
%�"
 �
inputs���������0
� "����������	�
#__inference__wrapped_model_13537116� &'-.45>;=<CD4�1
*�'
%�"
Input_input���������0
� "5�2
0
	softmax_9#� 
	softmax_9���������	�
S__inference_batch_normalization_9_layer_call_and_return_conditional_losses_13538362b=>;<3�0
)�&
 �
inputs���������0
p
� "%�"
�
0���������0
� �
S__inference_batch_normalization_9_layer_call_and_return_conditional_losses_13538382b>;=<3�0
)�&
 �
inputs���������0
p 
� "%�"
�
0���������0
� �
8__inference_batch_normalization_9_layer_call_fn_13538395U=>;<3�0
)�&
 �
inputs���������0
p
� "����������0�
8__inference_batch_normalization_9_layer_call_fn_13538408U>;=<3�0
)�&
 �
inputs���������0
p 
� "����������0�
&__inference_signature_wrapper_13537844� &'-.45>;=<CDC�@
� 
9�6
4
Input_input%�"
Input_input���������0"5�2
0
	softmax_9#� 
	softmax_9���������	�
G__inference_softmax_9_layer_call_and_return_conditional_losses_13538432X/�,
%�"
 �
inputs���������	
� "%�"
�
0���������	
� {
,__inference_softmax_9_layer_call_fn_13538437K/�,
%�"
 �
inputs���������	
� "����������	