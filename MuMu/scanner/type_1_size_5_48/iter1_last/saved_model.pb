��
��
8
Const
output"dtype"
valuetensor"
dtypetype

NoOp
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
@
ReadVariableOp
resource
value"dtype"
dtypetype�
�
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring �
q
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape�"serve*2.2.02unknown8��
x
Input_7/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*
shared_nameInput_7/kernel
q
"Input_7/kernel/Read/ReadVariableOpReadVariableOpInput_7/kernel*
_output_shapes

:00*
dtype0
p
Input_7/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*
shared_nameInput_7/bias
i
 Input_7/bias/Read/ReadVariableOpReadVariableOpInput_7/bias*
_output_shapes
:0*
dtype0
x
type0_7/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*
shared_nametype0_7/kernel
q
"type0_7/kernel/Read/ReadVariableOpReadVariableOptype0_7/kernel*
_output_shapes

:00*
dtype0
p
type0_7/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*
shared_nametype0_7/bias
i
 type0_7/bias/Read/ReadVariableOpReadVariableOptype0_7/bias*
_output_shapes
:0*
dtype0
x
type1_7/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*
shared_nametype1_7/kernel
q
"type1_7/kernel/Read/ReadVariableOpReadVariableOptype1_7/kernel*
_output_shapes

:00*
dtype0
p
type1_7/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*
shared_nametype1_7/bias
i
 type1_7/bias/Read/ReadVariableOpReadVariableOptype1_7/bias*
_output_shapes
:0*
dtype0
x
type2_7/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*
shared_nametype2_7/kernel
q
"type2_7/kernel/Read/ReadVariableOpReadVariableOptype2_7/kernel*
_output_shapes

:00*
dtype0
p
type2_7/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*
shared_nametype2_7/bias
i
 type2_7/bias/Read/ReadVariableOpReadVariableOptype2_7/bias*
_output_shapes
:0*
dtype0
x
type3_7/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*
shared_nametype3_7/kernel
q
"type3_7/kernel/Read/ReadVariableOpReadVariableOptype3_7/kernel*
_output_shapes

:00*
dtype0
p
type3_7/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*
shared_nametype3_7/bias
i
 type3_7/bias/Read/ReadVariableOpReadVariableOptype3_7/bias*
_output_shapes
:0*
dtype0
x
type4_7/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*
shared_nametype4_7/kernel
q
"type4_7/kernel/Read/ReadVariableOpReadVariableOptype4_7/kernel*
_output_shapes

:00*
dtype0
p
type4_7/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*
shared_nametype4_7/bias
i
 type4_7/bias/Read/ReadVariableOpReadVariableOptype4_7/bias*
_output_shapes
:0*
dtype0
�
batch_normalization_9/gammaVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*,
shared_namebatch_normalization_9/gamma
�
/batch_normalization_9/gamma/Read/ReadVariableOpReadVariableOpbatch_normalization_9/gamma*
_output_shapes
:0*
dtype0
�
batch_normalization_9/betaVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*+
shared_namebatch_normalization_9/beta
�
.batch_normalization_9/beta/Read/ReadVariableOpReadVariableOpbatch_normalization_9/beta*
_output_shapes
:0*
dtype0
�
!batch_normalization_9/moving_meanVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*2
shared_name#!batch_normalization_9/moving_mean
�
5batch_normalization_9/moving_mean/Read/ReadVariableOpReadVariableOp!batch_normalization_9/moving_mean*
_output_shapes
:0*
dtype0
�
%batch_normalization_9/moving_varianceVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*6
shared_name'%batch_normalization_9/moving_variance
�
9batch_normalization_9/moving_variance/Read/ReadVariableOpReadVariableOp%batch_normalization_9/moving_variance*
_output_shapes
:0*
dtype0
z
Output_9/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:0	* 
shared_nameOutput_9/kernel
s
#Output_9/kernel/Read/ReadVariableOpReadVariableOpOutput_9/kernel*
_output_shapes

:0	*
dtype0
r
Output_9/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:	*
shared_nameOutput_9/bias
k
!Output_9/bias/Read/ReadVariableOpReadVariableOpOutput_9/bias*
_output_shapes
:	*
dtype0
f
	Adam/iterVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_name	Adam/iter
_
Adam/iter/Read/ReadVariableOpReadVariableOp	Adam/iter*
_output_shapes
: *
dtype0	
j
Adam/beta_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_1
c
Adam/beta_1/Read/ReadVariableOpReadVariableOpAdam/beta_1*
_output_shapes
: *
dtype0
j
Adam/beta_2VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_2
c
Adam/beta_2/Read/ReadVariableOpReadVariableOpAdam/beta_2*
_output_shapes
: *
dtype0
h

Adam/decayVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name
Adam/decay
a
Adam/decay/Read/ReadVariableOpReadVariableOp
Adam/decay*
_output_shapes
: *
dtype0
x
Adam/learning_rateVarHandleOp*
_output_shapes
: *
dtype0*
shape: *#
shared_nameAdam/learning_rate
q
&Adam/learning_rate/Read/ReadVariableOpReadVariableOpAdam/learning_rate*
_output_shapes
: *
dtype0
^
totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nametotal
W
total/Read/ReadVariableOpReadVariableOptotal*
_output_shapes
: *
dtype0
^
countVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namecount
W
count/Read/ReadVariableOpReadVariableOpcount*
_output_shapes
: *
dtype0
b
total_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	total_1
[
total_1/Read/ReadVariableOpReadVariableOptotal_1*
_output_shapes
: *
dtype0
b
count_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	count_1
[
count_1/Read/ReadVariableOpReadVariableOpcount_1*
_output_shapes
: *
dtype0
t
true_positivesVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_nametrue_positives
m
"true_positives/Read/ReadVariableOpReadVariableOptrue_positives*
_output_shapes
:*
dtype0
v
false_positivesVarHandleOp*
_output_shapes
: *
dtype0*
shape:* 
shared_namefalse_positives
o
#false_positives/Read/ReadVariableOpReadVariableOpfalse_positives*
_output_shapes
:*
dtype0
�
Adam/Input_7/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*&
shared_nameAdam/Input_7/kernel/m

)Adam/Input_7/kernel/m/Read/ReadVariableOpReadVariableOpAdam/Input_7/kernel/m*
_output_shapes

:00*
dtype0
~
Adam/Input_7/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*$
shared_nameAdam/Input_7/bias/m
w
'Adam/Input_7/bias/m/Read/ReadVariableOpReadVariableOpAdam/Input_7/bias/m*
_output_shapes
:0*
dtype0
�
Adam/type0_7/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*&
shared_nameAdam/type0_7/kernel/m

)Adam/type0_7/kernel/m/Read/ReadVariableOpReadVariableOpAdam/type0_7/kernel/m*
_output_shapes

:00*
dtype0
~
Adam/type0_7/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*$
shared_nameAdam/type0_7/bias/m
w
'Adam/type0_7/bias/m/Read/ReadVariableOpReadVariableOpAdam/type0_7/bias/m*
_output_shapes
:0*
dtype0
�
Adam/type1_7/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*&
shared_nameAdam/type1_7/kernel/m

)Adam/type1_7/kernel/m/Read/ReadVariableOpReadVariableOpAdam/type1_7/kernel/m*
_output_shapes

:00*
dtype0
~
Adam/type1_7/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*$
shared_nameAdam/type1_7/bias/m
w
'Adam/type1_7/bias/m/Read/ReadVariableOpReadVariableOpAdam/type1_7/bias/m*
_output_shapes
:0*
dtype0
�
Adam/type2_7/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*&
shared_nameAdam/type2_7/kernel/m

)Adam/type2_7/kernel/m/Read/ReadVariableOpReadVariableOpAdam/type2_7/kernel/m*
_output_shapes

:00*
dtype0
~
Adam/type2_7/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*$
shared_nameAdam/type2_7/bias/m
w
'Adam/type2_7/bias/m/Read/ReadVariableOpReadVariableOpAdam/type2_7/bias/m*
_output_shapes
:0*
dtype0
�
Adam/type3_7/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*&
shared_nameAdam/type3_7/kernel/m

)Adam/type3_7/kernel/m/Read/ReadVariableOpReadVariableOpAdam/type3_7/kernel/m*
_output_shapes

:00*
dtype0
~
Adam/type3_7/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*$
shared_nameAdam/type3_7/bias/m
w
'Adam/type3_7/bias/m/Read/ReadVariableOpReadVariableOpAdam/type3_7/bias/m*
_output_shapes
:0*
dtype0
�
Adam/type4_7/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*&
shared_nameAdam/type4_7/kernel/m

)Adam/type4_7/kernel/m/Read/ReadVariableOpReadVariableOpAdam/type4_7/kernel/m*
_output_shapes

:00*
dtype0
~
Adam/type4_7/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*$
shared_nameAdam/type4_7/bias/m
w
'Adam/type4_7/bias/m/Read/ReadVariableOpReadVariableOpAdam/type4_7/bias/m*
_output_shapes
:0*
dtype0
�
"Adam/batch_normalization_9/gamma/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*3
shared_name$"Adam/batch_normalization_9/gamma/m
�
6Adam/batch_normalization_9/gamma/m/Read/ReadVariableOpReadVariableOp"Adam/batch_normalization_9/gamma/m*
_output_shapes
:0*
dtype0
�
!Adam/batch_normalization_9/beta/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*2
shared_name#!Adam/batch_normalization_9/beta/m
�
5Adam/batch_normalization_9/beta/m/Read/ReadVariableOpReadVariableOp!Adam/batch_normalization_9/beta/m*
_output_shapes
:0*
dtype0
�
Adam/Output_9/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:0	*'
shared_nameAdam/Output_9/kernel/m
�
*Adam/Output_9/kernel/m/Read/ReadVariableOpReadVariableOpAdam/Output_9/kernel/m*
_output_shapes

:0	*
dtype0
�
Adam/Output_9/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:	*%
shared_nameAdam/Output_9/bias/m
y
(Adam/Output_9/bias/m/Read/ReadVariableOpReadVariableOpAdam/Output_9/bias/m*
_output_shapes
:	*
dtype0
�
Adam/Input_7/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*&
shared_nameAdam/Input_7/kernel/v

)Adam/Input_7/kernel/v/Read/ReadVariableOpReadVariableOpAdam/Input_7/kernel/v*
_output_shapes

:00*
dtype0
~
Adam/Input_7/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*$
shared_nameAdam/Input_7/bias/v
w
'Adam/Input_7/bias/v/Read/ReadVariableOpReadVariableOpAdam/Input_7/bias/v*
_output_shapes
:0*
dtype0
�
Adam/type0_7/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*&
shared_nameAdam/type0_7/kernel/v

)Adam/type0_7/kernel/v/Read/ReadVariableOpReadVariableOpAdam/type0_7/kernel/v*
_output_shapes

:00*
dtype0
~
Adam/type0_7/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*$
shared_nameAdam/type0_7/bias/v
w
'Adam/type0_7/bias/v/Read/ReadVariableOpReadVariableOpAdam/type0_7/bias/v*
_output_shapes
:0*
dtype0
�
Adam/type1_7/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*&
shared_nameAdam/type1_7/kernel/v

)Adam/type1_7/kernel/v/Read/ReadVariableOpReadVariableOpAdam/type1_7/kernel/v*
_output_shapes

:00*
dtype0
~
Adam/type1_7/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*$
shared_nameAdam/type1_7/bias/v
w
'Adam/type1_7/bias/v/Read/ReadVariableOpReadVariableOpAdam/type1_7/bias/v*
_output_shapes
:0*
dtype0
�
Adam/type2_7/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*&
shared_nameAdam/type2_7/kernel/v

)Adam/type2_7/kernel/v/Read/ReadVariableOpReadVariableOpAdam/type2_7/kernel/v*
_output_shapes

:00*
dtype0
~
Adam/type2_7/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*$
shared_nameAdam/type2_7/bias/v
w
'Adam/type2_7/bias/v/Read/ReadVariableOpReadVariableOpAdam/type2_7/bias/v*
_output_shapes
:0*
dtype0
�
Adam/type3_7/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*&
shared_nameAdam/type3_7/kernel/v

)Adam/type3_7/kernel/v/Read/ReadVariableOpReadVariableOpAdam/type3_7/kernel/v*
_output_shapes

:00*
dtype0
~
Adam/type3_7/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*$
shared_nameAdam/type3_7/bias/v
w
'Adam/type3_7/bias/v/Read/ReadVariableOpReadVariableOpAdam/type3_7/bias/v*
_output_shapes
:0*
dtype0
�
Adam/type4_7/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:00*&
shared_nameAdam/type4_7/kernel/v

)Adam/type4_7/kernel/v/Read/ReadVariableOpReadVariableOpAdam/type4_7/kernel/v*
_output_shapes

:00*
dtype0
~
Adam/type4_7/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*$
shared_nameAdam/type4_7/bias/v
w
'Adam/type4_7/bias/v/Read/ReadVariableOpReadVariableOpAdam/type4_7/bias/v*
_output_shapes
:0*
dtype0
�
"Adam/batch_normalization_9/gamma/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*3
shared_name$"Adam/batch_normalization_9/gamma/v
�
6Adam/batch_normalization_9/gamma/v/Read/ReadVariableOpReadVariableOp"Adam/batch_normalization_9/gamma/v*
_output_shapes
:0*
dtype0
�
!Adam/batch_normalization_9/beta/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:0*2
shared_name#!Adam/batch_normalization_9/beta/v
�
5Adam/batch_normalization_9/beta/v/Read/ReadVariableOpReadVariableOp!Adam/batch_normalization_9/beta/v*
_output_shapes
:0*
dtype0
�
Adam/Output_9/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:0	*'
shared_nameAdam/Output_9/kernel/v
�
*Adam/Output_9/kernel/v/Read/ReadVariableOpReadVariableOpAdam/Output_9/kernel/v*
_output_shapes

:0	*
dtype0
�
Adam/Output_9/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:	*%
shared_nameAdam/Output_9/bias/v
y
(Adam/Output_9/bias/v/Read/ReadVariableOpReadVariableOpAdam/Output_9/bias/v*
_output_shapes
:	*
dtype0

NoOpNoOp
�h
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*�g
value�gB�g B�g
�
layer_with_weights-0
layer-0
layer_with_weights-1
layer-1
layer_with_weights-2
layer-2
layer_with_weights-3
layer-3
layer_with_weights-4
layer-4
layer_with_weights-5
layer-5
layer_with_weights-6
layer-6
layer_with_weights-7
layer-7
	layer-8

	optimizer
	variables
trainable_variables
regularization_losses
	keras_api

signatures
x

activation

kernel
bias
	variables
trainable_variables
regularization_losses
	keras_api
x

activation

kernel
bias
	variables
trainable_variables
regularization_losses
	keras_api
x

activation

kernel
 bias
!	variables
"trainable_variables
#regularization_losses
$	keras_api
x
%
activation

&kernel
'bias
(	variables
)trainable_variables
*regularization_losses
+	keras_api
x
,
activation

-kernel
.bias
/	variables
0trainable_variables
1regularization_losses
2	keras_api
x
3
activation

4kernel
5bias
6	variables
7trainable_variables
8regularization_losses
9	keras_api
�
:axis
	;gamma
<beta
=moving_mean
>moving_variance
?	variables
@trainable_variables
Aregularization_losses
B	keras_api
h

Ckernel
Dbias
E	variables
Ftrainable_variables
Gregularization_losses
H	keras_api
R
I	variables
Jtrainable_variables
Kregularization_losses
L	keras_api
�
Miter

Nbeta_1

Obeta_2
	Pdecay
Qlearning_ratem�m�m�m�m� m�&m�'m�-m�.m�4m�5m�;m�<m�Cm�Dm�v�v�v�v�v� v�&v�'v�-v�.v�4v�5v�;v�<v�Cv�Dv�
�
0
1
2
3
4
 5
&6
'7
-8
.9
410
511
;12
<13
=14
>15
C16
D17
v
0
1
2
3
4
 5
&6
'7
-8
.9
410
511
;12
<13
C14
D15
 
�
Rlayer_regularization_losses
Snon_trainable_variables

Tlayers
Ulayer_metrics
	variables
trainable_variables
Vmetrics
regularization_losses
 
R
W	variables
Xtrainable_variables
Yregularization_losses
Z	keras_api
ZX
VARIABLE_VALUEInput_7/kernel6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUE
VT
VARIABLE_VALUEInput_7/bias4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUE

0
1

0
1
 
�
[layer_regularization_losses
\non_trainable_variables

]layers
^layer_metrics
	variables
trainable_variables
_metrics
regularization_losses
R
`	variables
atrainable_variables
bregularization_losses
c	keras_api
ZX
VARIABLE_VALUEtype0_7/kernel6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUE
VT
VARIABLE_VALUEtype0_7/bias4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUE

0
1

0
1
 
�
dlayer_regularization_losses
enon_trainable_variables

flayers
glayer_metrics
	variables
trainable_variables
hmetrics
regularization_losses
R
i	variables
jtrainable_variables
kregularization_losses
l	keras_api
ZX
VARIABLE_VALUEtype1_7/kernel6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUE
VT
VARIABLE_VALUEtype1_7/bias4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUE

0
 1

0
 1
 
�
mlayer_regularization_losses
nnon_trainable_variables

olayers
player_metrics
!	variables
"trainable_variables
qmetrics
#regularization_losses
R
r	variables
strainable_variables
tregularization_losses
u	keras_api
ZX
VARIABLE_VALUEtype2_7/kernel6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUE
VT
VARIABLE_VALUEtype2_7/bias4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUE

&0
'1

&0
'1
 
�
vlayer_regularization_losses
wnon_trainable_variables

xlayers
ylayer_metrics
(	variables
)trainable_variables
zmetrics
*regularization_losses
R
{	variables
|trainable_variables
}regularization_losses
~	keras_api
ZX
VARIABLE_VALUEtype3_7/kernel6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUE
VT
VARIABLE_VALUEtype3_7/bias4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUE

-0
.1

-0
.1
 
�
layer_regularization_losses
�non_trainable_variables
�layers
�layer_metrics
/	variables
0trainable_variables
�metrics
1regularization_losses
V
�	variables
�trainable_variables
�regularization_losses
�	keras_api
ZX
VARIABLE_VALUEtype4_7/kernel6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUE
VT
VARIABLE_VALUEtype4_7/bias4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUE

40
51

40
51
 
�
 �layer_regularization_losses
�non_trainable_variables
�layers
�layer_metrics
6	variables
7trainable_variables
�metrics
8regularization_losses
 
fd
VARIABLE_VALUEbatch_normalization_9/gamma5layer_with_weights-6/gamma/.ATTRIBUTES/VARIABLE_VALUE
db
VARIABLE_VALUEbatch_normalization_9/beta4layer_with_weights-6/beta/.ATTRIBUTES/VARIABLE_VALUE
rp
VARIABLE_VALUE!batch_normalization_9/moving_mean;layer_with_weights-6/moving_mean/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUE%batch_normalization_9/moving_variance?layer_with_weights-6/moving_variance/.ATTRIBUTES/VARIABLE_VALUE

;0
<1
=2
>3

;0
<1
 
�
 �layer_regularization_losses
�non_trainable_variables
�layers
�layer_metrics
?	variables
@trainable_variables
�metrics
Aregularization_losses
[Y
VARIABLE_VALUEOutput_9/kernel6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUE
WU
VARIABLE_VALUEOutput_9/bias4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUE

C0
D1

C0
D1
 
�
 �layer_regularization_losses
�non_trainable_variables
�layers
�layer_metrics
E	variables
Ftrainable_variables
�metrics
Gregularization_losses
 
 
 
�
 �layer_regularization_losses
�non_trainable_variables
�layers
�layer_metrics
I	variables
Jtrainable_variables
�metrics
Kregularization_losses
HF
VARIABLE_VALUE	Adam/iter)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_1+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUE
LJ
VARIABLE_VALUEAdam/beta_2+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUE
JH
VARIABLE_VALUE
Adam/decay*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUE
ZX
VARIABLE_VALUEAdam/learning_rate2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUE
 

=0
>1
?
0
1
2
3
4
5
6
7
	8
 

�0
�1
�2
 
 
 
�
 �layer_regularization_losses
�non_trainable_variables
�layers
�layer_metrics
W	variables
Xtrainable_variables
�metrics
Yregularization_losses
 
 

0
 
 
 
 
 
�
 �layer_regularization_losses
�non_trainable_variables
�layers
�layer_metrics
`	variables
atrainable_variables
�metrics
bregularization_losses
 
 

0
 
 
 
 
 
�
 �layer_regularization_losses
�non_trainable_variables
�layers
�layer_metrics
i	variables
jtrainable_variables
�metrics
kregularization_losses
 
 

0
 
 
 
 
 
�
 �layer_regularization_losses
�non_trainable_variables
�layers
�layer_metrics
r	variables
strainable_variables
�metrics
tregularization_losses
 
 

%0
 
 
 
 
 
�
 �layer_regularization_losses
�non_trainable_variables
�layers
�layer_metrics
{	variables
|trainable_variables
�metrics
}regularization_losses
 
 

,0
 
 
 
 
 
�
 �layer_regularization_losses
�non_trainable_variables
�layers
�layer_metrics
�	variables
�trainable_variables
�metrics
�regularization_losses
 
 

30
 
 
 

=0
>1
 
 
 
 
 
 
 
 
 
 
 
 
 
8

�total

�count
�	variables
�	keras_api
I

�total

�count
�
_fn_kwargs
�	variables
�	keras_api
\
�
thresholds
�true_positives
�false_positives
�	variables
�	keras_api
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
OM
VARIABLE_VALUEtotal4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE
OM
VARIABLE_VALUEcount4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE

�0
�1

�	variables
QO
VARIABLE_VALUEtotal_14keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUE
QO
VARIABLE_VALUEcount_14keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUE
 

�0
�1

�	variables
 
a_
VARIABLE_VALUEtrue_positives=keras_api/metrics/2/true_positives/.ATTRIBUTES/VARIABLE_VALUE
ca
VARIABLE_VALUEfalse_positives>keras_api/metrics/2/false_positives/.ATTRIBUTES/VARIABLE_VALUE

�0
�1

�	variables
}{
VARIABLE_VALUEAdam/Input_7/kernel/mRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/Input_7/bias/mPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/type0_7/kernel/mRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/type0_7/bias/mPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/type1_7/kernel/mRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/type1_7/bias/mPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/type2_7/kernel/mRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/type2_7/bias/mPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/type3_7/kernel/mRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/type3_7/bias/mPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/type4_7/kernel/mRlayer_with_weights-5/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/type4_7/bias/mPlayer_with_weights-5/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE"Adam/batch_normalization_9/gamma/mQlayer_with_weights-6/gamma/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE!Adam/batch_normalization_9/beta/mPlayer_with_weights-6/beta/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/Output_9/kernel/mRlayer_with_weights-7/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/Output_9/bias/mPlayer_with_weights-7/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/Input_7/kernel/vRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/Input_7/bias/vPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/type0_7/kernel/vRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/type0_7/bias/vPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/type1_7/kernel/vRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/type1_7/bias/vPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/type2_7/kernel/vRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/type2_7/bias/vPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/type3_7/kernel/vRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/type3_7/bias/vPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
}{
VARIABLE_VALUEAdam/type4_7/kernel/vRlayer_with_weights-5/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
yw
VARIABLE_VALUEAdam/type4_7/bias/vPlayer_with_weights-5/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE"Adam/batch_normalization_9/gamma/vQlayer_with_weights-6/gamma/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUE!Adam/batch_normalization_9/beta/vPlayer_with_weights-6/beta/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/Output_9/kernel/vRlayer_with_weights-7/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
zx
VARIABLE_VALUEAdam/Output_9/bias/vPlayer_with_weights-7/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
~
serving_default_Input_inputPlaceholder*'
_output_shapes
:���������0*
dtype0*
shape:���������0
�
StatefulPartitionedCallStatefulPartitionedCallserving_default_Input_inputInput_7/kernelInput_7/biastype0_7/kerneltype0_7/biastype1_7/kerneltype1_7/biastype2_7/kerneltype2_7/biastype3_7/kerneltype3_7/biastype4_7/kerneltype4_7/bias%batch_normalization_9/moving_variancebatch_normalization_9/gamma!batch_normalization_9/moving_meanbatch_normalization_9/betaOutput_9/kernelOutput_9/bias*
Tin
2*
Tout
2*'
_output_shapes
:���������	*4
_read_only_resource_inputs
	
**
config_proto

CPU

GPU 2J 8*.
f)R'
%__inference_signature_wrapper_7693588
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
�
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filename"Input_7/kernel/Read/ReadVariableOp Input_7/bias/Read/ReadVariableOp"type0_7/kernel/Read/ReadVariableOp type0_7/bias/Read/ReadVariableOp"type1_7/kernel/Read/ReadVariableOp type1_7/bias/Read/ReadVariableOp"type2_7/kernel/Read/ReadVariableOp type2_7/bias/Read/ReadVariableOp"type3_7/kernel/Read/ReadVariableOp type3_7/bias/Read/ReadVariableOp"type4_7/kernel/Read/ReadVariableOp type4_7/bias/Read/ReadVariableOp/batch_normalization_9/gamma/Read/ReadVariableOp.batch_normalization_9/beta/Read/ReadVariableOp5batch_normalization_9/moving_mean/Read/ReadVariableOp9batch_normalization_9/moving_variance/Read/ReadVariableOp#Output_9/kernel/Read/ReadVariableOp!Output_9/bias/Read/ReadVariableOpAdam/iter/Read/ReadVariableOpAdam/beta_1/Read/ReadVariableOpAdam/beta_2/Read/ReadVariableOpAdam/decay/Read/ReadVariableOp&Adam/learning_rate/Read/ReadVariableOptotal/Read/ReadVariableOpcount/Read/ReadVariableOptotal_1/Read/ReadVariableOpcount_1/Read/ReadVariableOp"true_positives/Read/ReadVariableOp#false_positives/Read/ReadVariableOp)Adam/Input_7/kernel/m/Read/ReadVariableOp'Adam/Input_7/bias/m/Read/ReadVariableOp)Adam/type0_7/kernel/m/Read/ReadVariableOp'Adam/type0_7/bias/m/Read/ReadVariableOp)Adam/type1_7/kernel/m/Read/ReadVariableOp'Adam/type1_7/bias/m/Read/ReadVariableOp)Adam/type2_7/kernel/m/Read/ReadVariableOp'Adam/type2_7/bias/m/Read/ReadVariableOp)Adam/type3_7/kernel/m/Read/ReadVariableOp'Adam/type3_7/bias/m/Read/ReadVariableOp)Adam/type4_7/kernel/m/Read/ReadVariableOp'Adam/type4_7/bias/m/Read/ReadVariableOp6Adam/batch_normalization_9/gamma/m/Read/ReadVariableOp5Adam/batch_normalization_9/beta/m/Read/ReadVariableOp*Adam/Output_9/kernel/m/Read/ReadVariableOp(Adam/Output_9/bias/m/Read/ReadVariableOp)Adam/Input_7/kernel/v/Read/ReadVariableOp'Adam/Input_7/bias/v/Read/ReadVariableOp)Adam/type0_7/kernel/v/Read/ReadVariableOp'Adam/type0_7/bias/v/Read/ReadVariableOp)Adam/type1_7/kernel/v/Read/ReadVariableOp'Adam/type1_7/bias/v/Read/ReadVariableOp)Adam/type2_7/kernel/v/Read/ReadVariableOp'Adam/type2_7/bias/v/Read/ReadVariableOp)Adam/type3_7/kernel/v/Read/ReadVariableOp'Adam/type3_7/bias/v/Read/ReadVariableOp)Adam/type4_7/kernel/v/Read/ReadVariableOp'Adam/type4_7/bias/v/Read/ReadVariableOp6Adam/batch_normalization_9/gamma/v/Read/ReadVariableOp5Adam/batch_normalization_9/beta/v/Read/ReadVariableOp*Adam/Output_9/kernel/v/Read/ReadVariableOp(Adam/Output_9/bias/v/Read/ReadVariableOpConst*J
TinC
A2?	*
Tout
2*
_output_shapes
: * 
_read_only_resource_inputs
 **
config_proto

CPU

GPU 2J 8*)
f$R"
 __inference__traced_save_7694301
�
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenameInput_7/kernelInput_7/biastype0_7/kerneltype0_7/biastype1_7/kerneltype1_7/biastype2_7/kerneltype2_7/biastype3_7/kerneltype3_7/biastype4_7/kerneltype4_7/biasbatch_normalization_9/gammabatch_normalization_9/beta!batch_normalization_9/moving_mean%batch_normalization_9/moving_varianceOutput_9/kernelOutput_9/bias	Adam/iterAdam/beta_1Adam/beta_2
Adam/decayAdam/learning_ratetotalcounttotal_1count_1true_positivesfalse_positivesAdam/Input_7/kernel/mAdam/Input_7/bias/mAdam/type0_7/kernel/mAdam/type0_7/bias/mAdam/type1_7/kernel/mAdam/type1_7/bias/mAdam/type2_7/kernel/mAdam/type2_7/bias/mAdam/type3_7/kernel/mAdam/type3_7/bias/mAdam/type4_7/kernel/mAdam/type4_7/bias/m"Adam/batch_normalization_9/gamma/m!Adam/batch_normalization_9/beta/mAdam/Output_9/kernel/mAdam/Output_9/bias/mAdam/Input_7/kernel/vAdam/Input_7/bias/vAdam/type0_7/kernel/vAdam/type0_7/bias/vAdam/type1_7/kernel/vAdam/type1_7/bias/vAdam/type2_7/kernel/vAdam/type2_7/bias/vAdam/type3_7/kernel/vAdam/type3_7/bias/vAdam/type4_7/kernel/vAdam/type4_7/bias/v"Adam/batch_normalization_9/gamma/v!Adam/batch_normalization_9/beta/vAdam/Output_9/kernel/vAdam/Output_9/bias/v*I
TinB
@2>*
Tout
2*
_output_shapes
: * 
_read_only_resource_inputs
 **
config_proto

CPU

GPU 2J 8*,
f'R%
#__inference__traced_restore_7694496��	
�K
�
T__inference_type_1_size_5_48_iter_1_layer_call_and_return_conditional_losses_7693742

inputs(
$input_matmul_readvariableop_resource)
%input_biasadd_readvariableop_resource(
$type0_matmul_readvariableop_resource)
%type0_biasadd_readvariableop_resource(
$type1_matmul_readvariableop_resource)
%type1_biasadd_readvariableop_resource(
$type2_matmul_readvariableop_resource)
%type2_biasadd_readvariableop_resource(
$type3_matmul_readvariableop_resource)
%type3_biasadd_readvariableop_resource(
$type4_matmul_readvariableop_resource)
%type4_biasadd_readvariableop_resource;
7batch_normalization_9_batchnorm_readvariableop_resource?
;batch_normalization_9_batchnorm_mul_readvariableop_resource=
9batch_normalization_9_batchnorm_readvariableop_1_resource=
9batch_normalization_9_batchnorm_readvariableop_2_resource)
%output_matmul_readvariableop_resource*
&output_biasadd_readvariableop_resource
identity��
Input/MatMul/ReadVariableOpReadVariableOp$input_matmul_readvariableop_resource*
_output_shapes

:00*
dtype02
Input/MatMul/ReadVariableOp�
Input/MatMulMatMulinputs#Input/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
Input/MatMul�
Input/BiasAdd/ReadVariableOpReadVariableOp%input_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
Input/BiasAdd/ReadVariableOp�
Input/BiasAddBiasAddInput/MatMul:product:0$Input/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
Input/BiasAddu
Input/elu_55/EluEluInput/BiasAdd:output:0*
T0*'
_output_shapes
:���������02
Input/elu_55/Elu�
type0/MatMul/ReadVariableOpReadVariableOp$type0_matmul_readvariableop_resource*
_output_shapes

:00*
dtype02
type0/MatMul/ReadVariableOp�
type0/MatMulMatMulInput/elu_55/Elu:activations:0#type0/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
type0/MatMul�
type0/BiasAdd/ReadVariableOpReadVariableOp%type0_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
type0/BiasAdd/ReadVariableOp�
type0/BiasAddBiasAddtype0/MatMul:product:0$type0/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
type0/BiasAddu
type0/elu_56/EluElutype0/BiasAdd:output:0*
T0*'
_output_shapes
:���������02
type0/elu_56/Elu�
type1/MatMul/ReadVariableOpReadVariableOp$type1_matmul_readvariableop_resource*
_output_shapes

:00*
dtype02
type1/MatMul/ReadVariableOp�
type1/MatMulMatMultype0/elu_56/Elu:activations:0#type1/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
type1/MatMul�
type1/BiasAdd/ReadVariableOpReadVariableOp%type1_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
type1/BiasAdd/ReadVariableOp�
type1/BiasAddBiasAddtype1/MatMul:product:0$type1/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
type1/BiasAddu
type1/elu_57/EluElutype1/BiasAdd:output:0*
T0*'
_output_shapes
:���������02
type1/elu_57/Elu�
type2/MatMul/ReadVariableOpReadVariableOp$type2_matmul_readvariableop_resource*
_output_shapes

:00*
dtype02
type2/MatMul/ReadVariableOp�
type2/MatMulMatMultype1/elu_57/Elu:activations:0#type2/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
type2/MatMul�
type2/BiasAdd/ReadVariableOpReadVariableOp%type2_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
type2/BiasAdd/ReadVariableOp�
type2/BiasAddBiasAddtype2/MatMul:product:0$type2/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
type2/BiasAddu
type2/elu_58/EluElutype2/BiasAdd:output:0*
T0*'
_output_shapes
:���������02
type2/elu_58/Elu�
type3/MatMul/ReadVariableOpReadVariableOp$type3_matmul_readvariableop_resource*
_output_shapes

:00*
dtype02
type3/MatMul/ReadVariableOp�
type3/MatMulMatMultype2/elu_58/Elu:activations:0#type3/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
type3/MatMul�
type3/BiasAdd/ReadVariableOpReadVariableOp%type3_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
type3/BiasAdd/ReadVariableOp�
type3/BiasAddBiasAddtype3/MatMul:product:0$type3/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
type3/BiasAddu
type3/elu_59/EluElutype3/BiasAdd:output:0*
T0*'
_output_shapes
:���������02
type3/elu_59/Elu�
type4/MatMul/ReadVariableOpReadVariableOp$type4_matmul_readvariableop_resource*
_output_shapes

:00*
dtype02
type4/MatMul/ReadVariableOp�
type4/MatMulMatMultype3/elu_59/Elu:activations:0#type4/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
type4/MatMul�
type4/BiasAdd/ReadVariableOpReadVariableOp%type4_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
type4/BiasAdd/ReadVariableOp�
type4/BiasAddBiasAddtype4/MatMul:product:0$type4/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
type4/BiasAddu
type4/elu_60/EluElutype4/BiasAdd:output:0*
T0*'
_output_shapes
:���������02
type4/elu_60/Elu�
.batch_normalization_9/batchnorm/ReadVariableOpReadVariableOp7batch_normalization_9_batchnorm_readvariableop_resource*
_output_shapes
:0*
dtype020
.batch_normalization_9/batchnorm/ReadVariableOp�
%batch_normalization_9/batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2'
%batch_normalization_9/batchnorm/add/y�
#batch_normalization_9/batchnorm/addAddV26batch_normalization_9/batchnorm/ReadVariableOp:value:0.batch_normalization_9/batchnorm/add/y:output:0*
T0*
_output_shapes
:02%
#batch_normalization_9/batchnorm/add�
%batch_normalization_9/batchnorm/RsqrtRsqrt'batch_normalization_9/batchnorm/add:z:0*
T0*
_output_shapes
:02'
%batch_normalization_9/batchnorm/Rsqrt�
2batch_normalization_9/batchnorm/mul/ReadVariableOpReadVariableOp;batch_normalization_9_batchnorm_mul_readvariableop_resource*
_output_shapes
:0*
dtype024
2batch_normalization_9/batchnorm/mul/ReadVariableOp�
#batch_normalization_9/batchnorm/mulMul)batch_normalization_9/batchnorm/Rsqrt:y:0:batch_normalization_9/batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes
:02%
#batch_normalization_9/batchnorm/mul�
%batch_normalization_9/batchnorm/mul_1Multype4/elu_60/Elu:activations:0'batch_normalization_9/batchnorm/mul:z:0*
T0*'
_output_shapes
:���������02'
%batch_normalization_9/batchnorm/mul_1�
0batch_normalization_9/batchnorm/ReadVariableOp_1ReadVariableOp9batch_normalization_9_batchnorm_readvariableop_1_resource*
_output_shapes
:0*
dtype022
0batch_normalization_9/batchnorm/ReadVariableOp_1�
%batch_normalization_9/batchnorm/mul_2Mul8batch_normalization_9/batchnorm/ReadVariableOp_1:value:0'batch_normalization_9/batchnorm/mul:z:0*
T0*
_output_shapes
:02'
%batch_normalization_9/batchnorm/mul_2�
0batch_normalization_9/batchnorm/ReadVariableOp_2ReadVariableOp9batch_normalization_9_batchnorm_readvariableop_2_resource*
_output_shapes
:0*
dtype022
0batch_normalization_9/batchnorm/ReadVariableOp_2�
#batch_normalization_9/batchnorm/subSub8batch_normalization_9/batchnorm/ReadVariableOp_2:value:0)batch_normalization_9/batchnorm/mul_2:z:0*
T0*
_output_shapes
:02%
#batch_normalization_9/batchnorm/sub�
%batch_normalization_9/batchnorm/add_1AddV2)batch_normalization_9/batchnorm/mul_1:z:0'batch_normalization_9/batchnorm/sub:z:0*
T0*'
_output_shapes
:���������02'
%batch_normalization_9/batchnorm/add_1�
Output/MatMul/ReadVariableOpReadVariableOp%output_matmul_readvariableop_resource*
_output_shapes

:0	*
dtype02
Output/MatMul/ReadVariableOp�
Output/MatMulMatMul)batch_normalization_9/batchnorm/add_1:z:0$Output/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������	2
Output/MatMul�
Output/BiasAdd/ReadVariableOpReadVariableOp&output_biasadd_readvariableop_resource*
_output_shapes
:	*
dtype02
Output/BiasAdd/ReadVariableOp�
Output/BiasAddBiasAddOutput/MatMul:product:0%Output/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������	2
Output/BiasAdd|
softmax_9/SoftmaxSoftmaxOutput/BiasAdd:output:0*
T0*'
_output_shapes
:���������	2
softmax_9/Softmaxo
IdentityIdentitysoftmax_9/Softmax:softmax:0*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*n
_input_shapes]
[:���������0:::::::::::::::::::O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
9__inference_type_1_size_5_48_iter_1_layer_call_fn_7693824

inputs
unknown
	unknown_0
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
	unknown_7
	unknown_8
	unknown_9

unknown_10

unknown_11

unknown_12

unknown_13

unknown_14

unknown_15

unknown_16
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16*
Tin
2*
Tout
2*'
_output_shapes
:���������	*4
_read_only_resource_inputs
	
**
config_proto

CPU

GPU 2J 8*]
fXRV
T__inference_type_1_size_5_48_iter_1_layer_call_and_return_conditional_losses_76934622
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*n
_input_shapes]
[:���������0::::::::::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�p
�
T__inference_type_1_size_5_48_iter_1_layer_call_and_return_conditional_losses_7693673

inputs(
$input_matmul_readvariableop_resource)
%input_biasadd_readvariableop_resource(
$type0_matmul_readvariableop_resource)
%type0_biasadd_readvariableop_resource(
$type1_matmul_readvariableop_resource)
%type1_biasadd_readvariableop_resource(
$type2_matmul_readvariableop_resource)
%type2_biasadd_readvariableop_resource(
$type3_matmul_readvariableop_resource)
%type3_biasadd_readvariableop_resource(
$type4_matmul_readvariableop_resource)
%type4_biasadd_readvariableop_resource1
-batch_normalization_9_assignmovingavg_76936413
/batch_normalization_9_assignmovingavg_1_7693647?
;batch_normalization_9_batchnorm_mul_readvariableop_resource;
7batch_normalization_9_batchnorm_readvariableop_resource)
%output_matmul_readvariableop_resource*
&output_biasadd_readvariableop_resource
identity��9batch_normalization_9/AssignMovingAvg/AssignSubVariableOp�;batch_normalization_9/AssignMovingAvg_1/AssignSubVariableOp�
Input/MatMul/ReadVariableOpReadVariableOp$input_matmul_readvariableop_resource*
_output_shapes

:00*
dtype02
Input/MatMul/ReadVariableOp�
Input/MatMulMatMulinputs#Input/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
Input/MatMul�
Input/BiasAdd/ReadVariableOpReadVariableOp%input_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
Input/BiasAdd/ReadVariableOp�
Input/BiasAddBiasAddInput/MatMul:product:0$Input/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
Input/BiasAddu
Input/elu_55/EluEluInput/BiasAdd:output:0*
T0*'
_output_shapes
:���������02
Input/elu_55/Elu�
type0/MatMul/ReadVariableOpReadVariableOp$type0_matmul_readvariableop_resource*
_output_shapes

:00*
dtype02
type0/MatMul/ReadVariableOp�
type0/MatMulMatMulInput/elu_55/Elu:activations:0#type0/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
type0/MatMul�
type0/BiasAdd/ReadVariableOpReadVariableOp%type0_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
type0/BiasAdd/ReadVariableOp�
type0/BiasAddBiasAddtype0/MatMul:product:0$type0/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
type0/BiasAddu
type0/elu_56/EluElutype0/BiasAdd:output:0*
T0*'
_output_shapes
:���������02
type0/elu_56/Elu�
type1/MatMul/ReadVariableOpReadVariableOp$type1_matmul_readvariableop_resource*
_output_shapes

:00*
dtype02
type1/MatMul/ReadVariableOp�
type1/MatMulMatMultype0/elu_56/Elu:activations:0#type1/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
type1/MatMul�
type1/BiasAdd/ReadVariableOpReadVariableOp%type1_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
type1/BiasAdd/ReadVariableOp�
type1/BiasAddBiasAddtype1/MatMul:product:0$type1/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
type1/BiasAddu
type1/elu_57/EluElutype1/BiasAdd:output:0*
T0*'
_output_shapes
:���������02
type1/elu_57/Elu�
type2/MatMul/ReadVariableOpReadVariableOp$type2_matmul_readvariableop_resource*
_output_shapes

:00*
dtype02
type2/MatMul/ReadVariableOp�
type2/MatMulMatMultype1/elu_57/Elu:activations:0#type2/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
type2/MatMul�
type2/BiasAdd/ReadVariableOpReadVariableOp%type2_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
type2/BiasAdd/ReadVariableOp�
type2/BiasAddBiasAddtype2/MatMul:product:0$type2/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
type2/BiasAddu
type2/elu_58/EluElutype2/BiasAdd:output:0*
T0*'
_output_shapes
:���������02
type2/elu_58/Elu�
type3/MatMul/ReadVariableOpReadVariableOp$type3_matmul_readvariableop_resource*
_output_shapes

:00*
dtype02
type3/MatMul/ReadVariableOp�
type3/MatMulMatMultype2/elu_58/Elu:activations:0#type3/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
type3/MatMul�
type3/BiasAdd/ReadVariableOpReadVariableOp%type3_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
type3/BiasAdd/ReadVariableOp�
type3/BiasAddBiasAddtype3/MatMul:product:0$type3/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
type3/BiasAddu
type3/elu_59/EluElutype3/BiasAdd:output:0*
T0*'
_output_shapes
:���������02
type3/elu_59/Elu�
type4/MatMul/ReadVariableOpReadVariableOp$type4_matmul_readvariableop_resource*
_output_shapes

:00*
dtype02
type4/MatMul/ReadVariableOp�
type4/MatMulMatMultype3/elu_59/Elu:activations:0#type4/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
type4/MatMul�
type4/BiasAdd/ReadVariableOpReadVariableOp%type4_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
type4/BiasAdd/ReadVariableOp�
type4/BiasAddBiasAddtype4/MatMul:product:0$type4/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
type4/BiasAddu
type4/elu_60/EluElutype4/BiasAdd:output:0*
T0*'
_output_shapes
:���������02
type4/elu_60/Elu�
4batch_normalization_9/moments/mean/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 26
4batch_normalization_9/moments/mean/reduction_indices�
"batch_normalization_9/moments/meanMeantype4/elu_60/Elu:activations:0=batch_normalization_9/moments/mean/reduction_indices:output:0*
T0*
_output_shapes

:0*
	keep_dims(2$
"batch_normalization_9/moments/mean�
*batch_normalization_9/moments/StopGradientStopGradient+batch_normalization_9/moments/mean:output:0*
T0*
_output_shapes

:02,
*batch_normalization_9/moments/StopGradient�
/batch_normalization_9/moments/SquaredDifferenceSquaredDifferencetype4/elu_60/Elu:activations:03batch_normalization_9/moments/StopGradient:output:0*
T0*'
_output_shapes
:���������021
/batch_normalization_9/moments/SquaredDifference�
8batch_normalization_9/moments/variance/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 2:
8batch_normalization_9/moments/variance/reduction_indices�
&batch_normalization_9/moments/varianceMean3batch_normalization_9/moments/SquaredDifference:z:0Abatch_normalization_9/moments/variance/reduction_indices:output:0*
T0*
_output_shapes

:0*
	keep_dims(2(
&batch_normalization_9/moments/variance�
%batch_normalization_9/moments/SqueezeSqueeze+batch_normalization_9/moments/mean:output:0*
T0*
_output_shapes
:0*
squeeze_dims
 2'
%batch_normalization_9/moments/Squeeze�
'batch_normalization_9/moments/Squeeze_1Squeeze/batch_normalization_9/moments/variance:output:0*
T0*
_output_shapes
:0*
squeeze_dims
 2)
'batch_normalization_9/moments/Squeeze_1�
+batch_normalization_9/AssignMovingAvg/decayConst*@
_class6
42loc:@batch_normalization_9/AssignMovingAvg/7693641*
_output_shapes
: *
dtype0*
valueB
 *
�#<2-
+batch_normalization_9/AssignMovingAvg/decay�
4batch_normalization_9/AssignMovingAvg/ReadVariableOpReadVariableOp-batch_normalization_9_assignmovingavg_7693641*
_output_shapes
:0*
dtype026
4batch_normalization_9/AssignMovingAvg/ReadVariableOp�
)batch_normalization_9/AssignMovingAvg/subSub<batch_normalization_9/AssignMovingAvg/ReadVariableOp:value:0.batch_normalization_9/moments/Squeeze:output:0*
T0*@
_class6
42loc:@batch_normalization_9/AssignMovingAvg/7693641*
_output_shapes
:02+
)batch_normalization_9/AssignMovingAvg/sub�
)batch_normalization_9/AssignMovingAvg/mulMul-batch_normalization_9/AssignMovingAvg/sub:z:04batch_normalization_9/AssignMovingAvg/decay:output:0*
T0*@
_class6
42loc:@batch_normalization_9/AssignMovingAvg/7693641*
_output_shapes
:02+
)batch_normalization_9/AssignMovingAvg/mul�
9batch_normalization_9/AssignMovingAvg/AssignSubVariableOpAssignSubVariableOp-batch_normalization_9_assignmovingavg_7693641-batch_normalization_9/AssignMovingAvg/mul:z:05^batch_normalization_9/AssignMovingAvg/ReadVariableOp*@
_class6
42loc:@batch_normalization_9/AssignMovingAvg/7693641*
_output_shapes
 *
dtype02;
9batch_normalization_9/AssignMovingAvg/AssignSubVariableOp�
-batch_normalization_9/AssignMovingAvg_1/decayConst*B
_class8
64loc:@batch_normalization_9/AssignMovingAvg_1/7693647*
_output_shapes
: *
dtype0*
valueB
 *
�#<2/
-batch_normalization_9/AssignMovingAvg_1/decay�
6batch_normalization_9/AssignMovingAvg_1/ReadVariableOpReadVariableOp/batch_normalization_9_assignmovingavg_1_7693647*
_output_shapes
:0*
dtype028
6batch_normalization_9/AssignMovingAvg_1/ReadVariableOp�
+batch_normalization_9/AssignMovingAvg_1/subSub>batch_normalization_9/AssignMovingAvg_1/ReadVariableOp:value:00batch_normalization_9/moments/Squeeze_1:output:0*
T0*B
_class8
64loc:@batch_normalization_9/AssignMovingAvg_1/7693647*
_output_shapes
:02-
+batch_normalization_9/AssignMovingAvg_1/sub�
+batch_normalization_9/AssignMovingAvg_1/mulMul/batch_normalization_9/AssignMovingAvg_1/sub:z:06batch_normalization_9/AssignMovingAvg_1/decay:output:0*
T0*B
_class8
64loc:@batch_normalization_9/AssignMovingAvg_1/7693647*
_output_shapes
:02-
+batch_normalization_9/AssignMovingAvg_1/mul�
;batch_normalization_9/AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOp/batch_normalization_9_assignmovingavg_1_7693647/batch_normalization_9/AssignMovingAvg_1/mul:z:07^batch_normalization_9/AssignMovingAvg_1/ReadVariableOp*B
_class8
64loc:@batch_normalization_9/AssignMovingAvg_1/7693647*
_output_shapes
 *
dtype02=
;batch_normalization_9/AssignMovingAvg_1/AssignSubVariableOp�
%batch_normalization_9/batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2'
%batch_normalization_9/batchnorm/add/y�
#batch_normalization_9/batchnorm/addAddV20batch_normalization_9/moments/Squeeze_1:output:0.batch_normalization_9/batchnorm/add/y:output:0*
T0*
_output_shapes
:02%
#batch_normalization_9/batchnorm/add�
%batch_normalization_9/batchnorm/RsqrtRsqrt'batch_normalization_9/batchnorm/add:z:0*
T0*
_output_shapes
:02'
%batch_normalization_9/batchnorm/Rsqrt�
2batch_normalization_9/batchnorm/mul/ReadVariableOpReadVariableOp;batch_normalization_9_batchnorm_mul_readvariableop_resource*
_output_shapes
:0*
dtype024
2batch_normalization_9/batchnorm/mul/ReadVariableOp�
#batch_normalization_9/batchnorm/mulMul)batch_normalization_9/batchnorm/Rsqrt:y:0:batch_normalization_9/batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes
:02%
#batch_normalization_9/batchnorm/mul�
%batch_normalization_9/batchnorm/mul_1Multype4/elu_60/Elu:activations:0'batch_normalization_9/batchnorm/mul:z:0*
T0*'
_output_shapes
:���������02'
%batch_normalization_9/batchnorm/mul_1�
%batch_normalization_9/batchnorm/mul_2Mul.batch_normalization_9/moments/Squeeze:output:0'batch_normalization_9/batchnorm/mul:z:0*
T0*
_output_shapes
:02'
%batch_normalization_9/batchnorm/mul_2�
.batch_normalization_9/batchnorm/ReadVariableOpReadVariableOp7batch_normalization_9_batchnorm_readvariableop_resource*
_output_shapes
:0*
dtype020
.batch_normalization_9/batchnorm/ReadVariableOp�
#batch_normalization_9/batchnorm/subSub6batch_normalization_9/batchnorm/ReadVariableOp:value:0)batch_normalization_9/batchnorm/mul_2:z:0*
T0*
_output_shapes
:02%
#batch_normalization_9/batchnorm/sub�
%batch_normalization_9/batchnorm/add_1AddV2)batch_normalization_9/batchnorm/mul_1:z:0'batch_normalization_9/batchnorm/sub:z:0*
T0*'
_output_shapes
:���������02'
%batch_normalization_9/batchnorm/add_1�
Output/MatMul/ReadVariableOpReadVariableOp%output_matmul_readvariableop_resource*
_output_shapes

:0	*
dtype02
Output/MatMul/ReadVariableOp�
Output/MatMulMatMul)batch_normalization_9/batchnorm/add_1:z:0$Output/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������	2
Output/MatMul�
Output/BiasAdd/ReadVariableOpReadVariableOp&output_biasadd_readvariableop_resource*
_output_shapes
:	*
dtype02
Output/BiasAdd/ReadVariableOp�
Output/BiasAddBiasAddOutput/MatMul:product:0%Output/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������	2
Output/BiasAdd|
softmax_9/SoftmaxSoftmaxOutput/BiasAdd:output:0*
T0*'
_output_shapes
:���������	2
softmax_9/Softmax�
IdentityIdentitysoftmax_9/Softmax:softmax:0:^batch_normalization_9/AssignMovingAvg/AssignSubVariableOp<^batch_normalization_9/AssignMovingAvg_1/AssignSubVariableOp*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*n
_input_shapes]
[:���������0::::::::::::::::::2v
9batch_normalization_9/AssignMovingAvg/AssignSubVariableOp9batch_normalization_9/AssignMovingAvg/AssignSubVariableOp2z
;batch_normalization_9/AssignMovingAvg_1/AssignSubVariableOp;batch_normalization_9/AssignMovingAvg_1/AssignSubVariableOp:O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
B__inference_type1_layer_call_and_return_conditional_losses_7693875

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:00*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02	
BiasAddc

elu_57/EluEluBiasAdd:output:0*
T0*'
_output_shapes
:���������02

elu_57/Elul
IdentityIdentityelu_57/Elu:activations:0*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0:::O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�*
�
R__inference_batch_normalization_9_layer_call_and_return_conditional_losses_7692986

inputs
assignmovingavg_7692961
assignmovingavg_1_7692967)
%batchnorm_mul_readvariableop_resource%
!batchnorm_readvariableop_resource
identity��#AssignMovingAvg/AssignSubVariableOp�%AssignMovingAvg_1/AssignSubVariableOp�
moments/mean/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 2 
moments/mean/reduction_indices�
moments/meanMeaninputs'moments/mean/reduction_indices:output:0*
T0*
_output_shapes

:0*
	keep_dims(2
moments/mean|
moments/StopGradientStopGradientmoments/mean:output:0*
T0*
_output_shapes

:02
moments/StopGradient�
moments/SquaredDifferenceSquaredDifferenceinputsmoments/StopGradient:output:0*
T0*'
_output_shapes
:���������02
moments/SquaredDifference�
"moments/variance/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 2$
"moments/variance/reduction_indices�
moments/varianceMeanmoments/SquaredDifference:z:0+moments/variance/reduction_indices:output:0*
T0*
_output_shapes

:0*
	keep_dims(2
moments/variance�
moments/SqueezeSqueezemoments/mean:output:0*
T0*
_output_shapes
:0*
squeeze_dims
 2
moments/Squeeze�
moments/Squeeze_1Squeezemoments/variance:output:0*
T0*
_output_shapes
:0*
squeeze_dims
 2
moments/Squeeze_1�
AssignMovingAvg/decayConst**
_class 
loc:@AssignMovingAvg/7692961*
_output_shapes
: *
dtype0*
valueB
 *
�#<2
AssignMovingAvg/decay�
AssignMovingAvg/ReadVariableOpReadVariableOpassignmovingavg_7692961*
_output_shapes
:0*
dtype02 
AssignMovingAvg/ReadVariableOp�
AssignMovingAvg/subSub&AssignMovingAvg/ReadVariableOp:value:0moments/Squeeze:output:0*
T0**
_class 
loc:@AssignMovingAvg/7692961*
_output_shapes
:02
AssignMovingAvg/sub�
AssignMovingAvg/mulMulAssignMovingAvg/sub:z:0AssignMovingAvg/decay:output:0*
T0**
_class 
loc:@AssignMovingAvg/7692961*
_output_shapes
:02
AssignMovingAvg/mul�
#AssignMovingAvg/AssignSubVariableOpAssignSubVariableOpassignmovingavg_7692961AssignMovingAvg/mul:z:0^AssignMovingAvg/ReadVariableOp**
_class 
loc:@AssignMovingAvg/7692961*
_output_shapes
 *
dtype02%
#AssignMovingAvg/AssignSubVariableOp�
AssignMovingAvg_1/decayConst*,
_class"
 loc:@AssignMovingAvg_1/7692967*
_output_shapes
: *
dtype0*
valueB
 *
�#<2
AssignMovingAvg_1/decay�
 AssignMovingAvg_1/ReadVariableOpReadVariableOpassignmovingavg_1_7692967*
_output_shapes
:0*
dtype02"
 AssignMovingAvg_1/ReadVariableOp�
AssignMovingAvg_1/subSub(AssignMovingAvg_1/ReadVariableOp:value:0moments/Squeeze_1:output:0*
T0*,
_class"
 loc:@AssignMovingAvg_1/7692967*
_output_shapes
:02
AssignMovingAvg_1/sub�
AssignMovingAvg_1/mulMulAssignMovingAvg_1/sub:z:0 AssignMovingAvg_1/decay:output:0*
T0*,
_class"
 loc:@AssignMovingAvg_1/7692967*
_output_shapes
:02
AssignMovingAvg_1/mul�
%AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOpassignmovingavg_1_7692967AssignMovingAvg_1/mul:z:0!^AssignMovingAvg_1/ReadVariableOp*,
_class"
 loc:@AssignMovingAvg_1/7692967*
_output_shapes
 *
dtype02'
%AssignMovingAvg_1/AssignSubVariableOpg
batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2
batchnorm/add/y�
batchnorm/addAddV2moments/Squeeze_1:output:0batchnorm/add/y:output:0*
T0*
_output_shapes
:02
batchnorm/addc
batchnorm/RsqrtRsqrtbatchnorm/add:z:0*
T0*
_output_shapes
:02
batchnorm/Rsqrt�
batchnorm/mul/ReadVariableOpReadVariableOp%batchnorm_mul_readvariableop_resource*
_output_shapes
:0*
dtype02
batchnorm/mul/ReadVariableOp�
batchnorm/mulMulbatchnorm/Rsqrt:y:0$batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes
:02
batchnorm/mulv
batchnorm/mul_1Mulinputsbatchnorm/mul:z:0*
T0*'
_output_shapes
:���������02
batchnorm/mul_1{
batchnorm/mul_2Mulmoments/Squeeze:output:0batchnorm/mul:z:0*
T0*
_output_shapes
:02
batchnorm/mul_2�
batchnorm/ReadVariableOpReadVariableOp!batchnorm_readvariableop_resource*
_output_shapes
:0*
dtype02
batchnorm/ReadVariableOp�
batchnorm/subSub batchnorm/ReadVariableOp:value:0batchnorm/mul_2:z:0*
T0*
_output_shapes
:02
batchnorm/sub�
batchnorm/add_1AddV2batchnorm/mul_1:z:0batchnorm/sub:z:0*
T0*'
_output_shapes
:���������02
batchnorm/add_1�
IdentityIdentitybatchnorm/add_1:z:0$^AssignMovingAvg/AssignSubVariableOp&^AssignMovingAvg_1/AssignSubVariableOp*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*6
_input_shapes%
#:���������0::::2J
#AssignMovingAvg/AssignSubVariableOp#AssignMovingAvg/AssignSubVariableOp2N
%AssignMovingAvg_1/AssignSubVariableOp%AssignMovingAvg_1/AssignSubVariableOp:O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
B__inference_type1_layer_call_and_return_conditional_losses_7693099

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:00*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02	
BiasAddc

elu_57/EluEluBiasAdd:output:0*
T0*'
_output_shapes
:���������02

elu_57/Elul
IdentityIdentityelu_57/Elu:activations:0*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0:::O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
�
 __inference__traced_save_7694301
file_prefix-
)savev2_input_7_kernel_read_readvariableop+
'savev2_input_7_bias_read_readvariableop-
)savev2_type0_7_kernel_read_readvariableop+
'savev2_type0_7_bias_read_readvariableop-
)savev2_type1_7_kernel_read_readvariableop+
'savev2_type1_7_bias_read_readvariableop-
)savev2_type2_7_kernel_read_readvariableop+
'savev2_type2_7_bias_read_readvariableop-
)savev2_type3_7_kernel_read_readvariableop+
'savev2_type3_7_bias_read_readvariableop-
)savev2_type4_7_kernel_read_readvariableop+
'savev2_type4_7_bias_read_readvariableop:
6savev2_batch_normalization_9_gamma_read_readvariableop9
5savev2_batch_normalization_9_beta_read_readvariableop@
<savev2_batch_normalization_9_moving_mean_read_readvariableopD
@savev2_batch_normalization_9_moving_variance_read_readvariableop.
*savev2_output_9_kernel_read_readvariableop,
(savev2_output_9_bias_read_readvariableop(
$savev2_adam_iter_read_readvariableop	*
&savev2_adam_beta_1_read_readvariableop*
&savev2_adam_beta_2_read_readvariableop)
%savev2_adam_decay_read_readvariableop1
-savev2_adam_learning_rate_read_readvariableop$
 savev2_total_read_readvariableop$
 savev2_count_read_readvariableop&
"savev2_total_1_read_readvariableop&
"savev2_count_1_read_readvariableop-
)savev2_true_positives_read_readvariableop.
*savev2_false_positives_read_readvariableop4
0savev2_adam_input_7_kernel_m_read_readvariableop2
.savev2_adam_input_7_bias_m_read_readvariableop4
0savev2_adam_type0_7_kernel_m_read_readvariableop2
.savev2_adam_type0_7_bias_m_read_readvariableop4
0savev2_adam_type1_7_kernel_m_read_readvariableop2
.savev2_adam_type1_7_bias_m_read_readvariableop4
0savev2_adam_type2_7_kernel_m_read_readvariableop2
.savev2_adam_type2_7_bias_m_read_readvariableop4
0savev2_adam_type3_7_kernel_m_read_readvariableop2
.savev2_adam_type3_7_bias_m_read_readvariableop4
0savev2_adam_type4_7_kernel_m_read_readvariableop2
.savev2_adam_type4_7_bias_m_read_readvariableopA
=savev2_adam_batch_normalization_9_gamma_m_read_readvariableop@
<savev2_adam_batch_normalization_9_beta_m_read_readvariableop5
1savev2_adam_output_9_kernel_m_read_readvariableop3
/savev2_adam_output_9_bias_m_read_readvariableop4
0savev2_adam_input_7_kernel_v_read_readvariableop2
.savev2_adam_input_7_bias_v_read_readvariableop4
0savev2_adam_type0_7_kernel_v_read_readvariableop2
.savev2_adam_type0_7_bias_v_read_readvariableop4
0savev2_adam_type1_7_kernel_v_read_readvariableop2
.savev2_adam_type1_7_bias_v_read_readvariableop4
0savev2_adam_type2_7_kernel_v_read_readvariableop2
.savev2_adam_type2_7_bias_v_read_readvariableop4
0savev2_adam_type3_7_kernel_v_read_readvariableop2
.savev2_adam_type3_7_bias_v_read_readvariableop4
0savev2_adam_type4_7_kernel_v_read_readvariableop2
.savev2_adam_type4_7_bias_v_read_readvariableopA
=savev2_adam_batch_normalization_9_gamma_v_read_readvariableop@
<savev2_adam_batch_normalization_9_beta_v_read_readvariableop5
1savev2_adam_output_9_kernel_v_read_readvariableop3
/savev2_adam_output_9_bias_v_read_readvariableop
savev2_1_const

identity_1��MergeV2Checkpoints�SaveV2�SaveV2_1�
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*2
StaticRegexFullMatchc
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.part2
Const�
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*<
value3B1 B+_temp_5b3c7e977c1742ac844f7119931866ad/part2	
Const_1�
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: 2
Selectt

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: 2

StringJoinZ

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :2

num_shards
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : 2
ShardedFilename/shard�
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilename�"
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:=*
dtype0*�!
value�!B�!=B6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-6/gamma/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-6/beta/.ATTRIBUTES/VARIABLE_VALUEB;layer_with_weights-6/moving_mean/.ATTRIBUTES/VARIABLE_VALUEB?layer_with_weights-6/moving_variance/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/2/true_positives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/2/false_positives/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-5/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-5/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-6/gamma/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-6/beta/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-7/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-7/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-5/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-5/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-6/gamma/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-6/beta/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-7/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-7/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE2
SaveV2/tensor_names�
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:=*
dtype0*�
value�B�=B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
SaveV2/shape_and_slices�
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:0)savev2_input_7_kernel_read_readvariableop'savev2_input_7_bias_read_readvariableop)savev2_type0_7_kernel_read_readvariableop'savev2_type0_7_bias_read_readvariableop)savev2_type1_7_kernel_read_readvariableop'savev2_type1_7_bias_read_readvariableop)savev2_type2_7_kernel_read_readvariableop'savev2_type2_7_bias_read_readvariableop)savev2_type3_7_kernel_read_readvariableop'savev2_type3_7_bias_read_readvariableop)savev2_type4_7_kernel_read_readvariableop'savev2_type4_7_bias_read_readvariableop6savev2_batch_normalization_9_gamma_read_readvariableop5savev2_batch_normalization_9_beta_read_readvariableop<savev2_batch_normalization_9_moving_mean_read_readvariableop@savev2_batch_normalization_9_moving_variance_read_readvariableop*savev2_output_9_kernel_read_readvariableop(savev2_output_9_bias_read_readvariableop$savev2_adam_iter_read_readvariableop&savev2_adam_beta_1_read_readvariableop&savev2_adam_beta_2_read_readvariableop%savev2_adam_decay_read_readvariableop-savev2_adam_learning_rate_read_readvariableop savev2_total_read_readvariableop savev2_count_read_readvariableop"savev2_total_1_read_readvariableop"savev2_count_1_read_readvariableop)savev2_true_positives_read_readvariableop*savev2_false_positives_read_readvariableop0savev2_adam_input_7_kernel_m_read_readvariableop.savev2_adam_input_7_bias_m_read_readvariableop0savev2_adam_type0_7_kernel_m_read_readvariableop.savev2_adam_type0_7_bias_m_read_readvariableop0savev2_adam_type1_7_kernel_m_read_readvariableop.savev2_adam_type1_7_bias_m_read_readvariableop0savev2_adam_type2_7_kernel_m_read_readvariableop.savev2_adam_type2_7_bias_m_read_readvariableop0savev2_adam_type3_7_kernel_m_read_readvariableop.savev2_adam_type3_7_bias_m_read_readvariableop0savev2_adam_type4_7_kernel_m_read_readvariableop.savev2_adam_type4_7_bias_m_read_readvariableop=savev2_adam_batch_normalization_9_gamma_m_read_readvariableop<savev2_adam_batch_normalization_9_beta_m_read_readvariableop1savev2_adam_output_9_kernel_m_read_readvariableop/savev2_adam_output_9_bias_m_read_readvariableop0savev2_adam_input_7_kernel_v_read_readvariableop.savev2_adam_input_7_bias_v_read_readvariableop0savev2_adam_type0_7_kernel_v_read_readvariableop.savev2_adam_type0_7_bias_v_read_readvariableop0savev2_adam_type1_7_kernel_v_read_readvariableop.savev2_adam_type1_7_bias_v_read_readvariableop0savev2_adam_type2_7_kernel_v_read_readvariableop.savev2_adam_type2_7_bias_v_read_readvariableop0savev2_adam_type3_7_kernel_v_read_readvariableop.savev2_adam_type3_7_bias_v_read_readvariableop0savev2_adam_type4_7_kernel_v_read_readvariableop.savev2_adam_type4_7_bias_v_read_readvariableop=savev2_adam_batch_normalization_9_gamma_v_read_readvariableop<savev2_adam_batch_normalization_9_beta_v_read_readvariableop1savev2_adam_output_9_kernel_v_read_readvariableop/savev2_adam_output_9_bias_v_read_readvariableop"/device:CPU:0*
_output_shapes
 *K
dtypesA
?2=	2
SaveV2�
ShardedFilename_1/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B :2
ShardedFilename_1/shard�
ShardedFilename_1ShardedFilenameStringJoin:output:0 ShardedFilename_1/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: 2
ShardedFilename_1�
SaveV2_1/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*1
value(B&B_CHECKPOINTABLE_OBJECT_GRAPH2
SaveV2_1/tensor_names�
SaveV2_1/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*
valueB
B 2
SaveV2_1/shape_and_slices�
SaveV2_1SaveV2ShardedFilename_1:filename:0SaveV2_1/tensor_names:output:0"SaveV2_1/shape_and_slices:output:0savev2_1_const^SaveV2"/device:CPU:0*
_output_shapes
 *
dtypes
22

SaveV2_1�
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0ShardedFilename_1:filename:0^SaveV2	^SaveV2_1"/device:CPU:0*
N*
T0*
_output_shapes
:2(
&MergeV2Checkpoints/checkpoint_prefixes�
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix	^SaveV2_1"/device:CPU:0*
_output_shapes
 2
MergeV2Checkpointsr
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: 2

Identity�

Identity_1IdentityIdentity:output:0^MergeV2Checkpoints^SaveV2	^SaveV2_1*
T0*
_output_shapes
: 2

Identity_1"!

identity_1Identity_1:output:0*�
_input_shapes�
�: :00:0:00:0:00:0:00:0:00:0:00:0:0:0:0:0:0	:	: : : : : : : : : :::00:0:00:0:00:0:00:0:00:0:00:0:0:0:0	:	:00:0:00:0:00:0:00:0:00:0:00:0:0:0:0	:	: 2(
MergeV2CheckpointsMergeV2Checkpoints2
SaveV2SaveV22
SaveV2_1SaveV2_1:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:$ 

_output_shapes

:00: 

_output_shapes
:0:$ 

_output_shapes

:00: 

_output_shapes
:0:$ 

_output_shapes

:00: 

_output_shapes
:0:$ 

_output_shapes

:00: 

_output_shapes
:0:$	 

_output_shapes

:00: 


_output_shapes
:0:$ 

_output_shapes

:00: 

_output_shapes
:0: 

_output_shapes
:0: 

_output_shapes
:0: 

_output_shapes
:0: 

_output_shapes
:0:$ 

_output_shapes

:0	: 

_output_shapes
:	:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: : 

_output_shapes
:: 

_output_shapes
::$ 

_output_shapes

:00: 

_output_shapes
:0:$  

_output_shapes

:00: !

_output_shapes
:0:$" 

_output_shapes

:00: #

_output_shapes
:0:$$ 

_output_shapes

:00: %

_output_shapes
:0:$& 

_output_shapes

:00: '

_output_shapes
:0:$( 

_output_shapes

:00: )

_output_shapes
:0: *

_output_shapes
:0: +

_output_shapes
:0:$, 

_output_shapes

:0	: -

_output_shapes
:	:$. 

_output_shapes

:00: /

_output_shapes
:0:$0 

_output_shapes

:00: 1

_output_shapes
:0:$2 

_output_shapes

:00: 3

_output_shapes
:0:$4 

_output_shapes

:00: 5

_output_shapes
:0:$6 

_output_shapes

:00: 7

_output_shapes
:0:$8 

_output_shapes

:00: 9

_output_shapes
:0: :

_output_shapes
:0: ;

_output_shapes
:0:$< 

_output_shapes

:0	: =

_output_shapes
:	:>

_output_shapes
: 
�
�
B__inference_type0_layer_call_and_return_conditional_losses_7693855

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:00*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02	
BiasAddc

elu_56/EluEluBiasAdd:output:0*
T0*'
_output_shapes
:���������02

elu_56/Elul
IdentityIdentityelu_56/Elu:activations:0*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0:::O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
�
9__inference_type_1_size_5_48_iter_1_layer_call_fn_7693783

inputs
unknown
	unknown_0
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
	unknown_7
	unknown_8
	unknown_9

unknown_10

unknown_11

unknown_12

unknown_13

unknown_14

unknown_15

unknown_16
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16*
Tin
2*
Tout
2*'
_output_shapes
:���������	*2
_read_only_resource_inputs
	
**
config_proto

CPU

GPU 2J 8*]
fXRV
T__inference_type_1_size_5_48_iter_1_layer_call_and_return_conditional_losses_76933722
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*n
_input_shapes]
[:���������0::::::::::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
|
'__inference_type0_layer_call_fn_7693864

inputs
unknown
	unknown_0
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_type0_layer_call_and_return_conditional_losses_76930722
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�/
�
T__inference_type_1_size_5_48_iter_1_layer_call_and_return_conditional_losses_7693271
input_input
input_7693056
input_7693058
type0_7693083
type0_7693085
type1_7693110
type1_7693112
type2_7693137
type2_7693139
type3_7693164
type3_7693166
type4_7693191
type4_7693193!
batch_normalization_9_7693222!
batch_normalization_9_7693224!
batch_normalization_9_7693226!
batch_normalization_9_7693228
output_7693252
output_7693254
identity��Input/StatefulPartitionedCall�Output/StatefulPartitionedCall�-batch_normalization_9/StatefulPartitionedCall�type0/StatefulPartitionedCall�type1/StatefulPartitionedCall�type2/StatefulPartitionedCall�type3/StatefulPartitionedCall�type4/StatefulPartitionedCall�
Input/StatefulPartitionedCallStatefulPartitionedCallinput_inputinput_7693056input_7693058*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_Input_layer_call_and_return_conditional_losses_76930452
Input/StatefulPartitionedCall�
type0/StatefulPartitionedCallStatefulPartitionedCall&Input/StatefulPartitionedCall:output:0type0_7693083type0_7693085*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_type0_layer_call_and_return_conditional_losses_76930722
type0/StatefulPartitionedCall�
type1/StatefulPartitionedCallStatefulPartitionedCall&type0/StatefulPartitionedCall:output:0type1_7693110type1_7693112*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_type1_layer_call_and_return_conditional_losses_76930992
type1/StatefulPartitionedCall�
type2/StatefulPartitionedCallStatefulPartitionedCall&type1/StatefulPartitionedCall:output:0type2_7693137type2_7693139*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_type2_layer_call_and_return_conditional_losses_76931262
type2/StatefulPartitionedCall�
type3/StatefulPartitionedCallStatefulPartitionedCall&type2/StatefulPartitionedCall:output:0type3_7693164type3_7693166*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_type3_layer_call_and_return_conditional_losses_76931532
type3/StatefulPartitionedCall�
type4/StatefulPartitionedCallStatefulPartitionedCall&type3/StatefulPartitionedCall:output:0type4_7693191type4_7693193*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_type4_layer_call_and_return_conditional_losses_76931802
type4/StatefulPartitionedCall�
-batch_normalization_9/StatefulPartitionedCallStatefulPartitionedCall&type4/StatefulPartitionedCall:output:0batch_normalization_9_7693222batch_normalization_9_7693224batch_normalization_9_7693226batch_normalization_9_7693228*
Tin	
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*[
fVRT
R__inference_batch_normalization_9_layer_call_and_return_conditional_losses_76929862/
-batch_normalization_9/StatefulPartitionedCall�
Output/StatefulPartitionedCallStatefulPartitionedCall6batch_normalization_9/StatefulPartitionedCall:output:0output_7693252output_7693254*
Tin
2*
Tout
2*'
_output_shapes
:���������	*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*L
fGRE
C__inference_Output_layer_call_and_return_conditional_losses_76932412 
Output/StatefulPartitionedCall�
softmax_9/PartitionedCallPartitionedCall'Output/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*'
_output_shapes
:���������	* 
_read_only_resource_inputs
 **
config_proto

CPU

GPU 2J 8*O
fJRH
F__inference_softmax_9_layer_call_and_return_conditional_losses_76932622
softmax_9/PartitionedCall�
IdentityIdentity"softmax_9/PartitionedCall:output:0^Input/StatefulPartitionedCall^Output/StatefulPartitionedCall.^batch_normalization_9/StatefulPartitionedCall^type0/StatefulPartitionedCall^type1/StatefulPartitionedCall^type2/StatefulPartitionedCall^type3/StatefulPartitionedCall^type4/StatefulPartitionedCall*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*n
_input_shapes]
[:���������0::::::::::::::::::2>
Input/StatefulPartitionedCallInput/StatefulPartitionedCall2@
Output/StatefulPartitionedCallOutput/StatefulPartitionedCall2^
-batch_normalization_9/StatefulPartitionedCall-batch_normalization_9/StatefulPartitionedCall2>
type0/StatefulPartitionedCalltype0/StatefulPartitionedCall2>
type1/StatefulPartitionedCalltype1/StatefulPartitionedCall2>
type2/StatefulPartitionedCalltype2/StatefulPartitionedCall2>
type3/StatefulPartitionedCalltype3/StatefulPartitionedCall2>
type4/StatefulPartitionedCalltype4/StatefulPartitionedCall:T P
'
_output_shapes
:���������0
%
_user_specified_nameInput_input:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
B__inference_type3_layer_call_and_return_conditional_losses_7693915

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:00*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02	
BiasAddc

elu_59/EluEluBiasAdd:output:0*
T0*'
_output_shapes
:���������02

elu_59/Elul
IdentityIdentityelu_59/Elu:activations:0*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0:::O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
|
'__inference_type4_layer_call_fn_7693944

inputs
unknown
	unknown_0
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_type4_layer_call_and_return_conditional_losses_76931802
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
�
C__inference_Output_layer_call_and_return_conditional_losses_7694072

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:0	*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������	2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:	*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������	2	
BiasAddd
IdentityIdentityBiasAdd:output:0*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0:::O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
G
+__inference_softmax_9_layer_call_fn_7694091

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*'
_output_shapes
:���������	* 
_read_only_resource_inputs
 **
config_proto

CPU

GPU 2J 8*O
fJRH
F__inference_softmax_9_layer_call_and_return_conditional_losses_76932622
PartitionedCalll
IdentityIdentityPartitionedCall:output:0*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*&
_input_shapes
:���������	:O K
'
_output_shapes
:���������	
 
_user_specified_nameinputs
�
|
'__inference_Input_layer_call_fn_7693844

inputs
unknown
	unknown_0
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_Input_layer_call_and_return_conditional_losses_76930452
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
�
B__inference_Input_layer_call_and_return_conditional_losses_7693045

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:00*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02	
BiasAddc

elu_55/EluEluBiasAdd:output:0*
T0*'
_output_shapes
:���������02

elu_55/Elul
IdentityIdentityelu_55/Elu:activations:0*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0:::O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�*
�
R__inference_batch_normalization_9_layer_call_and_return_conditional_losses_7694016

inputs
assignmovingavg_7693991
assignmovingavg_1_7693997)
%batchnorm_mul_readvariableop_resource%
!batchnorm_readvariableop_resource
identity��#AssignMovingAvg/AssignSubVariableOp�%AssignMovingAvg_1/AssignSubVariableOp�
moments/mean/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 2 
moments/mean/reduction_indices�
moments/meanMeaninputs'moments/mean/reduction_indices:output:0*
T0*
_output_shapes

:0*
	keep_dims(2
moments/mean|
moments/StopGradientStopGradientmoments/mean:output:0*
T0*
_output_shapes

:02
moments/StopGradient�
moments/SquaredDifferenceSquaredDifferenceinputsmoments/StopGradient:output:0*
T0*'
_output_shapes
:���������02
moments/SquaredDifference�
"moments/variance/reduction_indicesConst*
_output_shapes
:*
dtype0*
valueB: 2$
"moments/variance/reduction_indices�
moments/varianceMeanmoments/SquaredDifference:z:0+moments/variance/reduction_indices:output:0*
T0*
_output_shapes

:0*
	keep_dims(2
moments/variance�
moments/SqueezeSqueezemoments/mean:output:0*
T0*
_output_shapes
:0*
squeeze_dims
 2
moments/Squeeze�
moments/Squeeze_1Squeezemoments/variance:output:0*
T0*
_output_shapes
:0*
squeeze_dims
 2
moments/Squeeze_1�
AssignMovingAvg/decayConst**
_class 
loc:@AssignMovingAvg/7693991*
_output_shapes
: *
dtype0*
valueB
 *
�#<2
AssignMovingAvg/decay�
AssignMovingAvg/ReadVariableOpReadVariableOpassignmovingavg_7693991*
_output_shapes
:0*
dtype02 
AssignMovingAvg/ReadVariableOp�
AssignMovingAvg/subSub&AssignMovingAvg/ReadVariableOp:value:0moments/Squeeze:output:0*
T0**
_class 
loc:@AssignMovingAvg/7693991*
_output_shapes
:02
AssignMovingAvg/sub�
AssignMovingAvg/mulMulAssignMovingAvg/sub:z:0AssignMovingAvg/decay:output:0*
T0**
_class 
loc:@AssignMovingAvg/7693991*
_output_shapes
:02
AssignMovingAvg/mul�
#AssignMovingAvg/AssignSubVariableOpAssignSubVariableOpassignmovingavg_7693991AssignMovingAvg/mul:z:0^AssignMovingAvg/ReadVariableOp**
_class 
loc:@AssignMovingAvg/7693991*
_output_shapes
 *
dtype02%
#AssignMovingAvg/AssignSubVariableOp�
AssignMovingAvg_1/decayConst*,
_class"
 loc:@AssignMovingAvg_1/7693997*
_output_shapes
: *
dtype0*
valueB
 *
�#<2
AssignMovingAvg_1/decay�
 AssignMovingAvg_1/ReadVariableOpReadVariableOpassignmovingavg_1_7693997*
_output_shapes
:0*
dtype02"
 AssignMovingAvg_1/ReadVariableOp�
AssignMovingAvg_1/subSub(AssignMovingAvg_1/ReadVariableOp:value:0moments/Squeeze_1:output:0*
T0*,
_class"
 loc:@AssignMovingAvg_1/7693997*
_output_shapes
:02
AssignMovingAvg_1/sub�
AssignMovingAvg_1/mulMulAssignMovingAvg_1/sub:z:0 AssignMovingAvg_1/decay:output:0*
T0*,
_class"
 loc:@AssignMovingAvg_1/7693997*
_output_shapes
:02
AssignMovingAvg_1/mul�
%AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOpassignmovingavg_1_7693997AssignMovingAvg_1/mul:z:0!^AssignMovingAvg_1/ReadVariableOp*,
_class"
 loc:@AssignMovingAvg_1/7693997*
_output_shapes
 *
dtype02'
%AssignMovingAvg_1/AssignSubVariableOpg
batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2
batchnorm/add/y�
batchnorm/addAddV2moments/Squeeze_1:output:0batchnorm/add/y:output:0*
T0*
_output_shapes
:02
batchnorm/addc
batchnorm/RsqrtRsqrtbatchnorm/add:z:0*
T0*
_output_shapes
:02
batchnorm/Rsqrt�
batchnorm/mul/ReadVariableOpReadVariableOp%batchnorm_mul_readvariableop_resource*
_output_shapes
:0*
dtype02
batchnorm/mul/ReadVariableOp�
batchnorm/mulMulbatchnorm/Rsqrt:y:0$batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes
:02
batchnorm/mulv
batchnorm/mul_1Mulinputsbatchnorm/mul:z:0*
T0*'
_output_shapes
:���������02
batchnorm/mul_1{
batchnorm/mul_2Mulmoments/Squeeze:output:0batchnorm/mul:z:0*
T0*
_output_shapes
:02
batchnorm/mul_2�
batchnorm/ReadVariableOpReadVariableOp!batchnorm_readvariableop_resource*
_output_shapes
:0*
dtype02
batchnorm/ReadVariableOp�
batchnorm/subSub batchnorm/ReadVariableOp:value:0batchnorm/mul_2:z:0*
T0*
_output_shapes
:02
batchnorm/sub�
batchnorm/add_1AddV2batchnorm/mul_1:z:0batchnorm/sub:z:0*
T0*'
_output_shapes
:���������02
batchnorm/add_1�
IdentityIdentitybatchnorm/add_1:z:0$^AssignMovingAvg/AssignSubVariableOp&^AssignMovingAvg_1/AssignSubVariableOp*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*6
_input_shapes%
#:���������0::::2J
#AssignMovingAvg/AssignSubVariableOp#AssignMovingAvg/AssignSubVariableOp2N
%AssignMovingAvg_1/AssignSubVariableOp%AssignMovingAvg_1/AssignSubVariableOp:O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
B__inference_type0_layer_call_and_return_conditional_losses_7693072

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:00*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02	
BiasAddc

elu_56/EluEluBiasAdd:output:0*
T0*'
_output_shapes
:���������02

elu_56/Elul
IdentityIdentityelu_56/Elu:activations:0*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0:::O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
�
B__inference_Input_layer_call_and_return_conditional_losses_7693835

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:00*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02	
BiasAddc

elu_55/EluEluBiasAdd:output:0*
T0*'
_output_shapes
:���������02

elu_55/Elul
IdentityIdentityelu_55/Elu:activations:0*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0:::O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
�
C__inference_Output_layer_call_and_return_conditional_losses_7693241

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:0	*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������	2
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:	*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������	2	
BiasAddd
IdentityIdentityBiasAdd:output:0*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0:::O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�l
�

"__inference__wrapped_model_7692890
input_input@
<type_1_size_5_48_iter_1_input_matmul_readvariableop_resourceA
=type_1_size_5_48_iter_1_input_biasadd_readvariableop_resource@
<type_1_size_5_48_iter_1_type0_matmul_readvariableop_resourceA
=type_1_size_5_48_iter_1_type0_biasadd_readvariableop_resource@
<type_1_size_5_48_iter_1_type1_matmul_readvariableop_resourceA
=type_1_size_5_48_iter_1_type1_biasadd_readvariableop_resource@
<type_1_size_5_48_iter_1_type2_matmul_readvariableop_resourceA
=type_1_size_5_48_iter_1_type2_biasadd_readvariableop_resource@
<type_1_size_5_48_iter_1_type3_matmul_readvariableop_resourceA
=type_1_size_5_48_iter_1_type3_biasadd_readvariableop_resource@
<type_1_size_5_48_iter_1_type4_matmul_readvariableop_resourceA
=type_1_size_5_48_iter_1_type4_biasadd_readvariableop_resourceS
Otype_1_size_5_48_iter_1_batch_normalization_9_batchnorm_readvariableop_resourceW
Stype_1_size_5_48_iter_1_batch_normalization_9_batchnorm_mul_readvariableop_resourceU
Qtype_1_size_5_48_iter_1_batch_normalization_9_batchnorm_readvariableop_1_resourceU
Qtype_1_size_5_48_iter_1_batch_normalization_9_batchnorm_readvariableop_2_resourceA
=type_1_size_5_48_iter_1_output_matmul_readvariableop_resourceB
>type_1_size_5_48_iter_1_output_biasadd_readvariableop_resource
identity��
3type_1_size_5_48_iter_1/Input/MatMul/ReadVariableOpReadVariableOp<type_1_size_5_48_iter_1_input_matmul_readvariableop_resource*
_output_shapes

:00*
dtype025
3type_1_size_5_48_iter_1/Input/MatMul/ReadVariableOp�
$type_1_size_5_48_iter_1/Input/MatMulMatMulinput_input;type_1_size_5_48_iter_1/Input/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02&
$type_1_size_5_48_iter_1/Input/MatMul�
4type_1_size_5_48_iter_1/Input/BiasAdd/ReadVariableOpReadVariableOp=type_1_size_5_48_iter_1_input_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype026
4type_1_size_5_48_iter_1/Input/BiasAdd/ReadVariableOp�
%type_1_size_5_48_iter_1/Input/BiasAddBiasAdd.type_1_size_5_48_iter_1/Input/MatMul:product:0<type_1_size_5_48_iter_1/Input/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02'
%type_1_size_5_48_iter_1/Input/BiasAdd�
(type_1_size_5_48_iter_1/Input/elu_55/EluElu.type_1_size_5_48_iter_1/Input/BiasAdd:output:0*
T0*'
_output_shapes
:���������02*
(type_1_size_5_48_iter_1/Input/elu_55/Elu�
3type_1_size_5_48_iter_1/type0/MatMul/ReadVariableOpReadVariableOp<type_1_size_5_48_iter_1_type0_matmul_readvariableop_resource*
_output_shapes

:00*
dtype025
3type_1_size_5_48_iter_1/type0/MatMul/ReadVariableOp�
$type_1_size_5_48_iter_1/type0/MatMulMatMul6type_1_size_5_48_iter_1/Input/elu_55/Elu:activations:0;type_1_size_5_48_iter_1/type0/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02&
$type_1_size_5_48_iter_1/type0/MatMul�
4type_1_size_5_48_iter_1/type0/BiasAdd/ReadVariableOpReadVariableOp=type_1_size_5_48_iter_1_type0_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype026
4type_1_size_5_48_iter_1/type0/BiasAdd/ReadVariableOp�
%type_1_size_5_48_iter_1/type0/BiasAddBiasAdd.type_1_size_5_48_iter_1/type0/MatMul:product:0<type_1_size_5_48_iter_1/type0/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02'
%type_1_size_5_48_iter_1/type0/BiasAdd�
(type_1_size_5_48_iter_1/type0/elu_56/EluElu.type_1_size_5_48_iter_1/type0/BiasAdd:output:0*
T0*'
_output_shapes
:���������02*
(type_1_size_5_48_iter_1/type0/elu_56/Elu�
3type_1_size_5_48_iter_1/type1/MatMul/ReadVariableOpReadVariableOp<type_1_size_5_48_iter_1_type1_matmul_readvariableop_resource*
_output_shapes

:00*
dtype025
3type_1_size_5_48_iter_1/type1/MatMul/ReadVariableOp�
$type_1_size_5_48_iter_1/type1/MatMulMatMul6type_1_size_5_48_iter_1/type0/elu_56/Elu:activations:0;type_1_size_5_48_iter_1/type1/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02&
$type_1_size_5_48_iter_1/type1/MatMul�
4type_1_size_5_48_iter_1/type1/BiasAdd/ReadVariableOpReadVariableOp=type_1_size_5_48_iter_1_type1_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype026
4type_1_size_5_48_iter_1/type1/BiasAdd/ReadVariableOp�
%type_1_size_5_48_iter_1/type1/BiasAddBiasAdd.type_1_size_5_48_iter_1/type1/MatMul:product:0<type_1_size_5_48_iter_1/type1/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02'
%type_1_size_5_48_iter_1/type1/BiasAdd�
(type_1_size_5_48_iter_1/type1/elu_57/EluElu.type_1_size_5_48_iter_1/type1/BiasAdd:output:0*
T0*'
_output_shapes
:���������02*
(type_1_size_5_48_iter_1/type1/elu_57/Elu�
3type_1_size_5_48_iter_1/type2/MatMul/ReadVariableOpReadVariableOp<type_1_size_5_48_iter_1_type2_matmul_readvariableop_resource*
_output_shapes

:00*
dtype025
3type_1_size_5_48_iter_1/type2/MatMul/ReadVariableOp�
$type_1_size_5_48_iter_1/type2/MatMulMatMul6type_1_size_5_48_iter_1/type1/elu_57/Elu:activations:0;type_1_size_5_48_iter_1/type2/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02&
$type_1_size_5_48_iter_1/type2/MatMul�
4type_1_size_5_48_iter_1/type2/BiasAdd/ReadVariableOpReadVariableOp=type_1_size_5_48_iter_1_type2_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype026
4type_1_size_5_48_iter_1/type2/BiasAdd/ReadVariableOp�
%type_1_size_5_48_iter_1/type2/BiasAddBiasAdd.type_1_size_5_48_iter_1/type2/MatMul:product:0<type_1_size_5_48_iter_1/type2/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02'
%type_1_size_5_48_iter_1/type2/BiasAdd�
(type_1_size_5_48_iter_1/type2/elu_58/EluElu.type_1_size_5_48_iter_1/type2/BiasAdd:output:0*
T0*'
_output_shapes
:���������02*
(type_1_size_5_48_iter_1/type2/elu_58/Elu�
3type_1_size_5_48_iter_1/type3/MatMul/ReadVariableOpReadVariableOp<type_1_size_5_48_iter_1_type3_matmul_readvariableop_resource*
_output_shapes

:00*
dtype025
3type_1_size_5_48_iter_1/type3/MatMul/ReadVariableOp�
$type_1_size_5_48_iter_1/type3/MatMulMatMul6type_1_size_5_48_iter_1/type2/elu_58/Elu:activations:0;type_1_size_5_48_iter_1/type3/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02&
$type_1_size_5_48_iter_1/type3/MatMul�
4type_1_size_5_48_iter_1/type3/BiasAdd/ReadVariableOpReadVariableOp=type_1_size_5_48_iter_1_type3_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype026
4type_1_size_5_48_iter_1/type3/BiasAdd/ReadVariableOp�
%type_1_size_5_48_iter_1/type3/BiasAddBiasAdd.type_1_size_5_48_iter_1/type3/MatMul:product:0<type_1_size_5_48_iter_1/type3/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02'
%type_1_size_5_48_iter_1/type3/BiasAdd�
(type_1_size_5_48_iter_1/type3/elu_59/EluElu.type_1_size_5_48_iter_1/type3/BiasAdd:output:0*
T0*'
_output_shapes
:���������02*
(type_1_size_5_48_iter_1/type3/elu_59/Elu�
3type_1_size_5_48_iter_1/type4/MatMul/ReadVariableOpReadVariableOp<type_1_size_5_48_iter_1_type4_matmul_readvariableop_resource*
_output_shapes

:00*
dtype025
3type_1_size_5_48_iter_1/type4/MatMul/ReadVariableOp�
$type_1_size_5_48_iter_1/type4/MatMulMatMul6type_1_size_5_48_iter_1/type3/elu_59/Elu:activations:0;type_1_size_5_48_iter_1/type4/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02&
$type_1_size_5_48_iter_1/type4/MatMul�
4type_1_size_5_48_iter_1/type4/BiasAdd/ReadVariableOpReadVariableOp=type_1_size_5_48_iter_1_type4_biasadd_readvariableop_resource*
_output_shapes
:0*
dtype026
4type_1_size_5_48_iter_1/type4/BiasAdd/ReadVariableOp�
%type_1_size_5_48_iter_1/type4/BiasAddBiasAdd.type_1_size_5_48_iter_1/type4/MatMul:product:0<type_1_size_5_48_iter_1/type4/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02'
%type_1_size_5_48_iter_1/type4/BiasAdd�
(type_1_size_5_48_iter_1/type4/elu_60/EluElu.type_1_size_5_48_iter_1/type4/BiasAdd:output:0*
T0*'
_output_shapes
:���������02*
(type_1_size_5_48_iter_1/type4/elu_60/Elu�
Ftype_1_size_5_48_iter_1/batch_normalization_9/batchnorm/ReadVariableOpReadVariableOpOtype_1_size_5_48_iter_1_batch_normalization_9_batchnorm_readvariableop_resource*
_output_shapes
:0*
dtype02H
Ftype_1_size_5_48_iter_1/batch_normalization_9/batchnorm/ReadVariableOp�
=type_1_size_5_48_iter_1/batch_normalization_9/batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2?
=type_1_size_5_48_iter_1/batch_normalization_9/batchnorm/add/y�
;type_1_size_5_48_iter_1/batch_normalization_9/batchnorm/addAddV2Ntype_1_size_5_48_iter_1/batch_normalization_9/batchnorm/ReadVariableOp:value:0Ftype_1_size_5_48_iter_1/batch_normalization_9/batchnorm/add/y:output:0*
T0*
_output_shapes
:02=
;type_1_size_5_48_iter_1/batch_normalization_9/batchnorm/add�
=type_1_size_5_48_iter_1/batch_normalization_9/batchnorm/RsqrtRsqrt?type_1_size_5_48_iter_1/batch_normalization_9/batchnorm/add:z:0*
T0*
_output_shapes
:02?
=type_1_size_5_48_iter_1/batch_normalization_9/batchnorm/Rsqrt�
Jtype_1_size_5_48_iter_1/batch_normalization_9/batchnorm/mul/ReadVariableOpReadVariableOpStype_1_size_5_48_iter_1_batch_normalization_9_batchnorm_mul_readvariableop_resource*
_output_shapes
:0*
dtype02L
Jtype_1_size_5_48_iter_1/batch_normalization_9/batchnorm/mul/ReadVariableOp�
;type_1_size_5_48_iter_1/batch_normalization_9/batchnorm/mulMulAtype_1_size_5_48_iter_1/batch_normalization_9/batchnorm/Rsqrt:y:0Rtype_1_size_5_48_iter_1/batch_normalization_9/batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes
:02=
;type_1_size_5_48_iter_1/batch_normalization_9/batchnorm/mul�
=type_1_size_5_48_iter_1/batch_normalization_9/batchnorm/mul_1Mul6type_1_size_5_48_iter_1/type4/elu_60/Elu:activations:0?type_1_size_5_48_iter_1/batch_normalization_9/batchnorm/mul:z:0*
T0*'
_output_shapes
:���������02?
=type_1_size_5_48_iter_1/batch_normalization_9/batchnorm/mul_1�
Htype_1_size_5_48_iter_1/batch_normalization_9/batchnorm/ReadVariableOp_1ReadVariableOpQtype_1_size_5_48_iter_1_batch_normalization_9_batchnorm_readvariableop_1_resource*
_output_shapes
:0*
dtype02J
Htype_1_size_5_48_iter_1/batch_normalization_9/batchnorm/ReadVariableOp_1�
=type_1_size_5_48_iter_1/batch_normalization_9/batchnorm/mul_2MulPtype_1_size_5_48_iter_1/batch_normalization_9/batchnorm/ReadVariableOp_1:value:0?type_1_size_5_48_iter_1/batch_normalization_9/batchnorm/mul:z:0*
T0*
_output_shapes
:02?
=type_1_size_5_48_iter_1/batch_normalization_9/batchnorm/mul_2�
Htype_1_size_5_48_iter_1/batch_normalization_9/batchnorm/ReadVariableOp_2ReadVariableOpQtype_1_size_5_48_iter_1_batch_normalization_9_batchnorm_readvariableop_2_resource*
_output_shapes
:0*
dtype02J
Htype_1_size_5_48_iter_1/batch_normalization_9/batchnorm/ReadVariableOp_2�
;type_1_size_5_48_iter_1/batch_normalization_9/batchnorm/subSubPtype_1_size_5_48_iter_1/batch_normalization_9/batchnorm/ReadVariableOp_2:value:0Atype_1_size_5_48_iter_1/batch_normalization_9/batchnorm/mul_2:z:0*
T0*
_output_shapes
:02=
;type_1_size_5_48_iter_1/batch_normalization_9/batchnorm/sub�
=type_1_size_5_48_iter_1/batch_normalization_9/batchnorm/add_1AddV2Atype_1_size_5_48_iter_1/batch_normalization_9/batchnorm/mul_1:z:0?type_1_size_5_48_iter_1/batch_normalization_9/batchnorm/sub:z:0*
T0*'
_output_shapes
:���������02?
=type_1_size_5_48_iter_1/batch_normalization_9/batchnorm/add_1�
4type_1_size_5_48_iter_1/Output/MatMul/ReadVariableOpReadVariableOp=type_1_size_5_48_iter_1_output_matmul_readvariableop_resource*
_output_shapes

:0	*
dtype026
4type_1_size_5_48_iter_1/Output/MatMul/ReadVariableOp�
%type_1_size_5_48_iter_1/Output/MatMulMatMulAtype_1_size_5_48_iter_1/batch_normalization_9/batchnorm/add_1:z:0<type_1_size_5_48_iter_1/Output/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������	2'
%type_1_size_5_48_iter_1/Output/MatMul�
5type_1_size_5_48_iter_1/Output/BiasAdd/ReadVariableOpReadVariableOp>type_1_size_5_48_iter_1_output_biasadd_readvariableop_resource*
_output_shapes
:	*
dtype027
5type_1_size_5_48_iter_1/Output/BiasAdd/ReadVariableOp�
&type_1_size_5_48_iter_1/Output/BiasAddBiasAdd/type_1_size_5_48_iter_1/Output/MatMul:product:0=type_1_size_5_48_iter_1/Output/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������	2(
&type_1_size_5_48_iter_1/Output/BiasAdd�
)type_1_size_5_48_iter_1/softmax_9/SoftmaxSoftmax/type_1_size_5_48_iter_1/Output/BiasAdd:output:0*
T0*'
_output_shapes
:���������	2+
)type_1_size_5_48_iter_1/softmax_9/Softmax�
IdentityIdentity3type_1_size_5_48_iter_1/softmax_9/Softmax:softmax:0*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*n
_input_shapes]
[:���������0:::::::::::::::::::T P
'
_output_shapes
:���������0
%
_user_specified_nameInput_input:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
B__inference_type2_layer_call_and_return_conditional_losses_7693126

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:00*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02	
BiasAddc

elu_58/EluEluBiasAdd:output:0*
T0*'
_output_shapes
:���������02

elu_58/Elul
IdentityIdentityelu_58/Elu:activations:0*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0:::O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
|
'__inference_type2_layer_call_fn_7693904

inputs
unknown
	unknown_0
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_type2_layer_call_and_return_conditional_losses_76931262
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
|
'__inference_type3_layer_call_fn_7693924

inputs
unknown
	unknown_0
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_type3_layer_call_and_return_conditional_losses_76931532
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
�
9__inference_type_1_size_5_48_iter_1_layer_call_fn_7693501
input_input
unknown
	unknown_0
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
	unknown_7
	unknown_8
	unknown_9

unknown_10

unknown_11

unknown_12

unknown_13

unknown_14

unknown_15

unknown_16
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16*
Tin
2*
Tout
2*'
_output_shapes
:���������	*4
_read_only_resource_inputs
	
**
config_proto

CPU

GPU 2J 8*]
fXRV
T__inference_type_1_size_5_48_iter_1_layer_call_and_return_conditional_losses_76934622
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*n
_input_shapes]
[:���������0::::::::::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:T P
'
_output_shapes
:���������0
%
_user_specified_nameInput_input:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
9__inference_type_1_size_5_48_iter_1_layer_call_fn_7693411
input_input
unknown
	unknown_0
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
	unknown_7
	unknown_8
	unknown_9

unknown_10

unknown_11

unknown_12

unknown_13

unknown_14

unknown_15

unknown_16
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16*
Tin
2*
Tout
2*'
_output_shapes
:���������	*2
_read_only_resource_inputs
	
**
config_proto

CPU

GPU 2J 8*]
fXRV
T__inference_type_1_size_5_48_iter_1_layer_call_and_return_conditional_losses_76933722
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*n
_input_shapes]
[:���������0::::::::::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:T P
'
_output_shapes
:���������0
%
_user_specified_nameInput_input:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
%__inference_signature_wrapper_7693588
input_input
unknown
	unknown_0
	unknown_1
	unknown_2
	unknown_3
	unknown_4
	unknown_5
	unknown_6
	unknown_7
	unknown_8
	unknown_9

unknown_10

unknown_11

unknown_12

unknown_13

unknown_14

unknown_15

unknown_16
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16*
Tin
2*
Tout
2*'
_output_shapes
:���������	*4
_read_only_resource_inputs
	
**
config_proto

CPU

GPU 2J 8*+
f&R$
"__inference__wrapped_model_76928902
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*n
_input_shapes]
[:���������0::::::::::::::::::22
StatefulPartitionedCallStatefulPartitionedCall:T P
'
_output_shapes
:���������0
%
_user_specified_nameInput_input:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
B__inference_type4_layer_call_and_return_conditional_losses_7693935

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:00*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02	
BiasAddc

elu_60/EluEluBiasAdd:output:0*
T0*'
_output_shapes
:���������02

elu_60/Elul
IdentityIdentityelu_60/Elu:activations:0*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0:::O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
�
7__inference_batch_normalization_9_layer_call_fn_7694049

inputs
unknown
	unknown_0
	unknown_1
	unknown_2
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2*
Tin	
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*[
fVRT
R__inference_batch_normalization_9_layer_call_and_return_conditional_losses_76929862
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*6
_input_shapes%
#:���������0::::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�/
�
T__inference_type_1_size_5_48_iter_1_layer_call_and_return_conditional_losses_7693372

inputs
input_7693326
input_7693328
type0_7693331
type0_7693333
type1_7693336
type1_7693338
type2_7693341
type2_7693343
type3_7693346
type3_7693348
type4_7693351
type4_7693353!
batch_normalization_9_7693356!
batch_normalization_9_7693358!
batch_normalization_9_7693360!
batch_normalization_9_7693362
output_7693365
output_7693367
identity��Input/StatefulPartitionedCall�Output/StatefulPartitionedCall�-batch_normalization_9/StatefulPartitionedCall�type0/StatefulPartitionedCall�type1/StatefulPartitionedCall�type2/StatefulPartitionedCall�type3/StatefulPartitionedCall�type4/StatefulPartitionedCall�
Input/StatefulPartitionedCallStatefulPartitionedCallinputsinput_7693326input_7693328*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_Input_layer_call_and_return_conditional_losses_76930452
Input/StatefulPartitionedCall�
type0/StatefulPartitionedCallStatefulPartitionedCall&Input/StatefulPartitionedCall:output:0type0_7693331type0_7693333*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_type0_layer_call_and_return_conditional_losses_76930722
type0/StatefulPartitionedCall�
type1/StatefulPartitionedCallStatefulPartitionedCall&type0/StatefulPartitionedCall:output:0type1_7693336type1_7693338*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_type1_layer_call_and_return_conditional_losses_76930992
type1/StatefulPartitionedCall�
type2/StatefulPartitionedCallStatefulPartitionedCall&type1/StatefulPartitionedCall:output:0type2_7693341type2_7693343*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_type2_layer_call_and_return_conditional_losses_76931262
type2/StatefulPartitionedCall�
type3/StatefulPartitionedCallStatefulPartitionedCall&type2/StatefulPartitionedCall:output:0type3_7693346type3_7693348*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_type3_layer_call_and_return_conditional_losses_76931532
type3/StatefulPartitionedCall�
type4/StatefulPartitionedCallStatefulPartitionedCall&type3/StatefulPartitionedCall:output:0type4_7693351type4_7693353*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_type4_layer_call_and_return_conditional_losses_76931802
type4/StatefulPartitionedCall�
-batch_normalization_9/StatefulPartitionedCallStatefulPartitionedCall&type4/StatefulPartitionedCall:output:0batch_normalization_9_7693356batch_normalization_9_7693358batch_normalization_9_7693360batch_normalization_9_7693362*
Tin	
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*[
fVRT
R__inference_batch_normalization_9_layer_call_and_return_conditional_losses_76929862/
-batch_normalization_9/StatefulPartitionedCall�
Output/StatefulPartitionedCallStatefulPartitionedCall6batch_normalization_9/StatefulPartitionedCall:output:0output_7693365output_7693367*
Tin
2*
Tout
2*'
_output_shapes
:���������	*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*L
fGRE
C__inference_Output_layer_call_and_return_conditional_losses_76932412 
Output/StatefulPartitionedCall�
softmax_9/PartitionedCallPartitionedCall'Output/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*'
_output_shapes
:���������	* 
_read_only_resource_inputs
 **
config_proto

CPU

GPU 2J 8*O
fJRH
F__inference_softmax_9_layer_call_and_return_conditional_losses_76932622
softmax_9/PartitionedCall�
IdentityIdentity"softmax_9/PartitionedCall:output:0^Input/StatefulPartitionedCall^Output/StatefulPartitionedCall.^batch_normalization_9/StatefulPartitionedCall^type0/StatefulPartitionedCall^type1/StatefulPartitionedCall^type2/StatefulPartitionedCall^type3/StatefulPartitionedCall^type4/StatefulPartitionedCall*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*n
_input_shapes]
[:���������0::::::::::::::::::2>
Input/StatefulPartitionedCallInput/StatefulPartitionedCall2@
Output/StatefulPartitionedCallOutput/StatefulPartitionedCall2^
-batch_normalization_9/StatefulPartitionedCall-batch_normalization_9/StatefulPartitionedCall2>
type0/StatefulPartitionedCalltype0/StatefulPartitionedCall2>
type1/StatefulPartitionedCalltype1/StatefulPartitionedCall2>
type2/StatefulPartitionedCalltype2/StatefulPartitionedCall2>
type3/StatefulPartitionedCalltype3/StatefulPartitionedCall2>
type4/StatefulPartitionedCalltype4/StatefulPartitionedCall:O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
B__inference_type2_layer_call_and_return_conditional_losses_7693895

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:00*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02	
BiasAddc

elu_58/EluEluBiasAdd:output:0*
T0*'
_output_shapes
:���������02

elu_58/Elul
IdentityIdentityelu_58/Elu:activations:0*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0:::O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
�
7__inference_batch_normalization_9_layer_call_fn_7694062

inputs
unknown
	unknown_0
	unknown_1
	unknown_2
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2*
Tin	
2*
Tout
2*'
_output_shapes
:���������0*&
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*[
fVRT
R__inference_batch_normalization_9_layer_call_and_return_conditional_losses_76930192
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*6
_input_shapes%
#:���������0::::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
R__inference_batch_normalization_9_layer_call_and_return_conditional_losses_7694036

inputs%
!batchnorm_readvariableop_resource)
%batchnorm_mul_readvariableop_resource'
#batchnorm_readvariableop_1_resource'
#batchnorm_readvariableop_2_resource
identity��
batchnorm/ReadVariableOpReadVariableOp!batchnorm_readvariableop_resource*
_output_shapes
:0*
dtype02
batchnorm/ReadVariableOpg
batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2
batchnorm/add/y�
batchnorm/addAddV2 batchnorm/ReadVariableOp:value:0batchnorm/add/y:output:0*
T0*
_output_shapes
:02
batchnorm/addc
batchnorm/RsqrtRsqrtbatchnorm/add:z:0*
T0*
_output_shapes
:02
batchnorm/Rsqrt�
batchnorm/mul/ReadVariableOpReadVariableOp%batchnorm_mul_readvariableop_resource*
_output_shapes
:0*
dtype02
batchnorm/mul/ReadVariableOp�
batchnorm/mulMulbatchnorm/Rsqrt:y:0$batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes
:02
batchnorm/mulv
batchnorm/mul_1Mulinputsbatchnorm/mul:z:0*
T0*'
_output_shapes
:���������02
batchnorm/mul_1�
batchnorm/ReadVariableOp_1ReadVariableOp#batchnorm_readvariableop_1_resource*
_output_shapes
:0*
dtype02
batchnorm/ReadVariableOp_1�
batchnorm/mul_2Mul"batchnorm/ReadVariableOp_1:value:0batchnorm/mul:z:0*
T0*
_output_shapes
:02
batchnorm/mul_2�
batchnorm/ReadVariableOp_2ReadVariableOp#batchnorm_readvariableop_2_resource*
_output_shapes
:0*
dtype02
batchnorm/ReadVariableOp_2�
batchnorm/subSub"batchnorm/ReadVariableOp_2:value:0batchnorm/mul_2:z:0*
T0*
_output_shapes
:02
batchnorm/sub�
batchnorm/add_1AddV2batchnorm/mul_1:z:0batchnorm/sub:z:0*
T0*'
_output_shapes
:���������02
batchnorm/add_1g
IdentityIdentitybatchnorm/add_1:z:0*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*6
_input_shapes%
#:���������0:::::O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
b
F__inference_softmax_9_layer_call_and_return_conditional_losses_7693262

inputs
identityW
SoftmaxSoftmaxinputs*
T0*'
_output_shapes
:���������	2	
Softmaxe
IdentityIdentitySoftmax:softmax:0*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*&
_input_shapes
:���������	:O K
'
_output_shapes
:���������	
 
_user_specified_nameinputs
�
}
(__inference_Output_layer_call_fn_7694081

inputs
unknown
	unknown_0
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*'
_output_shapes
:���������	*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*L
fGRE
C__inference_Output_layer_call_and_return_conditional_losses_76932412
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
b
F__inference_softmax_9_layer_call_and_return_conditional_losses_7694086

inputs
identityW
SoftmaxSoftmaxinputs*
T0*'
_output_shapes
:���������	2	
Softmaxe
IdentityIdentitySoftmax:softmax:0*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*&
_input_shapes
:���������	:O K
'
_output_shapes
:���������	
 
_user_specified_nameinputs
܄
� 
#__inference__traced_restore_7694496
file_prefix#
assignvariableop_input_7_kernel#
assignvariableop_1_input_7_bias%
!assignvariableop_2_type0_7_kernel#
assignvariableop_3_type0_7_bias%
!assignvariableop_4_type1_7_kernel#
assignvariableop_5_type1_7_bias%
!assignvariableop_6_type2_7_kernel#
assignvariableop_7_type2_7_bias%
!assignvariableop_8_type3_7_kernel#
assignvariableop_9_type3_7_bias&
"assignvariableop_10_type4_7_kernel$
 assignvariableop_11_type4_7_bias3
/assignvariableop_12_batch_normalization_9_gamma2
.assignvariableop_13_batch_normalization_9_beta9
5assignvariableop_14_batch_normalization_9_moving_mean=
9assignvariableop_15_batch_normalization_9_moving_variance'
#assignvariableop_16_output_9_kernel%
!assignvariableop_17_output_9_bias!
assignvariableop_18_adam_iter#
assignvariableop_19_adam_beta_1#
assignvariableop_20_adam_beta_2"
assignvariableop_21_adam_decay*
&assignvariableop_22_adam_learning_rate
assignvariableop_23_total
assignvariableop_24_count
assignvariableop_25_total_1
assignvariableop_26_count_1&
"assignvariableop_27_true_positives'
#assignvariableop_28_false_positives-
)assignvariableop_29_adam_input_7_kernel_m+
'assignvariableop_30_adam_input_7_bias_m-
)assignvariableop_31_adam_type0_7_kernel_m+
'assignvariableop_32_adam_type0_7_bias_m-
)assignvariableop_33_adam_type1_7_kernel_m+
'assignvariableop_34_adam_type1_7_bias_m-
)assignvariableop_35_adam_type2_7_kernel_m+
'assignvariableop_36_adam_type2_7_bias_m-
)assignvariableop_37_adam_type3_7_kernel_m+
'assignvariableop_38_adam_type3_7_bias_m-
)assignvariableop_39_adam_type4_7_kernel_m+
'assignvariableop_40_adam_type4_7_bias_m:
6assignvariableop_41_adam_batch_normalization_9_gamma_m9
5assignvariableop_42_adam_batch_normalization_9_beta_m.
*assignvariableop_43_adam_output_9_kernel_m,
(assignvariableop_44_adam_output_9_bias_m-
)assignvariableop_45_adam_input_7_kernel_v+
'assignvariableop_46_adam_input_7_bias_v-
)assignvariableop_47_adam_type0_7_kernel_v+
'assignvariableop_48_adam_type0_7_bias_v-
)assignvariableop_49_adam_type1_7_kernel_v+
'assignvariableop_50_adam_type1_7_bias_v-
)assignvariableop_51_adam_type2_7_kernel_v+
'assignvariableop_52_adam_type2_7_bias_v-
)assignvariableop_53_adam_type3_7_kernel_v+
'assignvariableop_54_adam_type3_7_bias_v-
)assignvariableop_55_adam_type4_7_kernel_v+
'assignvariableop_56_adam_type4_7_bias_v:
6assignvariableop_57_adam_batch_normalization_9_gamma_v9
5assignvariableop_58_adam_batch_normalization_9_beta_v.
*assignvariableop_59_adam_output_9_kernel_v,
(assignvariableop_60_adam_output_9_bias_v
identity_62��AssignVariableOp�AssignVariableOp_1�AssignVariableOp_10�AssignVariableOp_11�AssignVariableOp_12�AssignVariableOp_13�AssignVariableOp_14�AssignVariableOp_15�AssignVariableOp_16�AssignVariableOp_17�AssignVariableOp_18�AssignVariableOp_19�AssignVariableOp_2�AssignVariableOp_20�AssignVariableOp_21�AssignVariableOp_22�AssignVariableOp_23�AssignVariableOp_24�AssignVariableOp_25�AssignVariableOp_26�AssignVariableOp_27�AssignVariableOp_28�AssignVariableOp_29�AssignVariableOp_3�AssignVariableOp_30�AssignVariableOp_31�AssignVariableOp_32�AssignVariableOp_33�AssignVariableOp_34�AssignVariableOp_35�AssignVariableOp_36�AssignVariableOp_37�AssignVariableOp_38�AssignVariableOp_39�AssignVariableOp_4�AssignVariableOp_40�AssignVariableOp_41�AssignVariableOp_42�AssignVariableOp_43�AssignVariableOp_44�AssignVariableOp_45�AssignVariableOp_46�AssignVariableOp_47�AssignVariableOp_48�AssignVariableOp_49�AssignVariableOp_5�AssignVariableOp_50�AssignVariableOp_51�AssignVariableOp_52�AssignVariableOp_53�AssignVariableOp_54�AssignVariableOp_55�AssignVariableOp_56�AssignVariableOp_57�AssignVariableOp_58�AssignVariableOp_59�AssignVariableOp_6�AssignVariableOp_60�AssignVariableOp_7�AssignVariableOp_8�AssignVariableOp_9�	RestoreV2�RestoreV2_1�"
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:=*
dtype0*�!
value�!B�!=B6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUEB5layer_with_weights-6/gamma/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-6/beta/.ATTRIBUTES/VARIABLE_VALUEB;layer_with_weights-6/moving_mean/.ATTRIBUTES/VARIABLE_VALUEB?layer_with_weights-6/moving_variance/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEB=keras_api/metrics/2/true_positives/.ATTRIBUTES/VARIABLE_VALUEB>keras_api/metrics/2/false_positives/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-5/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-5/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-6/gamma/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-6/beta/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-7/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-7/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-0/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-0/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-3/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-3/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-4/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-4/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-5/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-5/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBQlayer_with_weights-6/gamma/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-6/beta/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-7/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-7/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE2
RestoreV2/tensor_names�
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:=*
dtype0*�
value�B�=B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B 2
RestoreV2/shape_and_slices�
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*�
_output_shapes�
�:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*K
dtypesA
?2=	2
	RestoreV2X
IdentityIdentityRestoreV2:tensors:0*
T0*
_output_shapes
:2

Identity�
AssignVariableOpAssignVariableOpassignvariableop_input_7_kernelIdentity:output:0*
_output_shapes
 *
dtype02
AssignVariableOp\

Identity_1IdentityRestoreV2:tensors:1*
T0*
_output_shapes
:2

Identity_1�
AssignVariableOp_1AssignVariableOpassignvariableop_1_input_7_biasIdentity_1:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_1\

Identity_2IdentityRestoreV2:tensors:2*
T0*
_output_shapes
:2

Identity_2�
AssignVariableOp_2AssignVariableOp!assignvariableop_2_type0_7_kernelIdentity_2:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_2\

Identity_3IdentityRestoreV2:tensors:3*
T0*
_output_shapes
:2

Identity_3�
AssignVariableOp_3AssignVariableOpassignvariableop_3_type0_7_biasIdentity_3:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_3\

Identity_4IdentityRestoreV2:tensors:4*
T0*
_output_shapes
:2

Identity_4�
AssignVariableOp_4AssignVariableOp!assignvariableop_4_type1_7_kernelIdentity_4:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_4\

Identity_5IdentityRestoreV2:tensors:5*
T0*
_output_shapes
:2

Identity_5�
AssignVariableOp_5AssignVariableOpassignvariableop_5_type1_7_biasIdentity_5:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_5\

Identity_6IdentityRestoreV2:tensors:6*
T0*
_output_shapes
:2

Identity_6�
AssignVariableOp_6AssignVariableOp!assignvariableop_6_type2_7_kernelIdentity_6:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_6\

Identity_7IdentityRestoreV2:tensors:7*
T0*
_output_shapes
:2

Identity_7�
AssignVariableOp_7AssignVariableOpassignvariableop_7_type2_7_biasIdentity_7:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_7\

Identity_8IdentityRestoreV2:tensors:8*
T0*
_output_shapes
:2

Identity_8�
AssignVariableOp_8AssignVariableOp!assignvariableop_8_type3_7_kernelIdentity_8:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_8\

Identity_9IdentityRestoreV2:tensors:9*
T0*
_output_shapes
:2

Identity_9�
AssignVariableOp_9AssignVariableOpassignvariableop_9_type3_7_biasIdentity_9:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_9_
Identity_10IdentityRestoreV2:tensors:10*
T0*
_output_shapes
:2
Identity_10�
AssignVariableOp_10AssignVariableOp"assignvariableop_10_type4_7_kernelIdentity_10:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_10_
Identity_11IdentityRestoreV2:tensors:11*
T0*
_output_shapes
:2
Identity_11�
AssignVariableOp_11AssignVariableOp assignvariableop_11_type4_7_biasIdentity_11:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_11_
Identity_12IdentityRestoreV2:tensors:12*
T0*
_output_shapes
:2
Identity_12�
AssignVariableOp_12AssignVariableOp/assignvariableop_12_batch_normalization_9_gammaIdentity_12:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_12_
Identity_13IdentityRestoreV2:tensors:13*
T0*
_output_shapes
:2
Identity_13�
AssignVariableOp_13AssignVariableOp.assignvariableop_13_batch_normalization_9_betaIdentity_13:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_13_
Identity_14IdentityRestoreV2:tensors:14*
T0*
_output_shapes
:2
Identity_14�
AssignVariableOp_14AssignVariableOp5assignvariableop_14_batch_normalization_9_moving_meanIdentity_14:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_14_
Identity_15IdentityRestoreV2:tensors:15*
T0*
_output_shapes
:2
Identity_15�
AssignVariableOp_15AssignVariableOp9assignvariableop_15_batch_normalization_9_moving_varianceIdentity_15:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_15_
Identity_16IdentityRestoreV2:tensors:16*
T0*
_output_shapes
:2
Identity_16�
AssignVariableOp_16AssignVariableOp#assignvariableop_16_output_9_kernelIdentity_16:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_16_
Identity_17IdentityRestoreV2:tensors:17*
T0*
_output_shapes
:2
Identity_17�
AssignVariableOp_17AssignVariableOp!assignvariableop_17_output_9_biasIdentity_17:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_17_
Identity_18IdentityRestoreV2:tensors:18*
T0	*
_output_shapes
:2
Identity_18�
AssignVariableOp_18AssignVariableOpassignvariableop_18_adam_iterIdentity_18:output:0*
_output_shapes
 *
dtype0	2
AssignVariableOp_18_
Identity_19IdentityRestoreV2:tensors:19*
T0*
_output_shapes
:2
Identity_19�
AssignVariableOp_19AssignVariableOpassignvariableop_19_adam_beta_1Identity_19:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_19_
Identity_20IdentityRestoreV2:tensors:20*
T0*
_output_shapes
:2
Identity_20�
AssignVariableOp_20AssignVariableOpassignvariableop_20_adam_beta_2Identity_20:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_20_
Identity_21IdentityRestoreV2:tensors:21*
T0*
_output_shapes
:2
Identity_21�
AssignVariableOp_21AssignVariableOpassignvariableop_21_adam_decayIdentity_21:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_21_
Identity_22IdentityRestoreV2:tensors:22*
T0*
_output_shapes
:2
Identity_22�
AssignVariableOp_22AssignVariableOp&assignvariableop_22_adam_learning_rateIdentity_22:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_22_
Identity_23IdentityRestoreV2:tensors:23*
T0*
_output_shapes
:2
Identity_23�
AssignVariableOp_23AssignVariableOpassignvariableop_23_totalIdentity_23:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_23_
Identity_24IdentityRestoreV2:tensors:24*
T0*
_output_shapes
:2
Identity_24�
AssignVariableOp_24AssignVariableOpassignvariableop_24_countIdentity_24:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_24_
Identity_25IdentityRestoreV2:tensors:25*
T0*
_output_shapes
:2
Identity_25�
AssignVariableOp_25AssignVariableOpassignvariableop_25_total_1Identity_25:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_25_
Identity_26IdentityRestoreV2:tensors:26*
T0*
_output_shapes
:2
Identity_26�
AssignVariableOp_26AssignVariableOpassignvariableop_26_count_1Identity_26:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_26_
Identity_27IdentityRestoreV2:tensors:27*
T0*
_output_shapes
:2
Identity_27�
AssignVariableOp_27AssignVariableOp"assignvariableop_27_true_positivesIdentity_27:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_27_
Identity_28IdentityRestoreV2:tensors:28*
T0*
_output_shapes
:2
Identity_28�
AssignVariableOp_28AssignVariableOp#assignvariableop_28_false_positivesIdentity_28:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_28_
Identity_29IdentityRestoreV2:tensors:29*
T0*
_output_shapes
:2
Identity_29�
AssignVariableOp_29AssignVariableOp)assignvariableop_29_adam_input_7_kernel_mIdentity_29:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_29_
Identity_30IdentityRestoreV2:tensors:30*
T0*
_output_shapes
:2
Identity_30�
AssignVariableOp_30AssignVariableOp'assignvariableop_30_adam_input_7_bias_mIdentity_30:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_30_
Identity_31IdentityRestoreV2:tensors:31*
T0*
_output_shapes
:2
Identity_31�
AssignVariableOp_31AssignVariableOp)assignvariableop_31_adam_type0_7_kernel_mIdentity_31:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_31_
Identity_32IdentityRestoreV2:tensors:32*
T0*
_output_shapes
:2
Identity_32�
AssignVariableOp_32AssignVariableOp'assignvariableop_32_adam_type0_7_bias_mIdentity_32:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_32_
Identity_33IdentityRestoreV2:tensors:33*
T0*
_output_shapes
:2
Identity_33�
AssignVariableOp_33AssignVariableOp)assignvariableop_33_adam_type1_7_kernel_mIdentity_33:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_33_
Identity_34IdentityRestoreV2:tensors:34*
T0*
_output_shapes
:2
Identity_34�
AssignVariableOp_34AssignVariableOp'assignvariableop_34_adam_type1_7_bias_mIdentity_34:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_34_
Identity_35IdentityRestoreV2:tensors:35*
T0*
_output_shapes
:2
Identity_35�
AssignVariableOp_35AssignVariableOp)assignvariableop_35_adam_type2_7_kernel_mIdentity_35:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_35_
Identity_36IdentityRestoreV2:tensors:36*
T0*
_output_shapes
:2
Identity_36�
AssignVariableOp_36AssignVariableOp'assignvariableop_36_adam_type2_7_bias_mIdentity_36:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_36_
Identity_37IdentityRestoreV2:tensors:37*
T0*
_output_shapes
:2
Identity_37�
AssignVariableOp_37AssignVariableOp)assignvariableop_37_adam_type3_7_kernel_mIdentity_37:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_37_
Identity_38IdentityRestoreV2:tensors:38*
T0*
_output_shapes
:2
Identity_38�
AssignVariableOp_38AssignVariableOp'assignvariableop_38_adam_type3_7_bias_mIdentity_38:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_38_
Identity_39IdentityRestoreV2:tensors:39*
T0*
_output_shapes
:2
Identity_39�
AssignVariableOp_39AssignVariableOp)assignvariableop_39_adam_type4_7_kernel_mIdentity_39:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_39_
Identity_40IdentityRestoreV2:tensors:40*
T0*
_output_shapes
:2
Identity_40�
AssignVariableOp_40AssignVariableOp'assignvariableop_40_adam_type4_7_bias_mIdentity_40:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_40_
Identity_41IdentityRestoreV2:tensors:41*
T0*
_output_shapes
:2
Identity_41�
AssignVariableOp_41AssignVariableOp6assignvariableop_41_adam_batch_normalization_9_gamma_mIdentity_41:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_41_
Identity_42IdentityRestoreV2:tensors:42*
T0*
_output_shapes
:2
Identity_42�
AssignVariableOp_42AssignVariableOp5assignvariableop_42_adam_batch_normalization_9_beta_mIdentity_42:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_42_
Identity_43IdentityRestoreV2:tensors:43*
T0*
_output_shapes
:2
Identity_43�
AssignVariableOp_43AssignVariableOp*assignvariableop_43_adam_output_9_kernel_mIdentity_43:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_43_
Identity_44IdentityRestoreV2:tensors:44*
T0*
_output_shapes
:2
Identity_44�
AssignVariableOp_44AssignVariableOp(assignvariableop_44_adam_output_9_bias_mIdentity_44:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_44_
Identity_45IdentityRestoreV2:tensors:45*
T0*
_output_shapes
:2
Identity_45�
AssignVariableOp_45AssignVariableOp)assignvariableop_45_adam_input_7_kernel_vIdentity_45:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_45_
Identity_46IdentityRestoreV2:tensors:46*
T0*
_output_shapes
:2
Identity_46�
AssignVariableOp_46AssignVariableOp'assignvariableop_46_adam_input_7_bias_vIdentity_46:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_46_
Identity_47IdentityRestoreV2:tensors:47*
T0*
_output_shapes
:2
Identity_47�
AssignVariableOp_47AssignVariableOp)assignvariableop_47_adam_type0_7_kernel_vIdentity_47:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_47_
Identity_48IdentityRestoreV2:tensors:48*
T0*
_output_shapes
:2
Identity_48�
AssignVariableOp_48AssignVariableOp'assignvariableop_48_adam_type0_7_bias_vIdentity_48:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_48_
Identity_49IdentityRestoreV2:tensors:49*
T0*
_output_shapes
:2
Identity_49�
AssignVariableOp_49AssignVariableOp)assignvariableop_49_adam_type1_7_kernel_vIdentity_49:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_49_
Identity_50IdentityRestoreV2:tensors:50*
T0*
_output_shapes
:2
Identity_50�
AssignVariableOp_50AssignVariableOp'assignvariableop_50_adam_type1_7_bias_vIdentity_50:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_50_
Identity_51IdentityRestoreV2:tensors:51*
T0*
_output_shapes
:2
Identity_51�
AssignVariableOp_51AssignVariableOp)assignvariableop_51_adam_type2_7_kernel_vIdentity_51:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_51_
Identity_52IdentityRestoreV2:tensors:52*
T0*
_output_shapes
:2
Identity_52�
AssignVariableOp_52AssignVariableOp'assignvariableop_52_adam_type2_7_bias_vIdentity_52:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_52_
Identity_53IdentityRestoreV2:tensors:53*
T0*
_output_shapes
:2
Identity_53�
AssignVariableOp_53AssignVariableOp)assignvariableop_53_adam_type3_7_kernel_vIdentity_53:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_53_
Identity_54IdentityRestoreV2:tensors:54*
T0*
_output_shapes
:2
Identity_54�
AssignVariableOp_54AssignVariableOp'assignvariableop_54_adam_type3_7_bias_vIdentity_54:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_54_
Identity_55IdentityRestoreV2:tensors:55*
T0*
_output_shapes
:2
Identity_55�
AssignVariableOp_55AssignVariableOp)assignvariableop_55_adam_type4_7_kernel_vIdentity_55:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_55_
Identity_56IdentityRestoreV2:tensors:56*
T0*
_output_shapes
:2
Identity_56�
AssignVariableOp_56AssignVariableOp'assignvariableop_56_adam_type4_7_bias_vIdentity_56:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_56_
Identity_57IdentityRestoreV2:tensors:57*
T0*
_output_shapes
:2
Identity_57�
AssignVariableOp_57AssignVariableOp6assignvariableop_57_adam_batch_normalization_9_gamma_vIdentity_57:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_57_
Identity_58IdentityRestoreV2:tensors:58*
T0*
_output_shapes
:2
Identity_58�
AssignVariableOp_58AssignVariableOp5assignvariableop_58_adam_batch_normalization_9_beta_vIdentity_58:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_58_
Identity_59IdentityRestoreV2:tensors:59*
T0*
_output_shapes
:2
Identity_59�
AssignVariableOp_59AssignVariableOp*assignvariableop_59_adam_output_9_kernel_vIdentity_59:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_59_
Identity_60IdentityRestoreV2:tensors:60*
T0*
_output_shapes
:2
Identity_60�
AssignVariableOp_60AssignVariableOp(assignvariableop_60_adam_output_9_bias_vIdentity_60:output:0*
_output_shapes
 *
dtype02
AssignVariableOp_60�
RestoreV2_1/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*1
value(B&B_CHECKPOINTABLE_OBJECT_GRAPH2
RestoreV2_1/tensor_names�
RestoreV2_1/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*
valueB
B 2
RestoreV2_1/shape_and_slices�
RestoreV2_1	RestoreV2file_prefix!RestoreV2_1/tensor_names:output:0%RestoreV2_1/shape_and_slices:output:0
^RestoreV2"/device:CPU:0*
_output_shapes
:*
dtypes
22
RestoreV2_19
NoOpNoOp"/device:CPU:0*
_output_shapes
 2
NoOp�
Identity_61Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_37^AssignVariableOp_38^AssignVariableOp_39^AssignVariableOp_4^AssignVariableOp_40^AssignVariableOp_41^AssignVariableOp_42^AssignVariableOp_43^AssignVariableOp_44^AssignVariableOp_45^AssignVariableOp_46^AssignVariableOp_47^AssignVariableOp_48^AssignVariableOp_49^AssignVariableOp_5^AssignVariableOp_50^AssignVariableOp_51^AssignVariableOp_52^AssignVariableOp_53^AssignVariableOp_54^AssignVariableOp_55^AssignVariableOp_56^AssignVariableOp_57^AssignVariableOp_58^AssignVariableOp_59^AssignVariableOp_6^AssignVariableOp_60^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9^NoOp"/device:CPU:0*
T0*
_output_shapes
: 2
Identity_61�
Identity_62IdentityIdentity_61:output:0^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_37^AssignVariableOp_38^AssignVariableOp_39^AssignVariableOp_4^AssignVariableOp_40^AssignVariableOp_41^AssignVariableOp_42^AssignVariableOp_43^AssignVariableOp_44^AssignVariableOp_45^AssignVariableOp_46^AssignVariableOp_47^AssignVariableOp_48^AssignVariableOp_49^AssignVariableOp_5^AssignVariableOp_50^AssignVariableOp_51^AssignVariableOp_52^AssignVariableOp_53^AssignVariableOp_54^AssignVariableOp_55^AssignVariableOp_56^AssignVariableOp_57^AssignVariableOp_58^AssignVariableOp_59^AssignVariableOp_6^AssignVariableOp_60^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9
^RestoreV2^RestoreV2_1*
T0*
_output_shapes
: 2
Identity_62"#
identity_62Identity_62:output:0*�
_input_shapes�
�: :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132*
AssignVariableOp_14AssignVariableOp_142*
AssignVariableOp_15AssignVariableOp_152*
AssignVariableOp_16AssignVariableOp_162*
AssignVariableOp_17AssignVariableOp_172*
AssignVariableOp_18AssignVariableOp_182*
AssignVariableOp_19AssignVariableOp_192(
AssignVariableOp_2AssignVariableOp_22*
AssignVariableOp_20AssignVariableOp_202*
AssignVariableOp_21AssignVariableOp_212*
AssignVariableOp_22AssignVariableOp_222*
AssignVariableOp_23AssignVariableOp_232*
AssignVariableOp_24AssignVariableOp_242*
AssignVariableOp_25AssignVariableOp_252*
AssignVariableOp_26AssignVariableOp_262*
AssignVariableOp_27AssignVariableOp_272*
AssignVariableOp_28AssignVariableOp_282*
AssignVariableOp_29AssignVariableOp_292(
AssignVariableOp_3AssignVariableOp_32*
AssignVariableOp_30AssignVariableOp_302*
AssignVariableOp_31AssignVariableOp_312*
AssignVariableOp_32AssignVariableOp_322*
AssignVariableOp_33AssignVariableOp_332*
AssignVariableOp_34AssignVariableOp_342*
AssignVariableOp_35AssignVariableOp_352*
AssignVariableOp_36AssignVariableOp_362*
AssignVariableOp_37AssignVariableOp_372*
AssignVariableOp_38AssignVariableOp_382*
AssignVariableOp_39AssignVariableOp_392(
AssignVariableOp_4AssignVariableOp_42*
AssignVariableOp_40AssignVariableOp_402*
AssignVariableOp_41AssignVariableOp_412*
AssignVariableOp_42AssignVariableOp_422*
AssignVariableOp_43AssignVariableOp_432*
AssignVariableOp_44AssignVariableOp_442*
AssignVariableOp_45AssignVariableOp_452*
AssignVariableOp_46AssignVariableOp_462*
AssignVariableOp_47AssignVariableOp_472*
AssignVariableOp_48AssignVariableOp_482*
AssignVariableOp_49AssignVariableOp_492(
AssignVariableOp_5AssignVariableOp_52*
AssignVariableOp_50AssignVariableOp_502*
AssignVariableOp_51AssignVariableOp_512*
AssignVariableOp_52AssignVariableOp_522*
AssignVariableOp_53AssignVariableOp_532*
AssignVariableOp_54AssignVariableOp_542*
AssignVariableOp_55AssignVariableOp_552*
AssignVariableOp_56AssignVariableOp_562*
AssignVariableOp_57AssignVariableOp_572*
AssignVariableOp_58AssignVariableOp_582*
AssignVariableOp_59AssignVariableOp_592(
AssignVariableOp_6AssignVariableOp_62*
AssignVariableOp_60AssignVariableOp_602(
AssignVariableOp_7AssignVariableOp_72(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_92
	RestoreV2	RestoreV22
RestoreV2_1RestoreV2_1:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: : 

_output_shapes
: :!

_output_shapes
: :"

_output_shapes
: :#

_output_shapes
: :$

_output_shapes
: :%

_output_shapes
: :&

_output_shapes
: :'

_output_shapes
: :(

_output_shapes
: :)

_output_shapes
: :*

_output_shapes
: :+

_output_shapes
: :,

_output_shapes
: :-

_output_shapes
: :.

_output_shapes
: :/

_output_shapes
: :0

_output_shapes
: :1

_output_shapes
: :2

_output_shapes
: :3

_output_shapes
: :4

_output_shapes
: :5

_output_shapes
: :6

_output_shapes
: :7

_output_shapes
: :8

_output_shapes
: :9

_output_shapes
: ::

_output_shapes
: :;

_output_shapes
: :<

_output_shapes
: :=

_output_shapes
: 
�
|
'__inference_type1_layer_call_fn_7693884

inputs
unknown
	unknown_0
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_type1_layer_call_and_return_conditional_losses_76930992
StatefulPartitionedCall�
IdentityIdentity StatefulPartitionedCall:output:0^StatefulPartitionedCall*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0::22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
�
B__inference_type3_layer_call_and_return_conditional_losses_7693153

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:00*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02	
BiasAddc

elu_59/EluEluBiasAdd:output:0*
T0*'
_output_shapes
:���������02

elu_59/Elul
IdentityIdentityelu_59/Elu:activations:0*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0:::O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: 
�
�
R__inference_batch_normalization_9_layer_call_and_return_conditional_losses_7693019

inputs%
!batchnorm_readvariableop_resource)
%batchnorm_mul_readvariableop_resource'
#batchnorm_readvariableop_1_resource'
#batchnorm_readvariableop_2_resource
identity��
batchnorm/ReadVariableOpReadVariableOp!batchnorm_readvariableop_resource*
_output_shapes
:0*
dtype02
batchnorm/ReadVariableOpg
batchnorm/add/yConst*
_output_shapes
: *
dtype0*
valueB
 *o�:2
batchnorm/add/y�
batchnorm/addAddV2 batchnorm/ReadVariableOp:value:0batchnorm/add/y:output:0*
T0*
_output_shapes
:02
batchnorm/addc
batchnorm/RsqrtRsqrtbatchnorm/add:z:0*
T0*
_output_shapes
:02
batchnorm/Rsqrt�
batchnorm/mul/ReadVariableOpReadVariableOp%batchnorm_mul_readvariableop_resource*
_output_shapes
:0*
dtype02
batchnorm/mul/ReadVariableOp�
batchnorm/mulMulbatchnorm/Rsqrt:y:0$batchnorm/mul/ReadVariableOp:value:0*
T0*
_output_shapes
:02
batchnorm/mulv
batchnorm/mul_1Mulinputsbatchnorm/mul:z:0*
T0*'
_output_shapes
:���������02
batchnorm/mul_1�
batchnorm/ReadVariableOp_1ReadVariableOp#batchnorm_readvariableop_1_resource*
_output_shapes
:0*
dtype02
batchnorm/ReadVariableOp_1�
batchnorm/mul_2Mul"batchnorm/ReadVariableOp_1:value:0batchnorm/mul:z:0*
T0*
_output_shapes
:02
batchnorm/mul_2�
batchnorm/ReadVariableOp_2ReadVariableOp#batchnorm_readvariableop_2_resource*
_output_shapes
:0*
dtype02
batchnorm/ReadVariableOp_2�
batchnorm/subSub"batchnorm/ReadVariableOp_2:value:0batchnorm/mul_2:z:0*
T0*
_output_shapes
:02
batchnorm/sub�
batchnorm/add_1AddV2batchnorm/mul_1:z:0batchnorm/sub:z:0*
T0*'
_output_shapes
:���������02
batchnorm/add_1g
IdentityIdentitybatchnorm/add_1:z:0*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*6
_input_shapes%
#:���������0:::::O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�/
�
T__inference_type_1_size_5_48_iter_1_layer_call_and_return_conditional_losses_7693320
input_input
input_7693274
input_7693276
type0_7693279
type0_7693281
type1_7693284
type1_7693286
type2_7693289
type2_7693291
type3_7693294
type3_7693296
type4_7693299
type4_7693301!
batch_normalization_9_7693304!
batch_normalization_9_7693306!
batch_normalization_9_7693308!
batch_normalization_9_7693310
output_7693313
output_7693315
identity��Input/StatefulPartitionedCall�Output/StatefulPartitionedCall�-batch_normalization_9/StatefulPartitionedCall�type0/StatefulPartitionedCall�type1/StatefulPartitionedCall�type2/StatefulPartitionedCall�type3/StatefulPartitionedCall�type4/StatefulPartitionedCall�
Input/StatefulPartitionedCallStatefulPartitionedCallinput_inputinput_7693274input_7693276*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_Input_layer_call_and_return_conditional_losses_76930452
Input/StatefulPartitionedCall�
type0/StatefulPartitionedCallStatefulPartitionedCall&Input/StatefulPartitionedCall:output:0type0_7693279type0_7693281*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_type0_layer_call_and_return_conditional_losses_76930722
type0/StatefulPartitionedCall�
type1/StatefulPartitionedCallStatefulPartitionedCall&type0/StatefulPartitionedCall:output:0type1_7693284type1_7693286*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_type1_layer_call_and_return_conditional_losses_76930992
type1/StatefulPartitionedCall�
type2/StatefulPartitionedCallStatefulPartitionedCall&type1/StatefulPartitionedCall:output:0type2_7693289type2_7693291*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_type2_layer_call_and_return_conditional_losses_76931262
type2/StatefulPartitionedCall�
type3/StatefulPartitionedCallStatefulPartitionedCall&type2/StatefulPartitionedCall:output:0type3_7693294type3_7693296*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_type3_layer_call_and_return_conditional_losses_76931532
type3/StatefulPartitionedCall�
type4/StatefulPartitionedCallStatefulPartitionedCall&type3/StatefulPartitionedCall:output:0type4_7693299type4_7693301*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_type4_layer_call_and_return_conditional_losses_76931802
type4/StatefulPartitionedCall�
-batch_normalization_9/StatefulPartitionedCallStatefulPartitionedCall&type4/StatefulPartitionedCall:output:0batch_normalization_9_7693304batch_normalization_9_7693306batch_normalization_9_7693308batch_normalization_9_7693310*
Tin	
2*
Tout
2*'
_output_shapes
:���������0*&
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*[
fVRT
R__inference_batch_normalization_9_layer_call_and_return_conditional_losses_76930192/
-batch_normalization_9/StatefulPartitionedCall�
Output/StatefulPartitionedCallStatefulPartitionedCall6batch_normalization_9/StatefulPartitionedCall:output:0output_7693313output_7693315*
Tin
2*
Tout
2*'
_output_shapes
:���������	*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*L
fGRE
C__inference_Output_layer_call_and_return_conditional_losses_76932412 
Output/StatefulPartitionedCall�
softmax_9/PartitionedCallPartitionedCall'Output/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*'
_output_shapes
:���������	* 
_read_only_resource_inputs
 **
config_proto

CPU

GPU 2J 8*O
fJRH
F__inference_softmax_9_layer_call_and_return_conditional_losses_76932622
softmax_9/PartitionedCall�
IdentityIdentity"softmax_9/PartitionedCall:output:0^Input/StatefulPartitionedCall^Output/StatefulPartitionedCall.^batch_normalization_9/StatefulPartitionedCall^type0/StatefulPartitionedCall^type1/StatefulPartitionedCall^type2/StatefulPartitionedCall^type3/StatefulPartitionedCall^type4/StatefulPartitionedCall*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*n
_input_shapes]
[:���������0::::::::::::::::::2>
Input/StatefulPartitionedCallInput/StatefulPartitionedCall2@
Output/StatefulPartitionedCallOutput/StatefulPartitionedCall2^
-batch_normalization_9/StatefulPartitionedCall-batch_normalization_9/StatefulPartitionedCall2>
type0/StatefulPartitionedCalltype0/StatefulPartitionedCall2>
type1/StatefulPartitionedCalltype1/StatefulPartitionedCall2>
type2/StatefulPartitionedCalltype2/StatefulPartitionedCall2>
type3/StatefulPartitionedCalltype3/StatefulPartitionedCall2>
type4/StatefulPartitionedCalltype4/StatefulPartitionedCall:T P
'
_output_shapes
:���������0
%
_user_specified_nameInput_input:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�/
�
T__inference_type_1_size_5_48_iter_1_layer_call_and_return_conditional_losses_7693462

inputs
input_7693416
input_7693418
type0_7693421
type0_7693423
type1_7693426
type1_7693428
type2_7693431
type2_7693433
type3_7693436
type3_7693438
type4_7693441
type4_7693443!
batch_normalization_9_7693446!
batch_normalization_9_7693448!
batch_normalization_9_7693450!
batch_normalization_9_7693452
output_7693455
output_7693457
identity��Input/StatefulPartitionedCall�Output/StatefulPartitionedCall�-batch_normalization_9/StatefulPartitionedCall�type0/StatefulPartitionedCall�type1/StatefulPartitionedCall�type2/StatefulPartitionedCall�type3/StatefulPartitionedCall�type4/StatefulPartitionedCall�
Input/StatefulPartitionedCallStatefulPartitionedCallinputsinput_7693416input_7693418*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_Input_layer_call_and_return_conditional_losses_76930452
Input/StatefulPartitionedCall�
type0/StatefulPartitionedCallStatefulPartitionedCall&Input/StatefulPartitionedCall:output:0type0_7693421type0_7693423*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_type0_layer_call_and_return_conditional_losses_76930722
type0/StatefulPartitionedCall�
type1/StatefulPartitionedCallStatefulPartitionedCall&type0/StatefulPartitionedCall:output:0type1_7693426type1_7693428*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_type1_layer_call_and_return_conditional_losses_76930992
type1/StatefulPartitionedCall�
type2/StatefulPartitionedCallStatefulPartitionedCall&type1/StatefulPartitionedCall:output:0type2_7693431type2_7693433*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_type2_layer_call_and_return_conditional_losses_76931262
type2/StatefulPartitionedCall�
type3/StatefulPartitionedCallStatefulPartitionedCall&type2/StatefulPartitionedCall:output:0type3_7693436type3_7693438*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_type3_layer_call_and_return_conditional_losses_76931532
type3/StatefulPartitionedCall�
type4/StatefulPartitionedCallStatefulPartitionedCall&type3/StatefulPartitionedCall:output:0type4_7693441type4_7693443*
Tin
2*
Tout
2*'
_output_shapes
:���������0*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*K
fFRD
B__inference_type4_layer_call_and_return_conditional_losses_76931802
type4/StatefulPartitionedCall�
-batch_normalization_9/StatefulPartitionedCallStatefulPartitionedCall&type4/StatefulPartitionedCall:output:0batch_normalization_9_7693446batch_normalization_9_7693448batch_normalization_9_7693450batch_normalization_9_7693452*
Tin	
2*
Tout
2*'
_output_shapes
:���������0*&
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*[
fVRT
R__inference_batch_normalization_9_layer_call_and_return_conditional_losses_76930192/
-batch_normalization_9/StatefulPartitionedCall�
Output/StatefulPartitionedCallStatefulPartitionedCall6batch_normalization_9/StatefulPartitionedCall:output:0output_7693455output_7693457*
Tin
2*
Tout
2*'
_output_shapes
:���������	*$
_read_only_resource_inputs
**
config_proto

CPU

GPU 2J 8*L
fGRE
C__inference_Output_layer_call_and_return_conditional_losses_76932412 
Output/StatefulPartitionedCall�
softmax_9/PartitionedCallPartitionedCall'Output/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*'
_output_shapes
:���������	* 
_read_only_resource_inputs
 **
config_proto

CPU

GPU 2J 8*O
fJRH
F__inference_softmax_9_layer_call_and_return_conditional_losses_76932622
softmax_9/PartitionedCall�
IdentityIdentity"softmax_9/PartitionedCall:output:0^Input/StatefulPartitionedCall^Output/StatefulPartitionedCall.^batch_normalization_9/StatefulPartitionedCall^type0/StatefulPartitionedCall^type1/StatefulPartitionedCall^type2/StatefulPartitionedCall^type3/StatefulPartitionedCall^type4/StatefulPartitionedCall*
T0*'
_output_shapes
:���������	2

Identity"
identityIdentity:output:0*n
_input_shapes]
[:���������0::::::::::::::::::2>
Input/StatefulPartitionedCallInput/StatefulPartitionedCall2@
Output/StatefulPartitionedCallOutput/StatefulPartitionedCall2^
-batch_normalization_9/StatefulPartitionedCall-batch_normalization_9/StatefulPartitionedCall2>
type0/StatefulPartitionedCalltype0/StatefulPartitionedCall2>
type1/StatefulPartitionedCalltype1/StatefulPartitionedCall2>
type2/StatefulPartitionedCalltype2/StatefulPartitionedCall2>
type3/StatefulPartitionedCalltype3/StatefulPartitionedCall2>
type4/StatefulPartitionedCalltype4/StatefulPartitionedCall:O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
B__inference_type4_layer_call_and_return_conditional_losses_7693180

inputs"
matmul_readvariableop_resource#
biasadd_readvariableop_resource
identity��
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:00*
dtype02
MatMul/ReadVariableOps
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02
MatMul�
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:0*
dtype02
BiasAdd/ReadVariableOp�
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������02	
BiasAddc

elu_60/EluEluBiasAdd:output:0*
T0*'
_output_shapes
:���������02

elu_60/Elul
IdentityIdentityelu_60/Elu:activations:0*
T0*'
_output_shapes
:���������02

Identity"
identityIdentity:output:0*.
_input_shapes
:���������0:::O K
'
_output_shapes
:���������0
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: "�L
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*�
serving_default�
C
Input_input4
serving_default_Input_input:0���������0=
	softmax_90
StatefulPartitionedCall:0���������	tensorflow/serving/predict:��
�R
layer_with_weights-0
layer-0
layer_with_weights-1
layer-1
layer_with_weights-2
layer-2
layer_with_weights-3
layer-3
layer_with_weights-4
layer-4
layer_with_weights-5
layer-5
layer_with_weights-6
layer-6
layer_with_weights-7
layer-7
	layer-8

	optimizer
	variables
trainable_variables
regularization_losses
	keras_api

signatures
�__call__
+�&call_and_return_all_conditional_losses
�_default_save_signature"�N
_tf_keras_sequential�N{"class_name": "Sequential", "name": "type_1_size_5_48_iter_1", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "config": {"name": "type_1_size_5_48_iter_1", "layers": [{"class_name": "Dense", "config": {"name": "Input", "trainable": true, "batch_input_shape": {"class_name": "__tuple__", "items": [null, 48]}, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_55", "trainable": true, "dtype": "float32", "alpha": 1.0}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Dense", "config": {"name": "type0", "trainable": true, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_56", "trainable": true, "dtype": "float32", "alpha": 1.0}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Dense", "config": {"name": "type1", "trainable": true, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_57", "trainable": true, "dtype": "float32", "alpha": 1.0}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Dense", "config": {"name": "type2", "trainable": true, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_58", "trainable": true, "dtype": "float32", "alpha": 1.0}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Dense", "config": {"name": "type3", "trainable": true, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_59", "trainable": true, "dtype": "float32", "alpha": 1.0}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Dense", "config": {"name": "type4", "trainable": true, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_60", "trainable": true, "dtype": "float32", "alpha": 1.0}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "BatchNormalization", "config": {"name": "batch_normalization_9", "trainable": true, "dtype": "float32", "axis": [1], "momentum": 0.99, "epsilon": 0.001, "center": true, "scale": true, "beta_initializer": {"class_name": "Zeros", "config": {}}, "gamma_initializer": {"class_name": "Ones", "config": {}}, "moving_mean_initializer": {"class_name": "Zeros", "config": {}}, "moving_variance_initializer": {"class_name": "Ones", "config": {}}, "beta_regularizer": null, "gamma_regularizer": null, "beta_constraint": null, "gamma_constraint": null}}, {"class_name": "Dense", "config": {"name": "Output", "trainable": true, "dtype": "float32", "units": 9, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Softmax", "config": {"name": "softmax_9", "trainable": true, "dtype": "float32", "axis": -1}}], "build_input_shape": {"class_name": "TensorShape", "items": [null, 48]}}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 48}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 48]}, "is_graph_network": true, "keras_version": "2.3.0-tf", "backend": "tensorflow", "model_config": {"class_name": "Sequential", "config": {"name": "type_1_size_5_48_iter_1", "layers": [{"class_name": "Dense", "config": {"name": "Input", "trainable": true, "batch_input_shape": {"class_name": "__tuple__", "items": [null, 48]}, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_55", "trainable": true, "dtype": "float32", "alpha": 1.0}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Dense", "config": {"name": "type0", "trainable": true, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_56", "trainable": true, "dtype": "float32", "alpha": 1.0}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Dense", "config": {"name": "type1", "trainable": true, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_57", "trainable": true, "dtype": "float32", "alpha": 1.0}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Dense", "config": {"name": "type2", "trainable": true, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_58", "trainable": true, "dtype": "float32", "alpha": 1.0}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Dense", "config": {"name": "type3", "trainable": true, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_59", "trainable": true, "dtype": "float32", "alpha": 1.0}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Dense", "config": {"name": "type4", "trainable": true, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_60", "trainable": true, "dtype": "float32", "alpha": 1.0}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "BatchNormalization", "config": {"name": "batch_normalization_9", "trainable": true, "dtype": "float32", "axis": [1], "momentum": 0.99, "epsilon": 0.001, "center": true, "scale": true, "beta_initializer": {"class_name": "Zeros", "config": {}}, "gamma_initializer": {"class_name": "Ones", "config": {}}, "moving_mean_initializer": {"class_name": "Zeros", "config": {}}, "moving_variance_initializer": {"class_name": "Ones", "config": {}}, "beta_regularizer": null, "gamma_regularizer": null, "beta_constraint": null, "gamma_constraint": null}}, {"class_name": "Dense", "config": {"name": "Output", "trainable": true, "dtype": "float32", "units": 9, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}}, {"class_name": "Softmax", "config": {"name": "softmax_9", "trainable": true, "dtype": "float32", "axis": -1}}], "build_input_shape": {"class_name": "TensorShape", "items": [null, 48]}}}, "training_config": {"loss": {"class_name": "CategoricalCrossentropy", "config": {"reduction": "auto", "name": "categorical_crossentropy", "from_logits": false, "label_smoothing": 0}}, "metrics": null, "weighted_metrics": ["accuracy", {"class_name": "Precision", "config": {"name": "precision", "dtype": "float32", "thresholds": null, "top_k": null, "class_id": null}}], "loss_weights": null, "sample_weight_mode": null, "optimizer_config": {"class_name": "Adam", "config": {"name": "Adam", "learning_rate": 0.0010000000474974513, "decay": 0.0, "beta_1": 0.8999999761581421, "beta_2": 0.9990000128746033, "epsilon": 1e-07, "amsgrad": false}}}}
�	

activation

kernel
bias
	variables
trainable_variables
regularization_losses
	keras_api
�__call__
+�&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "Dense", "name": "Input", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": {"class_name": "__tuple__", "items": [null, 48]}, "stateful": false, "config": {"name": "Input", "trainable": true, "batch_input_shape": {"class_name": "__tuple__", "items": [null, 48]}, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_55", "trainable": true, "dtype": "float32", "alpha": 1.0}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 48}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 48]}}
�

activation

kernel
bias
	variables
trainable_variables
regularization_losses
	keras_api
�__call__
+�&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "Dense", "name": "type0", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "type0", "trainable": true, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_56", "trainable": true, "dtype": "float32", "alpha": 1.0}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 48}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 48]}}
�

activation

kernel
 bias
!	variables
"trainable_variables
#regularization_losses
$	keras_api
�__call__
+�&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "Dense", "name": "type1", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "type1", "trainable": true, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_57", "trainable": true, "dtype": "float32", "alpha": 1.0}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 48}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 48]}}
�
%
activation

&kernel
'bias
(	variables
)trainable_variables
*regularization_losses
+	keras_api
�__call__
+�&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "Dense", "name": "type2", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "type2", "trainable": true, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_58", "trainable": true, "dtype": "float32", "alpha": 1.0}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 48}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 48]}}
�
,
activation

-kernel
.bias
/	variables
0trainable_variables
1regularization_losses
2	keras_api
�__call__
+�&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "Dense", "name": "type3", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "type3", "trainable": true, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_59", "trainable": true, "dtype": "float32", "alpha": 1.0}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 48}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 48]}}
�
3
activation

4kernel
5bias
6	variables
7trainable_variables
8regularization_losses
9	keras_api
�__call__
+�&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "Dense", "name": "type4", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "type4", "trainable": true, "dtype": "float32", "units": 48, "activation": {"class_name": "ELU", "config": {"name": "elu_60", "trainable": true, "dtype": "float32", "alpha": 1.0}}, "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 48}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 48]}}
�	
:axis
	;gamma
<beta
=moving_mean
>moving_variance
?	variables
@trainable_variables
Aregularization_losses
B	keras_api
�__call__
+�&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "BatchNormalization", "name": "batch_normalization_9", "trainable": true, "expects_training_arg": true, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "batch_normalization_9", "trainable": true, "dtype": "float32", "axis": [1], "momentum": 0.99, "epsilon": 0.001, "center": true, "scale": true, "beta_initializer": {"class_name": "Zeros", "config": {}}, "gamma_initializer": {"class_name": "Ones", "config": {}}, "moving_mean_initializer": {"class_name": "Zeros", "config": {}}, "moving_variance_initializer": {"class_name": "Ones", "config": {}}, "beta_regularizer": null, "gamma_regularizer": null, "beta_constraint": null, "gamma_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": 2, "max_ndim": null, "min_ndim": null, "axes": {"1": 48}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 48]}}
�

Ckernel
Dbias
E	variables
Ftrainable_variables
Gregularization_losses
H	keras_api
�__call__
+�&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "Dense", "name": "Output", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "Output", "trainable": true, "dtype": "float32", "units": 9, "activation": "linear", "use_bias": true, "kernel_initializer": {"class_name": "GlorotUniform", "config": {"seed": null}}, "bias_initializer": {"class_name": "Zeros", "config": {}}, "kernel_regularizer": null, "bias_regularizer": null, "activity_regularizer": null, "kernel_constraint": null, "bias_constraint": null}, "input_spec": {"class_name": "InputSpec", "config": {"dtype": null, "shape": null, "ndim": null, "max_ndim": null, "min_ndim": 2, "axes": {"-1": 48}}}, "build_input_shape": {"class_name": "TensorShape", "items": [null, 48]}}
�
I	variables
Jtrainable_variables
Kregularization_losses
L	keras_api
�__call__
+�&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "Softmax", "name": "softmax_9", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "softmax_9", "trainable": true, "dtype": "float32", "axis": -1}}
�
Miter

Nbeta_1

Obeta_2
	Pdecay
Qlearning_ratem�m�m�m�m� m�&m�'m�-m�.m�4m�5m�;m�<m�Cm�Dm�v�v�v�v�v� v�&v�'v�-v�.v�4v�5v�;v�<v�Cv�Dv�"
	optimizer
�
0
1
2
3
4
 5
&6
'7
-8
.9
410
511
;12
<13
=14
>15
C16
D17"
trackable_list_wrapper
�
0
1
2
3
4
 5
&6
'7
-8
.9
410
511
;12
<13
C14
D15"
trackable_list_wrapper
 "
trackable_list_wrapper
�
Rlayer_regularization_losses
Snon_trainable_variables

Tlayers
Ulayer_metrics
	variables
trainable_variables
Vmetrics
regularization_losses
�__call__
�_default_save_signature
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
-
�serving_default"
signature_map
�
W	variables
Xtrainable_variables
Yregularization_losses
Z	keras_api
�__call__
+�&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "ELU", "name": "elu_55", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "elu_55", "trainable": true, "dtype": "float32", "alpha": 1.0}}
 :002Input_7/kernel
:02Input_7/bias
.
0
1"
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
[layer_regularization_losses
\non_trainable_variables

]layers
^layer_metrics
	variables
trainable_variables
_metrics
regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�
`	variables
atrainable_variables
bregularization_losses
c	keras_api
�__call__
+�&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "ELU", "name": "elu_56", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "elu_56", "trainable": true, "dtype": "float32", "alpha": 1.0}}
 :002type0_7/kernel
:02type0_7/bias
.
0
1"
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
dlayer_regularization_losses
enon_trainable_variables

flayers
glayer_metrics
	variables
trainable_variables
hmetrics
regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�
i	variables
jtrainable_variables
kregularization_losses
l	keras_api
�__call__
+�&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "ELU", "name": "elu_57", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "elu_57", "trainable": true, "dtype": "float32", "alpha": 1.0}}
 :002type1_7/kernel
:02type1_7/bias
.
0
 1"
trackable_list_wrapper
.
0
 1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
mlayer_regularization_losses
nnon_trainable_variables

olayers
player_metrics
!	variables
"trainable_variables
qmetrics
#regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�
r	variables
strainable_variables
tregularization_losses
u	keras_api
�__call__
+�&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "ELU", "name": "elu_58", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "elu_58", "trainable": true, "dtype": "float32", "alpha": 1.0}}
 :002type2_7/kernel
:02type2_7/bias
.
&0
'1"
trackable_list_wrapper
.
&0
'1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
vlayer_regularization_losses
wnon_trainable_variables

xlayers
ylayer_metrics
(	variables
)trainable_variables
zmetrics
*regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�
{	variables
|trainable_variables
}regularization_losses
~	keras_api
�__call__
+�&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "ELU", "name": "elu_59", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "elu_59", "trainable": true, "dtype": "float32", "alpha": 1.0}}
 :002type3_7/kernel
:02type3_7/bias
.
-0
.1"
trackable_list_wrapper
.
-0
.1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
layer_regularization_losses
�non_trainable_variables
�layers
�layer_metrics
/	variables
0trainable_variables
�metrics
1regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�
�	variables
�trainable_variables
�regularization_losses
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses"�
_tf_keras_layer�{"class_name": "ELU", "name": "elu_60", "trainable": true, "expects_training_arg": false, "dtype": "float32", "batch_input_shape": null, "stateful": false, "config": {"name": "elu_60", "trainable": true, "dtype": "float32", "alpha": 1.0}}
 :002type4_7/kernel
:02type4_7/bias
.
40
51"
trackable_list_wrapper
.
40
51"
trackable_list_wrapper
 "
trackable_list_wrapper
�
 �layer_regularization_losses
�non_trainable_variables
�layers
�layer_metrics
6	variables
7trainable_variables
�metrics
8regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
):'02batch_normalization_9/gamma
(:&02batch_normalization_9/beta
1:/0 (2!batch_normalization_9/moving_mean
5:30 (2%batch_normalization_9/moving_variance
<
;0
<1
=2
>3"
trackable_list_wrapper
.
;0
<1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
 �layer_regularization_losses
�non_trainable_variables
�layers
�layer_metrics
?	variables
@trainable_variables
�metrics
Aregularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
!:0	2Output_9/kernel
:	2Output_9/bias
.
C0
D1"
trackable_list_wrapper
.
C0
D1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
 �layer_regularization_losses
�non_trainable_variables
�layers
�layer_metrics
E	variables
Ftrainable_variables
�metrics
Gregularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
 �layer_regularization_losses
�non_trainable_variables
�layers
�layer_metrics
I	variables
Jtrainable_variables
�metrics
Kregularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
:	 (2	Adam/iter
: (2Adam/beta_1
: (2Adam/beta_2
: (2
Adam/decay
: (2Adam/learning_rate
 "
trackable_list_wrapper
.
=0
>1"
trackable_list_wrapper
_
0
1
2
3
4
5
6
7
	8"
trackable_list_wrapper
 "
trackable_dict_wrapper
8
�0
�1
�2"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
 �layer_regularization_losses
�non_trainable_variables
�layers
�layer_metrics
W	variables
Xtrainable_variables
�metrics
Yregularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
'
0"
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
 �layer_regularization_losses
�non_trainable_variables
�layers
�layer_metrics
`	variables
atrainable_variables
�metrics
bregularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
'
0"
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
 �layer_regularization_losses
�non_trainable_variables
�layers
�layer_metrics
i	variables
jtrainable_variables
�metrics
kregularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
'
0"
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
 �layer_regularization_losses
�non_trainable_variables
�layers
�layer_metrics
r	variables
strainable_variables
�metrics
tregularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
'
%0"
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
 �layer_regularization_losses
�non_trainable_variables
�layers
�layer_metrics
{	variables
|trainable_variables
�metrics
}regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
'
,0"
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
 �layer_regularization_losses
�non_trainable_variables
�layers
�layer_metrics
�	variables
�trainable_variables
�metrics
�regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
'
30"
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
.
=0
>1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
�

�total

�count
�	variables
�	keras_api"�
_tf_keras_metricj{"class_name": "Mean", "name": "loss", "dtype": "float32", "config": {"name": "loss", "dtype": "float32"}}
�

�total

�count
�
_fn_kwargs
�	variables
�	keras_api"�
_tf_keras_metric�{"class_name": "MeanMetricWrapper", "name": "accuracy", "dtype": "float32", "config": {"name": "accuracy", "dtype": "float32", "fn": "categorical_accuracy"}}
�
�
thresholds
�true_positives
�false_positives
�	variables
�	keras_api"�
_tf_keras_metric�{"class_name": "Precision", "name": "precision", "dtype": "float32", "config": {"name": "precision", "dtype": "float32", "thresholds": null, "top_k": null, "class_id": null}}
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
:  (2total
:  (2count
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
:  (2total
:  (2count
 "
trackable_dict_wrapper
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
 "
trackable_list_wrapper
: (2true_positives
: (2false_positives
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
%:#002Adam/Input_7/kernel/m
:02Adam/Input_7/bias/m
%:#002Adam/type0_7/kernel/m
:02Adam/type0_7/bias/m
%:#002Adam/type1_7/kernel/m
:02Adam/type1_7/bias/m
%:#002Adam/type2_7/kernel/m
:02Adam/type2_7/bias/m
%:#002Adam/type3_7/kernel/m
:02Adam/type3_7/bias/m
%:#002Adam/type4_7/kernel/m
:02Adam/type4_7/bias/m
.:,02"Adam/batch_normalization_9/gamma/m
-:+02!Adam/batch_normalization_9/beta/m
&:$0	2Adam/Output_9/kernel/m
 :	2Adam/Output_9/bias/m
%:#002Adam/Input_7/kernel/v
:02Adam/Input_7/bias/v
%:#002Adam/type0_7/kernel/v
:02Adam/type0_7/bias/v
%:#002Adam/type1_7/kernel/v
:02Adam/type1_7/bias/v
%:#002Adam/type2_7/kernel/v
:02Adam/type2_7/bias/v
%:#002Adam/type3_7/kernel/v
:02Adam/type3_7/bias/v
%:#002Adam/type4_7/kernel/v
:02Adam/type4_7/bias/v
.:,02"Adam/batch_normalization_9/gamma/v
-:+02!Adam/batch_normalization_9/beta/v
&:$0	2Adam/Output_9/kernel/v
 :	2Adam/Output_9/bias/v
�2�
9__inference_type_1_size_5_48_iter_1_layer_call_fn_7693501
9__inference_type_1_size_5_48_iter_1_layer_call_fn_7693824
9__inference_type_1_size_5_48_iter_1_layer_call_fn_7693411
9__inference_type_1_size_5_48_iter_1_layer_call_fn_7693783�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
T__inference_type_1_size_5_48_iter_1_layer_call_and_return_conditional_losses_7693742
T__inference_type_1_size_5_48_iter_1_layer_call_and_return_conditional_losses_7693673
T__inference_type_1_size_5_48_iter_1_layer_call_and_return_conditional_losses_7693320
T__inference_type_1_size_5_48_iter_1_layer_call_and_return_conditional_losses_7693271�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
"__inference__wrapped_model_7692890�
���
FullArgSpec
args� 
varargsjargs
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� **�'
%�"
Input_input���������0
�2�
'__inference_Input_layer_call_fn_7693844�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
B__inference_Input_layer_call_and_return_conditional_losses_7693835�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
'__inference_type0_layer_call_fn_7693864�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
B__inference_type0_layer_call_and_return_conditional_losses_7693855�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
'__inference_type1_layer_call_fn_7693884�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
B__inference_type1_layer_call_and_return_conditional_losses_7693875�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
'__inference_type2_layer_call_fn_7693904�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
B__inference_type2_layer_call_and_return_conditional_losses_7693895�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
'__inference_type3_layer_call_fn_7693924�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
B__inference_type3_layer_call_and_return_conditional_losses_7693915�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
'__inference_type4_layer_call_fn_7693944�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
B__inference_type4_layer_call_and_return_conditional_losses_7693935�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
7__inference_batch_normalization_9_layer_call_fn_7694049
7__inference_batch_normalization_9_layer_call_fn_7694062�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
R__inference_batch_normalization_9_layer_call_and_return_conditional_losses_7694036
R__inference_batch_normalization_9_layer_call_and_return_conditional_losses_7694016�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
(__inference_Output_layer_call_fn_7694081�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
C__inference_Output_layer_call_and_return_conditional_losses_7694072�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
+__inference_softmax_9_layer_call_fn_7694091�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
F__inference_softmax_9_layer_call_and_return_conditional_losses_7694086�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
8B6
%__inference_signature_wrapper_7693588Input_input
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2��
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 �
B__inference_Input_layer_call_and_return_conditional_losses_7693835\/�,
%�"
 �
inputs���������0
� "%�"
�
0���������0
� z
'__inference_Input_layer_call_fn_7693844O/�,
%�"
 �
inputs���������0
� "����������0�
C__inference_Output_layer_call_and_return_conditional_losses_7694072\CD/�,
%�"
 �
inputs���������0
� "%�"
�
0���������	
� {
(__inference_Output_layer_call_fn_7694081OCD/�,
%�"
 �
inputs���������0
� "����������	�
"__inference__wrapped_model_7692890� &'-.45>;=<CD4�1
*�'
%�"
Input_input���������0
� "5�2
0
	softmax_9#� 
	softmax_9���������	�
R__inference_batch_normalization_9_layer_call_and_return_conditional_losses_7694016b=>;<3�0
)�&
 �
inputs���������0
p
� "%�"
�
0���������0
� �
R__inference_batch_normalization_9_layer_call_and_return_conditional_losses_7694036b>;=<3�0
)�&
 �
inputs���������0
p 
� "%�"
�
0���������0
� �
7__inference_batch_normalization_9_layer_call_fn_7694049U=>;<3�0
)�&
 �
inputs���������0
p
� "����������0�
7__inference_batch_normalization_9_layer_call_fn_7694062U>;=<3�0
)�&
 �
inputs���������0
p 
� "����������0�
%__inference_signature_wrapper_7693588� &'-.45>;=<CDC�@
� 
9�6
4
Input_input%�"
Input_input���������0"5�2
0
	softmax_9#� 
	softmax_9���������	�
F__inference_softmax_9_layer_call_and_return_conditional_losses_7694086X/�,
%�"
 �
inputs���������	
� "%�"
�
0���������	
� z
+__inference_softmax_9_layer_call_fn_7694091K/�,
%�"
 �
inputs���������	
� "����������	�
B__inference_type0_layer_call_and_return_conditional_losses_7693855\/�,
%�"
 �
inputs���������0
� "%�"
�
0���������0
� z
'__inference_type0_layer_call_fn_7693864O/�,
%�"
 �
inputs���������0
� "����������0�
B__inference_type1_layer_call_and_return_conditional_losses_7693875\ /�,
%�"
 �
inputs���������0
� "%�"
�
0���������0
� z
'__inference_type1_layer_call_fn_7693884O /�,
%�"
 �
inputs���������0
� "����������0�
B__inference_type2_layer_call_and_return_conditional_losses_7693895\&'/�,
%�"
 �
inputs���������0
� "%�"
�
0���������0
� z
'__inference_type2_layer_call_fn_7693904O&'/�,
%�"
 �
inputs���������0
� "����������0�
B__inference_type3_layer_call_and_return_conditional_losses_7693915\-./�,
%�"
 �
inputs���������0
� "%�"
�
0���������0
� z
'__inference_type3_layer_call_fn_7693924O-./�,
%�"
 �
inputs���������0
� "����������0�
B__inference_type4_layer_call_and_return_conditional_losses_7693935\45/�,
%�"
 �
inputs���������0
� "%�"
�
0���������0
� z
'__inference_type4_layer_call_fn_7693944O45/�,
%�"
 �
inputs���������0
� "����������0�
T__inference_type_1_size_5_48_iter_1_layer_call_and_return_conditional_losses_7693271y &'-.45=>;<CD<�9
2�/
%�"
Input_input���������0
p

 
� "%�"
�
0���������	
� �
T__inference_type_1_size_5_48_iter_1_layer_call_and_return_conditional_losses_7693320y &'-.45>;=<CD<�9
2�/
%�"
Input_input���������0
p 

 
� "%�"
�
0���������	
� �
T__inference_type_1_size_5_48_iter_1_layer_call_and_return_conditional_losses_7693673t &'-.45=>;<CD7�4
-�*
 �
inputs���������0
p

 
� "%�"
�
0���������	
� �
T__inference_type_1_size_5_48_iter_1_layer_call_and_return_conditional_losses_7693742t &'-.45>;=<CD7�4
-�*
 �
inputs���������0
p 

 
� "%�"
�
0���������	
� �
9__inference_type_1_size_5_48_iter_1_layer_call_fn_7693411l &'-.45=>;<CD<�9
2�/
%�"
Input_input���������0
p

 
� "����������	�
9__inference_type_1_size_5_48_iter_1_layer_call_fn_7693501l &'-.45>;=<CD<�9
2�/
%�"
Input_input���������0
p 

 
� "����������	�
9__inference_type_1_size_5_48_iter_1_layer_call_fn_7693783g &'-.45=>;<CD7�4
-�*
 �
inputs���������0
p

 
� "����������	�
9__inference_type_1_size_5_48_iter_1_layer_call_fn_7693824g &'-.45>;=<CD7�4
-�*
 �
inputs���������0
p 

 
� "����������	