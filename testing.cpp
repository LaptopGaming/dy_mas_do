#include <string>
#include <vector>
#include <iostream>
#include <algorithm>
#include </home/dominykas/Software/root_install/include/TGraphAsymmErrors.h>
#include </home/dominykas/Software/root_install/include/TFile.h>
int testing()
{
    std::string data_loc = "/home/dominykas/data/";
    TFile reco_file_BF((data_loc + "corrections/effSF_muon/" + "Tracking_SF_RunGtoH.root").c_str(), "READ");
    TGraphAsymmErrors* reco_whist_BF = (TGraphAsymmErrors*)reco_file_BF.Get("ratio_eff_eta3_dr030e030_corr");
    //reco_whist_BF->Draw();
    int size = reco_whist_BF->GetMaxSize();
    std::vector<std::vector<double>> reco_hist(2);
    reco_hist[0] = std::vector<double>(size);
    reco_hist[1] = std::vector<double>(size+1, 0);
    double X;
    reco_hist[1][0] = reco_whist_BF->GetPointX(0) - reco_whist_BF->GetErrorXlow(0);
    for(int i = 0; i < size; ++i)
    {
        reco_whist_BF->GetPoint(i, X, reco_hist[0][i]);
        reco_hist[1][i+1] = X + reco_whist_BF->GetErrorXhigh(i);
        std::cout << reco_hist[1][i] << " " << X - reco_whist_BF->GetErrorXlow(i) << " " << reco_hist[0][i] << "\n";
    }
    auto test = std::lower_bound(reco_hist[1].begin(), reco_hist[1].end(), -0.7) - reco_hist[1].begin() - 1;
    std::cout << reco_hist[0][test] << "\n";
    return 0;
}
