#include <vector>
#include <iostream>
#include <string>
#include <fstream>
#include <stdbool.h>
#include <unistd.h>
#include <algorithm>
#include <numeric>
#include <cmath>
#include <limits>
#include <time.h>
#include </home/dominykas/Software/root_install/include/TH1D.h>
#include </home/dominykas/Software/root_install/include/TF1.h>
#include </home/dominykas/Software/root_install/include/TH2D.h>
#include </home/dominykas/Software/root_install/include/THStack.h>
#include </home/dominykas/Software/root_install/include/TCanvas.h>
#include </home/dominykas/Software/root_install/include/TLegend.h>
#include </home/dominykas/Software/root_install/include/TFile.h>
#include </home/dominykas/Software/root_install/include/TChain.h>
#include </home/dominykas/Software/root_install/include/TVirtualFitter.h>
#include </home/dominykas/Software/root_install/include/Math/PdfFuncMathCore.h>
#include "/home/dominykas/dy_mas_do/hists1d.h"


//-------------------------------------------------

Double_t fitf_2gauss_sqrt(Double_t *x, Double_t *par)
{
    double arg1 = 0;
    if(par[2]!=0)
    {
        arg1 = (x[0] - par[1])/par[2];
    }
    double arg2 = 0;
    if(par[5]!=0)
    {
        arg2 = (x[0] - par[4])/par[5];
    }
    double arg3 = par[6]*x[0]*x[0] + par[7];
    if(arg3 > 0)
        arg3 = std::sqrt(arg3);
    else
        arg3 = 1;
    
    Double_t fitval = (par[0]*std::exp(-0.5*arg1*arg1) + par[3]*std::exp(-0.5*arg2*arg2))*arg3;
    return fitval;
}


Double_t fitf_2lorentz_sqrt(Double_t *x, Double_t *par)
{
    Double_t fitval = par[0]*par[2]*par[2]/(par[2]*par[2] + (x[0] - par[1])*(x[0] - par[1]));
    fitval += par[3]*par[5]*par[5]/(par[5]*par[5] + (x[0] - par[4])*(x[0] - par[4]));
    double arg3 = x[0]*x[0] + par[6];
    if(arg3 > 0)
        arg3 = std::sqrt(arg3);
    else
        arg3 = 1;
    
    return fitval*arg3;
}

Double_t fitf_lorentz_gauss_sqrt(Double_t *x, Double_t *par)
{
    Double_t fitval = par[0]*par[2]*par[2]/(par[2]*par[2] + (x[0] - par[1])*(x[0] - par[1]));
    double arg1 = 0;
    if(par[2]!=0)
    {
        arg1 = (x[0] - par[1])/par[2];
    }
    fitval += par[3]*std::exp(-0.5*arg1*arg1);
    double arg3 = x[0]*x[0] + par[6];
    if(arg3 > 0)
        arg3 = std::sqrt(arg3);
    else
        arg3 = 1;
    
    return fitval*arg3;
}

Double_t fitf_lorentz_gauss_logc(Double_t *x, Double_t *par)
{
    Double_t fitval = par[0]*par[2]*par[2]/(par[2]*par[2] + (x[0] - par[1])*(x[0] - par[1]));
    double arg1 = 0;
    if(par[2]!=0)
    {
        arg1 = (x[0] - par[1])/par[2];
    }
    fitval += par[3]*std::exp(-0.5*arg1*arg1);
    
    double arg3 = (1 - par[7])/(1 + std::exp(-par[6]*x[0])) + par[7];
    
    return fitval*arg3;
}


Double_t fitf_lorentz(Double_t *x, Double_t *par)
{
    Double_t fitval = par[0]*par[2]*par[2]/(par[2]*par[2] + (x[0] - par[1])*(x[0] - par[1]));
    return fitval;
}

Double_t fitf_lorentz_exp(Double_t *x, Double_t *par)
{
    Double_t fitval = par[0]*par[2]*par[2]/(par[2]*par[2] + (x[0] - par[1])*(x[0] - par[1]));
    fitval += par[3]*std::exp(-par[4]*x[0]);
    return fitval;
}

Double_t fitf_lorentz_exp_logc(Double_t *x, Double_t *par)
{
    Double_t fitval = par[0]*par[2]*par[2]/(par[2]*par[2] + (x[0] - par[1])*(x[0] - par[1]));
    fitval += par[3]*std::exp(-par[4]*x[0]);
    double arg = 1/(1 + std::exp(-par[5]*x[0]));
    return fitval*arg;
}

Double_t fitf_lorentz_exp_sq2(Double_t *x, Double_t *par)
{
    Double_t fitval = par[0]*par[2]*par[2]/(par[2]*par[2] + (x[0] - par[1])*(x[0] - par[1]));
    fitval += par[3]*std::exp(-par[4]*x[0]);
    double arg = x[0]/std::sqrt(x[0]*x[0] + par[5]);
    return fitval*arg;
}

Double_t fitf_2lorentz_sq(Double_t *x, Double_t *par)
{
    Double_t fitval = par[0]*par[2]*par[2]/(par[2]*par[2] + (x[0] - par[1])*(x[0] - par[1]));
    fitval += par[3]*par[4]*par[4]/(par[4]*par[4] + x[0]*x[0]);
    double arg = x[0]/std::sqrt(x[0]*x[0] + par[5]);
    return fitval*arg;
}

Double_t fitf_2lorentz_tanh(Double_t *x, Double_t *par)
{
    Double_t fitval = par[0]*par[2]*par[2]/(par[2]*par[2] + (x[0] - par[1])*(x[0] - par[1]));
    fitval += par[3]*par[4]*par[4]/(par[4]*par[4] + x[0]*x[0]);
    double arg = std::tanh(par[5]*x[0]);
    return fitval*arg;
}

Double_t fitf_2lorentz(Double_t *x, Double_t *par)
{
    Double_t fitval = par[0]*par[2]*par[2]/(par[2]*par[2] + (x[0] - par[1])*(x[0] - par[1]));
    fitval += par[3]*par[4]*par[4]/(par[4]*par[4] + x[0]*x[0]);
    return fitval;
}

Double_t fitf_2lorentz_logc(Double_t *x, Double_t *par)
{
    Double_t fitval = par[0]*par[2]*par[2]/(par[2]*par[2] + (x[0] - par[1])*(x[0] - par[1]));
    fitval += par[3]*par[4]*par[4]/(par[4]*par[4] + x[0]*x[0]);
    double arg = 1/(1 + std::exp(-par[5]*x[0])) - 0.5;
    return fitval*arg;
}

Double_t fitf_lorentz_exp_sq1(Double_t *x, Double_t *par)
{
    Double_t fitval = par[0]*par[2]*par[2]/(par[2]*par[2] + (x[0] - par[1])*(x[0] - par[1]));
    fitval += par[3]*std::exp(-0.015*x[0]);
    return std::sqrt(fitval*fitval + par[4]);
}

Double_t fitf_gauss(Double_t *x, Double_t *par)
{
    double arg = 0;
    if(par[2]!=0)
    {
        arg = (x[0] - par[1])/par[2];
    }
    Double_t fitval = par[0]*std::exp(-0.5*arg*arg);
    return fitval;
}

Double_t fitf_gauss_exp(Double_t *x, Double_t *par)
{
    double arg = 0;
    if(par[2]!=0)
    {
        arg = (x[0] - par[1])/par[2];
    }
    Double_t fitval = par[0]*std::exp(-0.5*arg*arg);
    fitval += par[3]*std::exp(-0.015*x[0]);
    return fitval;
}


Double_t fitf_gamma(Double_t *x, Double_t *par)
{
    //double gam = TMath::Gamma(par[0], x[0]);
    double fitval = ROOT::Math::gamma_pdf(x[0], par[0], par[1], par[2]);
    return fitval;
}

Double_t fitf_gamma_exp(Double_t *x, Double_t *par)
{
    //double gam = TMath::Gamma(par[0], x[0]);
    double fitval = ROOT::Math::gamma_pdf(x[0], par[0], par[1], par[2])*par[3];
    fitval += par[4]*std::exp(-0.015*x[0]);
    return fitval;
}

Double_t fitf_2BreitWigner(Double_t *x, Double_t *par)
{
    double gamm = std::sqrt(par[1]*par[1]*(par[1]*par[1]* + par[2]*par[2]));
    double k = (2*std::sqrt(2)*par[1]*par[2]*gamm)/(M_PI*std::sqrt(par[1]*par[1] + gamm));
    double dif = (13*1E12)*(13*1E12) - par[1]*par[1];
    double fitval = k/(dif*dif + par[1]*par[1]*par[2]*par[2]);
    return par[0]*fitval;
}

//-------------------------------------------------

int main()
{
    std::string data_loc;
    std::string particle;
    
    bool electron = true;
    if(electron)
    {
        data_loc = "/home/dominykas/dy_mas_do/EE/Electron/";
        particle = "Electron_";
    }else
    {
        data_loc = "/home/dominykas/dy_mas_do/MuMu/Muon/";
        particle = "Muon_";
    }
    
    std::string param = "InvM";
    std::string param_sub = "_400";
    
    hists1d h1;
    h1.load(data_loc + particle + param + ".root");
    
    int param_idx = 0;
    for(int i = 0; i < h1.sub_names.size(); ++i)
    {
        if(h1.sub_names[i] == param_sub)
        {
            param_idx = i;
            std::cout << param_sub << " found at idx " << i << "\n";
            break;
        }
    }
    
    TH1 * sum_h = (TH1D*)h1.MC[param_idx][0]->Clone("sum_h");
    for(int i = 1; i < h1.MC_data_names.size(); ++i)
        sum_h->Add(h1.MC[param_idx][i]);
    
    int max_bin = h1.Re[param_idx]->GetMaximumBin();
    double max_bin_GeV = h1.Re[param_idx]->GetBinCenter(max_bin);
    std::cout << "\nmax bin at " << max_bin_GeV << " GeV, with a bin width of " << h1.Re[param_idx]->GetBinWidth(max_bin) << " GeV\n\n";
    TVirtualFitter::SetMaxIterations(1000000);
    if(false)
    {
        TCanvas *c1 = new TCanvas("fit_MC_1", "fit_MC_1", 900, 900);
        TF1 *func1 = new TF1("fit1", fitf_lorentz_gauss_logc, 8, 8, 8);
        func1->SetParameter(0, 1E6);
        func1->SetParLimits(0, 0, 1E10);
        func1->SetParameter(1, 91.2);
        func1->SetParLimits(1, 85, 95);
        func1->SetParameter(2, 2.4952);
        func1->SetParLimits(2, 0.1, 4);
        func1->SetParameter(3, 1E10);
        func1->SetParLimits(3, 10, 1E15);
        func1->SetParameter(4, 0);
        func1->SetParLimits(4, -1, 1);
        func1->SetParameter(5, 200);
        func1->SetParLimits(5, 0.1, 400);
        func1->SetParameter(6, 100);
        func1->SetParLimits(6, 0, 20000);
        //func1->SetParameter(6, 25*25);
        //func1->SetParLimits(6, 100, 40*40);
        
        
        
        
        
        func1->SetParNames("Scale_1", "Mean_1", "Width_1", "Scale_2", "Mean_2", "Width_2", "slope", "offset");
        sum_h->Fit("fit1");
        c1->SetLogy();
    }
    if(true)
    {
        std::cout << "\n-----------------------------------------------------------------------------------------\n";
        TCanvas *c1 = new TCanvas("fit_MC_1", "fit_MC_1", 400, 300);
        TF1 *func1 = new TF1("fit1", fitf_gauss, 70, 110, 3);
        func1->SetParameter(2, 2.4952);
        func1->SetParLimits(2, 0.1, 4);
        func1->SetParameter(1, 91.2);
        func1->SetParLimits(1, 85, 95);
        
        func1->SetParameter(0, 1E7);
        //func2->SetParLimits(0, 0, 1E15);
        func1->SetParNames("Scale", "Mean", "Width");
        
        TF1 *func1l = new TF1("fit1l", fitf_gauss, 70, 110, 3);
        func1l->SetParameter(2, 2.4952);
        func1l->SetParLimits(2, 0.1, 10);
        func1l->SetParameter(1, 91.2);
        func1l->SetParLimits(1, 85, 95);
        
        func1l->SetParameter(0, 1E7);
        //func2->SetParLimits(0, 0, 1E15);
        func1l->SetParNames("Scale", "Mean", "Width");
        
        c1->SetLogy();
        
        
        std::string name = h1.common_name + h1.param_name + h1.sub_names[param_idx];
        THStack *hs = new THStack(name.c_str(), (";" + h1.param_name + ";Number of events").c_str());
        for(int i = 0; i < h1.MC_data_names.size(); ++i)
        {
            h1.MC[param_idx][i]->SetFillColor(i + 2);
            h1.MC[param_idx][i]->SetLineColorAlpha(i + 2, 0);
            hs->Add(h1.MC[param_idx][i]);
        }
        hs->Draw("HIST");
        TLegend* legend = new TLegend(0.85, 0.4, 0.99, 0.9);
        for(int i = 0; i < h1.MC_data_names.size(); ++i)
            legend->AddEntry(h1.MC[param_idx][i], h1.MC_data_names[i].c_str(), "f");
        
        sum_h->Fit("fit1", "RN");
        func1->SetLineColorAlpha(1, 0.7);
        func1->Draw("SAME");
        sum_h->Fit("fit1l", "RLN");
        func1l->SetLineColorAlpha(2, 0.7);
        func1l->Draw("SAME");
        legend->AddEntry(func1, "Fitted gaussian with #chi^2", "l");
        legend->AddEntry(func1l, "Fitted gaussian using ll", "l");
        legend->Draw();  
        
        c1->SaveAs((data_loc + particle + param + param_sub + "gauss_fit.pdf").c_str()); 
        std::cout << "\n-----------------------------------------------------------------------------------------\n";
    }
    if(true)
    {
        std::cout << "\n-----------------------------------------------------------------------------------------\n";
        TCanvas *c2 = new TCanvas("fit_MC_2", "fit_MC_2", 400, 300);
        TF1 *func2 = new TF1("fit2", fitf_lorentz, 70, 110, 3);
        func2->SetParameter(2, 2.4952);
        func2->SetParLimits(2, 0.1, 4);
        func2->SetParameter(1, 91.2);
        func2->SetParLimits(1, 85, 95);
        
        func2->SetParameter(0, 1E7);
        //func2->SetParLimits(0, 0, 1E15);
        
        
        func2->SetParNames("Scale", "Mean", "Width");
        
        TF1 *func2l = new TF1("fit2l", fitf_lorentz, 70, 110, 3);
        func2l->SetParameter(2, 2.4952);
        func2l->SetParLimits(2, 0.1, 4);
        func2l->SetParameter(1, 91.2);
        func2l->SetParLimits(1, 85, 95);
        
        func2l->SetParameter(0, 1E7);
        //func2->SetParLimits(0, 0, 1E15);
        
        
        func2l->SetParNames("Scale", "Mean", "Width");
        c2->SetLogy();
        
        std::string name = h1.common_name + h1.param_name + h1.sub_names[param_idx];
        THStack *hs = new THStack(name.c_str(), (";" + h1.param_name + ";Number of events").c_str());
        for(int i = 0; i < h1.MC_data_names.size(); ++i)
        {
            h1.MC[param_idx][i]->SetFillColor(i + 2);
            h1.MC[param_idx][i]->SetLineColorAlpha(i + 2, 0);
            hs->Add(h1.MC[param_idx][i]);
        }
        hs->Draw("HIST");
        TLegend* legend = new TLegend(0.85, 0.4, 0.99, 0.9);
        for(int i = 0; i < h1.MC_data_names.size(); ++i)
            legend->AddEntry(h1.MC[param_idx][i], h1.MC_data_names[i].c_str(), "f");
        
        sum_h->Fit("fit2", "RN");
        func2->SetLineColorAlpha(1, 0.7);
        func2->Draw("SAME");
        legend->AddEntry(func2, "Fitted Lorentzian peak using #chi^2", "l");
        sum_h->Fit("fit2l", "RLN");
        func2l->SetLineColorAlpha(2, 0.7);
        func2l->Draw("SAME");
        legend->AddEntry(func2l, "Fitted Lorentzian peak using ll", "l");
        legend->Draw();  
        
        c2->SaveAs((data_loc + particle + param + param_sub + "lorentz_fit.pdf").c_str()); 
        std::cout << "\n-----------------------------------------------------------------------------------------\n";
    }
    if(true)
    {
        std::cout << "\n-----------------------------------------------------------------------------------------\n";
        TCanvas *c2 = new TCanvas("fit_MC_2_no_log", "fit_MC_2_no_log", 400, 300);
        TF1 *func2 = new TF1("fit2", fitf_lorentz, 70, 110, 3);
        func2->SetParameter(2, 2.4952);
        func2->SetParLimits(2, 0.1, 4);
        func2->SetParameter(1, 91.2);
        func2->SetParLimits(1, 85, 95);
        
        func2->SetParameter(0, 1E7);
        //func2->SetParLimits(0, 0, 1E15);
        
        
        func2->SetParNames("Scale", "Mean", "Width");
        
        TF1 *func2l = new TF1("fit2l", fitf_lorentz, 70, 110, 3);
        func2l->SetParameter(2, 2.4952);
        func2l->SetParLimits(2, 0.1, 4);
        func2l->SetParameter(1, 91.2);
        func2l->SetParLimits(1, 85, 95);
        
        func2l->SetParameter(0, 1E7);
        //func2->SetParLimits(0, 0, 1E15);
        
        
        func2l->SetParNames("Scale", "Mean", "Width");
        
        std::string name = h1.common_name + h1.param_name + h1.sub_names[param_idx];
        THStack *hs = new THStack(name.c_str(), (";" + h1.param_name + ";Number of events").c_str());
        for(int i = 0; i < h1.MC_data_names.size(); ++i)
        {
            h1.MC[param_idx][i]->SetFillColor(i + 2);
            h1.MC[param_idx][i]->SetLineColorAlpha(i + 2, 0);
            hs->Add(h1.MC[param_idx][i]);
        }
        hs->Draw("HIST");
        TLegend* legend = new TLegend(0.85, 0.4, 0.99, 0.9);
        for(int i = 0; i < h1.MC_data_names.size(); ++i)
            legend->AddEntry(h1.MC[param_idx][i], h1.MC_data_names[i].c_str(), "f");
        
        sum_h->Fit("fit2", "RN");
        func2->SetLineColorAlpha(1, 0.7);
        func2->Draw("SAME");
        legend->AddEntry(func2, "Fitted Lorentzian peak using #chi^2", "l");
        sum_h->Fit("fit2l", "RLN");
        func2l->SetLineColorAlpha(2, 0.7);
        func2l->Draw("SAME");
        legend->AddEntry(func2l, "Fitted Lorentzian peak using ll", "l");
        legend->Draw();  
        
        c2->SaveAs((data_loc + particle + param + param_sub + "lorentz_fit_no_log_scale.pdf").c_str()); 
        std::cout << "\n-----------------------------------------------------------------------------------------\n";
    }
    if(false)
    {
        TCanvas *c3 = new TCanvas("fit_MC_3", "fit_MC_3", 900, 900);
        TF1 *func3 = new TF1("fit3", fitf_lorentz, 70, 110, 3);
        func3->SetParameter(2, 2.4952);
        //func3->FixParameter(2, 2.0);
        func3->SetParLimits(2, 0.1, 4);
        func3->SetParameter(1, 91.2);
        func3->SetParLimits(1, 85, 95);
        
        func3->SetParameter(0, 1.9E6);
        //func3->SetParLimits(0, 0, 1E15);
        
        //func3->SetParameter(3, 1E10);
        //func3->SetParameter(4, 0.015);
        //func3->SetParLimits(4, 0, 1);
        //func3->FixParameter(5, 0.015);
        //func3->SetParLimits(5, 0, 0.5);
        
        
        func3->SetParNames("Scale_1", "Mean_1", "Sigma_1", "Scale_2", "Sigma_2", "sq_offset");
        //h1.Re[param_idx]->SetMinimum(10);
        
        TFitResultPtr Fitted_3 = h1.Re[param_idx]->Fit("fit3", "LR");
        c3->SetLogy();
    }
    if(false)
    {
        TCanvas *c4 = new TCanvas("fit_MC_4", "fit_MC_4", 900, 900);
        TF1 *func4 = new TF1("fit4", fitf_gamma_exp, 70, 399, 5);
        
        func4->SetParameter(0, 1);
        func4->SetParLimits(0, 0, 100);
        
        func4->SetParameter(2, 91.2);
        //func4->SetParLimits(2, 85, 95);
        
        func4->SetParameter(1, 1);
        //func4->SetParLimits(1, 0, 1E15);
        func4->SetParameter(3, 1.9E6);
        func4->SetParameter(4, 2E4);
        //func4->SetParameter(4, 0.015);
        
        
        
        //func4->SetParNames("Scale_1", "Mean_1", "Sigma_1", "Scale_exp", "Slope");
        sum_h->Fit("fit4", "R");
        c4->SetLogy();
    }
    if(false)
    {
        TCanvas *c5 = new TCanvas("fit_MC_5", "fit_MC_5", 900, 900);
        TF1 *func5 = new TF1("fit5", fitf_lorentz_exp_sq1, 15, 399, 5);
        func5->SetParameter(2, 2.4952);
        func5->SetParameter(2, 2.0);
        //func5->SetParLimits(2, 0.1, 4);
        func5->SetParameter(1, 91.2);
        func5->SetParLimits(1, 85, 95);
        
        func5->SetParameter(0, 1.9E6);
        //func5->SetParLimits(0, 0, 1E15);
        
        func5->SetParameter(3, 2E4);
        func5->SetParameter(4, 25*25);
        func5->SetParLimits(4, -1E10, 40*40);
        
        
        func5->SetParNames("Scale_1", "Mean_1", "Sigma_1", "Scale_exp", "Slope");
        h1.Re[param_idx]->SetMinimum(10);
        h1.Re[param_idx]->Fit("fit5", "R");
        c5->SetLogy();
    }
    return 0;
}
