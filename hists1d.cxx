#include <vector>
#include <iostream>
#include <string>
#include <fstream>
#include <stdbool.h>
#include <unistd.h>
#include <algorithm>
#include <numeric>
#include <cmath>
#include <limits>
#include </home/dominykas/dy_mas_do/hists1d.h>
#include </home/dominykas/Software/root_install/include/TH1D.h>
#include </home/dominykas/Software/root_install/include/TH2D.h>
#include </home/dominykas/Software/root_install/include/THStack.h>
#include </home/dominykas/Software/root_install/include/TCanvas.h>
#include </home/dominykas/Software/root_install/include/TLegend.h>
#include </home/dominykas/Software/root_install/include/TFile.h>
#include </home/dominykas/Software/root_install/include/TLatex.h>

hists1d::hists1d()
{
    created = false;
}
    
hists1d::hists1d(int init_count, std::vector<std::string> MC_data_names_in, std::string common_name_in, std::vector<double> bins_in, std::string param_name_in, std::string param_txt_in):hists1d(MC_data_names_in, common_name_in, std::vector<std::vector<double>>(init_count, bins_in), param_name_in, param_txt_in) {}
    
hists1d::hists1d(std::vector<std::string> MC_data_names_in, std::string common_name_in, std::vector<std::vector<double>> bins_in, std::string param_name_in, std::string param_txt_in)
{
    MC_data_names = MC_data_names_in;
    common_name = common_name_in;
    bins = bins_in;
    param_name = param_name_in;
    param_txt = param_txt_in;
    MC = std::vector<std::vector<TH1*>>(bins_in.size(), std::vector<TH1*>(MC_data_names_in.size()));
    Re = std::vector<TH1*>(bins_in.size());
    for(int i = 0; i < bins_in.size(); ++i)
    {
        for(int j = 0; j < MC_data_names_in.size(); ++j)
        {
            MC[i][j] = new TH1D((MC_data_names_in[j] + common_name + param_txt + std::to_string(i)).c_str(),
                                (MC_data_names_in[j] + common_name + param_name + std::to_string(i)).c_str(),
                                bins_in[i].size() - 1, &bins_in[i][0]);
        }
        Re[i] = new TH1D(("real_data_" + common_name + param_txt + std::to_string(i)).c_str(),
                        ("real_data_" + common_name + param_name + std::to_string(i)).c_str(),
                        bins_in[i].size() - 1, &bins_in[i][0]);
    }
}
    
hists1d::hists1d(int init_count, std::vector<std::string> MC_data_names_in, std::string common_name_in, int bincount_in, double low_lim_in, double high_lim_in, std::string param_name_in, std::string param_txt_in):hists1d(
    MC_data_names_in, 
    common_name_in, 
    std::vector<int>(init_count, bincount_in), 
    std::vector<double>(init_count, low_lim_in), 
    std::vector<double>(init_count, high_lim_in), param_name_in, param_txt_in) {}
    
hists1d::hists1d(std::vector<std::string> MC_data_names_in, std::string common_name_in, std::vector<int> bincount_in, std::vector<double> low_lim_in, std::vector<double> high_lim_in, std::string param_name_in, std::string param_txt_in)
{
    MC_data_names = MC_data_names_in;
    common_name = common_name_in;
    bincount = bincount_in;
    low_lim = low_lim_in;
    high_lim = high_lim_in;
    param_name = param_name_in;
    param_txt = param_txt_in;
    MC = std::vector<std::vector<TH1*>>(bincount_in.size(), std::vector<TH1*>(MC_data_names_in.size()));
    Re = std::vector<TH1*>(bincount_in.size());
        
    for(int i = 0; i < bincount_in.size(); ++i)
    {
        for(int j = 0; j < MC_data_names_in.size(); ++j)
        {
            MC[i][j] = new TH1D((MC_data_names_in[j] + common_name + param_txt + std::to_string(i)).c_str(),
                                (MC_data_names_in[j] + common_name + param_name + std::to_string(i)).c_str(),
                                bincount_in[i], low_lim_in[i], high_lim_in[i]);
        }
        Re[i] = new TH1D(("real_data_" + common_name + param_txt + std::to_string(i)).c_str(),
                        ("real_data_" + common_name + param_name + std::to_string(i)).c_str(),
                        bincount_in[i], low_lim_in[i], high_lim_in[i]);
    }
}
    
hists1d::hists1d(std::vector<std::string> MC_data_names_in, std::string common_name_in, std::vector<std::string> sub_names_in, std::vector<double> bins_in, std::string param_name_in, std::string param_txt_in):hists1d(MC_data_names_in, common_name_in, sub_names_in, std::vector<std::vector<double>>(sub_names_in.size(), bins_in), param_name_in, param_txt_in) { }

    
hists1d::hists1d(std::vector<std::string> MC_data_names_in, std::string common_name_in, std::vector<std::string> sub_names_in, std::vector<std::vector<double>> bins_in, std::string param_name_in, std::string param_txt_in)
{
    MC_data_names = MC_data_names_in;
    common_name = common_name_in;
    bins = bins_in;
    sub_names = sub_names_in;
    param_name = param_name_in;
    param_txt = param_txt_in;
    MC = std::vector<std::vector<TH1*>>(bins_in.size(), std::vector<TH1*>(MC_data_names_in.size()));
    Re = std::vector<TH1*>(bins_in.size());
    for(int i = 0; i < bins_in.size(); ++i)
    {
        for(int j = 0; j < MC_data_names_in.size(); ++j)
        {
            MC[i][j] = new TH1D((MC_data_names_in[j] + common_name + param_txt + sub_names_in[i]).c_str(),
                                (MC_data_names_in[j] + common_name + param_name + sub_names_in[i]).c_str(),
                                bins_in[i].size() - 1, &bins_in[i][0]);
        }
        Re[i] = new TH1D(("real_data_" + common_name + param_txt + sub_names_in[i]).c_str(),
                        ("real_data_" + common_name + param_name + sub_names_in[i]).c_str(),
                        bins_in[i].size() - 1, &bins_in[i][0]);
    }
}
    
hists1d::hists1d(std::vector<std::string> MC_data_names_in, 
        std::string common_name_in, 
        std::vector<std::string> sub_names_in, 
        int bincount_in, double low_lim_in, double high_lim_in, std::string param_name_in, std::string param_txt_in):hists1d(
            MC_data_names_in, 
            common_name_in, 
            sub_names_in, 
            std::vector<int>(sub_names_in.size(), bincount_in), 
            std::vector<double>(sub_names_in.size(), low_lim_in), 
            std::vector<double>(sub_names_in.size(), high_lim_in), param_name_in, param_txt_in) {}
    
hists1d::hists1d(std::vector<std::string> MC_data_names_in, std::string common_name_in, std::vector<std::string> sub_names_in, std::vector<int> bincount_in, std::vector<double> low_lim_in, std::vector<double> high_lim_in, std::string param_name_in, std::string param_txt_in)
{
    MC_data_names = MC_data_names_in;
    common_name = common_name_in;
    bincount = bincount_in;
    low_lim = low_lim_in;
    high_lim = high_lim_in;
    sub_names =sub_names_in;
    param_name = param_name_in;
    param_txt = param_txt_in;
    MC = std::vector<std::vector<TH1*>>(bincount_in.size(), std::vector<TH1*>(MC_data_names_in.size()));
    Re = std::vector<TH1*>(bincount_in.size());
    for(int i = 0; i < bincount_in.size(); ++i)
    {
        for(int j = 0; j < MC_data_names_in.size(); ++j)
        {
            MC[i][j] = new TH1D((MC_data_names_in[j] + common_name + param_txt + sub_names_in[i]).c_str(),
                                (MC_data_names_in[j] + common_name + param_name + sub_names_in[i]).c_str(),
                                bincount_in[i], low_lim_in[i], high_lim_in[i]);
        }
        Re[i] = new TH1D(("real_data_" + common_name + param_txt + sub_names_in[i]).c_str(),
                        ("real_data_" + common_name + param_name + sub_names_in[i]).c_str(),
                        bincount_in[i], low_lim_in[i], high_lim_in[i]);
    }
}
    
void hists1d::expand(int expand_count)
{
    if(bins.size() > 0)
    {
        hists1d::expand(std::vector<std::vector<double>>(expand_count, bins[0]));
    } else
    {
        hists1d::expand(std::vector<int>(expand_count, bincount[0]), 
            std::vector<double>(expand_count, low_lim[0]), 
            std::vector<double>(expand_count, high_lim[0]));
    }
}
    
void hists1d::expand(std::vector<std::vector<double>> expand_bins)
{
    int temp = Re.size();
    for(int i = 0; i < expand_bins.size(); ++i)
    {
        bins.push_back(expand_bins[i]);
        MC.push_back(std::vector<TH1*>(MC_data_names.size()));
        for(int j = 0; j < MC_data_names.size(); ++j)
        {
            MC[temp + i][j] = new TH1D((MC_data_names[j] + common_name + param_txt + std::to_string(temp + i)).c_str(),
                                    (MC_data_names[j] + common_name + param_name + std::to_string(temp + i)).c_str(),
                                    expand_bins[i].size() - 1, &expand_bins[i][0]);
        }
        Re.push_back(new TH1D(("real_data_" + common_name + param_txt + std::to_string(temp + i)).c_str(),
                            ("real_data_" + common_name + param_name + std::to_string(temp + i)).c_str(),
                            expand_bins[i].size() - 1, &expand_bins[i][0]));
    }
}
void hists1d::expand(std::vector<int> expand_bincount, std::vector<double> expand_low_lim, std::vector<double> expand_high_lim)
{
    int temp = Re.size();
        
    for(int i = 0; i < expand_bincount.size(); ++i)
    {
        bincount.push_back(expand_bincount[i]);
        low_lim.push_back(expand_low_lim[i]);
        high_lim.push_back(expand_high_lim[i]);
        MC.push_back(std::vector<TH1*>(MC_data_names.size()));
        for(int j = 0; j < MC_data_names.size(); ++j)
        {
            MC[temp + i][j] = new TH1D((MC_data_names[j] + common_name + param_txt + std::to_string(temp + i)).c_str(),
                                    (MC_data_names[j] + common_name + param_name + std::to_string(temp + i)).c_str(),
                                    expand_bincount[i], expand_low_lim[i], expand_high_lim[i]);
            }
        Re.push_back(new TH1D(("real_data_" + common_name + param_txt + std::to_string(temp + i)).c_str(),
                            ("real_data_" + common_name + param_name + std::to_string(temp + i)).c_str(),
                            expand_bincount[i], expand_low_lim[i], expand_high_lim[i]));
    }
}
    
void hists1d::expand(std::vector<std::string> expand_names)
{
    if(bins.size() > 0)
    {
        hists1d::expand(expand_names, std::vector<std::vector<double>>(expand_names.size(), bins[0]));
    } else
    {
        hists1d::expand(expand_names, 
            std::vector<int>(expand_names.size(), bincount[0]), 
            std::vector<double>(expand_names.size(), low_lim[0]), 
            std::vector<double>(expand_names.size(), high_lim[0]));
    }
}
    
void hists1d::expand(std::vector<std::string> expand_names, std::vector<std::vector<double>> expand_bins)
{
    int temp = Re.size();
    for(int i = 0; i < expand_names.size(); ++i)
    {
        sub_names.push_back(expand_names[i]);
        bins.push_back(expand_bins[i]);
        MC.push_back(std::vector<TH1*>(MC_data_names.size()));
        for(int j = 0; j < MC_data_names.size(); ++j)
        {
            MC[temp + i][j] = new TH1D((MC_data_names[j] + common_name + param_txt + expand_names[i]).c_str(),
                                    (MC_data_names[j] + common_name + param_name + expand_names[i]).c_str(),
                                    expand_bins[i].size() - 1, &expand_bins[i][0]);
        }
        Re.push_back(new TH1D(("real_data_" + common_name + param_txt + expand_names[i]).c_str(),
                            ("real_data_" + common_name + param_name + expand_names[i]).c_str(),
                            expand_bins[i].size() - 1, &expand_bins[i][0]));
    }
}
void hists1d::expand(std::vector<std::string> expand_names, std::vector<int> expand_bincount, std::vector<double> expand_low_lim, std::vector<double> expand_high_lim)
{
    int temp = Re.size();
    for(int i = 0; i < expand_names.size(); ++i)
    {
        sub_names.push_back(expand_names[i]);
        bincount.push_back(expand_bincount[i]);
        low_lim.push_back(expand_low_lim[i]);
        high_lim.push_back(expand_high_lim[i]);
        MC.push_back(std::vector<TH1*>(MC_data_names.size()));
        for(int j = 0; j < MC_data_names.size(); ++j)
        {
            MC[temp + i][j] = new TH1D((MC_data_names[j] + common_name + param_txt + expand_names[i]).c_str(),
                                        (MC_data_names[j] + common_name + param_name + expand_names[i]).c_str(),
                                        expand_bincount[i], expand_low_lim[i], expand_high_lim[i]);
        }
        Re.push_back(new TH1D(("real_data_" + common_name + param_txt + expand_names[i]).c_str(),
                                ("real_data_" + common_name + param_name + expand_names[i]).c_str(),
                                expand_bincount[i], expand_low_lim[i], expand_high_lim[i]));
    }
}
    
void hists1d::plot(std::string data_save_core, bool logy, bool logx, int nDivY, int nDivY_rat, int nDivX, double min_val)
{
    hists1d::plot(data_save_core, std::vector<bool>(MC.size(), logy), std::vector<bool>(MC.size(), logx), nDivY, nDivY_rat, nDivX, min_val);
}
    
void hists1d::plot(std::string data_save_core, bool logy, std::vector<bool> logx, int nDivY, int nDivY_rat, int nDivX, double min_val)
{
    hists1d::plot(data_save_core, std::vector<bool>(MC.size(), logy), logx, nDivY, nDivY_rat, nDivX, min_val);
}
    
void hists1d::plot(std::string data_save_core, std::vector<bool> logy, bool logx, int nDivY, int nDivY_rat, int nDivX, double min_val)
{
    hists1d::plot(data_save_core, logy, std::vector<bool>(MC.size(), logx), nDivY, nDivY_rat, nDivX, min_val);
}
    
void hists1d::plot(std::string data_save_core, std::vector<bool> logy, std::vector<bool> logx, int nDivY, int nDivY_rat, int nDivX, double min_val)
{
    for(int j = 0; j < MC.size(); ++j)
    {
        std::string data_save;
        std::string name;
        std::string name_save;
        if(sub_names.size() == 0)
        {
            data_save = data_save_core + common_name + std::to_string(j) + "/";
            name = common_name + param_name + std::to_string(j);
            name_save = common_name + param_txt + std::to_string(j);
        } else
        {
            data_save = data_save_core;
            name = common_name + param_name + sub_names[j];
            name_save = common_name + param_txt + sub_names[j];
        }
        
        THStack *hs = new THStack(name.c_str(), "Preliminary data;;Number of events");
        double NumOfEntries = 0;
        for(int i = 0; i < MC[j].size(); ++i)
        {
            MC[j][i]->SetFillColor(i + 2);
            MC[j][i]->SetLineColorAlpha(i + 2, 0);
            hs->Add(MC[j][i]);
            NumOfEntries += MC[j][i]->GetEntries();
        }
        if((NumOfEntries <= 0) || (hs->GetMinimum() == hs->GetMaximum()))
        {
            std::cout << "No MC data for " + name_save + " graph not drawn\n";
            continue;
        }
        /*
        if((Re[j]->GetEntries() <= 0) || (Re[j]->GetMinimum() == Re[j]->GetMaximum()))
        {
            std::cout << "No Re data for " + name_save + " graph not drawn\n";
            continue;
        }
        */
        //std::cout << "\n";
        TCanvas *c = new TCanvas(name_save.c_str(), name.c_str(), 400, 300);
        c->SetTopMargin(0);
        c->SetBottomMargin(0);
        c->Divide(1, 2, 0.01, 0);
        TPad *p_1 = (TPad*)(c->cd(1));
        p_1->SetTopMargin(0.05);
        if(logy[j])
        {
            if(hs->GetMaximum() > 0)
                p_1->SetLogy();
            else
                std::cout << "Couldn't set logy for "  + name_save + " because max_val = " + hs->GetMaximum() + "\n";
        }
        hs->SetMinimum(min_val);
        if(logx[j])
            p_1->SetLogx();
        hs->Draw("HIST");
        hs->GetYaxis()->SetTitleSize(0.07);
        hs->GetYaxis()->SetLabelSize(0.07);
        hs->GetYaxis()->SetTitleOffset(0.55);
        hs->GetYaxis()->SetNdivisions(nDivY);
        hs->GetXaxis()->SetNdivisions(nDivX);
        Re[j]->SetMarkerStyle(5);
        Re[j]->SetMarkerSize(0.5);
        Re[j]->Draw("P0 same");
            
        TLegend* legend = new TLegend(0.85, 0.4, 0.99, 0.9);
        for(int i = 0; i < MC_data_names.size(); ++i)
            legend->AddEntry(MC[j][i], MC_data_names[i].c_str(), "f");
            
        legend->AddEntry(Re[j], "CMS data", "p");
        legend->Draw();            
            
        TPad *p_2 = (TPad*)(c->cd(2));
        p_2->SetBottomMargin(0.15);
        if(logx[j])
            p_2->SetLogx();
        p_2->SetGrid();
        TH1 * sum_h = (TH1D*)MC[j][0]->Clone("sum_h");
        for(int i = 1; i < MC_data_names.size(); ++i)
            sum_h->Add(MC[j][i]);
        TH1* rat_h = (TH1D*)Re[j]->Clone("rat_h");
        rat_h->Divide(sum_h);
        rat_h->SetMinimum(0.75);
        rat_h->SetMaximum(1.25);
        rat_h->SetTitle((";" + param_name + ";Data/MC").c_str());
        rat_h->GetYaxis()->SetTitleSize(0.07);
        rat_h->GetXaxis()->SetTitleSize(0.07);
        rat_h->GetYaxis()->SetLabelSize(0.07);
        rat_h->GetXaxis()->SetLabelSize(0.07);
        rat_h->GetYaxis()->SetTitleOffset(0.55);
        rat_h->GetXaxis()->SetTitleOffset(1);
        rat_h->GetYaxis()->SetNdivisions(nDivY_rat);
        rat_h->GetXaxis()->SetNdivisions(nDivX);
        rat_h->SetStats(0);
        rat_h->Draw();
            
        c->SaveAs((data_save + name_save + ".pdf").c_str());
    }
}
void hists1d::save_txt(std::string data_save, std::string delimit)
{
    std::ofstream hist_file;
    hist_file.open(data_save + common_name + param_txt + ".txt");
    if(hist_file.is_open())
    {
        hist_file << created << "\n";
        hist_file << common_name << "\n";
        hist_file << param_name << "\n";
        hist_file << param_txt << "\n";
        hist_file << MC.size() << "\n";
        hist_file << sub_names.size() << "\n";
        hist_file << bins.size() << "\n";
        hist_file << MC_data_names.size() << "\n";
        for(int j = 0; j < MC_data_names.size(); ++j)
        {
            hist_file << MC_data_names[j] << delimit;
        }
        hist_file << "\n";
        for(int i = 0; i < MC.size(); ++i)
        {
            if(sub_names.size() > 0)
            {
                hist_file << sub_names[i] << "\n";
            } else
            {
                hist_file << "\n";
            }
            if(bins.size() > 0)
            {
                hist_file << bins[i].size() << "\n";
                for(int j = 0; j < bins[i].size(); ++j)
                {
                    hist_file << bins[i][j] << delimit;
                }
                hist_file << "\n";
                for(int j = 0; j < MC_data_names.size(); ++j)
                {
                    for(int k = 0; k < bins[i].size() - 1; ++k)
                    {
                        hist_file << MC[i][j]->GetBinContent(k) << delimit;
                    }
                    hist_file << "\n";
                }
                for(int k = 0; k < bins[i].size() - 1; ++k)
                {
                    hist_file << Re[i]->GetBinContent(k) << delimit;
                }
                hist_file << "\n";
            }else
            {
                hist_file << bincount[i] << delimit;
                hist_file << low_lim[i] << delimit;
                hist_file << high_lim[i] << "\n";
                for(int j = 0; j < MC_data_names.size(); ++j)
                {
                    for(int k = 0; k < bincount[i]; ++k)
                    {
                        hist_file << MC[i][j]->GetBinContent(k) << delimit;
                    }
                    hist_file << "\n";
                }
            }
        
        }
    } else
    {
        std::cout << "Saving operation of file " + common_name + param_txt + ".txt failed\n";
    }
    hist_file.close();
    
}
    
void hists1d::load_txt(std::string data_save, std::string delimit)
{
    std::ifstream hist_file;
    hist_file.open(data_save);
    if(hist_file.is_open())
    {
        hist_file >> created;

        hist_file >> common_name;
        
        hist_file >> param_name;
        
        hist_file >> param_txt;
            
        int MC_size;
        hist_file >> MC_size;
            
        int sub_names_size;
        hist_file >> sub_names_size;
        if(sub_names_size > 0)
            sub_names = std::vector<std::string>(sub_names_size);
            
        int bins_size;
        hist_file >> bins_size;
        if(bins_size > 0)
        {
            bins = std::vector<std::vector<double>>(bins_size);
        }
        else
        {
            bincount = std::vector<int>(MC_size);
            low_lim = std::vector<double>(MC_size);
            high_lim = std::vector<double>(MC_size);
        }
            
        int MC_data_names_size;
        hist_file >> MC_data_names_size;
        MC_data_names = std::vector<std::string>(MC_data_names_size);
            
        MC = std::vector<std::vector<TH1*>>(MC_size, std::vector<TH1*>(MC_data_names.size()));
        Re = std::vector<TH1*>(bins.size());
            
        for(int j = 0; j < MC_data_names.size(); ++j)
        {
            hist_file >> MC_data_names[j];
        }
            
        for(int i = 0; i < MC.size(); ++i)
        {
            if(sub_names.size() > 0)
            {
                hist_file >> sub_names[i];
            }
                
            if(bins.size() > 0)
            {
                int bins_size_sub;
                hist_file >> bins_size_sub;
                bins[i] = std::vector<double>(bins_size_sub);
                for(int j = 0; j < bins[i].size(); ++j)
                {
                    hist_file >> bins[i][j];
                }
                if(sub_names.size() > 0)
                {
                    for(int j = 0; j < MC_data_names.size(); ++j)
                    {
                        MC[i][j] = new TH1D((MC_data_names[j] + common_name + param_txt + sub_names[i]).c_str(),
                                            (MC_data_names[j] + common_name + param_name + sub_names[i]).c_str(),
                                            bins[i].size() - 1, &bins[i][0]);
                    }
                    Re[i] = new TH1D(("real_data_" + common_name + param_txt + sub_names[i]).c_str(),
                                    ("real_data_" + common_name + param_name + sub_names[i]).c_str(),
                                    bins[i].size() - 1, &bins[i][0]);
                        
                }else
                {
                    for(int j = 0; j < MC_data_names.size(); ++j)
                    {
                        MC[i][j] = new TH1D((MC_data_names[j] + common_name + param_txt + std::to_string(i)).c_str(),
                                            (MC_data_names[j] + common_name + param_name + std::to_string(i)).c_str(),
                                            bins[i].size() - 1, &bins[i][0]);
                    }
                    Re[i] = new TH1D(("real_data_" + common_name + param_txt + std::to_string(i)).c_str(),
                                    ("real_data_" + common_name + param_name + std::to_string(i)).c_str(),
                                    bins[i].size() - 1, &bins[i][0]);
                }
                for(int j = 0; j < MC_data_names.size(); ++j)
                {
                    for(int k = 0; k < bins[i].size() - 1; ++k)
                    {
                        double hist_bin_val;
                        hist_file >> hist_bin_val;
                        MC.at(i).at(j)->SetBinContent(k, hist_bin_val);
                    }
                }
                for(int k = 0; k < bins[i].size() - 1; ++k)
                {
                    double hist_bin_val;
                    hist_file >> hist_bin_val;
                    Re[i]->SetBinContent(k, hist_bin_val);
                }
            }else
            {
                    
                hist_file >> bincount[i];
                hist_file >> low_lim[i];
                hist_file >> high_lim[i];
                if(sub_names.size() > 0)
                {
                    for(int j = 0; j < MC_data_names.size(); ++j)
                    {
                        MC[i][j] = new TH1D((MC_data_names[j] + common_name + param_txt + sub_names[i]).c_str(),
                                            (MC_data_names[j] + common_name + param_name + sub_names[i]).c_str(),
                                            bincount[i], low_lim[i], high_lim[i]);
                    }
                    Re[i] = new TH1D(("real_data_" + common_name + param_txt + sub_names[i]).c_str(),
                                    ("real_data_" + common_name + param_name + sub_names[i]).c_str(),
                                    bincount[i], low_lim[i], high_lim[i]);
                }else
                {
                    for(int j = 0; j < MC_data_names.size(); ++j)
                    {
                        MC[i][j] = new TH1D((MC_data_names[j] + common_name + param_txt + std::to_string(i)).c_str(),
                                            (MC_data_names[j] + common_name + param_name + std::to_string(i)).c_str(),
                                            bincount[i], low_lim[i], high_lim[i]);
                    }
                    Re[i] = new TH1D(("real_data_" + common_name + param_txt + std::to_string(i)).c_str(),
                                    ("real_data_" + common_name + param_name + std::to_string(i)).c_str(),
                                    bincount[i], low_lim[i], high_lim[i]);
                }
                for(int j = 0; j < MC_data_names.size(); ++j)
                {
                    for(int k = 0; k < bincount[i] - 1; ++k)
                    {
                        double hist_bin_val;
                        hist_file >> hist_bin_val;
                        MC[i][j]->SetBinContent(k, hist_bin_val);
                    }
                }
                for(int k = 0; k < bincount[i] - 1; ++k)
                {
                    double hist_bin_val;
                    hist_file >> hist_bin_val;
                    Re[i]->SetBinContent(k, hist_bin_val);
                }
            }
                
        }
    } else
    {
        std::cout << "Loading operation of file " + data_save + " failed\n";
    }
    hist_file.close();
}
void hists1d::save(std::string data_save)
{
    TFile hist_file((data_save + common_name + param_txt + ".root").c_str(), "RECREATE");
    std::string created_temp = std::to_string((int)created);
    hist_file.WriteObject(&created_temp, "created");
    hist_file.WriteObject(&MC_data_names, "MC_data_names");
    hist_file.WriteObject(&common_name, "common_name");
    hist_file.WriteObject(&bins, "bins");
    hist_file.WriteObject(&low_lim, "low_lim");
    hist_file.WriteObject(&high_lim, "high_lim");
    hist_file.WriteObject(&bincount, "bincount");
    hist_file.WriteObject(&sub_names, "sub_names");
    hist_file.WriteObject(&param_name, "param_name");
    hist_file.WriteObject(&param_txt, "param_txt");
    for(int i = 0; i < MC.size(); ++i)
    {
        for(int j = 0; j < MC[i].size(); ++j)
        {
            MC[i][j]->Write();
        }
        Re[i]->Write();
    }
    hist_file.Close();
}

void hists1d::load(std::string file_dir)
{
        
    TFile hist_file(file_dir.c_str(), "READ");
        
    std::vector<std::string> * MC_data_names_temp;
    hist_file.GetObject("MC_data_names", MC_data_names_temp);
    MC_data_names = *MC_data_names_temp;
        
    std::string * common_name_temp;
    hist_file.GetObject("common_name", common_name_temp);
    common_name = *common_name_temp;
        
    std::vector<std::vector<double>>* bins_temp;
    hist_file.GetObject("bins", bins_temp);
    bins = *bins_temp;
        
    std::vector<double>* low_lim_temp;
    hist_file.GetObject("low_lim", low_lim_temp);
    low_lim = * low_lim_temp;
        
    std::vector<double>* high_lim_temp;
    hist_file.GetObject("high_lim", high_lim_temp);
    high_lim = *high_lim_temp;
        
    std::vector<int>* bincount_temp;
    hist_file.GetObject("bincount", bincount_temp);
    bincount = *bincount_temp;
        
    std::vector<std::string>* sub_names_temp;
    hist_file.GetObject("sub_names", sub_names_temp);
    sub_names = *sub_names_temp;
    
    std::string * param_name_temp;
    hist_file.GetObject("param_name", param_name_temp);
    param_name = *param_name_temp;
    
    std::string * param_txt_temp;
    hist_file.GetObject("param_txt", param_txt_temp);
    param_txt = *param_txt_temp;
        
    MC = std::vector<std::vector<TH1*>>(std::max(bins.size(), bincount.size()), std::vector<TH1*>(MC_data_names.size()));
    Re = std::vector<TH1*>(std::max(bins.size(), bincount.size()));
        
    //std::cout << "\n" << max(bins.size(), bincount.size()) << "\n";
    for(int i = 0; i < MC.size(); ++i)
    {
        for(int j = 0; j < MC[i].size(); ++j)
        {
            if(sub_names.size() > 0)
                MC[i][j] = (TH1D*)hist_file.Get((MC_data_names[j] + common_name + param_txt + sub_names[i]).c_str());
            else
                MC[i][j] = (TH1D*)hist_file.Get((MC_data_names[j] + common_name + param_txt + std::to_string(i)).c_str());
            MC[i][j]->SetDirectory(0);
            //std::cout << MC[i][j]->GetBinContent(0) << " ";
        }
        std::cout << "\n";
        if(sub_names.size() > 0)
            Re[i] = (TH1D*)hist_file.Get(("real_data_" + common_name + param_txt + sub_names[i]).c_str());
        else
            Re[i] = (TH1D*)hist_file.Get(("real_data_" + common_name + param_txt + std::to_string(i)).c_str());
        Re[i]->SetDirectory(0);
    }
        
    hist_file.Close();
    
    std::cout << "load complete\n";
        
}
