#include <vector>
#include <iostream>
#include <string>
#include <fstream>
#include <stdbool.h>
#include <unistd.h>
#include <algorithm>
#include <numeric>
#include <cmath>
#include <limits>
#include <time.h>
#include "/home/dominykas/dy_mas_do/hists1d.h"
#include </home/dominykas/Software/root_install/include/TH1D.h>
#include </home/dominykas/Software/root_install/include/TH2D.h>
#include </home/dominykas/Software/root_install/include/THStack.h>
#include </home/dominykas/Software/root_install/include/TCanvas.h>
#include </home/dominykas/Software/root_install/include/TLegend.h>
#include </home/dominykas/Software/root_install/include/TFile.h>
#include </home/dominykas/Software/root_install/include/TChain.h>
#include </home/dominykas/Software/root_install/include/TBranch.h>
#include </home/dominykas/Software/root_install/include/TTree.h>
#include </home/dominykas/Software/root_install/include/TLatex.h>

int main()
{
    std::string data_loc;
    std::string particle;
    
    bool electron = true;
    if(electron)
    {
        data_loc = "/home/dominykas/dy_mas_do/EE/Electron/";
        particle = "Electron";
    }else
    {
        data_loc = "/home/dominykas/dy_mas_do/MuMu/Muon/";
        particle = "Muon";
    }
    
    std::string param = "InvM";
    
    hists1d h1;
    h1.load(data_loc + "data/" + particle + "_" + param + ".root");
    
    h1.plot(data_loc, true, {false, false, true});
    return 0;
}
