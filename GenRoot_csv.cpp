#include <vector>
#include <iostream>
#include <string>
#include <fstream>
#include <stdbool.h>
#include <unistd.h>
#include <algorithm>
#include <numeric>
#include <cmath>
#include <limits>
#include <time.h>
#include <cmath>
#include "/home/dominykas/dy_mas_do/hists1d.h"
#include </home/dominykas/Software/root_install/include/TH1D.h>
#include </home/dominykas/Software/root_install/include/TH2D.h>
#include </home/dominykas/Software/root_install/include/THStack.h>
#include </home/dominykas/Software/root_install/include/TCanvas.h>
#include </home/dominykas/Software/root_install/include/TLegend.h>
#include </home/dominykas/Software/root_install/include/TFile.h>
#include </home/dominykas/Software/root_install/include/TChain.h>
#include </home/dominykas/Software/root_install/include/TBranch.h>
#include </home/dominykas/Software/root_install/include/TTree.h>
#include </home/dominykas/Software/root_install/include/TLatex.h>
#include </home/dominykas/Software/root_install/include/TError.h>
#include </home/dominykas/Software/root_install/include/TGraphAsymmErrors.h>


struct particles_dat
{
    std::vector<double> pT;
    std::vector<double> eta;
    std::vector<double> phi;
    std::vector<double> E;
    double InvM;
};

struct evnt_dat
{
    bool correct_dat = false;
    double weight;
    double MET_pT;
    double MET_phi;
    int nVert;
};

struct full_particles_dat
{
    std::vector<int> corr_idx = {1, 2};
    std::string corr_name = "isSelPassed";
    std::string weight_name = "GENEvt_weight";
    std::string nPileUp_name = "nPileUp";
    std::string pT_name = "pT";
    std::string eta_name = "eta";
    std::string phi_name = "phi";
    std::string E_name = "Energy";
    std::string InvM_name = "InvM";
    std::string nVert_name = "nVertices";
    std::string MET_pT_name = "MET_pT";
    std::string MET_phi_name = "MET_phi";
    std::vector<std::vector<particles_dat>> part_data;
    std::vector<evnt_dat> event_data;
};
    
std::vector<std::string> add_each(std::vector<std::string> initi, std::string add);

full_particles_dat data_extract(
                                std::string tree_name,            //cant seem to pass a chain properly
                                std::string data_loc,             //it calls the constructor
                                std::string common_loc,           //so this is the sollution for now
                                std::vector<std::string> file_names,
                                std::vector<std::string> leaf_common_names,
                                TH1* nPileUp_wh,
                                Long64_t &numberOfEntries,
                                std::vector<int> &max_part);

std::vector<std::vector<std::string>> add_cut_names(std::vector<std::string> cut_sequence, std::string cut_name, std::string core_name, std::vector<std::vector<std::string>> ext, bool add_inf, bool skip_first);

void plots(std::vector<TH1*> h, TH1* hr, std::string name, std::string data_save, std::vector<std::string> legend_names, bool logy=false, bool logx=false, double min_val = 10);

void plots2d(std::vector<TH2*> h, TH2* hr, std::string name, std::string data_save, std::string param_name, 
             bool logz=false, bool logy=false, bool logx=false, int nDivY=510, int nDivX=510, double min_val = 10, 
             std::string stacked_type = "COLZ", std::string real_type = "COLZ", std::string ratio_type = "COLZ");

double InvM_calc2(std::vector<particles_dat>  particles, std::vector<std::vector<int>> limits = {{0, INT_MAX}, {0, INT_MAX}});

double InvM_calc2_w_dr_cut(std::vector<particles_dat>  particles, std::vector<double> dr_limits);

std::vector<std::vector<double>> dr_lep_phot(particles_dat lep, particles_dat phot);

template <typename T>
void print2d(std::vector<std::vector<T>> data);

template <typename T>
void print1d(std::vector<T> data);

template <typename T>
std::vector<size_t> sort_indexes(const std::vector<T> &v);

std::vector<std::vector<double>> reco_hist_create(std::vector<std::string> file_names, std::string graph_name, std::vector<double> Lumi);

TH2* ID_ISO_Trigger_scale_hist_create(std::vector<std::string> file_names, 
                                      std::vector<std::string> graph_names, 
                                      std::vector<double> Lumi);

template <typename T>
void csv_maker(std::vector<std::vector<T>> &data, std::vector<std::string> header, 
               std::string save_loc, Long64_t cut_size = 0);

int main(int argc, char* argv[]) //main
{
    bool electron;
    bool Corr_error = true;
    bool report = false;
    bool MC_save = false;
    if(strcmp(argv[1], "e") == 0)
        electron = true;
    else if(strcmp(argv[1], "m") == 0)
        electron = false;
    else
    {
        std::cout << "no known particle given (e/m)\n";
        return 0;
    }
    gErrorIgnoreLevel = kWarning;
    for(int i = 0; i < argc; i++)
        std::cout << i << ":" << argv[i] << " ";
    std::cout << "\n";
    std::string data_loc = "/home/dominykas/data/";
    std::vector<std::string> core_file_names;
    if(electron)
        core_file_names = {"SelectedEE_QCDEMEnriched", "SelectedEE_WJetsToLNu", "SelectedEE_WW", "SelectedEE_WZ", "SelectedEE_ZZ", "SelectedEE_tbarW", "SelectedEE_tW", "SelectedEE_ttbar", "SelectedEE_DYTauTau",  "SelectedEE_DYEE"};
    else
        core_file_names = {"SelectedMuMu_QCDMuEnriched", "SelectedMuMu_WJetsToLNu", "SelectedMuMu_WW", "SelectedMuMu_WZ", "SelectedMuMu_ZZ", "SelectedMuMu_tbarW", "SelectedMuMu_tW", "SelectedMuMu_ttbar", "SelectedMuMu_DYTauTau", "SelectedMuMu_DYMuMu"};
    
    std::vector<std::string> legend_names;
    
    if(electron)
    {
        legend_names = {"QCD", "W + Jets", "WW", "WZ", "ZZ", "#bar{t}W", "tW", "t#bar{t}", "Drell Yan #rightarrow #tau#tau", "Drell Yan #rightarrow ee"};
    }else
    {
        legend_names = {"QCD", "W + Jets", "WW", "WZ", "ZZ", "#bar{t}W", "tW", "t#bar{t}", "Drell Yan #rightarrow #tau#tau", "Drell Yan #rightarrow #mu#mu"};
    }
    
    
    std::vector<std::string> M_cut_sequence = {"10", "50", "100", "200", "400", "500", "700", "800", "1000", "1500", "2000", "3000"};
    std::vector<std::string> Pt_cut_sequence = {"15", "20", "30", "50", "80", "120", "170", "300", "470", "600", "800", "1000"};
    Long64_t numberOfEntries;
    std::vector<std::vector<std::vector<std::string>>> full_file_names(core_file_names.size());
    
    
    
    
    
    std::string EE_loc;
    if(electron)
    {
        EE_loc = "Dielectron_wPhotons/MC/";
    }else
    {
        EE_loc = "Dimuon_wPhotons/MC/";
    }
    std::vector<std::string> feature_list_vec_double = {"pT", "eta", "phi", "Energy"};
    std::vector<std::string> feature_list_double = {"InvM"};
    std::vector<std::string> feature_list_vec_int = {"charge"};
    std::vector<std::string> feature_list_int = {};
    
    std::vector<std::string> empty_ext = {""};
    
    std::vector<std::vector<std::string>> file_ext_temp;
    
    
    
    //print2d(full_file_names[0]);
    
    
    if(electron)
    {
        //std::cout << core_file_names[0] << "\n";
        file_ext_temp = {empty_ext, {"", "_ext1"}, {"", "_ext1"}, {"", "_ext1"}, {"", "_ext1"}, empty_ext, empty_ext};
        full_file_names[0] = add_cut_names({"20", "30", "50", "80", "120", "170", "300"}, "_Pt", core_file_names[0], file_ext_temp, true, false);
    }else
    {
        //std::cout << core_file_names[0] << "\n";
        file_ext_temp = {empty_ext, empty_ext, empty_ext, empty_ext, {"", "_ext1"}, {"", "_backup"}, {"", "_backup", "_ext1"}, {"", "_ext1", "_ext2"}, {"", "_ext1", "_ext2"}, {"", "_backup", "_ext1"}, {"", "_ext1", "_ext2"}, {"", "_ext1"}};
        full_file_names[0] = add_cut_names(Pt_cut_sequence, "_Pt", core_file_names[0], file_ext_temp, true, false);
    }
    //print2d(full_file_names[0]);
    
    //std::cout << core_file_names[1] << "\n";
    full_file_names[1] = {{core_file_names[1] + ".root", core_file_names[1] + "_ext.root", core_file_names[1] + "_ext2v5.root"}};
    
    //std::cout << core_file_names[1] << "\n"; 
    file_ext_temp = {{"", "Backup"}, empty_ext, empty_ext};
    full_file_names[7] = add_cut_names({"700", "1000"}, "_M", core_file_names[7], file_ext_temp, true, true);
    //print2d(full_file_names[7]);
    
    //std::cout << core_file_names[2] << "\n"; 
    full_file_names[6] = {{core_file_names[6] + ".root"}};
    
    //std::cout << core_file_names[3] << "\n";
    full_file_names[5] = {{core_file_names[5] + ".root"}};
    
    //std::cout << core_file_names[4] << "\n";
    full_file_names[4] = {{core_file_names[4] + ".root"}};
    
    //std::cout << core_file_names[5] << "\n";
    full_file_names[3] = {{core_file_names[3] + ".root"}};
    
    //std::cout << core_file_names[6] << "\n";
    full_file_names[2] = {{core_file_names[2] + ".root"}};
    
    //std::cout << core_file_names[8] << "\n";
    file_ext_temp = {{"_v1", "_v2", "_ext1v1"}, empty_ext};
    full_file_names[8] = add_cut_names({"10", "50"}, "_M", core_file_names[8], file_ext_temp, true, false);
    
    //std::cout << core_file_names[9] << "\n";
    file_ext_temp = {{"_v1", "_v2", "_ext1v1"}, empty_ext, empty_ext, empty_ext, empty_ext, empty_ext, empty_ext, empty_ext, empty_ext, empty_ext, empty_ext};
    full_file_names[9] = add_cut_names(M_cut_sequence, "_M", core_file_names[9], file_ext_temp, false, false);
    //print2d(full_file_names[9]);
    
    
    std::cout << "file_names done\n";    
        
    //return 0;    
    int bincount = 100;
    double L_int = 35867;
    std::vector<std::vector<double>> full_weights(core_file_names.size());
    
    std::vector<double> total_cross_DY_EE_MuMu = {6016.88, 1873.52, 76.2401, 2.67606, 0.139728, 0.0792496, 0.0123176, 0.01042, 0.00552772, 0.000741613, 0.000178737};
    if(electron)
    {
        std::vector<Long64_t> total_weights_DY_EE = {33340851, 26166194, 3179506, 560818, 50420, 48039, 46114, 44256, 39712, 37287, 34031};
        std::vector<double> full_weights_DY_EE_MuMu(total_cross_DY_EE_MuMu.size());
        for(int i = 0; i < total_cross_DY_EE_MuMu.size(); ++i)
        {
            full_weights_DY_EE_MuMu[i] = total_cross_DY_EE_MuMu[i]*L_int/total_weights_DY_EE[i];
        }
        full_weights[9] = full_weights_DY_EE_MuMu;
    }else
    {
        std::vector<Long64_t> total_weights_DY_MuMu = {33344557, 26175605, 3176762, 560322, 50136, 48188, 44984, 43496, 40110, 37176, 33360};
        std::vector<double> full_weights_DY_EE_MuMu(total_cross_DY_EE_MuMu.size());
        for(int i = 0; i < total_cross_DY_EE_MuMu.size(); ++i)
        {
            full_weights_DY_EE_MuMu[i] = total_cross_DY_EE_MuMu[i]*L_int/total_weights_DY_MuMu[i];
        }
        //full_weights_DY_EE_MuMu[0] = full_weights_DY_EE_MuMu[0]*66953/44626;
        full_weights[9] = full_weights_DY_EE_MuMu;
    }
    
    
    std::vector<Long64_t> total_weights_DYTauTau = {33145779, 27277866};
    std::vector<double> total_cross_DYTauTau = {6016.88, 1952.68432327};
    
    std::vector<double> full_weights_DYTauTau(total_cross_DYTauTau.size());
    for(int i = 0; i < total_cross_DYTauTau.size(); ++i)
        full_weights_DYTauTau[i] = total_cross_DYTauTau[i]*L_int/total_weights_DYTauTau[i];
    full_weights[8] = full_weights_DYTauTau;
    
    if(electron)
    {
        std::vector<Long64_t> total_weights_DYQCDEMEnriched = {9218952, 11498579, 45811236, 77695282, 77771309, 11540162, 7373633};
        std::vector<double> total_cross_DYQCDEMEnriched = {557600000.0 * 0.0096, 136000000.0 * 0.073, 19800000.0 * 0.146, 2800000.0 * 0.125, 477000.0 * 0.132, 114000.0 * 0.165, 9000.0 * 0.15};
        std::vector<double> full_weights_DYQCDEMEnriched(total_cross_DYQCDEMEnriched.size());
        for(int i = 0; i < total_cross_DYQCDEMEnriched.size(); ++i)
            full_weights_DYQCDEMEnriched[i] = total_cross_DYQCDEMEnriched[i]*L_int/total_weights_DYQCDEMEnriched[i];
        full_weights[0] = full_weights_DYQCDEMEnriched;
    }else
    {
        std::vector<Long64_t> total_weights_DYQCDMuEnriched = {4141251, 31475154, 37147024, 19806914, 23584212, 19980857, 36958003, 48995676, 19362940, 19738160, 19767432, 13599933};
        std::vector<double> total_cross_DYQCDMuEnriched = {1273190000.0 * 0.003, 558528000.0 * 0.0053, 139803000.0 * 0.01182, 19222500.0 * 0.02276, 2758420.0 * 0.03844, 469797.0 * 0.05362, 117989.0 * 0.07335, 7820.25 * 0.10196, 645.528 * 0.12242, 187.109 * 0.13412, 32.3486 * 0.14552, 10.4305 * 0.15544};
        std::vector<double> full_weights_DYQCDMuEnriched(total_cross_DYQCDMuEnriched.size());
        for(int i = 0; i < total_cross_DYQCDMuEnriched.size(); ++i)
            full_weights_DYQCDMuEnriched[i] = total_cross_DYQCDMuEnriched[i]*L_int/total_weights_DYQCDMuEnriched[i];
        full_weights[0] = full_weights_DYQCDMuEnriched;
    }
    
    
    std::vector<Long64_t> total_weights_ttbar = {135949780, 38578324, 24561630};
    std::vector<double> total_cross_ttbar = {734.577, 76.605, 20.578};
    std::vector<double> full_weights_ttbar(total_cross_ttbar.size());
    for(int i = 0; i < total_cross_ttbar.size(); ++i)
        full_weights_ttbar[i] = total_cross_ttbar[i]*L_int/total_weights_ttbar[i];
    full_weights[7] = full_weights_ttbar;
    
    full_weights[1] = {61526.7*L_int/431574324};
    
    std::vector<Long64_t> total_weights_plus = {6987123, 2995828, 998034, 6933093, 6952830};
    std::vector<double> total_cross_plus = {118.7, 47.13, 16.523, 35.85, 35.85};
    for(int i = 0; i < total_cross_plus.size(); ++i)
        full_weights[i + 2] = {total_cross_plus[i]*L_int/total_weights_plus[i]};
    
    
    
    std::vector<std::string> particles(2);
    if(electron)
        particles = {"Electron_", "Photon_"};
    else
        particles = {"Muon_", "Photon_"};
    
    std::vector<Long64_t> hist_res(bincount, 0);
    std::vector<double> hist_res_dub(bincount, 0);
    double low_hist_lim2 = 0, high_hist_lim2 = 200;
    double low_hist_lim3 = -3, high_hist_lim3 = 3;
    double low_hist_lim4 = -4, high_hist_lim4 = 4;
    std::vector<double> low_hist_lim1 = {0, 0};
    std::vector<double> high_hist_lim1 = {1000, 400};
    std::string param1 = "InvM";
    std::string param2 = "pT";
    std::string param3 = "eta";
    std::string param4 = "phi";
    std::vector<std::string> params_dub_vec = {"pT", "eta", "phi", "Energy"};
    const Double_t massbins[44] = {15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 64, 68, 72, 76, 81, 86, 91, 96, 101, 106, 110, 115, 120, 126, 133, 141, 150, 160, 171, 185, 200, 220, 243, 273, 320, 380, 440, 510, 600, 700, 830, 1000, 1500, 3000};
    
    std::vector<std::string> h1_subnames = {"_1000", "_400", "_custom_bins"};
    std::vector<std::string> h2_subnames = {"_l", "_t"};
    std::vector<std::string> h3_subnames = {"_l", "_t"};
    std::vector<std::string> h4_subnames = {"_l", "_t"};
    
    std::vector<double> h1_1_bins(1000-15);
    for(int i = 0; i < h1_1_bins.size(); ++i)
        h1_1_bins[i] = i+15;
    
    std::vector<double> h1_2_bins(400-15);
    for(int i = 0; i < h1_2_bins.size(); ++i)
        h1_2_bins[i] = i+15;
    
    std::vector<double> massbins_vec(44);
    for(int i = 0; i < massbins_vec.size(); ++i)
        massbins_vec[i] = massbins[i];
    
    std::string param_label_m;
    if(electron)
        param_label_m = "m_{ee} [GeV/c^{2}]";
    else
        param_label_m = "m_{#mu#mu} [GeV/c^{2}]";
    
    hists1d h1(legend_names, particles[0], h1_subnames, {h1_1_bins, h1_2_bins, massbins_vec}, param_label_m, param1);
    hists1d h1_just_phot(legend_names, particles[0], h1_subnames, {h1_1_bins, h1_2_bins, massbins_vec}, param_label_m, param1 + "_just_phot");
    std::string param_label_pt = "p_T [GeV/c]";
    hists1d h2(legend_names, particles[0], h2_subnames, bincount, low_hist_lim2, high_hist_lim2, param_label_pt, param2);
    std::string param_label_eta = "#eta";
    hists1d h3(legend_names, particles[0], h3_subnames, bincount, low_hist_lim3, high_hist_lim3, param_label_eta, param3);
    std::string param_label_phi = "#phi";
    hists1d h4(legend_names, particles[0], h4_subnames, bincount, low_hist_lim4, high_hist_lim4, param_label_phi, param4);
    
    double InvM_dr_limit = 0.3;
    
    hists1d hp1_full(legend_names, particles[1], h1_subnames, {h1_1_bins, h1_2_bins, massbins_vec}, param_label_m, param1 + "_plus_ph_full");
    hists1d hp1_full_low_dr(legend_names, particles[1], h1_subnames, {h1_1_bins, h1_2_bins, massbins_vec}, param_label_m, param1 + "_plus_ph_full_low_dr");
    
    std::vector<hists1d> hp_plus(3);
    hp_plus[0] = *(new hists1d(2, legend_names, particles[1],
                               (high_hist_lim1[0] - low_hist_lim1[0]), low_hist_lim1[0], high_hist_lim1[0], param_label_m, param1 + h1_subnames[0] + "_plus_ph"));
    hp_plus[1] = *(new hists1d(2, legend_names, particles[1],
                               (high_hist_lim1[1] - low_hist_lim1[1]), low_hist_lim1[1], high_hist_lim1[1], param_label_m, param1 + h1_subnames[1] + "_plus_ph"));
    hp_plus[2] = *(new hists1d(2, legend_names, particles[1],
                               massbins_vec, param_label_m, param1 + h1_subnames[2] + "_plus_ph"));
    std::vector<hists1d> hp_plus_low_dr(3);
    hp_plus_low_dr[0] = *(new hists1d(2, legend_names, particles[1],
                               (high_hist_lim1[0] - low_hist_lim1[0]), low_hist_lim1[0], high_hist_lim1[0], param_label_m, param1 + h1_subnames[0] + "_plus_ph_low_dr"));
    hp_plus_low_dr[1] = *(new hists1d(2, legend_names, particles[1],
                               (high_hist_lim1[1] - low_hist_lim1[1]), low_hist_lim1[1], high_hist_lim1[1], param_label_m, param1 + h1_subnames[1] + "_plus_ph_low_dr"));
    hp_plus_low_dr[2] = *(new hists1d(2, legend_names, particles[1],
                               massbins_vec, param_label_m, param1 + h1_subnames[2] + "_plus_ph_low_dr"));
    
    std::vector<double> exp_bins_hpdr(bincount);
    double exp_bins_hpdr_ll = -8;
    double exp_bins_hpdr_hl = 0;
    double exp_bins_hpdr_scale = 12;
    for(int i = 0; i < exp_bins_hpdr.size(); ++i)
    {
        exp_bins_hpdr[i] = exp_bins_hpdr_scale*std::exp(i/(double)exp_bins_hpdr.size()*(exp_bins_hpdr_hl - exp_bins_hpdr_ll) + exp_bins_hpdr_ll);
    }
    std::cout << "\n";
    
    std::string param_label_dr = "#Delta R";
    std::vector<hists1d> hpdr(2);
    hpdr[0] = *(new hists1d(2, legend_names, particles[1], exp_bins_hpdr, param_label_dr, "low_dr"));
    hpdr[1] = *(new hists1d(2, legend_names, particles[1], exp_bins_hpdr, param_label_dr, "high_dr"));
    
    hists1d hpdr_full(legend_names, particles[1], {"_low_dr_full", "_high_dr_full"}, exp_bins_hpdr, param_label_dr, "dr_full");
    
    double nVert_lim_low = 0, nVert_lim_high = 120;
    std::string param_label_nVert = "Number of Interactions";
    hists1d hp_nVert(2, legend_names, particles[1], (int)nVert_lim_high, nVert_lim_low, nVert_lim_high, param_label_nVert, "nVert");
    hists1d hp_nVert_full(legend_names, particles[1], {"_full"}, (int)nVert_lim_high, nVert_lim_low, nVert_lim_high, param_label_nVert, "nVert");
    
    hists1d hp2_full(legend_names, particles[1], {"_full"}, bincount, low_hist_lim2, high_hist_lim2, param_label_pt, param2);
    hists1d hp3_full(legend_names, particles[1], {"_full"}, bincount, low_hist_lim3, high_hist_lim3, param_label_eta, param3);
    hists1d hp4_full(legend_names, particles[1], {"_full"}, bincount, low_hist_lim4, high_hist_lim4, param_label_phi, param4);
    
    hists1d hp2_c(2, legend_names, particles[1], bincount, low_hist_lim2, high_hist_lim2, param_label_pt, param2 + "_count");
    hists1d hp3_c(2, legend_names, particles[1], bincount, low_hist_lim3, high_hist_lim3, param_label_eta, param3 + "_count");
    hists1d hp4_c(2, legend_names, particles[1], bincount, low_hist_lim4, high_hist_lim4, param_label_phi, param4 + "_count");
    hists1d hp2_n(2, legend_names, particles[1], bincount, low_hist_lim2, high_hist_lim2, param_label_pt, param2 + "_pT_order");
    hists1d hp3_n(2, legend_names, particles[1], bincount, low_hist_lim3, high_hist_lim3, param_label_eta, param3 + "_pT_order");
    hists1d hp4_n(2, legend_names, particles[1], bincount, low_hist_lim4, high_hist_lim4, param_label_phi, param4 + "_pT_order");
     
    hists1d hp2_full_dr_cut(legend_names, particles[1], {"_full_dr_cut_low", "_full_dr_cut_mid", "_full_dr_cut_high"}, bincount, low_hist_lim2, high_hist_lim2, param_label_pt, param2);
    hists1d hp3_full_dr_cut(legend_names, particles[1], {"_full_dr_cut_low", "_full_dr_cut_mid", "_full_dr_cut_high"}, bincount, low_hist_lim3, high_hist_lim3, param_label_eta, param3);
    hists1d hp4_full_dr_cut(legend_names, particles[1], {"_full_dr_cut_low", "_full_dr_cut_mid", "_full_dr_cut_high"}, bincount, low_hist_lim4, high_hist_lim4, param_label_phi, param4);
    
    double dr_cut_low_down = 0.005, dr_cut_low_up = 0.03;
    hists1d hp2_c_dr_cut_low(2, legend_names, particles[1], bincount, low_hist_lim2, high_hist_lim2, param_label_pt, param2 + "_dr_cut_low_count");
    hists1d hp3_c_dr_cut_low(2, legend_names, particles[1], bincount, low_hist_lim3, high_hist_lim3, param_label_eta, param3 + "_dr_cut_low_count");
    hists1d hp4_c_dr_cut_low(2, legend_names, particles[1], bincount, low_hist_lim4, high_hist_lim4, param_label_phi, param4 + "_dr_cut_low_count"); 
    hists1d hp2_n_dr_cut_low(2, legend_names, particles[1], bincount, low_hist_lim2, high_hist_lim2, param_label_pt, param2 + "_dr_cut_low_pT_order");
    hists1d hp3_n_dr_cut_low(2, legend_names, particles[1], bincount, low_hist_lim3, high_hist_lim3, param_label_eta, param3 + "_dr_cut_low_pT_order");
    hists1d hp4_n_dr_cut_low(2, legend_names, particles[1], bincount, low_hist_lim4, high_hist_lim4, param_label_phi, param4 + "_dr_cut_low_pT_order");
    
    double dr_cut_mid_down = 0.06, dr_cut_mid_up = 0.15;
    hists1d hp2_c_dr_cut_mid(2, legend_names, particles[1], bincount, low_hist_lim2, high_hist_lim2, param_label_pt, param2 + "_dr_cut_mid_count");
    hists1d hp3_c_dr_cut_mid(2, legend_names, particles[1], bincount, low_hist_lim3, high_hist_lim3, param_label_eta, param3 + "_dr_cut_mid_count");
    hists1d hp4_c_dr_cut_mid(2, legend_names, particles[1], bincount, low_hist_lim4, high_hist_lim4, param_label_phi, param4 + "_dr_cut_mid_count"); 
    hists1d hp2_n_dr_cut_mid(2, legend_names, particles[1], bincount, low_hist_lim2, high_hist_lim2, param_label_pt, param2 + "_dr_cut_mid_pT_order");
    hists1d hp3_n_dr_cut_mid(2, legend_names, particles[1], bincount, low_hist_lim3, high_hist_lim3, param_label_eta, param3 + "_dr_cut_mid_pT_order");
    hists1d hp4_n_dr_cut_mid(2, legend_names, particles[1], bincount, low_hist_lim4, high_hist_lim4, param_label_phi, param4 + "_dr_cut_mid_pT_order");
    
    double dr_cut_high_down = 0.4, dr_cut_high_up = 1;
    hists1d hp2_c_dr_cut_high(2, legend_names, particles[1], bincount, low_hist_lim2, high_hist_lim2, param_label_pt, param2 + "_dr_cut_high_count");
    hists1d hp3_c_dr_cut_high(2, legend_names, particles[1], bincount, low_hist_lim3, high_hist_lim3, param_label_eta, param3 + "_dr_cut_high_count");
    hists1d hp4_c_dr_cut_high(2, legend_names, particles[1], bincount, low_hist_lim4, high_hist_lim4, param_label_phi, param4 + "_dr_cut_high_count"); 
    hists1d hp2_n_dr_cut_high(2, legend_names, particles[1], bincount, low_hist_lim2, high_hist_lim2, param_label_pt, param2 + "_dr_cut_high_pT_order");
    hists1d hp3_n_dr_cut_high(2, legend_names, particles[1], bincount, low_hist_lim3, high_hist_lim3, param_label_eta, param3 + "_dr_cut_high_pT_order");
    hists1d hp4_n_dr_cut_high(2, legend_names, particles[1], bincount, low_hist_lim4, high_hist_lim4, param_label_phi, param4 + "_dr_cut_high_pT_order");
    
    std::vector<std::vector<TH2*>> hp2d(3, std::vector<TH2*>(full_file_names.size()));
    std::vector<std::vector<TH2*>> hp2d_low_dr(3, std::vector<TH2*>(full_file_names.size()));
    std::vector<std::vector<TH2*>> hpdr2d(2, std::vector<TH2*>(full_file_names.size()));
    std::vector<TH2*> hp_nVert_2d(full_file_names.size());
    for(int i = 0; i < full_file_names.size(); ++i)
    {
        for(int j = 0; j < hp2d.size() - 1; ++j)
        {
            hp2d[j][i] = new TH2D(
                (legend_names[i] + particles[1] + param1 + "_plus_ph_2d_" + h1_subnames[j]).c_str(), 
                (legend_names[i] + particles[1] + param1 + "_plus_ph_2d_" + h1_subnames[j]).c_str(), 
                bincount, low_hist_lim1[j], high_hist_lim1[j], 
                12, -0.5, 11.5);
        }
        hp2d[hp2d.size() - 1][i] = new TH2D(
            (legend_names[i] + particles[1] + param1 + "_plus_ph_2d_" + h1_subnames[hp2d.size() - 1]).c_str(),
            (legend_names[i] + particles[1] + param1 + "_plus_ph_2d_" + h1_subnames[hp2d.size() - 1]).c_str(), 43, massbins, 
            12, -0.5, 11.5);
        for(int j = 0; j < hp2d_low_dr.size() - 1; ++j)
        {
            hp2d_low_dr[j][i] = new TH2D(
                (legend_names[i] + particles[1] + param1 + "_plus_ph_2d_low_dr_" + h1_subnames[j]).c_str(), 
                (legend_names[i] + particles[1] + param1 + "_plus_ph_2d_low_dr_" + h1_subnames[j]).c_str(), 
                bincount, low_hist_lim1[j], high_hist_lim1[j], 
                12, -0.5, 11.5);
        }
        hp2d_low_dr[hp2d_low_dr.size() - 1][i] = new TH2D(
            (legend_names[i] + particles[1] + param1 + "_plus_ph_2d_low_dr_" + h1_subnames[hp2d_low_dr.size() - 1]).c_str(),
            (legend_names[i] + particles[1] + param1 + "_plus_ph_2d_low_dr_" + h1_subnames[hp2d_low_dr.size() - 1]).c_str(), 43, massbins, 
            12, -0.5, 11.5);
        
        
        hpdr2d[0][i] = new TH2D((legend_names[i] + particles[1] + "_low_dr_2d").c_str(),
                                (legend_names[i] + particles[1] + "_low_dr_2d").c_str(),
                                exp_bins_hpdr.size()-1, &exp_bins_hpdr[0], 12, -0.5, 11.5);
        
        hpdr2d[1][i] = new TH2D((legend_names[i] + particles[1] + "_high_dr_2d").c_str(),
                                (legend_names[i] + particles[1] + "_high_dr_2d").c_str(),
                                exp_bins_hpdr.size()-1, &exp_bins_hpdr[0], 12, -0.5, 11.5);
        
        hp_nVert_2d[i] = new TH2D((legend_names[i] + particles[1] + "_nVert_2d").c_str(),
                                (legend_names[i] + particles[1] + "_nVert_2d").c_str(),
                                (int)nVert_lim_high, nVert_lim_low, nVert_lim_high, 12, -0.5, 11.5);
    }
    
    
    double full_event_count = 0;
    int max_phot_MC = 0;
    TH1* nPileUp_whist;
    if(!electron & Corr_error)
    {
        TFile nPileUp_file((data_loc + "ROOTFile_PUReWeight_80X_v20170817_64mb.root").c_str(), "READ");
        nPileUp_whist = (TH1D*)nPileUp_file.Get("h_PUReWeights");
        nPileUp_whist->SetDirectory(0);
        nPileUp_file.Close();
    }
    
    std::vector<std::vector<double>> reco_hist;
    TH2* ID_scale_hist;
    TH2* ISO_scale_hist;
    TH2* Trigger_scale_hist;
    if(!electron)
    {
        reco_hist = reco_hist_create({data_loc + "corrections/effSF_muon/" + "Tracking_SF_RunBtoF.root",
                                      data_loc + "corrections/effSF_muon/" + "Tracking_SF_RunGtoH.root"},
                                     "ratio_eff_eta3_dr030e030_corr", {19721, 16146});
        ID_scale_hist = ID_ISO_Trigger_scale_hist_create({data_loc + "corrections/effSF_muon/" + "ID_SF_RunBtoF.root",
                                                          data_loc + "corrections/effSF_muon/" + "ID_SF_RunGtoH.root"},
                                                         {"MC_NUM_TightID_DEN_genTracks_PAR_pt_eta/efficienciesDATA/abseta_pt_DATA",
                                                          "MC_NUM_TightID_DEN_genTracks_PAR_pt_eta/efficienciesMC/abseta_pt_MC"},
                                                         {19721, 16146});
        ISO_scale_hist = ID_ISO_Trigger_scale_hist_create({data_loc + "corrections/effSF_muon/" + "ISO_SF_RunBtoF.root",
                                                           data_loc + "corrections/effSF_muon/" + "ISO_SF_RunGtoH.root"},
                                                          {"TightISO_TightID_pt_eta/efficienciesDATA/abseta_pt_DATA",
                                                           "TightISO_TightID_pt_eta/efficienciesMC/abseta_pt_MC"},
                                                          {19721, 16146});
        Trigger_scale_hist = ID_ISO_Trigger_scale_hist_create({data_loc + "corrections/effSF_muon/" + "Trigger_SF_RunBtoF.root",
                                                               data_loc + "corrections/effSF_muon/" + "Trigger_SF_RunGtoH.root"},
                                                              {"IsoMu24_OR_IsoTkMu24_PtEtaBins/efficienciesDATA/abseta_pt_DATA",
                                                               "IsoMu24_OR_IsoTkMu24_PtEtaBins/efficienciesMC/abseta_pt_MC"},
                                                              {19721, 16146});
    }
    
    //return 0;
    std::vector<std::vector<double>> full_MC_data;
    /*
    std::string MC_data_save;
    if(electron)
        {
            MC_data_save = "/home/dominykas/dy_mas_do/EE/";
        }
        else
        {
            MC_data_save = "/home/dominykas/dy_mas_do/MuMu/";
        }
    TFile full_MC_file((MC_data_save + "MC_data_vec.root").c_str(), "RECREATE");
    */
    for(int fn = 0; fn < full_file_names.size(); ++fn)
    {
        std::cout << core_file_names[fn] << "\n";
        for(int i = 0; i < full_file_names[fn].size(); ++i)
        {
            std::cout << fn << " " << i << "\n";
            std::vector<int> temp_max_part;
            print1d(full_file_names[fn][i]);
            std::cout << full_weights[fn][i] << "\n";
            full_particles_dat data = data_extract("DYTree",
                                                   data_loc,
                                                   EE_loc,
                                                   full_file_names[fn][i],
                                                   particles,
                                                   nPileUp_whist,
                                                   numberOfEntries,
                                                   temp_max_part);
            max_phot_MC = std::max(temp_max_part[1], max_phot_MC);
            std::ofstream report_file;
            if(report)
            {
                report_file.open("/home/dominykas/dy_mas_do/report/" + full_file_names[fn][i][0] + ".txt");
                if(!(report_file.is_open()))
                {
                    std::cout << "report file not open\n";
                }
                report_file << "File group: ";
                for(int files = 0; files < full_file_names[fn][i].size(); ++files)
                    report_file << full_file_names[fn][i][files] << "\t";
                report_file << "\n";
                report_file << data.corr_name + "\t" + data.weight_name + " * nPileUp fix" + "\tcrossection weight\t" + "Full event weight\t" + data.nVert_name + "\t" + data.InvM_name + "\n";
                report_file << "Lepton 1:\t" + data.pT_name + "\t" + data.phi_name + "\t" + data.eta_name + "\t" + data.E_name + "\treco_scale\t" + "ID_scale\t" + "ISO_scale\t" + "Trigger_scale\n";
                report_file << "Lepton 2:\t" + data.pT_name + "\t" + data.phi_name + "\t" + data.eta_name + "\t" + data.E_name + "\treco_scale\t" + "ID_scale\t" + "ISO_scale\t" + "Trigger_scale\n\n";
            }
            
            std::vector<double> lep_weight(data.event_data.size(), 0);
            int bad_idx_count = 0;
            for(int j = 0; j < data.event_data.size(); ++j)
            {
                if(report)
                {
                    if(data.event_data[j].correct_dat)
                        report_file << "true";
                    else
                        report_file << "false";
                }
                if(!(data.event_data[j].correct_dat))
                {
                    bad_idx_count++;
                    continue;
                }
                lep_weight[j] = data.event_data[j].weight*full_weights[fn][i];
                if((!electron) & (Corr_error))
                {
                    auto reco_l1 = std::lower_bound(reco_hist[1].begin(), 
                                                    reco_hist[1].end(), 
                                                    data.part_data[j][0].eta[0]) - reco_hist[1].begin() - 1;
                    auto reco_l2 = std::lower_bound(reco_hist[1].begin(), 
                                                    reco_hist[1].end(), 
                                                    data.part_data[j][0].eta[1]) - reco_hist[1].begin() - 1;
                          
                    double ID_scale_l1 = ID_scale_hist->GetBinContent(ID_scale_hist->FindBin(std::abs(data.part_data[j][0].eta[0]),
                                                                                             data.part_data[j][0].pT[0]));
                    
                    double ID_scale_l2 = ID_scale_hist->GetBinContent(ID_scale_hist->FindBin(std::abs(data.part_data[j][0].eta[1]),
                                                                                             data.part_data[j][0].pT[1]));
                    
                    double ISO_scale_l1 = ISO_scale_hist->GetBinContent(ISO_scale_hist->FindBin(std::abs(data.part_data[j][0].eta[0]),
                                                                                                data.part_data[j][0].pT[0]));
                    
                    double ISO_scale_l2 = ISO_scale_hist->GetBinContent(ISO_scale_hist->FindBin(std::abs(data.part_data[j][0].eta[1]),
                                                                                                data.part_data[j][0].pT[1]));
                    
                    double Trigger_scale_l1 = Trigger_scale_hist->GetBinContent(Trigger_scale_hist->FindBin(std::abs(data.part_data[j][0].eta[0]),
                                                                                                            data.part_data[j][0].pT[0]));
                    
                    double Trigger_scale_l2 = Trigger_scale_hist->GetBinContent(Trigger_scale_hist->FindBin(std::abs(data.part_data[j][0].eta[1]),
                                                                                                            data.part_data[j][0].pT[1]));
                    
                    lep_weight[j] *= reco_hist[0][reco_l1] * reco_hist[0][reco_l2] * ID_scale_l1 * ID_scale_l2 * ISO_scale_l1 * ISO_scale_l2 * (Trigger_scale_l1 + Trigger_scale_l2 - Trigger_scale_l1 * Trigger_scale_l2);
                    if(report)
                    {
                        report_file << "\t" << data.event_data[j].weight << "\t" << lep_weight[j] << "\t" << data.event_data[j].nVert << "\t" << data.part_data[j][0].InvM << "\n";
                        report_file << "Lepton 1:\t" << data.part_data[j][0].pT[0] << "\t" << data.part_data[j][0].phi[0] << "\t" << data.part_data[j][0].eta[0] << "\t" << data.part_data[j][0].E[0] << "\t" << reco_hist[0][reco_l1] << "\t" << ID_scale_l1 << "\t" << ISO_scale_l1 << "\t" << Trigger_scale_l1 << "\n";
                        report_file << "Lepton 2:\t" << data.part_data[j][0].pT[1] << "\t" << data.part_data[j][0].phi[1] << "\t" << data.part_data[j][0].eta[1] << "\t" << data.part_data[j][0].E[1] << "\t" << reco_hist[0][reco_l2] << "\t" << ID_scale_l2 << "\t" << ISO_scale_l2 << "\t" << Trigger_scale_l2 << "\n\n";
                    }
                    
                    
                    if(lep_weight[j] == 0)
                    {
                        std::cout << "Unexpected lep_weight = 0\n";
                        if(reco_hist[0][reco_l1] == 0)
                            std::cout << "reco_l1: " << reco_hist[0][reco_l1] << " " << reco_l1 << " ";
                        if(reco_hist[0][reco_l2] == 0)
                            std::cout << "reco_l2: " << reco_hist[0][reco_l2] << " " << reco_l2 << " ";
                        if(ID_scale_l1 == 0)
                        {
                            int temp = ID_scale_hist->FindBin(std::abs(data.part_data[j][0].eta[0]), data.part_data[j][0].pT[0]);
                            int t_x, t_y, t_z;
                            ID_scale_hist->GetBinXYZ(temp, t_x, t_y, t_z);
                            std::cout << "ID_scale_l1: " << ID_scale_l1 << " " << t_x << " " << t_y << " ";
                        }
                        if(ID_scale_l2 == 0)
                        {
                            int temp = ID_scale_hist->FindBin(std::abs(data.part_data[j][0].eta[1]), data.part_data[j][0].pT[1]);
                            int t_x, t_y, t_z;
                            ID_scale_hist->GetBinXYZ(temp, t_x, t_y, t_z);
                            std::cout << "ID_scale_l1: " << ID_scale_l2 << " " << t_x << " " << t_y << " ";
                        }
                        if(ISO_scale_l1 == 0)
                        {
                            int temp = ISO_scale_hist->FindBin(std::abs(data.part_data[j][0].eta[0]), data.part_data[j][0].pT[0]);
                            int t_x, t_y, t_z;
                            ISO_scale_hist->GetBinXYZ(temp, t_x, t_y, t_z);
                            std::cout << "ID_scale_l1: " << ISO_scale_l1 << " " << t_x << " " << t_y << " ";
                        }
                        if(ISO_scale_l2 == 0)
                        {
                            int temp = ISO_scale_hist->FindBin(std::abs(data.part_data[j][0].eta[1]), data.part_data[j][0].pT[1]);
                            int t_x, t_y, t_z;
                            ISO_scale_hist->GetBinXYZ(temp, t_x, t_y, t_z);
                            std::cout << "ID_scale_l1: " << ISO_scale_l2 << " " << t_x << " " << t_y << " ";
                        }
                        if(Trigger_scale_l1 == 0)
                        {
                            int temp = Trigger_scale_hist->FindBin(std::abs(data.part_data[j][0].eta[0]), data.part_data[j][0].pT[0]);
                            int t_x, t_y, t_z;
                            Trigger_scale_hist->GetBinXYZ(temp, t_x, t_y, t_z);
                            std::cout << "ID_scale_l1: " << Trigger_scale_l1 << " " << t_x << " " << t_y << " ";
                        }
                        if(Trigger_scale_l2 == 0)
                        {
                            int temp = Trigger_scale_hist->FindBin(std::abs(data.part_data[j][0].eta[1]), data.part_data[j][0].pT[1]);
                            int t_x, t_y, t_z;
                            Trigger_scale_hist->GetBinXYZ(temp, t_x, t_y, t_z);
                            std::cout << "ID_scale_l1: " << Trigger_scale_l2 << " " << t_x << " " << t_y << " ";
                        }
                        std::cout << "\n";
                    }
                } else if(report)
                {
                    report_file << "\t" << data.event_data[j].weight << "\t" << lep_weight[j] << "\t" << data.event_data[j].nVert << "\t" << data.part_data[j][0].InvM << "\n";
                    report_file << "Lepton 1:\t" << data.part_data[j][0].pT[0] << "\t" << data.part_data[j][0].phi[0] << "\t" << data.part_data[j][0].eta[0] << "\t" << data.part_data[j][0].E[0] << "\n";
                    report_file << "Lepton 2:\t" << data.part_data[j][0].pT[1] << "\t" << data.part_data[j][0].phi[1] << "\t" << data.part_data[j][0].eta[1] << "\t" << data.part_data[j][0].E[1] << "\n\n";
                }
                
            }
            report_file.close();
            
            double wsum = 0;
            for(int j = 0; j < data.event_data.size(); ++j)
                wsum += lep_weight[j];
            std::cout << wsum  <<"\n\n";
            full_event_count += wsum;
            
            for(int j = 0; j < data.event_data.size(); ++j)
            {
                if(!(data.event_data[j].correct_dat))
                    continue;
                std::vector<size_t> sorted_idx = sort_indexes(data.part_data[j][0].pT);
                for(int idx = 0; idx < sorted_idx.size(); ++idx)
                {
                    h2.MC.at(idx)[fn]->Fill(data.part_data[j][0].pT[sorted_idx[idx]], lep_weight[j]);
                    h3.MC[idx][fn]->Fill(data.part_data[j][0].eta[sorted_idx[idx]], lep_weight[j]);
                    h4.MC[idx][fn]->Fill(data.part_data[j][0].phi[sorted_idx[idx]], lep_weight[j]);
                }
                for(int hist_num = 0; hist_num < h1.MC.size(); ++hist_num)
                {
                    h1.MC[hist_num][fn]->Fill(data.part_data[j][0].InvM, lep_weight[j]);
                    hp2d[hist_num][fn]->Fill(data.part_data[j][0].InvM, -0.5, lep_weight[j]);
                    hp2d_low_dr[hist_num][fn]->Fill(data.part_data[j][0].InvM, -0.5, lep_weight[j]);
                }
                
            }
            std::cout << particles[0] + " fill complete\n\n";
            for(int j = 0; j < data.event_data.size(); ++j)
            {
                if(!(data.event_data[j].correct_dat))
                    continue;
                int phot_count = data.part_data[j][1].pT.size();
                if(phot_count < 1)
                    continue;
                std::vector<size_t> sorted_idx = sort_indexes(data.part_data[j][1].pT);
                
                int temp = hp2_c.MC.size();
                if(temp < phot_count)
                {
                    
                    hp2_c.expand(phot_count - temp);
                    hp3_c.expand(phot_count - temp);
                    hp4_c.expand(phot_count - temp);
                    
                    hp2_n.expand(phot_count - temp);
                    hp3_n.expand(phot_count - temp);
                    hp4_n.expand(phot_count - temp);
                    
                    hp2_c_dr_cut_low.expand(phot_count - temp);
                    hp3_c_dr_cut_low.expand(phot_count - temp);
                    hp4_c_dr_cut_low.expand(phot_count - temp);
                    
                    hp2_n_dr_cut_low.expand(phot_count - temp);
                    hp3_n_dr_cut_low.expand(phot_count - temp);
                    hp4_n_dr_cut_low.expand(phot_count - temp);
                    
                    hp2_c_dr_cut_mid.expand(phot_count - temp);
                    hp3_c_dr_cut_mid.expand(phot_count - temp);
                    hp4_c_dr_cut_mid.expand(phot_count - temp);
                    
                    hp2_n_dr_cut_mid.expand(phot_count - temp);
                    hp3_n_dr_cut_mid.expand(phot_count - temp);
                    hp4_n_dr_cut_mid.expand(phot_count - temp);
                    
                    hp2_c_dr_cut_high.expand(phot_count - temp);
                    hp3_c_dr_cut_high.expand(phot_count - temp);
                    hp4_c_dr_cut_high.expand(phot_count - temp);
                    
                    hp2_n_dr_cut_high.expand(phot_count - temp);
                    hp3_n_dr_cut_high.expand(phot_count - temp);
                    hp4_n_dr_cut_high.expand(phot_count - temp);
                    
                    hp_plus[0].expand(phot_count - temp);
                    hp_plus[1].expand(phot_count - temp);
                    hp_plus[2].expand(phot_count - temp);
                    hp_plus_low_dr[0].expand(phot_count - temp);
                    hp_plus_low_dr[1].expand(phot_count - temp);
                    hp_plus_low_dr[2].expand(phot_count - temp);
                    hpdr[0].expand(phot_count - temp);
                    hpdr[1].expand(phot_count - temp);
                    
                    hp_nVert.expand(phot_count - temp);
                    
                    std::cout << "expand_end\n";
                }
                std::vector<std::vector<double>> dr = dr_lep_phot(data.part_data[j][0], data.part_data[j][1]);
                double phot_weight = lep_weight[j];
                hp_nVert.MC.at(phot_count - 1).at(fn)->Fill((double)data.event_data[j].nVert, data.event_data[j].weight * full_weights[fn][i]);
                    
                hp_nVert_full.MC[0][fn]->Fill((double)data.event_data[j].nVert, data.event_data[j].weight * full_weights[fn][i]);
                    
                hp_nVert_2d[fn]->Fill((double)data.event_data[j].nVert, phot_count - 0.5, data.event_data[j].weight * full_weights[fn][i]);
                
                for(int idx = 0; idx < phot_count; ++idx)
                {
                    
                    hp2_c.MC[phot_count - 1][fn]->Fill(data.part_data[j][1].pT[sorted_idx[idx]], phot_weight);
                    hp3_c.MC[phot_count - 1][fn]->Fill(data.part_data[j][1].eta[sorted_idx[idx]], phot_weight);
                    hp4_c.MC[phot_count - 1][fn]->Fill(data.part_data[j][1].phi[sorted_idx[idx]], phot_weight);
                    
                    hp2_n.MC[idx][fn]->Fill(data.part_data[j][1].pT[sorted_idx[idx]], phot_weight);
                    hp3_n.MC[idx][fn]->Fill(data.part_data[j][1].eta[sorted_idx[idx]], phot_weight);
                    hp4_n.MC[idx][fn]->Fill(data.part_data[j][1].phi[sorted_idx[idx]], phot_weight);
                    if((dr[0][idx] > dr_cut_low_down) && (dr[0][idx] < dr_cut_low_up))
                    {
                        hp2_c_dr_cut_low.MC[phot_count - 1][fn]->Fill(data.part_data[j][1].pT[sorted_idx[idx]], phot_weight);
                        hp3_c_dr_cut_low.MC[phot_count - 1][fn]->Fill(data.part_data[j][1].eta[sorted_idx[idx]], phot_weight);
                        hp4_c_dr_cut_low.MC[phot_count - 1][fn]->Fill(data.part_data[j][1].phi[sorted_idx[idx]], phot_weight);
                        
                        hp2_n_dr_cut_low.MC[idx][fn]->Fill(data.part_data[j][1].pT[sorted_idx[idx]], phot_weight);
                        hp3_n_dr_cut_low.MC[idx][fn]->Fill(data.part_data[j][1].eta[sorted_idx[idx]], phot_weight);
                        hp4_n_dr_cut_low.MC[idx][fn]->Fill(data.part_data[j][1].phi[sorted_idx[idx]], phot_weight);
                        
                        hp2_full_dr_cut.MC[0][fn]->Fill(data.part_data[j][1].pT[sorted_idx[idx]], phot_weight);
                        hp3_full_dr_cut.MC[0][fn]->Fill(data.part_data[j][1].eta[sorted_idx[idx]], phot_weight);
                        hp4_full_dr_cut.MC[0][fn]->Fill(data.part_data[j][1].phi[sorted_idx[idx]], phot_weight);
                    }
                    
                    if((dr[0][idx] > dr_cut_mid_down) && (dr[0][idx] < dr_cut_mid_up))
                    {
                        hp2_c_dr_cut_mid.MC[phot_count - 1][fn]->Fill(data.part_data[j][1].pT[sorted_idx[idx]], phot_weight);
                        hp3_c_dr_cut_mid.MC[phot_count - 1][fn]->Fill(data.part_data[j][1].eta[sorted_idx[idx]], phot_weight);
                        hp4_c_dr_cut_mid.MC[phot_count - 1][fn]->Fill(data.part_data[j][1].phi[sorted_idx[idx]], phot_weight);
                        
                        hp2_n_dr_cut_mid.MC[idx][fn]->Fill(data.part_data[j][1].pT[sorted_idx[idx]], phot_weight);
                        hp3_n_dr_cut_mid.MC[idx][fn]->Fill(data.part_data[j][1].eta[sorted_idx[idx]], phot_weight);
                        hp4_n_dr_cut_mid.MC[idx][fn]->Fill(data.part_data[j][1].phi[sorted_idx[idx]], phot_weight);
                        
                        hp2_full_dr_cut.MC[1][fn]->Fill(data.part_data[j][1].pT[sorted_idx[idx]], phot_weight);
                        hp3_full_dr_cut.MC[1][fn]->Fill(data.part_data[j][1].eta[sorted_idx[idx]], phot_weight);
                        hp4_full_dr_cut.MC[1][fn]->Fill(data.part_data[j][1].phi[sorted_idx[idx]], phot_weight);
                    }
                    
                    if((dr[0][idx] > dr_cut_high_down) && (dr[0][idx] < dr_cut_high_up))
                    {
                        hp2_c_dr_cut_high.MC[phot_count - 1][fn]->Fill(data.part_data[j][1].pT[sorted_idx[idx]], phot_weight);
                        hp3_c_dr_cut_high.MC[phot_count - 1][fn]->Fill(data.part_data[j][1].eta[sorted_idx[idx]], phot_weight);
                        hp4_c_dr_cut_high.MC[phot_count - 1][fn]->Fill(data.part_data[j][1].phi[sorted_idx[idx]], phot_weight);
                        
                        hp2_n_dr_cut_high.MC[idx][fn]->Fill(data.part_data[j][1].pT[sorted_idx[idx]], phot_weight);
                        hp3_n_dr_cut_high.MC[idx][fn]->Fill(data.part_data[j][1].eta[sorted_idx[idx]], phot_weight);
                        hp4_n_dr_cut_high.MC[idx][fn]->Fill(data.part_data[j][1].phi[sorted_idx[idx]], phot_weight);
                        
                        hp2_full_dr_cut.MC[2][fn]->Fill(data.part_data[j][1].pT[sorted_idx[idx]], phot_weight);
                        hp3_full_dr_cut.MC[2][fn]->Fill(data.part_data[j][1].eta[sorted_idx[idx]], phot_weight);
                        hp4_full_dr_cut.MC[2][fn]->Fill(data.part_data[j][1].phi[sorted_idx[idx]], phot_weight);
                    }
                    
                    hpdr[0].MC[phot_count - 1][fn]->Fill(dr[0][idx], phot_weight);
                    hpdr[1].MC[phot_count - 1][fn]->Fill(dr[1][idx], phot_weight);
                    hpdr_full.MC[0][fn]->Fill(dr[0][idx], phot_weight);
                    hpdr_full.MC[1][fn]->Fill(dr[1][idx], phot_weight);
                    hpdr2d[0][fn]->Fill(dr[0][idx], phot_count - 0.5, phot_weight);
                    hpdr2d[1][fn]->Fill(dr[1][idx], phot_count - 0.5, phot_weight);
                    hp2_full.MC[0][fn]->Fill(data.part_data[j][1].pT[sorted_idx[idx]], phot_weight);
                    hp3_full.MC[0][fn]->Fill(data.part_data[j][1].eta[sorted_idx[idx]], phot_weight);
                    hp4_full.MC[0][fn]->Fill(data.part_data[j][1].phi[sorted_idx[idx]], phot_weight);
                }
                
            }
            std::cout << particles[1] + " fill complete\n\n";
            for(int j = 0; j < data.event_data.size(); ++j)
            {
                if(!(data.event_data[j].correct_dat))
                    continue;
                int phot_count = data.part_data[j][1].pT.size();
                if(phot_count < 1)
                    continue;
                std::vector<double> temp_E(data.part_data[j][1].pT.size());
                for(int i = 0; i < data.part_data[j][1].pT.size(); ++i)
                {
                    temp_E[i] = data.part_data[j][1].pT[i] * cosh(data.part_data[j][1].eta[i]);
                }
                data.part_data[j][1].E = temp_E;
                
                double temp_InvM = InvM_calc2(data.part_data[j], {{0, 2}, {0, phot_count}});
                double temp_InvM_w_dr_cut = InvM_calc2_w_dr_cut(data.part_data[j], {0, InvM_dr_limit});
                double phot_weight = lep_weight[j];
                for(int f = 0; f < hp_plus.size(); ++f)
                {
                    h1_just_phot.MC[f][fn]->Fill(data.part_data[j][0].InvM, phot_weight);
                    hp_plus[f].MC.at(phot_count - 1)[fn]->Fill(temp_InvM, phot_weight);
                    hp2d[f][fn]->Fill(temp_InvM, phot_count - 0.5, phot_weight);
                    hp1_full.MC[f][fn]->Fill(temp_InvM, phot_weight);
                    hp_plus_low_dr[f].MC[phot_count - 1][fn]->Fill(temp_InvM_w_dr_cut, phot_weight);
                    hp2d_low_dr[f][fn]->Fill(temp_InvM_w_dr_cut, phot_count - 0.5, phot_weight);
                    hp1_full_low_dr.MC[f][fn]->Fill(temp_InvM_w_dr_cut, phot_weight);
                }
            }
            std::cout << "InvM fill complete\n\n";
            std::vector<std::vector<double>> temp_MC_data(6 + (temp_max_part[0] + max_phot_MC)*4, 
                                                          std::vector<double>(data.event_data.size() - bad_idx_count, 0));
            int true_idx = 0;
            for(int j = 0; j < data.event_data.size(); ++j)
            {
                if(!(data.event_data[j].correct_dat))
                        continue;
                //data.event_data[j].weight = lep_weight[j];
                temp_MC_data[0][true_idx] = (double)fn;
                temp_MC_data[1][true_idx] = lep_weight[j];
                temp_MC_data[2][true_idx] = (double)data.part_data[j][1].pT.size();
                temp_MC_data[3][true_idx] = data.part_data[j][0].InvM;
                temp_MC_data[4][true_idx] = data.event_data[j].MET_pT;
                temp_MC_data[5][true_idx] = data.event_data[j].MET_phi;
                int part_num_l;
                for(part_num_l = 0; part_num_l < data.part_data[j][0].pT.size(); ++part_num_l)
                {
                    temp_MC_data[6 + part_num_l*4][true_idx] = data.part_data[j][0].pT[part_num_l];
                    temp_MC_data[7 + part_num_l*4][true_idx] = data.part_data[j][0].eta[part_num_l];
                    temp_MC_data[8 + part_num_l*4][true_idx] = data.part_data[j][0].phi[part_num_l];
                    temp_MC_data[9 + part_num_l*4][true_idx] = data.part_data[j][0].E[part_num_l];
                }
                for(int part_num = 0; part_num < data.part_data[j][1].pT.size(); ++part_num)
                {
                    temp_MC_data[6 + (part_num + part_num_l)*4][true_idx] = data.part_data[j][1].pT[part_num];
                    temp_MC_data[7 + (part_num + part_num_l)*4][true_idx] = data.part_data[j][1].eta[part_num];
                    temp_MC_data[8 + (part_num + part_num_l)*4][true_idx] = data.part_data[j][1].phi[part_num];
                    temp_MC_data[9 + (part_num + part_num_l)*4][true_idx] = data.part_data[j][1].E[part_num];
                }
                ++true_idx;
            }
            //full_MC_file.WriteObject(&temp_MC_data, legend_names[fn].c_str());
            if(full_MC_data.size() > 0)
            {
                std::vector<double> zero_vec(full_MC_data[0].size(), 0.);
                if(temp_MC_data.size() > full_MC_data.size())
                    full_MC_data.insert(full_MC_data.end(), temp_MC_data.size() - full_MC_data.size(), zero_vec);
                for(int para = 0; para < temp_MC_data.size(); ++para)
                {
                    full_MC_data.at(para).insert(full_MC_data[para].end(), temp_MC_data[para].begin(), temp_MC_data[para].end());
                }
            } else
                full_MC_data = temp_MC_data;
            
            
            std::cout << "----------------------------\n";
        }
    }
    //print2d(full_MC_data);
    //std::cout << "size of MC data: " << sizeof(full_MC_data) << "\n"; //doesn't work with pointers, just gives size of pointers
    if(MC_save)
    {
        std::string MC_data_save;
        if(electron)
            {
                MC_data_save = "/home/dominykas/dy_mas_do/EE/";
            }
            else
            {
                MC_data_save = "/home/dominykas/dy_mas_do/MuMu/";
            }
        
        //std::cout << "pre_write\n";
        
        
        
        TFile param_MC_file((MC_data_save + "MC_data_vec.root").c_str(), "RECREATE");
        param_MC_file.WriteObject(&legend_names, "legend_names");
        std::vector<std::string> MC_collumn_names(6 + (2 + max_phot_MC)*4);
        MC_collumn_names[0] = "Process";
        MC_collumn_names[1] = "Weight";
        MC_collumn_names[2] = "Phot_count";
        MC_collumn_names[3] = "InvM";
        MC_collumn_names[4] = "MET_pT";
        MC_collumn_names[5] = "MET_phi";
        int part_num_l;
        for(part_num_l = 0; part_num_l < 2; ++part_num_l)
        {
            MC_collumn_names[6 + part_num_l*4] = "pT_l" + std::to_string(part_num_l + 1);
            MC_collumn_names[7 + part_num_l*4] = "eta_l" + std::to_string(part_num_l + 1);
            MC_collumn_names[8 + part_num_l*4] = "phi_l" + std::to_string(part_num_l + 1);
            MC_collumn_names[9 + part_num_l*4] = "E_l" + std::to_string(part_num_l + 1);
        }
        for(int part_num = 0; part_num < max_phot_MC; ++part_num)
        {
            MC_collumn_names[6 + (part_num_l + part_num)*4] = "pT_p" + std::to_string(part_num);
            MC_collumn_names[7 + (part_num_l + part_num)*4] = "eta_p" + std::to_string(part_num);
            MC_collumn_names[8 + (part_num_l + part_num)*4] = "phi_p" + std::to_string(part_num);
            MC_collumn_names[9 + (part_num_l + part_num)*4] = "E_p" + std::to_string(part_num);
        }
        param_MC_file.WriteObject(&MC_collumn_names, "Collumn_names");
        
        
        
        std::vector<int> shuffle_idx(full_MC_data[0].size());
        std::iota(shuffle_idx.begin(), shuffle_idx.end(), 0);
        std::random_shuffle(shuffle_idx.begin(), shuffle_idx.end());
        std::vector<double> shuffled_vec(full_MC_data[0].size());
        Long64_t cut_size = 1000000;
        std::string cut_count_w = std::to_string(full_MC_data[0].size()/cut_size);
        int cut_count = full_MC_data[0].size()/cut_size;
        param_MC_file.WriteObject(&cut_count_w, "cut_count");
        
        for(int para = 0; para < full_MC_data.size(); ++para)
        {
            //TFile full_MC_file_par((MC_data_save + "MC_data_vec_" + MC_collumn_names[para] + ".root").c_str(), "RECREATE");
            std::cout << "Writting " << MC_collumn_names.at(para) << "\n";
            param_MC_file.WriteObject(&(full_MC_data.at(para)), MC_collumn_names.at(para).c_str());
            
            //param_MC_file.WriteObject(&(shuffled_vec), (MC_collumn_names[para] + "_shuffled").c_str());
        }
        //std::cout << "post_write\n";
        for(int para = 0; para < full_MC_data.size(); ++para)
        {
            for(int i = 0; i < full_MC_data[para].size(); ++i)
                shuffled_vec[i] = full_MC_data[para][shuffle_idx[i]];
            for(int i = 0; i < cut_count; ++i)
            {
                std::vector<double> tempo(shuffled_vec.begin() + i*cut_size, 
                                        shuffled_vec.begin() + std::min((i+1)*cut_size, (Long64_t)full_MC_data[para].size()));
                param_MC_file.WriteObject(&(tempo), (MC_collumn_names[para] + "_shuffled_" + std::to_string(i)).c_str());
            }
            full_MC_data[para] = shuffled_vec;
        }
        csv_maker(full_MC_data, MC_collumn_names, MC_data_save + "MC_data", cut_size);
        param_MC_file.Close();
    }
    
    
    full_MC_data.clear();
    
    
    std::cout << "Pull complete, full_event_count = " << full_event_count << " max_photon = " << max_phot_MC << "\n";
    //return 0;
    
    std::string r_data_loc;
    if(electron)
    {
        r_data_loc = "Dielectron_wPhotons/Data/";
    }else
    {
        r_data_loc = "Dimuon_wPhotons/Data/";
    }
    
    
    std::vector<std::string> file_names_real_data;
    if(electron)
    {
        file_names_real_data = {"SelectedEE_DoubleEG_B.root", "SelectedEE_DoubleEG_C.root",
                                "SelectedEE_DoubleEG_D.root", "SelectedEE_DoubleEG_E.root", 
                                "SelectedEE_DoubleEG_F.root", "SelectedEE_DoubleEG_G.root",
                                "SelectedEE_DoubleEG_Hver2.root", "SelectedEE_DoubleEG_Hver3.root"};
    }else
    {
        file_names_real_data = {"SelectedMuMu_SingleMuon_B.root", "SelectedMuMu_SingleMuon_C.root", 
                                "SelectedMuMu_SingleMuon_D.root", "SelectedMuMu_SingleMuon_E.root",
                                "SelectedMuMu_SingleMuon_F.root", "SelectedMuMu_SingleMuon_G.root", 
                                "SelectedMuMu_SingleMuon_Hver2.root", "SelectedMuMu_SingleMuon_Hver3.root"};
    }
    
    
    std::vector<TH2*> hpr2d(3);
    std::vector<TH2*> hpr2d_low_dr(3);
    std::vector<TH2*> hprdr2d(2);
    TH2* hpr_nVert_2d = new TH2D(("real_data_" + particles[1] + "_nVert_2d").c_str(),
                                 ("real_data_" + particles[1] + "_nVert_2d").c_str(),
                                 (int)nVert_lim_high, nVert_lim_low, nVert_lim_high, 12, -0.5, 11.5);
    
    for(int j = 0; j < hpr2d.size() - 1; ++j)
    {
        hpr2d[j] = new TH2D(
            ("real_data_" + particles[1] + "_" + param1 + "_plus_ph_2d_" + h1_subnames[j]).c_str(), 
            ("real_data_" + particles[1] + "_" + param1 + "_plus_ph_2d_" + h1_subnames[j]).c_str(), 
            bincount, low_hist_lim1[j], high_hist_lim1[j], 
            12, -0.5, 11.5);
    }
    hpr2d[hpr2d.size() - 1] = new TH2D(
        ("real_data_" + particles[1] + "_" + param1 + "_plus_ph_2d_" + h1_subnames[hpr2d.size() - 1]).c_str(),
        ("real_data_" + particles[1] + "_" + param1 + "_plus_ph_2d_" + h1_subnames[hpr2d.size() - 1]).c_str(), 43, massbins, 
        12, -0.5, 11.5);
    for(int j = 0; j < hpr2d_low_dr.size() - 1; ++j)
    {
        hpr2d_low_dr[j] = new TH2D(
            ("real_data_" + particles[1] + "_" + param1 + "_plus_ph_low_dr_2d_" + h1_subnames[j]).c_str(), 
            ("real_data_" + particles[1] + "_" + param1 + "_plus_ph_low_dr_2d_" + h1_subnames[j]).c_str(), 
            bincount, low_hist_lim1[j], high_hist_lim1[j], 
            12, -0.5, 11.5);
    }
    hpr2d_low_dr[hpr2d_low_dr.size() - 1] = new TH2D(
        ("real_data_" + particles[1] + "_" + param1 + "_plus_ph_2d_low_dr_" + h1_subnames[hpr2d_low_dr.size() - 1]).c_str(),
        ("real_data_" + particles[1] + "_" + param1 + "_plus_ph_2d_low_dr_" + h1_subnames[hpr2d_low_dr.size() - 1]).c_str(), 43, massbins, 
        12, -0.5, 11.5);

    hprdr2d[0] = new TH2D(("real_data_" + particles[1] + "_low_dr_2d").c_str(),
                        ("real_data_" + particles[1] + "_low_dr_2d").c_str(),
                        exp_bins_hpdr.size()-1, &exp_bins_hpdr[0], 12, -0.5, 11.5);
        
    hprdr2d[1] = new TH2D(("real_data_" + particles[1] + "_high_dr_2d").c_str(),
                        ("real_data_" + particles[1] + "_high_dr_2d").c_str(),
                        exp_bins_hpdr.size()-1, &exp_bins_hpdr[0], 12, -0.5, 11.5);
    std::vector<int> max_phot_r;
    int r_wsum = 0;
    TH1* nPileUp_dummy;    
    
    for(int i = 0; i < file_names_real_data.size(); ++i)
    {
        full_particles_dat r_data = data_extract(
                                                 "DYTree",
                                                 data_loc,
                                                 r_data_loc,
                                                 {file_names_real_data[i]},
                                                 particles,
                                                 nPileUp_dummy,
                                                 numberOfEntries,
                                                 max_phot_r);
        std::cout << "Real data count: " << numberOfEntries << " max_phot_r = " << max_phot_r[1] << "\n";
        
        for(int j = 0; j < r_data.event_data.size(); ++j)
            r_wsum += r_data.event_data[j].weight;
        double ratio = full_event_count/r_wsum;
        std::cout << "MC: " << full_event_count << " RE: " << r_wsum << " Ratio: " << ratio << "\n";
        ratio = 1;
        
        for(int j = 0; j < r_data.event_data.size(); ++j)
        {
            if(!(r_data.event_data[j].correct_dat))
                    continue;
            double lep_weight = r_data.event_data[j].weight;
            std::vector<size_t> sorted_idx = sort_indexes(r_data.part_data[j][0].pT);
            
            for(int idx = 0; idx < sorted_idx.size(); ++idx)
            {
                h2.Re[idx]->Fill(r_data.part_data[j][0].pT[sorted_idx[idx]],
                            lep_weight);
                        
                h3.Re[idx]->Fill(r_data.part_data[j][0].eta[sorted_idx[idx]], 
                            lep_weight);
                        
                h4.Re[idx]->Fill(r_data.part_data[j][0].phi[sorted_idx[idx]], 
                            lep_weight);
            }
            for(int hist_num = 0; hist_num < h1.Re.size(); ++hist_num)
            {
                h1.Re[hist_num]->Fill(r_data.part_data[j][0].InvM,
                                      lep_weight);
                hpr2d[hist_num]->Fill(r_data.part_data[j][0].InvM, -0.5,
                                      lep_weight);
                hpr2d_low_dr[hist_num]->Fill(r_data.part_data[j][0].InvM, -0.5,
                                      lep_weight);
            }         
        }
        std::cout << particles[0] + " fill complete \n\n";
        for(int j = 0; j < r_data.event_data.size(); ++j)
        {
            if(!(r_data.event_data[j].correct_dat))
                    continue;
            int phot_count = r_data.part_data[j][1].pT.size();
                if(phot_count < 1)
                    continue;
            std::vector<size_t> sorted_idx = sort_indexes(r_data.part_data[j][1].pT);
            int temp = hp2_c.Re.size();
            if(temp < phot_count)
            {
                hp2_c.expand(phot_count - temp);
                hp3_c.expand(phot_count - temp);
                hp4_c.expand(phot_count - temp);
                
                hp2_n.expand(phot_count - temp);
                hp3_n.expand(phot_count - temp);
                hp4_n.expand(phot_count - temp);
                
                hp2_c_dr_cut_low.expand(phot_count - temp);
                hp3_c_dr_cut_low.expand(phot_count - temp);
                hp4_c_dr_cut_low.expand(phot_count - temp);
                
                hp2_n_dr_cut_low.expand(phot_count - temp);
                hp3_n_dr_cut_low.expand(phot_count - temp);
                hp4_n_dr_cut_low.expand(phot_count - temp);
                
                hp2_c_dr_cut_mid.expand(phot_count - temp);
                hp3_c_dr_cut_mid.expand(phot_count - temp);
                hp4_c_dr_cut_mid.expand(phot_count - temp);
                
                hp2_n_dr_cut_mid.expand(phot_count - temp);
                hp3_n_dr_cut_mid.expand(phot_count - temp);
                hp4_n_dr_cut_mid.expand(phot_count - temp);
                
                hp2_c_dr_cut_high.expand(phot_count - temp);
                hp3_c_dr_cut_high.expand(phot_count - temp);
                hp4_c_dr_cut_high.expand(phot_count - temp);
                
                hp2_n_dr_cut_high.expand(phot_count - temp);
                hp3_n_dr_cut_high.expand(phot_count - temp);
                hp4_n_dr_cut_high.expand(phot_count - temp);
                
                hp_plus[0].expand(phot_count - temp);
                hp_plus[1].expand(phot_count - temp);
                hp_plus[2].expand(phot_count - temp);
                hp_plus_low_dr[0].expand(phot_count - temp);
                hp_plus_low_dr[1].expand(phot_count - temp);
                hp_plus_low_dr[2].expand(phot_count - temp);
                hpdr[0].expand(phot_count - temp);
                hpdr[1].expand(phot_count - temp);
                hp_nVert.expand(phot_count - temp);
            }
            
            std::vector<std::vector<double>> dr = dr_lep_phot(r_data.part_data[j][0], r_data.part_data[j][1]);
            hp_nVert.Re[phot_count - 1]->Fill((double)r_data.event_data[j].nVert, r_data.event_data[j].weight);
            hp_nVert_full.Re[0]->Fill((double)r_data.event_data[j].nVert, r_data.event_data[j].weight);
            hpr_nVert_2d->Fill((double)r_data.event_data[j].nVert, phot_count - 0.5, r_data.event_data[j].weight);
            
            for(int idx = 0; idx < phot_count; ++idx)
            {
                hp2_c.Re[phot_count - 1]->Fill(r_data.part_data[j][1].pT[sorted_idx[idx]], r_data.event_data[j].weight);
                hp3_c.Re[phot_count - 1]->Fill(r_data.part_data[j][1].eta[sorted_idx[idx]], r_data.event_data[j].weight);
                hp4_c.Re[phot_count - 1]->Fill(r_data.part_data[j][1].phi[sorted_idx[idx]], r_data.event_data[j].weight);
                
                hp2_n.Re[idx]->Fill(r_data.part_data[j][1].pT[sorted_idx[idx]], r_data.event_data[j].weight);
                hp3_n.Re[idx]->Fill(r_data.part_data[j][1].eta[sorted_idx[idx]], r_data.event_data[j].weight);
                hp4_n.Re[idx]->Fill(r_data.part_data[j][1].phi[sorted_idx[idx]], r_data.event_data[j].weight);
                
                if((dr[0][idx] > dr_cut_low_down) && (dr[0][idx] < dr_cut_low_up))
                {
                    hp2_c_dr_cut_low.Re[phot_count - 1]->Fill(r_data.part_data[j][1].pT[sorted_idx[idx]], r_data.event_data[j].weight);
                    hp3_c_dr_cut_low.Re[phot_count - 1]->Fill(r_data.part_data[j][1].eta[sorted_idx[idx]], r_data.event_data[j].weight);
                    hp4_c_dr_cut_low.Re[phot_count - 1]->Fill(r_data.part_data[j][1].phi[sorted_idx[idx]], r_data.event_data[j].weight);
                    
                    hp2_n_dr_cut_low.Re[idx]->Fill(r_data.part_data[j][1].pT[sorted_idx[idx]], r_data.event_data[j].weight);
                    hp3_n_dr_cut_low.Re[idx]->Fill(r_data.part_data[j][1].eta[sorted_idx[idx]], r_data.event_data[j].weight);
                    hp4_n_dr_cut_low.Re[idx]->Fill(r_data.part_data[j][1].phi[sorted_idx[idx]], r_data.event_data[j].weight);
                        
                    hp2_full_dr_cut.Re[0]->Fill(r_data.part_data[j][1].pT[sorted_idx[idx]], r_data.event_data[j].weight);
                    hp3_full_dr_cut.Re[0]->Fill(r_data.part_data[j][1].eta[sorted_idx[idx]], r_data.event_data[j].weight);
                    hp4_full_dr_cut.Re[0]->Fill(r_data.part_data[j][1].phi[sorted_idx[idx]], r_data.event_data[j].weight);
                }
                
                if((dr[0][idx] > dr_cut_mid_down) && (dr[0][idx] < dr_cut_mid_up))
                {
                    hp2_c_dr_cut_mid.Re[phot_count - 1]->Fill(r_data.part_data[j][1].pT[sorted_idx[idx]], r_data.event_data[j].weight);
                    hp3_c_dr_cut_mid.Re[phot_count - 1]->Fill(r_data.part_data[j][1].eta[sorted_idx[idx]], r_data.event_data[j].weight);
                    hp4_c_dr_cut_mid.Re[phot_count - 1]->Fill(r_data.part_data[j][1].phi[sorted_idx[idx]], r_data.event_data[j].weight);
                    
                    hp2_n_dr_cut_mid.Re[idx]->Fill(r_data.part_data[j][1].pT[sorted_idx[idx]], r_data.event_data[j].weight);
                    hp3_n_dr_cut_mid.Re[idx]->Fill(r_data.part_data[j][1].eta[sorted_idx[idx]], r_data.event_data[j].weight);
                    hp4_n_dr_cut_mid.Re[idx]->Fill(r_data.part_data[j][1].phi[sorted_idx[idx]], r_data.event_data[j].weight);
                        
                    hp2_full_dr_cut.Re[1]->Fill(r_data.part_data[j][1].pT[sorted_idx[idx]], r_data.event_data[j].weight);
                    hp3_full_dr_cut.Re[1]->Fill(r_data.part_data[j][1].eta[sorted_idx[idx]], r_data.event_data[j].weight);
                    hp4_full_dr_cut.Re[1]->Fill(r_data.part_data[j][1].phi[sorted_idx[idx]], r_data.event_data[j].weight);
                }
                
                if((dr[0][idx] > dr_cut_high_down) && (dr[0][idx] < dr_cut_high_up))
                {
                    hp2_c_dr_cut_high.Re[phot_count - 1]->Fill(r_data.part_data[j][1].pT[sorted_idx[idx]], r_data.event_data[j].weight);
                    hp3_c_dr_cut_high.Re[phot_count - 1]->Fill(r_data.part_data[j][1].eta[sorted_idx[idx]], r_data.event_data[j].weight);
                    hp4_c_dr_cut_high.Re[phot_count - 1]->Fill(r_data.part_data[j][1].phi[sorted_idx[idx]], r_data.event_data[j].weight);
                    
                    hp2_n_dr_cut_high.Re[idx]->Fill(r_data.part_data[j][1].pT[sorted_idx[idx]], r_data.event_data[j].weight);
                    hp3_n_dr_cut_high.Re[idx]->Fill(r_data.part_data[j][1].eta[sorted_idx[idx]], r_data.event_data[j].weight);
                    hp4_n_dr_cut_high.Re[idx]->Fill(r_data.part_data[j][1].phi[sorted_idx[idx]], r_data.event_data[j].weight);
                        
                    hp2_full_dr_cut.Re[2]->Fill(r_data.part_data[j][1].pT[sorted_idx[idx]], r_data.event_data[j].weight);
                    hp3_full_dr_cut.Re[2]->Fill(r_data.part_data[j][1].eta[sorted_idx[idx]], r_data.event_data[j].weight);
                    hp4_full_dr_cut.Re[2]->Fill(r_data.part_data[j][1].phi[sorted_idx[idx]], r_data.event_data[j].weight);
                }
                hpdr[0].Re[phot_count - 1]->Fill(dr[0][idx], r_data.event_data[j].weight);
                hpdr[1].Re[phot_count - 1]->Fill(dr[1][idx], r_data.event_data[j].weight);
                hpdr_full.Re[0]->Fill(dr[0][idx], r_data.event_data[j].weight);
                hpdr_full.Re[1]->Fill(dr[1][idx], r_data.event_data[j].weight);
                hprdr2d[0]->Fill(dr[0][idx], phot_count - 0.5, r_data.event_data[j].weight);
                hprdr2d[1]->Fill(dr[1][idx], phot_count - 0.5, r_data.event_data[j].weight);
                
                hp2_full.Re[0]->Fill(r_data.part_data[j][1].pT[sorted_idx[idx]], r_data.event_data[j].weight);
                hp3_full.Re[0]->Fill(r_data.part_data[j][1].eta[sorted_idx[idx]], r_data.event_data[j].weight);
                hp4_full.Re[0]->Fill(r_data.part_data[j][1].phi[sorted_idx[idx]], r_data.event_data[j].weight);
            }
        }
        std::cout << particles[1] + " fill complete\n\n";
        for(int j = 0; j < r_data.event_data.size(); ++j)
        {
            if(!(r_data.event_data[j].correct_dat))
                    continue;
            int phot_count = r_data.part_data[j][1].pT.size();
            if(phot_count < 1)
                    continue;
            std::vector<double> temp_E(r_data.part_data[j][1].pT.size());
            for(int i = 0; i < r_data.part_data[j][1].pT.size(); ++i)
            {
                temp_E[i] = r_data.part_data[j][1].pT[i] * cosh(r_data.part_data[j][1].eta[i]);
            }
            r_data.part_data[j][1].E = temp_E;
            
            double temp_InvM = InvM_calc2(r_data.part_data[j], {{0, 2}, {0, phot_count}});
            double temp_InvM_w_dr_cut = InvM_calc2_w_dr_cut(r_data.part_data[j], {0, InvM_dr_limit});
            for(int f = 0; f < hp_plus.size(); ++f)
            {
                h1_just_phot.Re[f]->Fill(r_data.part_data[j][0].InvM, r_data.event_data[j].weight);
                hp_plus[f].Re[phot_count - 1]->Fill(temp_InvM, r_data.event_data[j].weight);
                hpr2d[f]->Fill(temp_InvM, phot_count - 0.5, r_data.event_data[j].weight);
                hp1_full.Re[f]->Fill(temp_InvM, r_data.event_data[j].weight);
                hp_plus_low_dr[f].Re[phot_count - 1]->Fill(temp_InvM_w_dr_cut, r_data.event_data[j].weight);
                hpr2d_low_dr[f]->Fill(temp_InvM_w_dr_cut, phot_count - 0.5, r_data.event_data[j].weight);
                hp1_full_low_dr.Re[f]->Fill(temp_InvM_w_dr_cut, r_data.event_data[j].weight);
            }
            
        }
        std::cout << "InvM fill complete\n\n";
        
        std::cout << "----------------------------\n";
    }
    //return 0;
    std::string data_save;
    std::string data_save_raw;
    
    if(Corr_error)
    {
        if(electron)
        {
            data_save = "/home/dominykas/dy_mas_do/EE/Electron/";
            data_save_raw = "/home/dominykas/dy_mas_do/EE/Electron/data/";
        }
        else
        {
            data_save = "/home/dominykas/dy_mas_do/MuMu/Muon/";
            data_save_raw = "/home/dominykas/dy_mas_do/MuMu/Muon/data/";
        }
    } else
    {
        if(electron)
        {
            data_save = "/home/dominykas/dy_mas_do/EE_noPileUp/Electron/";
            data_save_raw = "/home/dominykas/dy_mas_do/EE_noPileUp/Electron/data/";
        }
        else
        {
            data_save = "/home/dominykas/dy_mas_do/MuMu_noPileUp/Muon/";
            data_save_raw = "/home/dominykas/dy_mas_do/MuMu_noPileUp/Muon/data/";
        }
    }
    
    bool temp = true;
    h1.save_txt(data_save_raw);
    h1.save(data_save_raw);
    h1.plot(data_save, temp, {false, false, true});
    
    h1_just_phot.save_txt(data_save_raw);
    h1_just_phot.save(data_save_raw);
    h1_just_phot.plot(data_save, temp, {false, false, true});
    
    h2.save_txt(data_save_raw);
    h2.save(data_save_raw);
    h2.plot(data_save, temp, false);
    
    h3.save_txt(data_save_raw);
    h3.save(data_save_raw);
    h3.plot(data_save, temp, false);
    
    h4.save_txt(data_save_raw);
    h4.save(data_save_raw);
    h4.plot(data_save, temp, false);
    
    if(Corr_error)
    {
        if(electron)
        {
            data_save = "/home/dominykas/dy_mas_do/EE/Photon/";
            data_save_raw = "/home/dominykas/dy_mas_do/EE/Photon/data/";
        }
        else
        {
            data_save = "/home/dominykas/dy_mas_do/MuMu/Photon/";
            data_save_raw = "/home/dominykas/dy_mas_do/MuMu/Photon/data/";
        }
    } else
    {
        if(electron)
        {
            data_save = "/home/dominykas/dy_mas_do/EE_noPileUp/Photon/";
            data_save_raw = "/home/dominykas/dy_mas_do/EE_noPileUp/Photon/data/";
        }
        else
        {
            data_save = "/home/dominykas/dy_mas_do/MuMu_noPileUp/Photon/";
            data_save_raw = "/home/dominykas/dy_mas_do/MuMu_noPileUp/Photon/data/";
        }
    }
    
    hp1_full.plot(data_save, temp, {false, false, true});
    hp1_full.save_txt(data_save_raw);
    hp1_full.save(data_save_raw);
    
    hp1_full_low_dr.plot(data_save, temp, {false, false, true});
    hp1_full_low_dr.save_txt(data_save_raw);
    hp1_full_low_dr.save(data_save_raw);
    
    hp2_c.plot(data_save, temp, false);
    hp2_c.save_txt(data_save_raw);
    hp2_c.save(data_save_raw);
    
    hp3_c.plot(data_save, temp, false);
    hp3_c.save_txt(data_save_raw);
    hp3_c.save(data_save_raw);
    
    hp4_c.plot(data_save, temp, false);
    hp4_c.save_txt(data_save_raw);
    hp4_c.save(data_save_raw);
    
    hp2_n.plot(data_save, temp, false);
    hp2_n.save_txt(data_save_raw);
    hp2_n.save(data_save_raw);
    
    hp3_n.plot(data_save, temp, false);
    hp3_n.save_txt(data_save_raw);
    hp3_n.save(data_save_raw);
    
    hp4_n.plot(data_save, temp, false);
    hp4_n.save_txt(data_save_raw);
    hp4_n.save(data_save_raw);
    
    hp2_full.plot(data_save, temp, false);
    hp2_full.save_txt(data_save_raw);
    hp2_full.save(data_save_raw);
    
    hp3_full.plot(data_save, temp, false);
    hp3_full.save_txt(data_save_raw);
    hp3_full.save(data_save_raw);
    
    hp4_full.plot(data_save, temp, false);
    hp4_full.save_txt(data_save_raw);
    hp4_full.save(data_save_raw);
    
    hp2_full_dr_cut.plot(data_save, temp, false);
    hp2_full_dr_cut.save_txt(data_save_raw);
    hp2_full_dr_cut.save(data_save_raw);
    
    hp3_full_dr_cut.plot(data_save, temp, false);
    hp3_full_dr_cut.save_txt(data_save_raw);
    hp3_full_dr_cut.save(data_save_raw);
    
    hp4_full_dr_cut.plot(data_save, temp, false);
    hp4_full_dr_cut.save_txt(data_save_raw);
    hp4_full_dr_cut.save(data_save_raw);
    
    hp_nVert.plot(data_save, true, false);
    hp_nVert.save_txt(data_save_raw);
    hp_nVert.save(data_save_raw);
    
    hp_nVert_full.plot(data_save, true, false);
    hp_nVert_full.save_txt(data_save_raw);
    hp_nVert_full.save(data_save_raw);
    
    hp2_c_dr_cut_low.plot(data_save, temp, false);
    hp2_c_dr_cut_low.save_txt(data_save_raw);
    hp2_c_dr_cut_low.save(data_save_raw);
    
    hp3_c_dr_cut_low.plot(data_save, temp, false);
    hp3_c_dr_cut_low.save_txt(data_save_raw);
    hp3_c_dr_cut_low.save(data_save_raw);
    
    hp4_c_dr_cut_low.plot(data_save, temp, false);
    hp4_c_dr_cut_low.save_txt(data_save_raw);
    hp4_c_dr_cut_low.save(data_save_raw);
    
    hp2_n_dr_cut_low.plot(data_save, temp, false);
    hp2_n_dr_cut_low.save_txt(data_save_raw);
    hp2_n_dr_cut_low.save(data_save_raw);
    
    hp3_n_dr_cut_low.plot(data_save, temp, false);
    hp3_n_dr_cut_low.save_txt(data_save_raw);
    hp3_n_dr_cut_low.save(data_save_raw);
    
    hp4_n_dr_cut_low.plot(data_save, temp, false);
    hp4_n_dr_cut_low.save_txt(data_save_raw);
    hp4_n_dr_cut_low.save(data_save_raw);
    
    hp2_c_dr_cut_mid.plot(data_save, temp, false);
    hp2_c_dr_cut_mid.save_txt(data_save_raw);
    hp2_c_dr_cut_mid.save(data_save_raw);
    
    hp3_c_dr_cut_mid.plot(data_save, temp, false);
    hp3_c_dr_cut_mid.save_txt(data_save_raw);
    hp3_c_dr_cut_mid.save(data_save_raw);
    
    hp4_c_dr_cut_mid.plot(data_save, temp, false);
    hp4_c_dr_cut_mid.save_txt(data_save_raw);
    hp4_c_dr_cut_mid.save(data_save_raw);
    
    hp2_n_dr_cut_mid.plot(data_save, temp, false);
    hp2_n_dr_cut_mid.save_txt(data_save_raw);
    hp2_n_dr_cut_mid.save(data_save_raw);
    
    hp3_n_dr_cut_mid.plot(data_save, temp, false);
    hp3_n_dr_cut_mid.save_txt(data_save_raw);
    hp3_n_dr_cut_mid.save(data_save_raw);
    
    hp4_n_dr_cut_mid.plot(data_save, temp, false);
    hp4_n_dr_cut_mid.save_txt(data_save_raw);
    hp4_n_dr_cut_mid.save(data_save_raw);
    
    hp2_c_dr_cut_high.plot(data_save, temp, false);
    hp2_c_dr_cut_high.save_txt(data_save_raw);
    hp2_c_dr_cut_high.save(data_save_raw);
    
    hp3_c_dr_cut_high.plot(data_save, temp, false);
    hp3_c_dr_cut_high.save_txt(data_save_raw);
    hp3_c_dr_cut_high.save(data_save_raw);
    
    hp4_c_dr_cut_high.plot(data_save, temp, false);
    hp4_c_dr_cut_high.save_txt(data_save_raw);
    hp4_c_dr_cut_high.save(data_save_raw);
    
    hp2_n_dr_cut_high.plot(data_save, temp, false);
    hp2_n_dr_cut_high.save_txt(data_save_raw);
    hp2_n_dr_cut_high.save(data_save_raw);
    
    hp3_n_dr_cut_high.plot(data_save, temp, false);
    hp3_n_dr_cut_high.save_txt(data_save_raw);
    hp3_n_dr_cut_high.save(data_save_raw);
    
    hp4_n_dr_cut_high.plot(data_save, temp, false);
    hp4_n_dr_cut_high.save_txt(data_save_raw);
    hp4_n_dr_cut_high.save(data_save_raw);
    
    hp_plus[0].save_txt(data_save_raw);
    hp_plus[0].save(data_save_raw);
    hp_plus[0].plot(data_save, temp, false);
    
    hp_plus[1].save_txt(data_save_raw);
    hp_plus[1].save(data_save_raw);
    hp_plus[1].plot(data_save, temp, false);
    
    hp_plus[2].save_txt(data_save_raw);
    hp_plus[2].save(data_save_raw);
    hp_plus[2].plot(data_save, temp, true);
    
    hp_plus_low_dr[0].save_txt(data_save_raw);
    hp_plus_low_dr[0].save(data_save_raw);
    hp_plus_low_dr[0].plot(data_save, temp, false);
    
    hp_plus_low_dr[1].save_txt(data_save_raw);
    hp_plus_low_dr[1].save(data_save_raw);
    hp_plus_low_dr[1].plot(data_save, temp, false);
    
    hp_plus_low_dr[2].save_txt(data_save_raw);
    hp_plus_low_dr[2].save(data_save_raw);
    hp_plus_low_dr[2].plot(data_save, temp, true);
    
    hpdr[0].save_txt(data_save_raw);
    hpdr[0].save(data_save_raw);
    hpdr[0].plot(data_save, temp, true);
    
    hpdr[1].save_txt(data_save_raw);
    hpdr[1].save(data_save_raw);
    hpdr[1].plot(data_save, temp, true);
    
    hpdr_full.save_txt(data_save_raw);
    hpdr_full.save(data_save_raw);
    hpdr_full.plot(data_save, temp, true);
    
    plots2d(hp2d[0], hpr2d[0], particles[1] + param1 + "_plus_ph_" + h1_subnames[0], data_save, param_label_m, temp, false, false, 12);
    plots2d(hp2d[1], hpr2d[1], particles[1] + param1 + "_plus_ph_" + h1_subnames[1], data_save, param_label_m, temp, false, false, 12);
    plots2d(hp2d[2], hpr2d[2], particles[1] + param1 + "_plus_ph_" + h1_subnames[2], data_save, param_label_m, temp, false, true, 12);
    
    plots2d(hp2d_low_dr[0], hpr2d_low_dr[0], particles[1] + param1 + "_plus_ph_low_dr_" + h1_subnames[0], data_save, param_label_m, temp, false, false, 12);
    plots2d(hp2d_low_dr[1], hpr2d_low_dr[1], particles[1] + param1 + "_plus_ph_low_dr_" + h1_subnames[1], data_save, param_label_m, temp, false, false, 12);
    plots2d(hp2d_low_dr[2], hpr2d_low_dr[2], particles[1] + param1 + "_plus_ph_low_dr_" + h1_subnames[2], data_save, param_label_m, temp, false, true, 12);
    
    plots2d(hpdr2d[0], hprdr2d[0], particles[1] + "_low_dr_2d", data_save, param_label_dr, temp, false, true, 12);
    plots2d(hpdr2d[1], hprdr2d[1], particles[1] + "_high_dr_2d", data_save, param_label_dr, temp, false, true, 12);
    
    plots2d(hp_nVert_2d, hpr_nVert_2d, particles[1] + "_nVert_2d", data_save, param_label_nVert, temp, false, false, 12);
    return 0;
}

std::vector<std::string> add_each(std::vector<std::string> initi, std::string add)
{
    std::vector<std::string> tempo(initi.size());
    for(int i = 0; i < initi.size(); ++i)
    {
        tempo[i] = initi[i] + add;
    }
    return tempo;
}

full_particles_dat data_extract(std::string tree_name,            //cant seem to pass a chain properly
                                std::string data_loc,             //it calls the constructor
                                std::string common_loc,           //so this is the sollution for now
                                std::vector<std::string> file_names, 
                                std::vector<std::string> leaf_common_names,
                                TH1* nPileUp_wh,
                                Long64_t &numberOfEntries,
                                std::vector<int> &max_part)
{
    max_part = std::vector<int>(leaf_common_names.size(), 0);
    TChain chain(tree_name.c_str());
    for(int j = 0; j < file_names.size(); ++j)
    {
        chain.AddFile((data_loc + common_loc + file_names[j]).c_str(), 0, tree_name.c_str());
    }
    
    numberOfEntries = chain.GetEntries();
    Long64_t entry_limit = numberOfEntries; //only needed for testing
    if(numberOfEntries < entry_limit)       //otherwise entry_limit = numberOfEntries
        entry_limit = numberOfEntries;
    std::cout << entry_limit << "\n";
    /*
    std::cout << numberOfEntries << "\n";
    for(int i = 0; i < file_names.size(); ++i)
        std::cout << file_names[i] << " ";
    std::cout << "\n";
    */
    full_particles_dat data;
    data.part_data = std::vector<std::vector<particles_dat>>(entry_limit, std::vector<particles_dat>(leaf_common_names.size()));
    data.event_data = std::vector<evnt_dat>(entry_limit);
    
    
    if(numberOfEntries < 1)
        return data;
    Long64_t offset = 0;
    for(int fi = 0; fi < file_names.size(); ++fi)
    {
        chain.LoadTree(offset);
        int dub_data_corr = 0;
        TBranch* lookout_branch = chain.FindBranch(data.corr_name.c_str());
        if(!lookout_branch)
        {
            std::cout << "Error: Required branch " << data.corr_name.c_str() << " not found\n";
            return data;
        }
        //std::cout << "\n" << numberOfEntries << " " << offset << "\n";
            
        Long64_t limiter = std::min(lookout_branch->GetEntries(), entry_limit - offset);
            
        lookout_branch->SetAddress(&dub_data_corr);
        //std::cout << limiter << " " << lookout_branch->GetEntries() << "\n\n";
        for(int entry = 0; entry < limiter; ++entry)
        {
            lookout_branch->GetEntry(entry);
            for(int c = 0; c < data.corr_idx.size(); ++c)
            {
                if(dub_data_corr == data.corr_idx[c])
                {
                    data.event_data[entry + offset].correct_dat = true;
                    break;
                }
            }
        }
        offset += lookout_branch->GetEntries();
    }
    
    Long64_t offset_w = 0;
    for(int fi = 0; fi < file_names.size(); ++fi)
    {
        chain.LoadTree(offset_w);
            
        TBranch* lookout_branch = chain.FindBranch(data.weight_name.c_str());
        if(!lookout_branch)
        {
            std::cout << "Error: Required branch " << data.weight_name.c_str() << " not found\n";
            return data;
        }
        std::cout << data.weight_name << "\n";
        double dub_data_w = 0;
        lookout_branch->SetAddress(&dub_data_w);
            
        Long64_t limiter = std::min(lookout_branch->GetEntries(), entry_limit - offset_w);
            
        if(nPileUp_wh)
        {
            TBranch* lookout_branch_npu = chain.FindBranch(data.nPileUp_name.c_str());
            if(!lookout_branch_npu)
            {
                std::cout << "Error: Required branch " << data.nPileUp_name << " not found\n";
                return data;
            }
            int dub_data_npu = 0;
            lookout_branch_npu->SetAddress(&dub_data_npu);
            for(int entry = 0; entry < limiter; ++entry)
            {
                lookout_branch_npu->GetEntry(entry);
                lookout_branch->GetEntry(entry);
                data.event_data[entry + offset_w].weight = dub_data_w * nPileUp_wh->GetBinContent(nPileUp_wh->FindBin(dub_data_npu));
            }
        } else
        {
            for(int entry = 0; entry < limiter; ++entry)
            {
                lookout_branch->GetEntry(entry);
                data.event_data[entry + offset_w].weight = dub_data_w;
            }
        }
        std::cout << "\n";
        offset_w += lookout_branch->GetEntries();
    }
    
    
    for(int k = 0; k<leaf_common_names.size(); ++k)
    {
        Long64_t offset = 0;
        for(int fi = 0; fi < file_names.size(); ++fi)
        {
            chain.LoadTree(offset);
            TBranch* lookout_branch = chain.FindBranch((leaf_common_names[k] + data.pT_name).c_str());
            if(lookout_branch)
            {
                std::vector<double> * dub_data = nullptr;
                
                Long64_t limiter = std::min(lookout_branch->GetEntries(), entry_limit - offset);
                lookout_branch->SetAddress(&dub_data);
                    
                std::cout << leaf_common_names[k] + data.pT_name << "\n";
                for(int entry = 0; entry < limiter; ++entry)
                {
                    lookout_branch->GetEntry(entry);
                    data.part_data[entry + offset][k].pT = *dub_data;
                    max_part[k] = std::max(max_part[k], (int)data.part_data[entry + offset][k].pT.size());
                }
                std::cout << "\n";
                offset += lookout_branch->GetEntries();
            }
        }
    }
    
    for(int k = 0; k<leaf_common_names.size(); ++k)
    {
        Long64_t offset = 0;
        for(int fi = 0; fi < file_names.size(); ++fi)
        {
            chain.LoadTree(offset);
            TBranch* lookout_branch = chain.FindBranch((leaf_common_names[k] + data.eta_name).c_str());
            if(lookout_branch)
            {
                std::vector<double> * dub_data = nullptr;
                
                Long64_t limiter = std::min(lookout_branch->GetEntries(), entry_limit - offset);
                lookout_branch->SetAddress(&dub_data);
                    
                std::cout << leaf_common_names[k] + data.eta_name << "\n";
                for(int entry = 0; entry < limiter; ++entry)
                {
                    lookout_branch->GetEntry(entry);
                    data.part_data[entry + offset][k].eta = *dub_data;
                }
                std::cout << "\n";
                offset += lookout_branch->GetEntries();
            }
        }
    }
    
    for(int k = 0; k<leaf_common_names.size(); ++k)
    {
        Long64_t offset = 0;
        for(int fi = 0; fi < file_names.size(); ++fi)
        {
            chain.LoadTree(offset);
            TBranch* lookout_branch = chain.FindBranch((leaf_common_names[k] + data.phi_name).c_str());
            if(lookout_branch)
            {
                std::vector<double> * dub_data = nullptr;
                
                Long64_t limiter = std::min(lookout_branch->GetEntries(), entry_limit - offset);
                lookout_branch->SetAddress(&dub_data);
                    
                std::cout << leaf_common_names[k] + data.phi_name << "\n";
                for(int entry = 0; entry < limiter; ++entry)
                {
                    lookout_branch->GetEntry(entry);
                    data.part_data[entry + offset][k].phi = *dub_data;
                }
                std::cout << "\n";
                offset += lookout_branch->GetEntries();
            }
        }
    }
    
    for(int k = 0; k<leaf_common_names.size(); ++k)
    {
        Long64_t offset = 0;
        for(int fi = 0; fi < file_names.size(); ++fi)
        {
            chain.LoadTree(offset);
            TBranch* lookout_branch = chain.FindBranch((leaf_common_names[k] + data.E_name).c_str());
            if(lookout_branch)
            {
                std::vector<double> * dub_data = nullptr;
                
                Long64_t limiter = std::min(lookout_branch->GetEntries(), entry_limit - offset);
                lookout_branch->SetAddress(&dub_data);
                    
                std::cout << leaf_common_names[k] + data.E_name << "\n";
                for(int entry = 0; entry < limiter; ++entry)
                {
                    lookout_branch->GetEntry(entry);
                    data.part_data[entry + offset][k].E = *dub_data;
                }
                std::cout << "\n";
                offset += lookout_branch->GetEntries();
            }
        }
    }
        
    
    for(int k = 0; k<leaf_common_names.size(); ++k)
    {
        Long64_t offset = 0;
        for(int fi = 0; fi < file_names.size(); ++fi)
        {
            chain.LoadTree(offset);
            TBranch* lookout_branch = chain.FindBranch((leaf_common_names[k] + data.InvM_name).c_str());
            if(lookout_branch)
            {
                double dub_data = 0;
                
                Long64_t limiter = std::min(lookout_branch->GetEntries(), entry_limit - offset);
                lookout_branch->SetAddress(&dub_data);
                    
                std::cout << leaf_common_names[k] + data.InvM_name << "\n";
                for(int entry = 0; entry < limiter; ++entry)
                {
                    lookout_branch->GetEntry(entry);
                    data.part_data[entry + offset][k].InvM = dub_data;
                }
                std::cout << "\n";
                offset += lookout_branch->GetEntries();
            }
        }
    }
    
    Long64_t offset_V = 0;
    for(int fi = 0; fi < file_names.size(); ++fi)
    {
        chain.LoadTree(offset_V);
        TBranch* lookout_branch = chain.FindBranch((data.nVert_name).c_str());
        if(lookout_branch)
        {
            int dub_data = 0;
                
            Long64_t limiter = std::min(lookout_branch->GetEntries(), entry_limit - offset_V);
            lookout_branch->SetAddress(&dub_data);
                    
            std::cout << data.nVert_name << "\n";
            for(int entry = 0; entry < limiter; ++entry)
            {
                lookout_branch->GetEntry(entry);
                data.event_data[entry + offset_V].nVert = dub_data;
            }
            std::cout << "\n";
            offset_V += lookout_branch->GetEntries();
        }
    }
    
    Long64_t offset_Mp = 0;
    for(int fi = 0; fi < file_names.size(); ++fi)
    {
        chain.LoadTree(offset_Mp);
        TBranch* lookout_branch = chain.FindBranch((data.MET_pT_name).c_str());
        if(lookout_branch)
        {
            int dub_data = 0;
                
            Long64_t limiter = std::min(lookout_branch->GetEntries(), entry_limit - offset_Mp);
            lookout_branch->SetAddress(&dub_data);
                    
            std::cout << data.MET_pT_name << "\n";
            for(int entry = 0; entry < limiter; ++entry)
            {
                lookout_branch->GetEntry(entry);
                data.event_data[entry + offset_Mp].MET_pT = dub_data;
            }
            std::cout << "\n";
            offset_Mp += lookout_branch->GetEntries();
        }
    }
    
    Long64_t offset_Mph = 0;
    for(int fi = 0; fi < file_names.size(); ++fi)
    {
        chain.LoadTree(offset_Mph);
        TBranch* lookout_branch = chain.FindBranch((data.MET_phi_name).c_str());
        if(lookout_branch)
        {
            int dub_data = 0;
                
            Long64_t limiter = std::min(lookout_branch->GetEntries(), entry_limit - offset_Mph);
            lookout_branch->SetAddress(&dub_data);
                    
            std::cout << data.MET_phi_name << "\n";
            for(int entry = 0; entry < limiter; ++entry)
            {
                lookout_branch->GetEntry(entry);
                data.event_data[entry + offset_Mph].MET_phi = dub_data;
            }
            std::cout << "\n";
            offset_Mph += lookout_branch->GetEntries();
        }
    }
    //numberOfEntries = offset_w;
    //chain.Delete("");
    std::cout << "\n";
    return data;
}

std::vector<std::vector<std::string>> add_cut_names(std::vector<std::string> cut_sequence, std::string cut_name, std::string core_name, std::vector<std::vector<std::string>> ext, bool add_inf, bool skip_first_seq)
{
    int len = cut_sequence.size();
    if(add_inf)
        ++len;
    std::vector<std::vector<std::string>> result(ext);
    //print2d(ext);
    //std::cout << result.at(0).at(0) <<"\n";
    int strt_boost = 0;
    if(skip_first_seq)
    {
        strt_boost = 1;
        for(int j = 0; j < ext[0].size(); ++j)
            result[0][j] = core_name + ext[0][j] + ".root";
    }
    for(int i = 0; i < cut_sequence.size() - 1; ++i)
    {
        for(int j = 0; j < ext[i + strt_boost].size(); ++j)
        {
            //std::cout << i << "\n";
            result[i + strt_boost][j] = core_name + cut_name + cut_sequence[i] + "to" + cut_sequence[i+1] + ext[i + strt_boost][j] + ".root";
            
        }
    }
    if(add_inf)
        for(int j = 0; j < ext[ext.size() - 1].size(); ++j)
            result[ext.size() - 1][j] = core_name + cut_name + cut_sequence[cut_sequence.size() - 1] + "to" + "Inf" + ext[ext.size() - 1][j] + ".root";
    return result;
}

void plots2d(std::vector<TH2*> h, TH2* hr, std::string name, std::string data_save, 
             std::string param_name, bool logz, bool logy, bool logx, int nDivY, int nDivX, double min_val, 
             std::string stacked_type, std::string real_type, std::string ratio_type)
{
    TCanvas *c = new TCanvas(name.c_str(), "Preliminary data", 2100, 600);
    c->Divide(3, 1, 0.001, 0.01);
    TPad *p_1 = (TPad*)(c->cd(1));
    if(logy)
        p_1->SetLogy();
    if(logx)
        p_1->SetLogx();
    if(logz)
        p_1->SetLogz();
    p_1->SetLeftMargin(0.15);
    p_1->SetBottomMargin(0.15);
    p_1->SetRightMargin(0.2);
    TH1 * sum_h = (TH1D*)h[0]->Clone("");
    for(int i = 1; i < h.size(); ++i)
        sum_h->Add(h[i]);
    sum_h->SetTitle((";" + param_name + ";Photon count").c_str());
    sum_h->SetStats(0);
    
    sum_h->GetXaxis()->SetTitleSize(0.07);
    sum_h->GetXaxis()->SetLabelSize(0.06);
    sum_h->GetXaxis()->SetTitleOffset(1);
    sum_h->GetXaxis()->SetNdivisions(nDivX);
    
    sum_h->GetYaxis()->SetTitleSize(0.07);
    sum_h->GetYaxis()->SetLabelSize(0.06);
    sum_h->GetYaxis()->SetTitleOffset(1);
    sum_h->GetYaxis()->SetNdivisions(nDivY);
    
    sum_h->GetZaxis()->SetLabelSize(0.06);
    sum_h->GetZaxis()->SetTitle("Number of Events");
    sum_h->GetZaxis()->SetTitleSize(0.06);
    sum_h->GetZaxis()->SetTitleOffset(1.1);
    
    sum_h->Draw(stacked_type.c_str());
    TPad *p_2 = (TPad*)(c->cd(2));
    if(logy)
        p_2->SetLogy();
    if(logx)
        p_2->SetLogx();
    if(logz)
        p_2->SetLogz();
    p_2->SetLeftMargin(0.05);
    p_2->SetBottomMargin(0.15);
    p_2->SetRightMargin(0.2);
    hr->SetStats(0);
    hr->SetTitle((";" + param_name + ";").c_str());
    
    hr->GetXaxis()->SetTitleSize(0.07);
    hr->GetXaxis()->SetLabelSize(0.06);
    hr->GetXaxis()->SetNdivisions(nDivX);
    hr->GetXaxis()->SetTitleOffset(1);
    
    hr->GetYaxis()->SetLabelSize(0.06);
    hr->GetYaxis()->SetNdivisions(nDivY);
    
    hr->GetZaxis()->SetLabelSize(0.06);
    hr->GetZaxis()->SetTitle("Number of Events");
    hr->GetZaxis()->SetTitleSize(0.06);
    hr->GetZaxis()->SetTitleOffset(1.1);
    //hr->GetXaxis()->SetTitle("#Delta R");
    hr->Draw(real_type.c_str());
    
    TPad *p_3 = (TPad*)(c->cd(3));
    if(logx)
        p_3->SetLogx();
    if(logy)
        p_3->SetLogy();
    p_3->SetLeftMargin(0.05);
    p_3->SetBottomMargin(0.15);
    p_3->SetRightMargin(0.2);
    TH1* rat_h = (TH1D*)hr->Clone("");
    rat_h->Divide(sum_h);
    
    double min2 = rat_h->GetMinimum();
    
    if(min2 < 0)
        rat_h->SetMinimum(0);
    double max2 = rat_h->GetMaximum();
    
    if(max2 > 2)
        rat_h->SetMaximum(2);
    rat_h->SetStats(0);
    rat_h->SetTitle((";" + param_name + ";").c_str());
    rat_h->GetXaxis()->SetTitleSize(0.07);
    rat_h->GetXaxis()->SetLabelSize(0.06);
    rat_h->GetXaxis()->SetTitleOffset(1);
    rat_h->GetXaxis()->SetNdivisions(nDivX);
    
    rat_h->GetYaxis()->SetLabelSize(0.06);
    rat_h->GetYaxis()->SetNdivisions(nDivY);
    
    rat_h->GetZaxis()->SetLabelSize(0.06);
    rat_h->GetZaxis()->SetTitle("Data/MC");
    rat_h->GetZaxis()->SetTitleSize(0.06);
    rat_h->GetZaxis()->SetTitleOffset(1.1);
    //rat_h->GetXaxis()->SetTitle("#Delta R");
    rat_h->Draw(ratio_type.c_str());
    
    c->SaveAs((data_save + name + ".pdf").c_str());
    
    TCanvas *c_MC = new TCanvas((name + "MC").c_str(), "Preliminary data", 700, 600);
    c_MC->SetLeftMargin(0.15);
    c_MC->SetBottomMargin(0.15);
    c_MC->SetRightMargin(0.2);
    if(logy)
        c_MC->SetLogy();
    if(logx)
        c_MC->SetLogx();
    if(logz)
        c_MC->SetLogz();
    sum_h->Draw(stacked_type.c_str());
    c_MC->SaveAs((data_save + "subplots/" + name + "MC" + ".pdf").c_str());
    
    TCanvas *c_Re = new TCanvas((name + "Re").c_str(), "Preliminary data", 700, 600);
    c_Re->SetLeftMargin(0.15);
    c_Re->SetBottomMargin(0.15);
    c_Re->SetRightMargin(0.2);
    if(logy)
        c_Re->SetLogy();
    if(logx)
        c_Re->SetLogx();
    if(logz)
        c_Re->SetLogz();
    hr->Draw(stacked_type.c_str());
    c_Re->SaveAs((data_save + "subplots/" + name + "Re" + ".pdf").c_str());
    
    TCanvas *c_Rat = new TCanvas((name + "Rat").c_str(), "Preliminary data", 700, 600);
    c_Rat->SetLeftMargin(0.15);
    c_Rat->SetBottomMargin(0.15);
    c_Rat->SetRightMargin(0.2);
    if(logy)
        c_Rat->SetLogy();
    if(logx)
        c_Rat->SetLogx();
    rat_h->Draw(stacked_type.c_str());
    c_Rat->SaveAs((data_save + "subplots/" + name + "Rat" + ".pdf").c_str());
    
}

template <typename T>
void print2d(std::vector<std::vector<T>> data)
{
    for(int i = 0; i < data.size(); ++i)
    {
        for(int j = 0; j < data.at(i).size(); ++j)
            std::cout << "[" << data.at(i).at(j) << "] ";
        std::cout << "\n";
    }
    std::cout << "---\n";
}

template <typename T>
void print1d(std::vector<T> data)
{
    for(int j = 0; j < data.size(); ++j)
        std::cout << "[" << data.at(j) << "] ";
    std::cout << "\n";
    std::cout << "---\n";
}

template <typename T>
std::vector<size_t> sort_indexes(const std::vector<T> &v) 
{
  std::vector<size_t> idx(v.size());
  iota(idx.begin(), idx.end(), 0);
  stable_sort(idx.begin(), idx.end(),
       [&v](size_t i1, size_t i2) {return v[i1] > v[i2];});

  return idx;
}

double InvM_calc2(std::vector<particles_dat>  particles, std::vector<std::vector<int>> limits)
{
    double E_sum = 0;
    double px_sum = 0;
    double py_sum = 0;
    double pz_sum = 0;
    for(int i = 0; i < particles.size(); ++i)
    {
        for(int j = limits[i][0]; j < std::min((int)particles[i].pT.size(), limits[i][1]); ++j)
        {
            E_sum += particles[i].E[j];
            px_sum += particles[i].pT[j] * cos(particles[i].phi[j]);
            py_sum += particles[i].pT[j] * sin(particles[i].phi[j]);
            pz_sum += particles[i].pT[j] * sinh(particles[i].eta[j]);
        }
    }
    return sqrt(E_sum * E_sum - px_sum * px_sum - py_sum * py_sum - pz_sum * pz_sum);
}

double InvM_calc2_w_dr_cut(std::vector<particles_dat>  particles, std::vector<double> dr_limits)
{
    double E_sum = 0;
    double px_sum = 0;
    double py_sum = 0;
    double pz_sum = 0;
    std::vector<std::vector<double>> dr = dr_lep_phot(particles[0], particles[1]);
    for(int j = 0; j < particles[0].pT.size(); ++j)
    {
        E_sum += particles[0].E[j];
        px_sum += particles[0].pT[j] * cos(particles[0].phi[j]);
        py_sum += particles[0].pT[j] * sin(particles[0].phi[j]);
        pz_sum += particles[0].pT[j] * sinh(particles[0].eta[j]);
    }
    for(int j = 0; j < particles[1].pT.size(); ++j)
    {
        if((dr[0][j] > dr_limits[0]) && (dr[0][j] < dr_limits[1]))
        {
            E_sum += particles[1].E[j];
            px_sum += particles[1].pT[j] * cos(particles[1].phi[j]);
            py_sum += particles[1].pT[j] * sin(particles[1].phi[j]);
            pz_sum += particles[1].pT[j] * sinh(particles[1].eta[j]);
        }
    }
    return sqrt(E_sum * E_sum - px_sum * px_sum - py_sum * py_sum - pz_sum * pz_sum);
}

std::vector<std::vector<double>> dr_lep_phot(particles_dat lep, particles_dat phot)
{
    //std::cout << lep.pT.size() << "\n";
    std::vector<std::vector<double>> dr(lep.pT.size(), std::vector<double>(phot.pT.size()));
    for(int p = 0; p < phot.pT.size(); ++p)
    {
        std::vector<double> dr_temp(lep.pT.size());
        for(int l = 0; l < lep.pT.size(); ++l)
        {
            dr_temp[l] = sqrt((lep.eta[l] - phot.eta[p])*(lep.eta[l] - phot.eta[p]) + (lep.phi[l] - phot.phi[p])*(lep.phi[l] - phot.phi[p]));
        }
        sort(dr_temp.begin(), dr_temp.end());
        for(int l = 0; l < lep.pT.size(); ++l)
        {
            dr[l][p] = dr_temp[l];
        }
    }
    for(int l = 0; l < lep.pT.size(); ++l)
    {
        //sort(dr[l].begin(), dr[l].end());
    }
    return dr;
}

std::vector<std::vector<double>> reco_hist_create(std::vector<std::string> file_names, std::string graph_name, std::vector<double> Lumi)
{
    std::vector<std::vector<double>> reco_hist_full(2);
    TFile reco_file_1(file_names[0].c_str(), "READ");
    TGraphAsymmErrors* reco_graph_1 = (TGraphAsymmErrors*)reco_file_1.Get(graph_name.c_str());
    int size = reco_graph_1->GetMaxSize();
    reco_hist_full[0] = std::vector<double>(size);
    reco_hist_full[1] = std::vector<double>(size+1, 0);
    double X;
    reco_hist_full[1][0] = reco_graph_1->GetPointX(0) - reco_graph_1->GetErrorXlow(0);
    for(int i = 0; i < size; ++i)
    {
        reco_graph_1->GetPoint(i, X, reco_hist_full[0][i]);
        reco_hist_full[0][i] *= Lumi[0];
        reco_hist_full[1][i+1] = X + reco_graph_1->GetErrorXhigh(i);
    }
    double Lumi_full = Lumi[0];
    for(int f = 1; f < file_names.size(); ++f)
    {
        TFile reco_file(file_names[f].c_str(), "READ");
        TGraphAsymmErrors* reco_graph = (TGraphAsymmErrors*)reco_file.Get(graph_name.c_str());
        for(int i = 0; i < size; ++i)
        {
            reco_hist_full[0][i] += reco_graph->GetPointY(i) * Lumi[f];
        }
        Lumi_full += Lumi[f];
    }
    
    for(int i = 0; i < size; ++i)
    {
        reco_hist_full[0][i] /= Lumi_full;
    }
    return reco_hist_full;
}

TH2* ID_ISO_Trigger_scale_hist_create(std::vector<std::string> file_names, 
                                      std::vector<std::string> graph_names, 
                                      std::vector<double> Lumi)
{
    TFile ID_scale_file_1(file_names[0].c_str(), "READ");
    TH2* hist_Re_1 = (TH2F*)ID_scale_file_1.Get(graph_names[0].c_str());
    hist_Re_1->SetDirectory(0);
    TH2* hist_MC_1 = (TH2F*)ID_scale_file_1.Get(graph_names[1].c_str());
    hist_MC_1->SetDirectory(0);
    hist_Re_1->Divide(hist_MC_1);
    hist_Re_1->Scale(Lumi[0]);
    TH2* ID_scale_hist = hist_Re_1;
    double Lumi_full = Lumi[0];
    for(int f = 1; f < file_names.size(); ++f)
    {
        TFile ID_scale_file(file_names[f].c_str(), "READ");
        TH2* hist_Re = (TH2F*)ID_scale_file.Get(graph_names[0].c_str());
        hist_Re->SetDirectory(0);
        TH2* hist_MC = (TH2F*)ID_scale_file.Get(graph_names[1].c_str());
        hist_MC->SetDirectory(0);
        hist_Re->Divide(hist_MC);
        hist_Re->Scale(Lumi[f]);
        ID_scale_hist->Add(hist_Re);
        Lumi_full += Lumi[f];
    }
    ID_scale_hist->Scale(1/Lumi_full);
    int Y_bincount = ID_scale_hist->GetNbinsY();
    for(int i = 0; i < ID_scale_hist->GetNbinsX() + 1; ++i)
    {
        ID_scale_hist->SetBinContent(i, Y_bincount + 1, ID_scale_hist->GetBinContent(i, Y_bincount));
        ID_scale_hist->SetBinContent(i, 0, ID_scale_hist->GetBinContent(i, 1));
    }
    return ID_scale_hist;
}

template <typename T>
void csv_maker(std::vector<std::vector<T>> &data, std::vector<std::string> header, 
               std::string save_loc, Long64_t cut_size)
{
    if((data.size() < 1) || (data[0].size() < 1))
        return;
    std::vector<double> min_vec(data.size(), 0);
    std::vector<double> max_vec(data.size(), 0);
    if(cut_size < 1)
        cut_size = data[0].size();
    Long64_t cut_count = data[0].size()/cut_size + 1;
    for(Long64_t cuts = 0; cuts < cut_count; ++cuts)
    {
        std::ofstream data_file;
        data_file.open(save_loc + "_" + std::to_string(cuts) + ".csv");
        if(!(data_file.is_open()))
            return;
        
        data_file << header[0];
        for(int j = 1; j < data.size(); ++j)
        {
            data_file << "," << header[j];
        }
        
        data_file << "\n";
        data_file << data[0][cut_size*cuts];
        min_vec[0] = std::min(min_vec[0], data[0][cut_size*cuts]);
        max_vec[0] = std::max(max_vec[0], data[0][cut_size*cuts]);
        for(int j = 1; j < data.size(); ++j)
        {
            data_file << "," << data[j][cut_size*cuts];
            min_vec[j] = std::min(min_vec[j], data[j][cut_size*cuts]);
            max_vec[j] = std::max(max_vec[j], data[j][cut_size*cuts]);
        }
        for(int i = cut_size*cuts + 1; i < std::min(cut_size*(cuts + 1), (Long64_t)data[0].size()); ++i)
        {
            data_file << "\n";
            data_file << data[0][i];
            for(int j = 1; j < data.size(); ++j)
            {
                data_file << "," << data[j][i];
                min_vec[j] = std::min(min_vec[j], data[j][i]);
                max_vec[j] = std::max(max_vec[j], data[j][i]);
            }
        }
        data_file.close();
        std::cout << "CSV file cut " << cuts << "/" << cut_count - 1 << " complete\n";
    }
    std::ofstream data_file_min_max;
    data_file_min_max.open(save_loc + "_min_max.csv");
    data_file_min_max << header[0];
    for(int j = 1; j < data.size(); ++j)
    {
        data_file_min_max << "," << header[j];
    }
    data_file_min_max << "\n";
    data_file_min_max << min_vec[0];
    for(int j = 1; j < data.size(); ++j)
    {
        data_file_min_max << "," << min_vec[j];
    }
    data_file_min_max << "\n";
    data_file_min_max << max_vec[0];
    for(int j = 1; j < data.size(); ++j)
    {
        data_file_min_max << "," << max_vec[j];
    }
    data_file_min_max.close();
}
