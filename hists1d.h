#ifndef HISTS1D_H
#define HISTS1D_H

#include <vector>
#include <iostream>
#include <string>
#include <fstream>
#include <stdbool.h>
#include <unistd.h>
#include <algorithm>
#include <numeric>
#include <cmath>
#include <limits>
#include </home/dominykas/Software/root_install/include/TH1.h>
#include </home/dominykas/Software/root_install/include/TH2.h>

class hists1d 
{
public:
    bool created = true;
    std::vector<std::string> MC_data_names;
    std::string common_name;
    std::vector<std::vector<double>> bins = {};
    std::vector<double> low_lim;
    std::vector<double> high_lim;
    std::vector<int> bincount;
    std::vector<std::string> sub_names;
    std::vector<std::vector<TH1*>> MC;
    std::vector<TH1*> Re;
    std::string param_name;
    std::string param_txt;
    hists1d();
    hists1d(int init_count, std::vector<std::string> MC_data_names_in, std::string common_name_in, 
            std::vector<double> bins_in, std::string param_name_in = "", std::string param_txt_in = "");
    hists1d(std::vector<std::string> MC_data_names_in, std::string common_name_in, std::vector<std::vector<double>> bins_in, std::string param_name_in = "", std::string param_txt_in = "");
    hists1d(int init_count, std::vector<std::string> MC_data_names_in, std::string common_name_in, int bincount_in, 
            double low_lim_in, double high_lim_in, std::string param_name_in = "", std::string param_txt_in = "");
    hists1d(std::vector<std::string> MC_data_names_in, std::string common_name_in, std::vector<int> bincount_in,
            std::vector<double> low_lim_in, std::vector<double> high_lim_in, std::string param_name_in = "", std::string param_txt_in = "");
    hists1d(std::vector<std::string> MC_data_names_in, std::string common_name_in, std::vector<std::string> sub_names_in,
            std::vector<double> bins_in, std::string param_name_in = "", std::string param_txt_in = "");
    hists1d(std::vector<std::string> MC_data_names_in, std::string common_name_in, std::vector<std::string> sub_names_in,
            std::vector<std::vector<double>> bins_in, std::string param_name_in = "", std::string param_txt_in = "");
    hists1d(std::vector<std::string> MC_data_names_in, std::string common_name_in, std::vector<std::string> sub_names_in, 
            int bincount_in, double low_lim_in, double high_lim_in, std::string param_name_in = "", std::string param_txt_in = "");
    hists1d(std::vector<std::string> MC_data_names_in, std::string common_name_in, std::vector<std::string> sub_names_in,
            std::vector<int> bincount_in, std::vector<double> low_lim_in, std::vector<double> high_lim_in, std::string param_name_in = "", std::string param_txt_in = "");
    void expand(int expand_count);
    void expand(std::vector<std::vector<double>> expand_bins);
    void expand(std::vector<int> expand_bincount, std::vector<double> expand_low_lim, std::vector<double> expand_high_lim);
    void expand(std::vector<std::string> expand_names);
    void expand(std::vector<std::string> expand_names, std::vector<std::vector<double>> expand_bins);
    void expand(std::vector<std::string> expand_names, std::vector<int> expand_bincount, 
                std::vector<double> expand_low_lim, std::vector<double> expand_high_lim);
    void plot(std::string data_save_core, bool logy, bool logx, int nDivY = 510, int nDivY_rat = 505, int nDivX = 510, double min_val = 1);
    void plot(std::string data_save_core, bool logy, std::vector<bool> logx, int nDivY = 510, int nDivY_rat = 505, int nDivX = 510, double min_val = 1);
    void plot(std::string data_save_core, std::vector<bool> logy, bool logx, int nDivY = 510, int nDivY_rat = 505, int nDivX = 510, double min_val = 1);
    void plot(std::string data_save_core, std::vector<bool> logy, std::vector<bool> logx, int nDivY = 510, int nDivY_rat = 505, int nDivX = 510, double min_val = 1);
    void save_txt(std::string data_save, std::string delimit = " ");
    void load_txt(std::string data_save, std::string delimit = " ");
    void save(std::string data_save);
    void load(std::string file_dir);    
};

#endif
