#include <vector>
#include <string>
#include <stdbool.h>
#include <iostream>
//#include </home/dominykas/Software/root_install/include/TFile.h>
#include </home/dominykas/dy_mas_do/OpenNN/opennn/opennn.h>
//#include </home/dominykas/dy_mas_do/OpenNN/eigen/eigen.h>

using namespace OpenNN;
using namespace Eigen;
using namespace std;

template <typename T>
void print2d(vector<vector<T>> data);


int main(int argc, char* argv[])
{
    bool electron = false;
    string data_loc;
    if(electron)
        data_loc = "/home/dominykas/dy_mas_do/EE/";
    else
        data_loc = "/home/dominykas/dy_mas_do/MuMu/";
    /*
    TFile MC_data_file((data_loc + "MC_data_vec.root").c_str(), "READ");
    
    vector<string> * legend_names_temp;
    MC_data_file.GetObject("legend_names", legend_names_temp);
    vector<string> legend_names = * legend_names_temp;
    
    vector<string> * Collumn_names_temp;
    MC_data_file.GetObject("Collumn_names", Collumn_names_temp);
    vector<string> Collumn_names = *Collumn_names_temp;
    
    string * cut_count_temp;
    MC_data_file.GetObject("cut_count", cut_count_temp);
    int cut_count = stoi(*cut_count_temp);
    cout << cut_count << "\n";
    
    vector<vector<double>> data_vec(Collumn_names.size());
    for(int i = 0; i < Collumn_names.size(); ++i)
    {
        vector<double> * data_vec_temp;
        MC_data_file.GetObject((Collumn_names[i] + "_shuffled_0").c_str(), data_vec_temp);
        data_vec[i] = *data_vec_temp;
    }
    
    cout << "Total data points: " << data_vec[0].size() << "\n";
    
    MC_data_file.Close();
    */
    DataSet data_set((data_loc + "MC_data_0.csv"),',',true);
    
}

template <typename T>
void print2d(vector<vector<T>> data)
{
    for(int i = 0; i < data.size(); ++i)
    {
        for(int j = 0; j < data.at(i).size(); ++j)
            cout << "[" << data.at(i).at(j) << "] ";
        cout << "\n";
    }
    cout << "---\n";
}
