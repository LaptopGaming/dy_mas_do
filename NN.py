#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  7 14:29:45 2022

@author: dominykas
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import OneHotEncoder
from sklearn.model_selection import train_test_split
import tensorflow as tf
#tf.config.set_visible_devices([], 'GPU')
print(tf.__version__)
gpu_devices = tf.config.experimental.list_physical_devices('GPU')
print("Num GPUs Available: ", len(gpu_devices))
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation, LeakyReLU, ELU, ReLU, GaussianNoise, Softmax
from tensorflow.keras.optimizers import SGD, Adam
from tensorflow.keras import regularizers
#from keras.layers.normalization import BatchNormalization
from tensorflow.keras.callbacks import EarlyStopping, TensorBoard
from tensorflow.keras.losses import MeanSquaredError, CategoricalCrossentropy
from tensorflow.keras.utils import plot_model
from tensorflow.keras.metrics import Precision
from tensorflow.keras.models import load_model
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay, plot_confusion_matrix
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score
from sklearn.metrics import accuracy_score, precision_score, recall_score
legend_names = ["$QCD$", "$W + Jets$", "$WW$", "$WZ$", "$ZZ$", "$\\bar{t}W$", "$tW$", "$t\\bar{t}$", 
                "$Drell Yan \\rightarrow \\tau\\tau$", "$Drell Yan \\rightarrow \\mu\\mu$"]
legend_names = legend_names[1:]
Process_limits = [None, 0, 0, 0, 0, 0, 0, 0, 0, 0]
Min_Max = pd.read_csv("/home/dominykas/dy_mas_do/MuMu/MC_data_min_max.csv")
def data_read_rescale(data_loc_name, scale=None, limit=None):
    #print("Reading " + data_loc_name)
    DF_init = pd.read_csv(data_loc_name, dtype=np.float32)
    DF = pd.DataFrame(columns=DF_init.columns)
    if limit is None:
        DF = DF_init
        #print(DF.shape, DF_init.shape)
    else:
        new_process_num = 0
        for i in range(len(limit)):
            if limit[i] is None:
                continue
            if limit[i] == 0:
                DF = pd.concat([DF, DF_init.loc[DF_init['Process'] == i]], ignore_index=True)
            else:
                DF = pd.concat([DF, DF_init.loc[DF_init['Process'] == i].reset_index(drop=True).iloc[:limit[i]]], ignore_index=True)
            DF['Process'].loc[DF['Process'] == i] = new_process_num
            #print(DF['Process'].unique(), i, new_process_num)
            new_process_num += 1
    if scale is None:
        return DF
    else:
        DF[DF.columns[3:]] = ((DF - scale[0]) / (scale[1] - scale[0]))[DF.columns[3:]]
        return DF

def Y_rebuild(Y, classes):
    Y_mod = pd.DataFrame()
    for i in range(len(classes)):
        Y_mod[classes[i]] = (Y == i)*1
    return Y_mod

DF = data_read_rescale("/home/dominykas/dy_mas_do/MuMu/MC_data_0.csv", scale=Min_Max.T, limit=Process_limits)
X = DF[DF.columns[2:]]
Y_s = DF['Process']
Y = Y_rebuild(Y_s, legend_names)

data_loc = "/home/dominykas/dy_mas_do/MuMu/"
data_name = "MC_data_"
bins = np.linspace(-0.5, len(legend_names) - 0.5, len(legend_names)+1)
w_counts_full = np.zeros(bins.shape[0] - 1)
counts_full = np.zeros(bins.shape[0] - 1)
for j in range(17):
    print("Reading " + data_name + str(j))
    DF_whist = data_read_rescale(data_loc + data_name + str(j) + ".csv", limit=Process_limits)
    w_counts, edges = np.histogram(DF_whist['Process'], weights=DF_whist['Weight'], bins=bins)
    counts, edges = np.histogram(DF_whist['Process'], bins=bins)
    w_counts_full = w_counts_full + w_counts
    counts_full = counts_full + counts
    #print(w_counts_full)
width = 0.35
fig_w_sum, ax_w_sum = plt.subplots(1, 1, figsize=[10, 6])
f_bars_1 = ax_w_sum.bar(bins[:-1] - width/2 + 0.5, counts_full, width
                   #, log=True
                  )
f_bars_2 = ax_w_sum.bar(bins[:-1] + width/2 + 0.5, w_counts_full, width
                   #, log=True
                  )
ax_w_sum.set_xticks(range(len(legend_names)), legend_names)
ax_w_sum.bar_label(f_bars_1
              , padding=10
             )
ax_w_sum.bar_label(f_bars_2)
ax_w_sum.legend(["Data count", "Weight sum"])
#plt.grid(which = 'both')
fig_w_sum.savefig(data_loc + "data_dist.pdf")


def model_fit(model_in, classes, data_loc, data_name, file_num, model_loc, model_name, min_max_scale, 
              class_weights_func, train_valid_split=0.7, epochs = 5): 
    model_checkpoint_callback_loss = tf.keras.callbacks.ModelCheckpoint(filepath=model_loc + model_name + "_loss",
                                                                       save_weights_only=True,
                                                                       monitor='val_loss',
                                                                       mode='min',
                                                                       save_best_only=True)
    model_checkpoint_callback_acc = tf.keras.callbacks.ModelCheckpoint(filepath=model_loc + model_name + "_acc",
                                                                       save_weights_only=True,
                                                                       monitor='val_accuracy',
                                                                       mode='max',
                                                                       save_best_only=True)
    model_checkpoint_callback_prec = tf.keras.callbacks.ModelCheckpoint(filepath=model_loc + model_name + "_prec",
                                                                       save_weights_only=True,
                                                                       monitor='val_precision',
                                                                       mode='max',
                                                                       save_best_only=True)
    train_hist_acc = np.array([])
    train_hist_loss = np.array([])
    valid_hist_acc = np.array([])
    valid_hist_loss = np.array([])
    Process_limits = [None, 0, 0, 0, 0, 0, 0, 0, 0, 75000]
    for j in range(file_num):
        print("Reading " + data_name + str(j))
        DF = data_read_rescale(data_loc + data_name + str(j) + ".csv", limit=Process_limits, scale=min_max_scale.T)
        Y = Y_rebuild(DF['Process'], classes)
        X = DF[DF.columns[2:]]
        split_num = int(X.shape[0]*train_valid_split)
        x_train = X.loc[:split_num-1]
        y_train = Y.loc[:split_num-1]
        x_valid = X.loc[split_num:]
        y_valid = Y.loc[split_num:]
        class_weights = class_weights_func(w_counts_full)
        weights = DF['Weight'].copy()
        for i in range(len(classes)):
            weights[DF['Process'] == i] *= class_weights[i]
        log = model_in.fit(x_train, y_train,
                        callbacks = [model_checkpoint_callback_loss, 
                                     model_checkpoint_callback_acc, 
                                     model_checkpoint_callback_prec],
                        batch_size = 150, 
                        epochs = epochs, 
                        verbose = 2,
                        sample_weight = weights[:split_num],
                        validation_data = (x_valid, y_valid, weights[split_num:]))
        train_hist_acc = np.append(train_hist_acc, log.history['accuracy'], axis=-1)
        train_hist_loss = np.append(train_hist_loss, log.history['loss'], axis=-1)
        valid_hist_acc = np.append(valid_hist_acc, log.history['val_accuracy'], axis=-1)
        valid_hist_loss = np.append(valid_hist_loss, log.history['val_loss'], axis=-1)
    model_in.save(model_loc + model_name + "_last")
    return model_in, train_hist_acc, train_hist_loss, valid_hist_acc, valid_hist_loss
data_loc = "/home/dominykas/dy_mas_do/MuMu/"
data_name = "MC_data_"

model = Sequential(name="Classifier1")
model.add(Dense(100, input_dim = X.shape[1], activation = ELU(1), name = 'ELU1'))
#model.add(ELU(1))
model.add(Dense(100, activation = ELU(1), name = 'ELU2'))
#model.add(ELU(1))
model.add(Dense(100, activation = ELU(1), name = 'ELU3'))
#model.add(ELU(1))
model.add(Dense(100, activation = ELU(1), name = 'ELU4'))
#model.add(ELU(1))
#model.add(BatchNormalization())
model.add(Dense(len(legend_names), name = 'output'))
model.add(Softmax())
model.summary()

model.compile(loss = CategoricalCrossentropy(), optimizer = Adam(), weighted_metrics = ['accuracy', Precision(name='precision')])

def cwf(counts):
    counts[counts == 0] = min(counts[counts != 0])
    return np.sqrt(max(counts)/counts)

model, train_hist_acc, train_hist_loss, valid_hist_acc, valid_hist_loss = model_fit(model, legend_names, 
                                                                                    data_loc, data_name, 16,
                                                                                    data_loc, "model", Min_Max, cwf)

def print_conf_matrix(y_true, y_pred, labels, ax, title, weights=np.array([])):
    if(weights.size != len(y_true)):
        weights = np.full(len(y_true), 1.)
    disp = ConfusionMatrixDisplay(
        confusion_matrix = confusion_matrix(y_true, y_pred, sample_weight=weights, normalize='true', labels=range(len(labels))), display_labels = labels)
    disp.plot(include_values = True, cmap = plt.cm.Blues, ax = ax)
    ax.set_title(f'{title} (acc = {accuracy_score(y_true, y_pred):.3f})')
    ax.set_xlabel("Modelio spėjimas")
    ax.set_ylabel("Tikroji klasė", rotation='vertical')
    

DF = data_read_rescale(data_loc + data_name + str(16) + ".csv", limit=Process_limits, scale=Min_Max.T)
X = DF[DF.columns[2:]]
model = load_model(data_loc + "model_last")
figm, axsm = plt.subplots(1, 1, figsize=(7, 7))
print_conf_matrix(DF['Process'], np.argmax(model.predict(X), axis=-1), legend_names, axsm, 'Testavimo aibė')

figm_loss, axsm_loss = plt.subplots(1, 1, figsize=(7, 7))
model.load_weights(data_loc + "model_loss")
print_conf_matrix(DF['Process'], np.argmax(model.predict(X), axis=-1), 
                  legend_names, axsm_loss, 'Testavimo aibė')
figm_loss.savefig(data_loc + "model_loss.pdf", bbox_inches="tight", pad_inches=0.15)

figm_acc, axsm_acc = plt.subplots(1, 1, figsize=(7, 7))
model.load_weights(data_loc + "model_acc")
print_conf_matrix(DF['Process'], np.argmax(model.predict(X), axis=-1), 
                  legend_names, axsm_acc, 'Testavimo aibė')
figm_acc.savefig(data_loc + "model_acc.pdf", bbox_inches="tight", pad_inches=0.15)

figm_prec, axsm_prec = plt.subplots(1, 1, figsize=(7, 7))
model.load_weights(data_loc + "model_prec")
print_conf_matrix(DF['Process'], np.argmax(model.predict(X), axis=-1), 
                  legend_names, axsm_prec, 'Testavimo aibė')
figm_prec.savefig(data_loc + "model_prec.pdf", bbox_inches="tight", pad_inches=0.15)

fig, axs = plt.subplots(1, 2, dpi=200, figsize=(27/2.54, 10/2.54))
axs[0].set_xlabel("Mokymosi epochos")
axs[0].set_ylabel("Prarastis")
axs[1].set_xlabel("Mokymosi epochos")
axs[1].set_ylabel("Tisklumas")
xepochs = range(1, len(train_hist_loss) + 1)

axs[0].plot(xepochs, train_hist_loss, label = 'Mokymosi aibė')
axs[0].plot(xepochs, valid_hist_loss, label = 'Validacijos aibė')
#axs[0].title.set_text('Loss (CE)')
axs[0].legend()
#axs[0].show()

axs[1].plot(xepochs, train_hist_acc, label = 'Mokymosi aibė')
axs[1].plot(xepochs, valid_hist_acc, label = 'Validacijos aibė')
#axs[1].title.set_text('Metrics (Accuracy)')
axs[1].legend()
#axs[1].show()

#fig.savefig(r"/home/dominykas/Software/plot/model_epoch")